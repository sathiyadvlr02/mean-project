const mongoose = require('mongoose');

const upload = mongoose.Schema({
    filetype:{
        type:String
    },
    description:{
        type:String
    },
    fileupload:{
        type:String
    },
    uploaded:{
        type:Date,
        default:Date.now()
    }
})

module.exports = mongoose.model('FileUploads',upload)