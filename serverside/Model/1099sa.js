const mongoose = require('mongoose');


const sa1099 = mongoose.Schema({
    corrected: {
        type: String
    },
    payerName: {
        type: String
    },
    payeraddress1: {
        type: String
    },
    payeraddress2: {
        type: String
    },
    payercity: {
        type: String
    },
    payerState: {
        type: String
    },
    payerzip: {
        type: String
    },
    payerphone: {
        type: String
    },
    payerFederalIdentificationNumber: {
        type: String
    },
    recipientno: {
        type: String
    },
    recipientname: {
        type: String
    },
    recipientaddress1: {
        type: String
    },
    recipientState: {
        type: String
    },
    recipientzip: {
        type: String
    },
    recipientcity: {
        type: String
    },
    acctno: {
        type: String
    },
    distributioncode: {
        type: String
    },
    earningsonexcess: {
        type: String
    },
    grossdistribution: {
        type: String
    },
    FMV: {
        type: String
    },
    archerMSA: {
        type: String
    },
    memo:{
        type:String
    },
    releaseDate:{
        type:Date
    },
    consentDate:{
        type:Date
    }
});

module.exports = mongoose.model('formsa', sa1099);