const mongoose = require('mongoose');

const sitedetails = mongoose.Schema({
    siteid:{
        type:String
    },
    description:{
        type:String
    }
})

module.exports = mongoose.model('sites',sitedetails)