const express = require('express');
const route = express.Router();
const userauth = require('./../Model/userauth');
const Bcrypt = require('bcryptjs');


route.get('/check',(req,res)=>{
res.json('UserAuth Created');
});


route.post('/login',(req,res)=>{
       userauth.find({useremail:req.body.useremail}).then(e=>{
          if(e){
              let encryptpassw = e[0].password;
              let pass = req.body.password;
              Bcrypt.compare(pass, encryptpassw, function(err, isMatch) {
                if (!isMatch) {
                    res.json({message:"Password doesn't match"});
                  } else {
                    res.json({message:"Password matches!"});
                  }
              });
          }
          
      });
  });


route.post('/register',(req,res)=>{
    let pass = Bcrypt.hashSync(req.body.password,10)
    var data = userauth({
        useremail :req.body.useremail,
        username : req.body.username,
        password: pass
    });
      data.save().then(e=>{
          if(e){     
       res.json({message:"Register Successfully"})
          }
      });
})


module.exports = route
