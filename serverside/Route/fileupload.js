const express = require('express');
const path = require('path');
const fs = require('fs');
const route = express.Router();
const multer = require('multer');
var mime = require('mime');
const filepath = require('./../Model/fileupload');

route.get('/',(req,res)=>{
res.json("FileUpload");
});


var DIR = './files';

var storage1 = multer.diskStorage({
  destination: (req,file,cb)=>{
    cb(null,DIR)
  },
  filename:(req,file,cb)=>{
    var filetype='';
    console.log(file.mimetype);
    if(file.mimetype === 'text/plain'){
      filetype = 'txt'
    }
    else if(file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
      filetype ='xlsx'
    }
    const number = Math.floor(
      Math.random() * (10)
    )
    cb(null,'file'+ number +'.'+filetype)
  }
});

var upload = multer({
storage : storage1
});

route.post('/upload_file',upload.single('fileupload'),function(req,res,next){
    req.body.fileupload = req.file.filename;
    filepath.create(req.body,function(err,data){
  if(err){
    return next(err)
  }
  res.json({message:'Upload Successfully'});
    });
});

route.get('/getfiles/:file', (req, res) => {
  const filepath = req.params.file
  res.download('./files/'+filepath);
})


route.get('/getallfiles',(req,res)=>{
  filepath.find().then(e=>{
    res.json(e);
  })
});

module.exports = route;
