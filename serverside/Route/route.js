const express = require('express');
const { findOneAndUpdate } = require('./../Model/1099sa');
const route = express.Router();
const formschema = require('./../Model/1099sa');


route.get('/',(req,res)=>{
  res.json("Route working");
});


//Create
route.post('/create',(req,res)=>{
 var data = formschema({
   corrected : req.body.corrected,
   payerName : req.body.payerName,
   payeraddress1  : req.body.payeraddress1,
   payeraddress2  : req.body.payeraddress2,
   payercity     : req.body.payercity,
   payerzip       : req.body.payerzip,
   payerState:req.body.payerState,
   payerphone: req.body.payerphone,
   payerFederalIdentificationNumber: req.body.payerFederalIdentificationNumber,
   recipientno : req.body.recipientno,
   recipientname : req.body.recipientname,
   recipientaddress1: req.body.recipientaddress1,
   recipientState : req.body.recipientState,
   recipientzip : req.body.recipientzip,
   recipientcity : req.body.recipientcity,
   acctno : req.body.acctno,
   distributioncode: req.body.distributioncode,
   earningsonexcess : req.body.earningsonexcess,
   grossdistribution   : req.body.grossdistribution,
   FMV  : req.body.FMV,
   archerMSA: req.body.archerMSA
 });
     data.save().
     then(post=>{
       if(post){
         res.status(201).json('Added Successfully')
       }
     }).catch(e=>{
    console.log(e);
     });   
});


//GetAll
route.get('/getdata',(req,res)=>{
  formschema.find((err,data)=>{
   if(err){
     return err
   }
   else{
    res.json(data);
    // for(i=0;i<data.length;i++)
    // {
    //   var obj ={
    //     CityName : JSON.stringify(data[i].payercity),
    //     corrected : JSON.stringify(data[i].corrected)
    //   };
    //   console.log(obj);
    // } 
   }
  });
})

//Update
route.put('/updateform',async(req,res)=>{
    var update = await formschema.findByIdAndUpdate({_id:req.body._id},{$set:{
      corrected : req.body.corrected,
      payerName : req.body.payerName,
      payeraddress1  : req.body.payeraddress1,
      payeraddress2  : req.body.payeraddress2,
      payercity     : req.body.payercity,
      payerzip       : req.body.payerzip,
      payerState:req.body.payerState,
      payerphone: req.body.payerphone,
      payerFederalIdentificationNumber: req.body.payerFederalIdentificationNumber,
      recipientno : req.body.recipientno,
      recipientname : req.body.recipientname,
      recipientaddress1: req.body.recipientaddress1,
      recipientState : req.body.recipientState,
      recipientzip : req.body.recipientzip,
      recipientcity : req.body.recipientcity,
      acctno : req.body.acctno,
      distributioncode: req.body.distributioncode,
      earningsonexcess : req.body.earningsonexcess,
      grossdistribution   : req.body.grossdistribution,
      FMV  : req.body.FMV,
      archerMSA: req.body.archerMSA,
      memo: req.body.memo,
      consentDate:req.body.consentDate,
      releaseDate:req.body.releaseDate

    }
        }).then(e=>{
          res.json("Updated Successfully");
        }); 
      
});

//delete
route.delete('/deleteformdata/:id',(req,res)=>{
 formschema.findByIdAndDelete(req.params.id).then(e=>{
    res.json("Deleted Successfully");
  });

})

//getdatabyid
route.get('/getbyid/:id',(req,res)=>{
  formschema.findById(req.params.id).then(e=>{
    res.json(e);
  });
});

module.exports = route