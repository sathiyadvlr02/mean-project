const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());

app.listen(3000,()=>{
    console.log("LocalHost:3000");
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
  });

mongoose.connect('mongodb://localhost:27017/taxform',(err)=>{
   if(!err){
  console.log("ConnectedSuccessfully");
   } 
});

// app.use('/',(req,res)=>{
//    res.json("Connect and working")
// });

const Router = require('./Route/route');
app.use('/Home',Router);


const Siteroute = require('./Route/siteroute');
app.use('/Site',Siteroute)

const fileroute = require('./Route/fileupload');
app.use('/file',fileroute);

const userroute = require('./Route/userauth');
app.use('/user',userroute);