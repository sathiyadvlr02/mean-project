import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1099INTRoutes } from './form109int.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099INTComponent } from './create1099intform/create1099int.component';
import { Update1099INTComponent } from './update1099intform/update1099int.component';
import { History1099INTComponent } from './history1099intform/history1099int.component';
import { FormEmail1099INT } from './email1099intform/email1099int.component';
import { SetPassword} from './email1099intform/setpassword.component';
 

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(Form1099INTRoutes)
  ],
  declarations: [Create1099INTComponent,Update1099INTComponent,History1099INTComponent,FormEmail1099INT,SetPassword]
})
export class Form1099INTModule {}
