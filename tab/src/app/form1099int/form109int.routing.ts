import { Routes } from '@angular/router';

import { Create1099INTComponent } from './create1099intform/create1099int.component'; 
import { Update1099INTComponent } from './update1099intform/update1099int.component';
import { History1099INTComponent } from './history1099intform/history1099int.component';
import { FormEmail1099INT } from './email1099intform/email1099int.component';

export const Form1099INTRoutes: Routes = [   
  {
    path: 'create1099int',
    component: Create1099INTComponent  
  },
  {
    path: 'form1099intcreate',
    component: Create1099INTComponent  
  }, 
  {
    path:'form1099intupdate',
    component:Update1099INTComponent
  },
  {
    path:'form1099inthistory',
    component:History1099INTComponent
  },
  {
    path:'form1099intemail',
    component:FormEmail1099INT
  }
];
