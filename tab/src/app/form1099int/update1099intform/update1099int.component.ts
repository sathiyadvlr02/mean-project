import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';

import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-update1099int',
    templateUrl: './update1099int.component.html',
    styleUrls: ['./update1099int.component.scss'],

})
export class Update1099INTComponent implements OnInit {
    createIntForm: FormGroup;
    stateList;
    country;
    taxyear;


    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public stateservice: StateService) {
    }

    ngOnInit(): void {
        this.createIntForm = this.fb.group({
            corrected: new FormControl(''),
            payersname: new FormControl('', [Validators.required]),
            payersaddress: new FormControl('', [Validators.required]),
            payersaddress2: new FormControl('', [Validators.required]),
            payersaddress3: new FormControl(''),
            payetscity: new FormControl('', [Validators.required]),
            payersstate: new FormControl('', [Validators.required]),
            payerszip: new FormControl('', [Validators.required]),
            payersphone: new FormControl('', [Validators.required]),
            payersphone2: new FormControl(''),
            payersFederalNo: new FormControl('', [Validators.required]),
            Recipientidentificationno: new FormControl('', [Validators.required]),
            Recipientiname:new FormControl('', [Validators.required]),
            Recipientiname2:new FormControl(''),
            Recipientiaddress1:new FormControl('', [Validators.required]),
            Recipientiaddress2:new FormControl('', [Validators.required]),
            Recipientiaddress3:new FormControl(''),
            Recipienticity:new FormControl('', [Validators.required]),
            Recipientistate:new FormControl(''),
            Recipientizip:new FormControl('', [Validators.required]),
            ForeignCountry:new FormControl(false),
            Country:new FormControl(''),
            TINnot:new FormControl(''),
            FATCAfilling:new FormControl(''),
            acctno:new FormControl(''),
            interestincone:new FormControl(''),
            withdrawlpenalty:new FormControl(''),
            interestonUS:new FormControl(''),
            federalincome:new FormControl(''),
            foreigntax:new FormControl(''),
            taxexempt:new FormControl(''),
            marketdiscount:new FormControl(''),
            investmentexpense:new FormControl(''),
            possession:new FormControl(''),
            privateactivity:new FormControl(''),
            bondpremium:new FormControl(''),
            treasury:new FormControl(''),
            taxexemptbond:new FormControl(''),
            CUSIPno:new FormControl(''),
            state:new FormControl(''),
            Stateidentificationno:new FormControl(''),
            Statetaxwithheld:new FormControl(''),
            releasedate:new FormControl(''),
            consentdate:new FormControl(''),
            memo:new FormControl(''),
        });
        this.taxyear =2020;
        this.getstatelist();
    }


    get trrowErrors() { return this.createIntForm.controls; }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    getstatelist() {
        this.stateservice.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
            this.getCountry();
    }

    getCountry() {
        this.stateservice.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
        }

    update() { }
}

