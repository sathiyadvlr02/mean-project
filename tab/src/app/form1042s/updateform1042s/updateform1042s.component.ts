import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EntityService } from './../../services/ein.service';
import { StateService } from './../../services/state.service';
import { Form1098tService } from './../../services/form1098t.service';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-updateform1042s',
  templateUrl: './updateform1042s.component.html'
})
export class Updateform1042sComponent implements OnInit {

  createForm: FormGroup;
  country;
  stateList;
  constructor(
      public navService: NavService,
      public router: Router,
      private fb: FormBuilder,
      public toasterService: ToastrService,
      private el: ElementRef,
      public dialog: MatDialog,
      private route: ActivatedRoute,
      public StateServices: StateService,
      public EntityService: EntityService,
      public Form1098tService: Form1098tService,
      private cookieService: CookieService,
  ) { }

  private CookieUserName = this.cookieService.get('username');
  private CookieSiteId = this.CookieUserName.split('_')[0];

  ngOnInit(): void {
    this.createForm = this.fb.group({
      formidentifier: new FormControl(''),
      amended: new FormControl(''),
      amendmentno: new FormControl(''),
      incomecode : new FormControl(''),
      exemptioncode: new FormControl(''),
      grossIncome: new FormControl(''),
      Exemptioncode: new FormControl(''),
      indicator: new FormControl('',[Validators.required]),
      taxRate3B: new FormControl(''),
      taxRate4B: new FormControl(''),
      withholdallowance: new FormControl(''),
      NetIncome: new FormControl(''),
      federaltax: new FormControl(''),
      federaltaxwithheld: new FormControl(''),
      withholding: new FormControl(''),
      otheragents: new FormControl(''),
      taxpaid: new FormControl(''),
      totalcredit: new FormControl('',[Validators.required]),
      amountrepaid: new FormControl(''),
      EIN: new FormControl('',[Validators.required]),
      statuscode12B: new FormControl(''),
      statuscode12C: new FormControl(''),
      agentName: new FormControl('',[Validators.required]),
      GIIN: new FormControl(''),
      countrycode: new FormControl(''),
      identificationNumber: new FormControl(''),
      Address12H: new FormControl('',[Validators.required]),
      City: new FormControl('',[Validators.required]),
      state: new FormControl(''),
      Zip: new FormControl('',[Validators.required]),
      foreigncontry: new FormControl(false),
      country: new FormControl(''),
      provincecode: new FormControl(''),
      Recipientname: new FormControl('',[Validators.required]),
      RecipientCountrycode: new FormControl(''),
      RecipientAddress: new FormControl('',[Validators.required]),
      RecipientCity: new FormControl('',[Validators.required]),
      RecipientState: new FormControl(''),
      RecipientZip: new FormControl('',[Validators.required]),
      RecipientForeignCountry: new FormControl(false),
      RecipientCountry: new FormControl(''),
      RecipientprovinceCode: new FormControl(''),
      RecipientTIN: new FormControl(''),
      statusCode13F: new FormControl(''),
      statuscode13G: new FormControl(''),
      RecipentGIIN: new FormControl(''),
      RecipientforeignTax: new FormControl(''),
      LOBcode: new FormControl(''),
      RecipientAccountNumber: new FormControl('',Validators.required),
      RecipientDOB: new FormControl(''),
      primarywithholding: new FormControl(''),
      primarywithholdingEIN: new FormControl(''),
      proratebasis: new FormControl(''),
      Intermediary: new FormControl(''),
      statuscode15B: new FormControl(''),
      statuscode15C: new FormControl(''),
      intermediary15D: new FormControl(''),
      Intermediary15E: new FormControl(''),
      CountryCode15F: new FormControl(''),
      ForeignidentificationNo15G: new FormControl(''),
      Address15H: new FormControl(''),
      City15I: new FormControl(''),
      state15I: new FormControl(''),
      Zip15I: new FormControl(''),
      ForeignCountry15I: new FormControl(false),
      Country15I: new FormControl(''),
      ProvinceCode15I: new FormControl(''),
      payersName: new FormControl(''),
      payersTIN: new FormControl(''),
      PayersGIIN: new FormControl(''),
      statuscode16D: new FormControl(''),
      Statuscode16E: new FormControl(''),
      Taxwithheld17A: new FormControl(''),
      payersTaxno: new FormControl(''),
      State17C: new FormControl(''),
      releasedate: new FormControl(''),
      consentdate: new FormControl(''),
      memo: new FormControl('')
  });
  this.getCountry();
}
get trrowErrors() { return this.createForm.controls; }

onBlurFloat(event) {
  if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2)
  }
}
getCountry() {
  this.StateServices.getCountry().subscribe(
      result => {
          this.country = JSON.parse(result.data);
      });
  this.getstatelist();
}


getstatelist() {
  this.StateServices.getStatelist().subscribe(
      result => {
          this.stateList = JSON.parse(result.data);
      });
}

  btn_history(){
    this.router.navigate(["/historyform1042s"]);
  }
  
}
