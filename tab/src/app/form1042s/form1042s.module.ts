import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';

import { Form1042sRoutes } from './form1042s-routing.module';
import { Updateform1042sComponent } from './updateform1042s/updateform1042s.component';
import { Createform1042sComponent } from './createform1042s/createform1042s.component';
import { Historyform1042sComponent } from './historyform1042s/historyform1042s.component';
import { FormEmail1042S } from './email1042sform/email1042s.component';
import { SetPassword } from './email1042sform/setpassword.component';
 
@NgModule({
  declarations: [Updateform1042sComponent,Createform1042sComponent,
    Historyform1042sComponent,FormEmail1042S,SetPassword],
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(Form1042sRoutes)
  ]
})
export class Form1042sModule { }
