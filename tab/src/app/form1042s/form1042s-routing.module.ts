import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Createform1042sComponent } from './createform1042s/createform1042s.component';
import { Updateform1042sComponent } from './updateform1042s/updateform1042s.component';
import { Historyform1042sComponent } from './historyform1042s/historyform1042s.component';
import { FormEmail1042S } from './email1042sform/email1042s.component';


export const Form1042sRoutes: Routes = [
  {
      path: 'createform1042s',
      component: Createform1042sComponent
  },
  {
    path: 'updateform1042s',
    component: Updateform1042sComponent
  },
  {
  path: 'historyform1042s',
  component: Historyform1042sComponent
  },
  {
    path: 'emailform1042s',
    component: FormEmail1042S
    }
]


