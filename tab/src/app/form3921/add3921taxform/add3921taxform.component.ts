import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { StateService } from './../../services/state.service';
@Component({
    selector: 'app-add3921taxform',
    templateUrl: './add3921taxform.component.html'
})
export class Add3921taxformComponent implements OnInit {
    Form3921add: FormGroup;
    stateList;

    constructor(
        public router: Router,
        private location: Location,
        public StateServices: StateService,
    ) { }

    ngOnInit(): void {
        this.Form3921add = new FormGroup({
            Transaction_Id: new FormControl('', Validators.required),
            box1datepicker: new FormControl('', Validators.required),
            box2datepicker: new FormControl('', Validators.required),
            Box3: new FormControl('', Validators.required),
            Box4: new FormControl('', Validators.required),
            Box5: new FormControl('', Validators.required),
            Box6: new FormControl('', Validators.required),
            Box7: new FormControl('', Validators.required),
            city: new FormControl('', Validators.required),
            state: new FormControl('', Validators.required),
            zip: new FormControl('', Validators.required),

        });
        this.getstatelist();

    }

    get trrowErrors() { return this.Form3921add.controls; }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }

    Backto3921(): void {
        this.location.back()
        //this.router.navigate(["create3921"]);
    }

    AddTransaction() {
        console.log(this.Form3921add.value);
    }


}
