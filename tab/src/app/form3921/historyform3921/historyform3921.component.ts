import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Add3921taxformComponent } from '../add3921taxform/add3921taxform.component';



@Component({
    selector: 'app-historyform3921',
    templateUrl: './historyform3921.component.html',
    styleUrls: ['./historyform3921.component.scss']
})
export class Historyform3921Component implements OnInit {
    selectedRow: any;
    taxyear;

    displayedColumns: string[] = ['seq', 'first_name', 'last_name', 'ssn', 'school_student_id', 'transferred_box5', 'EIN', 'Name', 'Address', 'citystatezip', 'action'];
    dataSource = ELEMENT_DATA;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(private router: Router, public dialog: MatDialog) { }

    ngOnInit(): void {
        this.taxyear=2020;
    }
   
    Add3921Record(): void {
        this.router.navigate(["add3921"]);
    }

}



export interface PeriodicElement {
    seq: string;
    first_name: string;
    last_name: string;
    ssn: string;
    school_student_id: string;
    transferred_box5: string;
    EIN: string;
    Name: string;
    Address: string;
    citystatezip: string;
}

const ELEMENT_DATA: PeriodicElement[] = [

    { seq: '0', first_name: 'Michela', last_name: 'BRUNELLI', ssn: '999-99-9999', school_student_id: '21691639', transferred_box5: 'Test', EIN: 'Test', Name: 'Test', Address: 'Test', citystatezip: 'Test' },
    { seq: '0', first_name: 'Michela', last_name: 'BRUNELLI', ssn: '999-99-9999', school_student_id: '21691639', transferred_box5: 'Test', EIN: 'Test', Name: 'Test', Address: 'Test', citystatezip: 'Test' },

];
