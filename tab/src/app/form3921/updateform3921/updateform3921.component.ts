import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, MatSort, MatTableDataSource, MatRadioChange } from '@angular/material';
import { Add3921taxformComponent } from '../add3921taxform/add3921taxform.component';
import { CookieService } from 'ngx-cookie-service';
import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-updateform3921',
    templateUrl: './updateform3921.component.html',
    styleUrls: ['./updateform3921.component.scss']
})
export class Updateform3921Component implements OnInit {
    createForm: FormGroup;
    stateList;
    selectedRow: any;
    taxyear;
    displayedColumns: string[] = ['seq', 'first_name', 'last_name', 'ssn', 'school_student_id', 'transferred_box5', 'EIN', 'Name', 'Address', 'citystatezip', 'action'];
    dataSource = ELEMENT_DATA;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices: StateService,
    ) { }

    ngOnInit(): void {
        this.createForm = this.fb.group({
            TransferName: new FormControl('', [Validators.required]),
            TransferName2: new FormControl(''),
            Transferaddress: new FormControl('', [Validators.required]),
            Transferaddress2: new FormControl('', [Validators.required]),
            Transferaddress3: new FormControl(''),
            Transfercity: new FormControl('', [Validators.required]),
            transferState: new FormControl('', [Validators.required]),
            Transferzip: new FormControl('', [Validators.required]),
            Transferphone: new FormControl('', [Validators.required]),
            EmployeeName: new FormControl('', [Validators.required]),
            Employeeaddress: new FormControl('', [Validators.required]),
            Employeeaddress2: new FormControl('', [Validators.required]),
            Employeeaddress3: new FormControl(''),
            Employeecity: new FormControl('', [Validators.required]),
            EmployeeState: new FormControl('', [Validators.required]),
            Employeezip: new FormControl('', [Validators.required]),
            transferFederalNo: new FormControl('', [Validators.required]),
            employee_identification_no: new FormControl('', [Validators.required]),
            formReleaseDate: new FormControl(''),
            consentDate: new FormControl(''),
            Memo: new FormControl(''),
        });
        this.taxyear = 2005;
        this.getstatelist();
    }
    Add3921Record(): void {
        const dialogRef = this.dialog.open(Add3921taxformComponent);
        // this.router.navigate(["add3921"]);
    }
    get trrowErrors() { return this.createForm.controls; }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }

    viewHistory() {
        this.router.navigate(["history3921"]);
    }
    public onSubmit() { }
}


export interface PeriodicElement {
    seq: string;
    first_name: string;
    last_name: string;
    ssn: string;
    school_student_id: string;
    transferred_box5: string;
    EIN: string;
    Name: string;
    Address: string;
    citystatezip: string;
}

const ELEMENT_DATA: PeriodicElement[] = [

    { seq: '0', first_name: 'Michela', last_name: 'BRUNELLI', ssn: '999-99-9999', school_student_id: '21691639', transferred_box5: 'Test', EIN: 'Test', Name: 'Test', Address: 'Test', citystatezip: 'Test' },
    { seq: '0', first_name: 'Michela', last_name: 'BRUNELLI', ssn: '999-99-9999', school_student_id: '21691639', transferred_box5: 'Test', EIN: 'Test', Name: 'Test', Address: 'Test', citystatezip: 'Test' },

];
