import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, MatSort, MatTableDataSource, MatRadioChange } from '@angular/material';
import { Add3921taxformComponent } from '../add3921taxform/add3921taxform.component';
import { CookieService } from 'ngx-cookie-service';
import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-createform3921',
    templateUrl: './createform3921.component.html',
    styleUrls: ['./create3921.component.scss']

})
export class Createform3921Component implements OnInit {
    createForm: FormGroup;
    stateList;
    selectedRow: any;
    taxyear;
    displayedColumns: string[] = ['seq', 'first_name', 'last_name', 'ssn', 'school_student_id', 'transferred_box5', 'EIN', 'Name', 'Address', 'citystatezip', 'action'];
    dataSource = ELEMENT_DATA;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices: StateService,
    ) { }

    ngOnInit(): void {
        this.createForm = this.fb.group({
            TransferName: new FormControl('', [Validators.required]),
            TransferName2: new FormControl(''),
            Transferaddress: new FormControl('', [Validators.required]),
            Transferaddress2: new FormControl('', [Validators.required]),
            Transferaddress3: new FormControl(''),
            Transfercity: new FormControl('', [Validators.required]),
            transferState: new FormControl('', [Validators.required]),
            Transferzip: new FormControl('', [Validators.required]),
            Transferphone: new FormControl('', [Validators.required]),
            employename: new FormControl('', [Validators.required]),
            employeaddress1: new FormControl('', [Validators.required]),
            employeaddress2: new FormControl('', [Validators.required]),
            employeaddress3: new FormControl(''),
            employecity: new FormControl('', [Validators.required]),
            employeeState: new FormControl('', [Validators.required]),
            employeezip: new FormControl('', [Validators.required]),
            federalidentification_no: new FormControl('', [Validators.required]),
            emp_identification_no: new FormControl('', [Validators.required]),
        });
        this.taxyear = 2005;
        this.getstatelist();
    }

    Add3921Record(): void {
        const dialogRef = this.dialog.open(Add3921taxformComponent);
        //this.router.navigate(["add3921"]);
        console.log("Add3921taxformComponent");
    }

    get trrowErrors() { return this.createForm.controls; }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }
    onSubmit() { }
}


export interface PeriodicElement {
    seq: string;
    first_name: string;
    last_name: string;
    ssn: string;
    school_student_id: string;
    transferred_box5: string;
    EIN: string;
    Name: string;
    Address: string;
    citystatezip: string;
}

const ELEMENT_DATA: PeriodicElement[] = [

    { seq: '123', first_name: '07/02/2020', last_name: '07/02/2020', ssn: '100.0000', school_student_id: '200.0000', transferred_box5: '2', EIN: '101010101', Name: 'Test Company', Address: 'P. O. Box 001', citystatezip: 'Chicago 60607' },
    { seq: '123', first_name: '07/02/2020', last_name: '07/02/2020', ssn: '100.0000', school_student_id: '200.0000', transferred_box5: '5', EIN: '101010101', Name: 'Test Company', Address: 'P. O. Box 001', citystatezip: 'Chicago 60607' },

];
