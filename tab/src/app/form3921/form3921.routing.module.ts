import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Createform3921Component } from './createform3921/createform3921.component';
import { Updateform3921Component } from './updateform3921/updateform3921.component';
import { Historyform3921Component } from './historyform3921/historyform3921.component';
import { Add3921taxformComponent } from './add3921taxform/add3921taxform.component';
import { FormEmail3921 } from './emailform3921/email3921.component';


export const Form3921Routes: Routes = [
    {
        path: 'add3921',
        component: Add3921taxformComponent
    },
    
    {
        path: 'create3921',
        component: Createform3921Component
    },
    {
        path: 'createform3921',
        component: Createform3921Component
    },
    {
        path: 'update3921',
        component: Updateform3921Component
    },
    {
        path: 'history3921',
        component: Historyform3921Component
    },
    {
        path: 'email3921',
        component: FormEmail3921
    },
]

