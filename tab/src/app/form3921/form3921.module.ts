import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { Form3921Routes } from './form3921.routing.module';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { Createform3921Component } from './createform3921/createform3921.component';
import { Updateform3921Component } from './updateform3921/updateform3921.component';
import { Historyform3921Component } from './historyform3921/historyform3921.component';
import { Add3921taxformComponent } from './add3921taxform/add3921taxform.component';
import { FormEmail3921 } from './emailform3921/email3921.component';
import { SetPassword } from './emailform3921/setpassword.component';



@NgModule({
  declarations: [Createform3921Component,Updateform3921Component,
    Historyform3921Component,Add3921taxformComponent,FormEmail3921,SetPassword],
  imports: [
    CommonModule,
    AppMaterialModule,
    FormsModule ,ReactiveFormsModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Form3921Routes)
  ]
})
export class Form3921Module { }
