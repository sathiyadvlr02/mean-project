import { Routes } from '@angular/router';

import { Create1094CComponent } from './create1094cform/create1094c.component'; 
import {Form1094CComponent } from './form1094cmanagement/form1094c.component';

export const Form1094CRoutes: Routes = [
    {
        path: 'form1094c',
        component: Form1094CComponent
    },
    {
        path: 'create1094c',
        component: Create1094CComponent
    }, 

];
