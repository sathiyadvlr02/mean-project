import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-create1094c',
  templateUrl: './create1094c.component.html',
  styleUrls:['./create1094c.component.scss']
  
})
export class Create1094CComponent implements OnInit {
  homeForm: FormGroup;
  
  displayedColumns: string[] = ['Month', 'MEC', 'Full_Time_Employee',  'Total_Employee_Count','Aggregated_Group_Indicator','Section_4980H'];
  dataSource = ELEMENT_DATA;

  
  AALEdisplayedColumns: string[] = ['Seq','EIN', 'NAME','Seq2', 'EIN2',  'NAME2'];
  AALEdataSource = AALE_ELEMENT_DATA;


  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }

}

/* Static data */

export interface PeriodicElement {
    Month: string;
    MEC: string;
    Full_Time_Employee:string ;    
    Total_Employee_Count:string ;
    Aggregated_Group_Indicator:string ;
    Section_4980H: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {  Month: '23 All 12 Months', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '24 Jan', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '25 Feb', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    

    {  Month: '26 Mar', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '27 Apr', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '28 May', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '29 Jun', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '30 Jul', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '31 Aug', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '32 Sep', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '33 Oct', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '34 Nov', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    
    {  Month: '35 Dec', MEC: '', Full_Time_Employee:'' , Total_Employee_Count:'',  Aggregated_Group_Indicator:'', Section_4980H: '' },    

];



export interface AALEElement {
    Seq:string ;
    EIN: string;
    NAME: string;
    Seq2:string ;
    EIN2:string ;    
    NAME2:string ; 
}

const AALE_ELEMENT_DATA: AALEElement[] = [
    { Seq:"36", EIN: '3610101', NAME: '',Seq2:"51", EIN2:'' , NAME2:''},    
    {  Seq:"37", EIN: '3610101', NAME: '',Seq2:"52", EIN2:'' , NAME2:''},    
    {  Seq:"38", EIN: '3610101', NAME: '', Seq2:"53",EIN2:'' , NAME2:''}, 
    {  Seq:"39", EIN: '3610101', NAME: '',Seq2:"54", EIN2:'' , NAME2:''}, 
    {  Seq:"40", EIN: '3610101', NAME: '', Seq2:"55",EIN2:'' , NAME2:''}, 
    {  Seq:"41", EIN: '3610101', NAME: '',Seq2:"56", EIN2:'' , NAME2:''}, 
    {  Seq:"42", EIN: '3610101', NAME: '', Seq2:"57",EIN2:'' , NAME2:''}, 
    {  Seq:"43", EIN: '3610101', NAME: '', Seq2:"58",EIN2:'' , NAME2:''}, 
    {  Seq:"44", EIN: '3610101', NAME: '', Seq2:"59",EIN2:'' , NAME2:''}, 
    {  Seq:"45", EIN: '3610101', NAME: '', Seq2:"60",EIN2:'' , NAME2:''}, 
    {  Seq:"46", EIN: '3610101', NAME: '', Seq2:"61",EIN2:'' , NAME2:''}, 
    {  Seq:"47", EIN: '3610101', NAME: '', Seq2:"62",EIN2:'' , NAME2:''}, 

    {  Seq:"48", EIN: '3610101', NAME: '',Seq2:"63", EIN2:'' , NAME2:''}, 
    {  Seq:"49", EIN: '3610101', NAME: '', Seq2:"64",EIN2:'' , NAME2:''}, 
    {  Seq:"50", EIN: '3610101', NAME: '', Seq2:"65",EIN2:'' , NAME2:''}, 
];
