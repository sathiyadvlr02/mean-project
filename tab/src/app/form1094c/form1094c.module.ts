import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1094CRoutes } from './form1094c.routing';
import { ChartistModule } from 'ng-chartist';

import { Create1094CComponent } from './create1094cform/create1094c.component'; 
import {Form1094CComponent } from './form1094cmanagement/form1094c.component';

//import { Update1099SAComponent } from './update1099saform/update1099sa.component'; 
//import { History1099SAComponent } from './history1099saform/history1099sa.component'; 


@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Form1094CRoutes)
  ],

  declarations: [Create1094CComponent,Form1094CComponent]

})
export class Form1094CModule {}
