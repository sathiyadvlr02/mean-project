import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';



@Component({
    selector: 'app-form1094c',
    templateUrl: './form1094c.component.html',

})
export class Form1094CComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    displayedColumns: string[] = ['Employer_EIN', 'Employer_Name', 'Employer_Address',  'action'];
    dataSource = ELEMENT_DATA;


    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(private router: Router) { }

    ngOnInit() {

        this.homeForm = new FormGroup({
        });
    }

    ngAfterViewInit() {
        //  this.dataSource.paginator = this.paginator;
    }


    submit(): void {
        //if (this.username == 'admin' && this.password == 'admin') {
        this.router.navigate(["create1094c"]);
        // } else {
        //  alert("Invalid credentials");
        // }
    }


}

/* Static data */

export interface PeriodicElement {
    Employer_EIN: string;
    Employer_Name: string;
    Employer_Address: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { Employer_Name: 'Tab Service Company', Employer_Address: '310 S Racine', Employer_EIN: '101010101' },
    { Employer_Name: 'Tab Service Company', Employer_Address: '310 S Racine', Employer_EIN: '101010101' },
    { Employer_Name: 'Tab Service Company', Employer_Address: '310 S Racine', Employer_EIN: '101010101' },
    { Employer_Name: 'Tab Service Company', Employer_Address: '310 S Racine', Employer_EIN: '101010101' }

];
