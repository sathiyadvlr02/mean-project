import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { changepasswordRoutes } from './changepassword.routing';
import { ChangePasswordComponent} from './changepassword.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FormsModule ,ReactiveFormsModule ,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(changepasswordRoutes)
  ],
  declarations: [ChangePasswordComponent]
})
export class changepasswordModule {}
