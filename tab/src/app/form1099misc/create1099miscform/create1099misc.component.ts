import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-create1099misc',
    templateUrl: './create1099misc.component.html',
    styleUrls: ['./create1099misc.component.scss'],
})
export class Create1099MISCComponent implements OnInit {

    createAppForm: FormGroup;
    stateList;
    country;
    taxyear;
    // foreign=false;
    //FATCA=false;
    // ombno=false;
    // income=false;
    // nonemployee=false;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices : StateService,
    ) { }

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            corrected: new FormControl(false),
            payerName: new FormControl('', [Validators.required]),
            payerAddress1: new FormControl('', [Validators.required]),
            payerAddress2: new FormControl(''),
            payerAddress3: new FormControl(''),
            payerCity: new FormControl('', [Validators.required]),
            payerState: new FormControl('', [Validators.required]),
            payerZip: new FormControl('', [Validators.required]),
            payerTelephoneNo1: new FormControl('', [Validators.required]),
            payerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            receiptIdentificationNumber: new FormControl('', [Validators.required]),
            receiptName1: new FormControl('', [Validators.required]),
            receiptName2: new FormControl('', [Validators.required]),
            receiptAddress1: new FormControl('', [Validators.required]),
            receiptAddress2: new FormControl(''),
            receiptAddress3: new FormControl(''),
            receiptCity: new FormControl(''),
            receiptState: new FormControl(''),
            receiptZip: new FormControl(''),
            forignCountry: new FormControl(false),
            receiptCountry: new FormControl(''),
            accountNumber: new FormControl('', [Validators.required]),
            fatca: new FormControl(false),
            rent: new FormControl(''),
            rolyalties: new FormControl(''),
            otherIncome: new FormControl(''),
            federalIncomeTaxWithheld: new FormControl(''),
            fishingBoatProceeds: new FormControl(''),
            medicalAndHealthCarePayments: new FormControl(''),
            payerMadeDirectSales: new FormControl(false),
            substituePaymentsInLieuOfDividendsOrInterest: new FormControl(''),
            cropInsuranceProceeds: new FormControl(''),
            grossProceedsPaidToAnAttorney: new FormControl(''),
            section409ADeferrals: new FormControl(''),
            excessGoldenParachutePayments: new FormControl(''),
            nonqualifiedDeferredCompensation: new FormControl(''),
            stateTaxWithheld1: new FormControl(''),
            stateTaxWithheld2: new FormControl(''),
            statePayersStateNo1: new FormControl(''),
            statePayersStateNo2: new FormControl(''),
            stateIncome1: new FormControl(''),
            stateIncome2: new FormControl(''),
            
        });
        this.taxyear = 2020; 
        this.getstatelist();
        this.getCountry();
    }

    get trrowErrors() { return this.createAppForm.controls; }

    onSubmit(){

    }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }



}

