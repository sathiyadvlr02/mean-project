import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { Form1099MISCRoutes } from './form109misc.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099MISCComponent } from './create1099miscform/create1099misc.component';
import { Update1099MISCComponent } from './update1099miscform/update1099misc.component';
import { History1099MISCComponent } from './history1099miscform/history1099misc.component';
import { FormEmail1099MISC } from './email1099miscform/email1099misc.component';
import { SetPassword } from './email1099miscform/setpassword.component';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule ,ReactiveFormsModule,
    RouterModule.forChild(Form1099MISCRoutes)
  ],
  declarations: [
    Create1099MISCComponent,
    Update1099MISCComponent,
    History1099MISCComponent,
    FormEmail1099MISC,
    SetPassword
  ]
})
export class Form1099MISCModule {}
