import { Routes } from '@angular/router';

import { Form1099MISCModule } from './form109misc.module';
import { Create1099MISCComponent } from './create1099miscform/create1099misc.component';
import { Update1099MISCComponent } from './update1099miscform/update1099misc.component';
import { History1099MISCComponent } from './history1099miscform/history1099misc.component';
import { FormEmail1099MISC } from './email1099miscform/email1099misc.component';

export const Form1099MISCRoutes: Routes = [
   
  {
    path: 'create1099misc',
    component: Create1099MISCComponent  
  },
  {
    path: 'form1099misccreate',
    component: Create1099MISCComponent  
  },
  {
    path: 'form1099miscupdate',
    component: Update1099MISCComponent 
  },
  {
    path: 'form1099mischistory',
    component: History1099MISCComponent 
  },
  {
    path: 'form1099miscemail',
    component: FormEmail1099MISC 
  },


];
