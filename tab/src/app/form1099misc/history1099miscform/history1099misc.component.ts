import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-history1099misc',
    templateUrl: './history1099misc.component.html',
    styleUrls: ['./history1099misc.component.scss'],
})
export class History1099MISCComponent implements OnInit {
    
    createAppForm: FormGroup;
    stateList;
    country;
    taxyear;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices : StateService,
    ) {}

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            corrected: new FormControl(false),
            payerName: new FormControl(''),
            payerAddress1: new FormControl(''),
            payerAddress2: new FormControl(''),
            payerAddress3: new FormControl(''),
            payerCity: new FormControl(''),
            payerState: new FormControl(''),
            payerZip: new FormControl(''),
            payerTelephoneNo1: new FormControl(''),
            payerFederalIdentificationNumber: new FormControl(''),
            receiptIdentificationNumber: new FormControl(''),
            receiptName1: new FormControl(''),
            receiptName2: new FormControl(''),
            receiptAddress1: new FormControl(''),
            receiptAddress2: new FormControl(''),
            receiptAddress3: new FormControl(''),
            receiptCity: new FormControl(''),
            receiptState: new FormControl(''),
            receiptZip: new FormControl(''),
            forignCountry: new FormControl(false),
            receiptCountry: new FormControl(''),
            accountNumber: new FormControl(''),
            fatca: new FormControl(false),
            rent: new FormControl(''),
            rolyalties: new FormControl(''),
            otherIncome: new FormControl(''),
            federalIncomeTaxWithheld: new FormControl(''),
            fishingBoatProceeds: new FormControl(''),
            medicalAndHealthCarePayments: new FormControl(''),
            payerMadeDirectSales: new FormControl(false),
            substituePaymentsInLieuOfDividendsOrInterest: new FormControl(''),
            cropInsuranceProceeds: new FormControl(''),
            grossProceedsPaidToAnAttorney: new FormControl(''),
            section409ADeferrals: new FormControl(''),
            excessGoldenParachutePayments: new FormControl(''),
            nonqualifiedDeferredCompensation: new FormControl(''),
            stateTaxWithheld1: new FormControl(''),
            stateTaxWithheld2: new FormControl(''),
            statePayersStateNo1: new FormControl(''),
            statePayersStateNo2: new FormControl(''),
            stateIncome1: new FormControl(''),
            stateIncome2: new FormControl(''),
            releaseDate: new FormControl(''),
            eConsentDate: new FormControl(''),
            memo: new FormControl(''),
        });
        this.taxyear=2020;
        this.getstatelist();
        this.getCountry();
    }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
    }


}

