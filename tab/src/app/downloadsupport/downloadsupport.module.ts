import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { DownloadSupportRoutes  } from './downloadsupport.routing';

import { StandardTemplatesComponent } from './standardtemplates/standardtemplates.component';
import { EmailsupportrequestComponent } from './emailsupportrequest/emailsupportrequest.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(DownloadSupportRoutes)
  ],
  declarations: [StandardTemplatesComponent,EmailsupportrequestComponent]
})
export class DownloadSupportModule {}
