import { Component, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TemplateService } from './.././../services/template.service';

declare var require: any;

@Component({
	selector: 'app-standardtemplates',
    templateUrl: './standardtemplates.component.html',
    styleUrls:['./standardtemplates.component.scss']

})
export class StandardTemplatesComponent {
	templateTaxYear: FormGroup;
    selectedRow:any;
    TemplateselectedRow:any;
	displayedColumns: string[] = ['form_type', 'description', 'file_link'];
	TemplatedataSource: MatTableDataSource<any>;

	manualdisplayedColumns: string[] = ['form_type', 'description', 'file_link'];
	ManualdataSource: MatTableDataSource<any>;

	yearList = [
        { name: 'Select Year', value: '' },
        { name: '2020', value: '2020' },
        { name: '2019', value: '2019' },
        { name: '2018', value: '2018' },
        { name: '2017', value: '2017' },
        { name: '2016', value: '2016' },
        { name: '2015', value: '2015' },
        { name: '2014', value: '2014' },
        { name: '2013', value: '2013' },
        { name: '2012', value: '2012' },
        { name: '2011', value: '2011' },
        { name: '2010', value: '2010' },
        { name: '2005', value: '2005' },
    ];

	@ViewChild('templatePaginator', { static: true, read: MatPaginator }) templatePaginator: MatPaginator;
    @ViewChild('templatePaginator', { static: true, read: MatSort }) templateSort: MatSort;
	@ViewChild('userGuidePaginator', { static: true, read: MatPaginator }) userGuidePaginator: MatPaginator;
    @ViewChild('userGuidePaginator', { static: true, read: MatSort }) userGuideSort: MatSort;





	constructor(
		private router: Router,
		private fb: FormBuilder,
		public _templateService: TemplateService
		) { }

	ngOnInit() {
        var myPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection;
        console.log('window', myPeerConnection);
		this.templateTaxYear = new FormGroup({
            formYear: new FormControl('2020', [Validators.required]),
        });
        this.gettaxyear('2020')
	}

	get errors() {
        return this.templateTaxYear.controls;
    }

	gettaxyear(event) {
		this.getFileDetails(event)
		this.getUserGuideDetails(event)
	}


	getUserGuideDetails(taxyr) {
        this._templateService
            .getUserGuide(taxyr)
            .subscribe(
                (result) => {
					console.log(JSON.parse(result.data))
                    if (result.isValid) {
                        this.ManualdataSource = new MatTableDataSource(JSON.parse(result.data));
                        this.ManualdataSource.paginator = this.userGuidePaginator;
                        this.ManualdataSource.sort = this.userGuideSort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

	getFileDetails(taxyr) {
        this._templateService
            .getTemplateList(taxyr)
            .subscribe(
                (result) => {
					console.log(JSON.parse(result.data))
                    if (result.isValid) {
                        this.TemplatedataSource = new MatTableDataSource(JSON.parse(result.data));
                        this.TemplatedataSource.paginator = this.templatePaginator;
                        this.TemplatedataSource.sort = this.templateSort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

	onClickDownloadFile(templateId){
		const link = document.createElement('a');
		link.setAttribute('target', '_blank');
		link.setAttribute('href', '/api/v1/TemplateFile/DownloadFile/' + templateId);
		document.body.appendChild(link);
		link.click();
		link.remove();
	}


}



