import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-emailsupportrequest',
    templateUrl: './emailsupportrequest.component.html'
})
export class EmailsupportrequestComponent implements OnInit {

    emailsupport: FormGroup;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(
        public _userService : UserService,
        public toasterService: ToastrService,
        private el: ElementRef,
        private cookieService: CookieService,
    ) { }

    ngOnInit(): void {
        this.emailsupport = new FormGroup({
            contactname: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required, Validators.email]),
            contactphone: new FormControl('', [Validators.required]),
            type: new FormControl('', [Validators.required]),
            issue: new FormControl('', [Validators.required]),
            description: new FormControl('', [Validators.required])
        })
    }

    get Errors() { return this.emailsupport.controls; }

    onSubmit(){
        if (this.emailsupport.invalid) {
            for (const key of Object.keys(this.emailsupport.controls)) {
              if (this.emailsupport.controls[key].invalid) {
                const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                invalidControl.focus();
                break;
              }
            }
            return;
        }
        let supportFormData = this.emailsupport.value;
        let datas = {
            sitE_ID: this.CookieSiteId.toString(),
            contacT_NAME: supportFormData.contactname,
            emaiL_ADDRESS: supportFormData.email,
            contacT_PHONE: supportFormData.contactphone,
            issuE_TYPE: supportFormData.issue,
            forM_TYPE: supportFormData.type,
            description: supportFormData.description
        };

        this._userService
        .emailSupportRequest(datas)
        .subscribe(
            (result) => {
                if(result.isValid){
                    this.toasterService.success(
                    result.message,
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    });
                    this.emailsupport.get('contactname').setValue('');
                    this.emailsupport.get('email').setValue('');
                    this.emailsupport.get('contactphone').setValue('');
                    this.emailsupport.get('type').setValue('');
                    this.emailsupport.get('issue').setValue('');
                    this.emailsupport.get('description').setValue('');
                } else {
                    this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    });
                }
            },
            (error) => {
                this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
            }
        );
    }

    typeofissue: TypeofIssue[] = [
        { value: 'Login Issue - High', viewValue: 'Login Issue - High' },
        { value: 'Printing Issue - Low', viewValue: 'Printing Issue - Low' },
        { value: 'Taxform update Issue - Middle', viewValue: 'Taxform update Issue - Middle' },
        { value: 'Taxform create Issue - Middle', viewValue: 'Taxform create Issue - Middle' },
        { value: 'Other Issue - Low', viewValue: 'Other Issue - Low' }
    ];
}


interface TypeofIssue {
    value: string;
    viewValue: string;
}