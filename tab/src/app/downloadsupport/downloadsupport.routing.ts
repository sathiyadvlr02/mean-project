import { Routes } from '@angular/router';
 
import { StandardTemplatesComponent } from './standardtemplates/standardtemplates.component';
import { EmailsupportrequestComponent } from './emailsupportrequest/emailsupportrequest.component';

export const DownloadSupportRoutes: Routes = [
  {
  path: 'downloadtools',
  component: StandardTemplatesComponent  
},
{
  path: 'standardtemplate1099',
  component: StandardTemplatesComponent  
},
{
  path: 'emailsupportrequest',
  component: EmailsupportrequestComponent  
},

];
