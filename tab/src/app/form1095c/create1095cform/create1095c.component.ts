import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-create1095c',
  templateUrl: './create1095c.component.html',
  styleUrls:['./create1095c.component.scss']
  
})

export class Create1095CComponent implements OnInit {
  homeForm: FormGroup;
  
  displayedColumns: string[] = ['Type', 'Allmonths', 'Jan',  'Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  dataSource = ELEMENT_DATA;

  
  AALEdisplayedColumns: string[] = ['Seq','EIN', 'NAME','Seq2', 'EIN2',  'NAME2'];
  AALEdataSource = AALE_ELEMENT_DATA;


  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }

}

/* Static data */

export interface PeriodicElement {
    Type: string;
    Allmonths: string;
    Jan:string ;    
    Feb:string ;
    Mar:string ;
    Apr: string;
    May: string;
    Jun: string;
    Jul: string;
    Aug: string;
    Sep: string;
    Oct: string;
    Nov: string;
    Dec: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {  Type: 'Offer of Coverage Code', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Type: 'Employee Required Contribution (see instructions)', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Type: 'Section 4980H Safe Harbor and Other Relief (enter code, if applicable)', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''}
];



export interface AALEElement {
    Seq:string ;
    EIN: string;
    NAME: string;
    Seq2:string ;
    EIN2:string ;    
    NAME2:string ; 
}

const AALE_ELEMENT_DATA: AALEElement[] = [
    { Seq:"36", EIN: '3610101', NAME: '',Seq2:"51", EIN2:'' , NAME2:''},    
    {  Seq:"37", EIN: '3610101', NAME: '',Seq2:"52", EIN2:'' , NAME2:''},    
    {  Seq:"38", EIN: '3610101', NAME: '', Seq2:"53",EIN2:'' , NAME2:''}, 
    {  Seq:"39", EIN: '3610101', NAME: '',Seq2:"54", EIN2:'' , NAME2:''}, 
    {  Seq:"40", EIN: '3610101', NAME: '', Seq2:"55",EIN2:'' , NAME2:''}, 
    {  Seq:"41", EIN: '3610101', NAME: '',Seq2:"56", EIN2:'' , NAME2:''}, 
    {  Seq:"42", EIN: '3610101', NAME: '', Seq2:"57",EIN2:'' , NAME2:''}, 
    {  Seq:"43", EIN: '3610101', NAME: '', Seq2:"58",EIN2:'' , NAME2:''}, 
    {  Seq:"44", EIN: '3610101', NAME: '', Seq2:"59",EIN2:'' , NAME2:''}, 
    {  Seq:"45", EIN: '3610101', NAME: '', Seq2:"60",EIN2:'' , NAME2:''}, 
    {  Seq:"46", EIN: '3610101', NAME: '', Seq2:"61",EIN2:'' , NAME2:''}, 
    {  Seq:"47", EIN: '3610101', NAME: '', Seq2:"62",EIN2:'' , NAME2:''}, 

    {  Seq:"48", EIN: '3610101', NAME: '',Seq2:"63", EIN2:'' , NAME2:''}, 
    {  Seq:"49", EIN: '3610101', NAME: '', Seq2:"64",EIN2:'' , NAME2:''}, 
    {  Seq:"50", EIN: '3610101', NAME: '', Seq2:"65",EIN2:'' , NAME2:''}, 
];
