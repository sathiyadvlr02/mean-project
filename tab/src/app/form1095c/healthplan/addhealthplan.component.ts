import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

@Component({
    selector: 'app-healthplan',
    templateUrl: './addhealthplan.component.html',
    styleUrls: ['./addhealthplan.component.scss']

})

export class AddHealthPlanComponent implements OnInit {


    displayedColumns: string[] = ['Month', 'MEC', 'Full_Time_Employee', 'Total_Employee_Count', 'Aggregated_Group_Indicator', 'Section_4980H'];
    dataSource = ELEMENT_DATA;

    ngOnInit(): void {
        // this.navService.closeNav();
    }

}


/* Static data */

export interface PeriodicElement {
    Month: string;
    MEC: string;
    Full_Time_Employee: string;
    Total_Employee_Count: string;
    Aggregated_Group_Indicator: string;
    Section_4980H: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { Month: 'All 12 Months', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Jan', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Feb', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },

    { Month: 'Mar', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Apr', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'May', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Jun', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Jul', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Aug', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Sep', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Oct', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Nov', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },
    { Month: 'Dec', MEC: '', Full_Time_Employee: '', Total_Employee_Count: '', Aggregated_Group_Indicator: '', Section_4980H: '' },

];

