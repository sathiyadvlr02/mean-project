import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-update1095c',
  templateUrl: './update1095c.component.html',
  styleUrls:['./update1095c.component.scss']  
})

export class Update1095CComponent implements OnInit {
  homeForm: FormGroup;
  selectedRow:any;
  CIselectedRow:any;
  
  displayedColumns: string[] = ['Type', 'Allmonths', 'Jan',  'Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  dataSource = ELEMENT_DATA;

   
  CIdisplayedColumns: string[] =  ['Name','SSN','DOB', 'Allmonths', 'Jan',  'Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','action'];
  CIdataSource = CI_ELEMENT_DATA;


  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }

}

/* Static data */

export interface PeriodicElement {
    Type: string;
    Allmonths: string;
    Jan:string ;    
    Feb:string ;
    Mar:string ;
    Apr: string;
    May: string;
    Jun: string;
    Jul: string;
    Aug: string;
    Sep: string;
    Oct: string;
    Nov: string;
    Dec: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {  Type: 'Offer of Coverage Code', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Type: 'Employee Required Contribution (see instructions)', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Type: 'Section 4980H Safe Harbor and Other Relief (enter code, if applicable)', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''}
];



export interface CIElement {
    Name:string;
    SSN:string;
    DOB:string;    
    Allmonths: string;
    Jan:string ;    
    Feb:string ;
    Mar:string ;
    Apr: string;
    May: string;
    Jun: string;
    Jul: string;
    Aug: string;
    Sep: string;
    Oct: string;
    Nov: string;
    Dec: string;
}

const CI_ELEMENT_DATA: CIElement[] = [
    {  Name:'',SSN:'',DOB:'', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Name:'',SSN:'',DOB:'', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''},    
    {  Name:'',SSN:'',DOB:'', Allmonths:'', Jan:'', Feb:'',Mar:'',Apr:'',May:'',Jun:'',Jul:'',Aug:'',Sep:'',Oct:'',Nov:'',Dec:''}

];
