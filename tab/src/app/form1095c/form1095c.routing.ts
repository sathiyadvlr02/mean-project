import { Routes } from '@angular/router';

import { Create1095CComponent } from './create1095cform/create1095c.component'; 
import { Update1095CComponent } from './update1095cform/update1095c.component'; 
import { HealthPlanComponent } from './healthplan/healthplan.component';
import { AddHealthPlanComponent } from './healthplan/addhealthplan.component';
import { TimePeriodComponent } from './timeperiod/timeperiod.component';
import { AddTimePeriodComponent } from './timeperiod/addtimeperiod.component';



export const Form1095CRoutes: Routes = [
    {
        path: 'delete1099',
        component: Create1095CComponent
    },
    {
        path: '1095C_employeemaster',
        component: Create1095CComponent
    }, 
    {
        path: 'update1095c',
        component: Update1095CComponent
    }, 
    {
        path: 'healthplan',
        component: HealthPlanComponent
    }, 
    {
        path: 'addhealthplan',
        component: AddHealthPlanComponent
    }, 

    {
        path: 'timeperiod',
        component: TimePeriodComponent
    }, 

    {
        path: 'addtimeperiod',
        component: AddTimePeriodComponent
    },

];
