import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-timeperiod',
  templateUrl: './timeperiod.component.html',
  styleUrls:['./timeperiod.component.scss']
  
})

export class TimePeriodComponent implements OnInit {
  homeForm: FormGroup;
  
  displayedColumns: string[] = ['employer_ein', 'healthplancode',  'code_description','action'];
  dataSource = ELEMENT_DATA;

    

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }

  
  submit(): void {
    //if (this.username == 'admin' && this.password == 'admin') {
    this.router.navigate(["addtimeperiod"]);
    // } else {
    //  alert("Invalid credentials");
    // }
  }

}

/* Static data */

export interface PeriodicElement {
   employer_ein:string;
   healthplancode:string ;
   planstartmonth:string ;
   code_description:string ; 

}

const ELEMENT_DATA: PeriodicElement[] = [
    {  employer_ein:'361010101', healthplancode:'FT001', planstartmonth:'01', code_description:'FT'},    
    {  employer_ein:'361010101', healthplancode:'FT002', planstartmonth:'01', code_description:'FT'},    
    {  employer_ein:'361010101', healthplancode:'FT003', planstartmonth:'04', code_description:'FT'},    
];

 