import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Form1095CRoutes } from './form1095c.routing';
import { ChartistModule } from 'ng-chartist';

import { Create1095CComponent } from './create1095cform/create1095c.component'; 
import { Update1095CComponent } from './update1095cform/update1095c.component'; 
import { HealthPlanComponent } from './healthplan/healthplan.component';
import { AddHealthPlanComponent } from './healthplan/addhealthplan.component';

import { TimePeriodComponent } from './timeperiod/timeperiod.component';
import { AddTimePeriodComponent } from './timeperiod/addtimeperiod.component';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    FormsModule, ReactiveFormsModule,
    ChartistModule,
    RouterModule.forChild(Form1095CRoutes)
  ],

  declarations: [
      Create1095CComponent,
      Update1095CComponent,
        HealthPlanComponent,
        AddHealthPlanComponent,
        TimePeriodComponent,
        AddTimePeriodComponent
    ]

})
export class Form1095CModule {}
