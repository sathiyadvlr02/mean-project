
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { ToastrModule } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FullComponent } from './layouts/full/full.component';
import { AdminLayoutComponent } from './layouts/full/adminlayout.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { MenuListItemComponent } from "./layouts/full/menu-list-item/menu-list-item.component";
import { AppSidebarAdminComponent } from './layouts/full/sidebar/sidebaradmin.component';

import { AppMaterialModule } from './app-material-module';
import { AuthGuardService } from './services/auth-guard.service';
import { NavService } from './nav.service';


import { AuthguradServiceService } from '../app/services/auth/authgurad-service.service';

import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';

import { LoginComponent } from './login/login.component';
import { Login1098Component } from './login/login1098.component';
import { LoginspsgzComponent } from './login/loginspsgz.component';

import { SendCodeComponent } from './send-code/send-code.component';
import { ValidateCodeComponent } from './validate-code/validate-code.component';

import { SendCode1098TComponent } from './send-code/send-code1098t.component';
import { ValidateCode1098TComponent } from './validate-code/validate-code1098t.component';

import { SendCodeSPSGZComponent } from './send-code/send-codeSPSGZ.component';
import { ValidateCodeSPSGZComponent } from './validate-code/validate-codeSPSGZ.component';


import { ForgotComponent } from './forgot/forgot.component';
import { ForgotSPSGZComponent } from './forgot/forgotspsgz.component';
import { Forgot1098Component } from './forgot/forgot1098.component';
import { ResetPasswordComponent } from './login/resetpassword.component';
import { ResetPassword1098Component } from './login/resetpassword1098.component';
import { ResetPasswordspsgzComponent } from './login/resetpasswordspsgz.component';

import { ChecklistDatabase } from './test/draganddrop.component';
import { DragandDropComponent } from './test/draganddrop.component';

import { StudentViewComponent } from './studentforms/viewforms/studentviewform.component';
import { OnlineConsentComponent } from './studentforms/onlineconsent/onlineconsent.component';



import { ToolTipRendererDirective } from './shared/tool-tip-renderer.directive';
import { CustomToolTipComponent } from './shared/custom-tool-tip/custom-tool-tip.component';
import { APIinterceptor } from './services/APIinteceptor.service';

@NgModule({
    declarations: [
        AppComponent,
        FullComponent,
        AdminLayoutComponent,
        AppHeaderComponent,
        SpinnerComponent,
        AppSidebarComponent,
        AppSidebarAdminComponent,
        MenuListItemComponent,
        LoginComponent,
        Login1098Component,
        LoginspsgzComponent,

        SendCodeComponent,
        ValidateCodeComponent,

        SendCode1098TComponent,
        ValidateCode1098TComponent,

        SendCodeSPSGZComponent,
        ValidateCodeSPSGZComponent,

        ForgotComponent,
        ForgotSPSGZComponent,
        Forgot1098Component,
        ResetPasswordComponent,
        ResetPassword1098Component,
        ResetPasswordspsgzComponent,
        StudentViewComponent,
        OnlineConsentComponent,
        CustomToolTipComponent,
        ToolTipRendererDirective,
        DragandDropComponent,



    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        HttpClientModule,
        SharedModule,
        RouterModule.forRoot(AppRoutes, { useHash: true }),         
        ToastrModule.forRoot(),
    ],
    providers: [AuthGuardService, AuthguradServiceService, CookieService, NavService, ChecklistDatabase,
        {

            provide: LocationStrategy,
            useClass: PathLocationStrategy
        },
        { provide: HTTP_INTERCEPTORS, useClass: APIinterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
  

})
export class AppModule { }
