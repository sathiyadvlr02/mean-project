import { Component, AfterViewInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder ,ReactiveFormsModule } from '@angular/forms'; 
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { PreofileService } from '../services/profile.service';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',	
})

export class ProfileComponent implements AfterViewInit {
    profileForm: FormGroup = new FormGroup({
        firstname: new FormControl('', Validators.required),
        lastname: new FormControl(''),
        email: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        mfaSettings: new FormControl(false),
        middlename: new FormControl(''),
        displayname: new FormControl(''),
        userId: new FormControl('')
    });

    loading: boolean;
    responseData = {};


    constructor(
        private router: Router,
        private toasterService: ToastrService,
        private _profileService: PreofileService,
        private fb: FormBuilder,
        ) { } 


    ngAfterViewInit() {
        this._profileService.getProfileData()
        .subscribe(
            (result) => {
                this.profileForm.get('firstname').setValue(result.firstname);
                this.profileForm.get('lastname').setValue(result.lastname);
                this.profileForm.get('email').setValue(result.email);
                this.profileForm.get('mfaSettings').setValue(result.isTwoFactorOn);
                this.profileForm.get('userId').setValue(result.userId);
                this.profileForm.get('middlename').setValue(result.middlename);
                this.profileForm.get('displayname').setValue(result.displayname);
                this.profileForm.get('phone').setValue(result.phone);
                this.responseData = result
            },
            (error) => {
                console.log('error', error)
            }
        )

    }

    onChangeToogle(){
        this.profileForm.get('mfaSettings').setValue(!this.profileForm.value.mfaSettings);
    }

    onSubmit(){
        let profileUpdateFormData = this.profileForm.value;
        let updateListData = {
            firstname: profileUpdateFormData.firstname,
            lastname: profileUpdateFormData.lastname,
            middlename: profileUpdateFormData.middlename,
            displayname: profileUpdateFormData.displayname,
            phone: profileUpdateFormData.phone,
            email: profileUpdateFormData.email,
            IsTwoFactorOn: profileUpdateFormData.mfaSettings,
            Username: profileUpdateFormData.userId,
        }
        this._profileService
            .upDateProfile(updateListData)
            .subscribe(
                (result) => {
                    this.router.navigate(['/home']);
                },
                (error) => {
                    console.log('profile error', error)
                }
            )
        console.log('responseData', this.responseData)
    }
}