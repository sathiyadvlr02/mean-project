import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { profileRoutes } from './profile.routing';
import { ProfileComponent } from './profile.component';

@NgModule({
    imports: [
      CommonModule,
      AppMaterialModule,
      FormsModule ,ReactiveFormsModule ,
      FlexLayoutModule,
      ChartistModule,
      RouterModule.forChild(profileRoutes)
    ],
    declarations: [ProfileComponent],
    
  })
  export class profileModule {}