import { Routes } from '@angular/router';

import { AdministrationManageSystemUserComponent } from './administrationmanagesystemuser/administrationmanagesystemuser.component';
import { ManageAdduserComponent } from './administrationmanagesystemuser/manage-adduser.component';
import { ManageEdituserComponent } from './administrationmanagesystemuser/manage-edituser.component';
import { AddEntityComponent } from './administrationsiteentity/addentity.component';
import { EditEntityComponent } from './administrationsiteentity/editentity.component';

import { AddPOCsComponent } from './administrationsiteentity/addpocs.component';
import { EditPOCsComponent } from './administrationsiteentity/editpocs.component';
import { AdministrationSiteEntityComponent } from './administrationsiteentity/administrationsiteentity.component';
import { AdministrationViewFormsAndReportsComponent } from './administrationviewformsandreports/administrationviewformsandreports.component';
import { BroadcastComponent } from './broadcastmessage/broadcast.component';
import { FAQComponent } from './faqcomponents/faq.component';


//import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangePasswordComponent } from '../changepassword/changepassword.component';
import { AdminUserTypeComponent } from './admin-user-type/admin-user-type.component';
import { AddAdminUserTypeComponent } from './admin-user-type/admin-adduser-type.component';
import { EditAdminUserTypeComponent } from './admin-user-type/admin-edituser-type.component';
import { EmailHistoryComponent } from './email-history/email-history.component';


export const AdministrationManageSystemUserRoutes: Routes = [
    {
        path: 'manageuser',
        component: AdministrationManageSystemUserComponent
    },

    {
        path: 'changepassword',
        component: ChangePasswordComponent
    },
    {
        path: 'menuadminemployeetype',
        component: AdminUserTypeComponent
    },

    {
        path: 'addadminusertype',
        component: AddAdminUserTypeComponent
    },
    {
        path: 'editadminusertype/:id',
        component: EditAdminUserTypeComponent
    },

    {
        path: 'emailhistory',
        component: EmailHistoryComponent
    },

    {
        path: 'addsystemUser',
        component: ManageAdduserComponent
    },
    {
        path: 'editsystemUser/:id',
        component: ManageEdituserComponent
    },
    {
        path: 'modifysite_clientadmin',
        component: AdministrationSiteEntityComponent
    },

    {
        path: 'addpocs',
        component: AddPOCsComponent
    },
    {
        path: 'editpocs/:id',
        component: EditPOCsComponent
    },
    
    {
        path: 'addentity',
        component: AddEntityComponent
    },
    {
        path: 'editentity/:id',
        component: EditEntityComponent
    },

    {
        path: 'faq1098t',
        component: FAQComponent
    },

    {
        path: 'broadcastmessageedit',
        component: BroadcastComponent
    },

    {
        path: 'viewformsandreports',
        component: AdministrationViewFormsAndReportsComponent
    },

];
