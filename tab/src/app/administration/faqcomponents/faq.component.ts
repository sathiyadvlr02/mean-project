import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

import { FAQService } from '../../services/faq.service';

declare var require: any;

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
})
export class FAQComponent {
  faqdata;
    constructor(private router: Router,
        private _faqervice: FAQService,
        public toasterService: ToastrService) {
            this.getFAQDetails();
         }

    scroll(el: any) {
        el.scrollIntoView({
            bahavior: "smooth"
        });
    }

    navigateToSection(section: string) {
        window.location.hash = '';
        window.location.hash = section;
    }


    getFAQDetails(){
        this._faqervice
        .getFAQDetails()
        .subscribe(
            (result) => {
                if(result.isValid){
                    this.faqdata= result.data;
                }
            },
            (error) => {
                console.log('error', error)
            }
        );
      }


}
