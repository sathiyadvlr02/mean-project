import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { EntityService} from '../../services/ein.service';
import { CookieService } from 'ngx-cookie-service';
import {StateService} from './../../services/state.service';  
@Component({
  selector: 'app-entity',
  templateUrl: './addentity.component.html'  
})
export class AddEntityComponent implements OnInit {
    addentity: FormGroup;
    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    public checkboxvalue="N";
    stateList;
  constructor(
    private el: ElementRef,
    public toasterService: ToastrService,
    private router: Router,
    public entityservice : EntityService,
    private cookieService: CookieService,
    public StateServices : StateService,
  ){}
    get Errors() { return this.addentity.controls; }
  ngOnInit(): void {
    this.addentity = new FormGroup({
        ein: new FormControl('', [Validators.pattern(/^\d{2}-\d{8}$/),Validators.required]),
        entityname: new FormControl('', [Validators.required]),
        address1: new FormControl('', [Validators.required]),
        address2: new FormControl(''),
        address3: new FormControl(''),
        city: new FormControl('', [Validators.required]),
        state: new FormControl('', [Validators.required]),
        zipcode: new FormControl('', [Validators.required]),
        phone1: new FormControl('', [Validators.required]),
        phone2: new FormControl(''),
        fax: new FormControl('',[Validators.required]),
        federal:new FormControl(false)
    });
    this.getstatelist();
  }


  // chkChanged(event){
  //   this.checkboxvalue = event.checked ? 'Y' : 'N';
  //   this.addentity.get('federal').setValue(this.checkboxvalue);
  // }

  getstatelist() {
    this.StateServices.getStatelist().subscribe(
        result => {
            this.stateList= JSON.parse(result.data);
        });
}

cancelButton(){
  this.cookieService.set('TabValue','1'); 
  this.router.navigate(['home/modifysite_clientadmin']);
}

  AddEntity(){
    if (this.addentity.invalid) {
        for (const key of Object.keys(this.addentity.controls)) {
          if (this.addentity.controls[key].invalid) {
            const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
            invalidControl.focus();
            break;
          }
        }
        return;
      }
      let addFormData = this.addentity.value;
      let datas = {
        ID: 0,
        SITE_ID: parseInt(this.CookieSiteId),
        EIN_NAME: addFormData.entityname,
        EIN_NUMBER: addFormData.ein,
        ADDRESS1: addFormData.address1,
        ADDRESS2: addFormData.address2,
        ADDRESS3: addFormData.address3,
        CITY: addFormData.city,
        STATE: addFormData.state,
        ZIP: addFormData.zipcode,
        PHONE: addFormData.phone1,
        PHONE2: addFormData.phone2,
        FAX: addFormData.fax,
        FOREIGN_COUNTRY_INDICATOR: '',
        FOREIGN_COUNTRY: '',
        FOREIGN_PROVINCE: '',
        FOREIGN_POSTAL_CODE: '',
        EFILE_DATE: '2021-06-09T12:19:49.607Z',
        DATABASE_LOAD_ID: '',
        IDMS_CLIENT_ID: '',
        IDMS_CLIENT_NBR: '',
        NAME_CONTROL: '',
        COMBINED_FEDERAL_STATE_FILING_PGM:  addFormData.federal ? 'Y' : 'N',
        EIN_NAME2: '',
        CONTACT_NAME: '',
        CONTACT_FIRSTNAME: '',
        CONTACT_LASTNAME: '',
        contacT_MIDDLENAME: '',
        CONTACT_SUFFIX: '',
        TOTAL_1095B_SUBMITTED: '',
        SIGNATURE: '',
        TITLE: '',
        DATE: ''
      };
      this.entityservice
      .addEntity(datas)
      .subscribe(
        (result) => {
        this.cookieService.set('TabValue','1');  
          if(result.isValid){
            this.toasterService.success(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.router.navigate(['home/modifysite_clientadmin']);
          } else {
            this.toasterService.error(
              'Please Try Again.',
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.router.navigate(['home/modifysite_clientadmin']);
          }
        },
        (error) => {
          this.toasterService.error(
            'Please Try Again.',
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
          this.router.navigate(['home/modifysite_clientadmin']);
        }
      );

  }


}


 