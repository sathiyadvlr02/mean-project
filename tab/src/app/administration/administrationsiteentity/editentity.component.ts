import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {StateService} from './../../services/state.service';   
import { EntityService } from '../../services/ein.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-editentity',
    templateUrl: './editentity.component.html'
})
export class EditEntityComponent implements OnInit {
    entity: FormGroup;
    public checkboxvalue="N";
    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    id: any;
      stateList;
    constructor(
        private el: ElementRef,
        public toasterService: ToastrService,
        public entityservice: EntityService,
        private cookieService: CookieService,
        private router: Router,
        private _Activatedroute:ActivatedRoute,
        public StateServices : StateService,
    ) {
        this.id = this._Activatedroute.snapshot.paramMap.get("id");
    }


    get Errors() { return this.entity.controls; }


    // chkChanged(event){
    //     this.checkboxvalue = event.checked ? 'Y' : 'N';
    //     this.entity.get('federal').setValue(this.checkboxvalue);
    //   }

    
    

    ngOnInit(): void {
        this.entity = new FormGroup({
            id: new FormControl(''),
            ein: new FormControl('', [Validators.pattern(/^\d{2}-\d{8}$/),Validators.required]),
            entityname: new FormControl('', [Validators.required]),
            address1: new FormControl('', [Validators.required]),
            address2: new FormControl(''),
            address3: new FormControl(''),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zipcode: new FormControl('', [Validators.required]),
            phone1: new FormControl('', [Validators.required]),
            phone2: new FormControl(''),
            fax: new FormControl('', [Validators.required]),
            federal: new FormControl(false)
        });
        this.getstatelist();

        if(this.id){
            this.entityservice
            .getSingleEntityListDetails(this.id)
            .subscribe(
                (result) => {
                    if(result.isValid){
                        this.entity.get('id').setValue(result.data.id);
                        this.entity.get('ein').setValue(result.data.eiN_NUMBER);
                        this.entity.get('entityname').setValue(result.data.eiN_NAME);
                        this.entity.get('address1').setValue(result.data.addresS1);
                        this.entity.get('address2').setValue(result.data.addresS2);
                        this.entity.get('address3').setValue(result.data.addresS3);
                        this.entity.get('city').setValue(result.data.city);
                        this.entity.get('state').setValue(result.data.state);
                        this.entity.get('zipcode').setValue(result.data.zip);
                        this.entity.get('phone1').setValue(result.data.phone);
                        this.entity.get('phone2').setValue(result.data.phone2);
                        this.entity.get('fax').setValue(result.data.fax);
                        this.entity.get('federal').setValue(result.data.combineD_FEDERAL_STATE_FILING_PGM == 'Y' ? true : false)
                    }
                },
                (error) => {
                   // console.log('error', error)
                }
            );
        }


    }
    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    cancelButton(){
        this.cookieService.set('TabValue','1'); 
        this.router.navigate(['home/modifysite_clientadmin']);
      }

    Editentity() {
       // console.log(this.Editentity.get('federal'));
        if (this.entity.invalid) {
            for (const key of Object.keys(this.entity.controls)) {
                if (this.entity.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }
        let addFormData = this.entity.value;
        let datas = {
        ID: parseInt(addFormData.id),
        SITE_ID: parseInt(this.CookieSiteId),
        EIN_NAME: addFormData.entityname,
        EIN_NUMBER: addFormData.ein,
        ADDRESS1: addFormData.address1,
        ADDRESS2: addFormData.address2,
        ADDRESS3: addFormData.address3,
        CITY: addFormData.city,
        STATE: addFormData.state,
        ZIP: addFormData.zipcode,
        PHONE: addFormData.phone1,
        PHONE2: addFormData.phone2,
        FAX: addFormData.fax,
        FOREIGN_COUNTRY_INDICATOR: '',
        FOREIGN_COUNTRY: '',
        FOREIGN_PROVINCE: '',
        FOREIGN_POSTAL_CODE: '',
        EFILE_DATE: '2021-06-09T12:19:49.607Z',
        DATABASE_LOAD_ID: '',
        IDMS_CLIENT_ID: '',
        IDMS_CLIENT_NBR: '',
        NAME_CONTROL: '',
        COMBINED_FEDERAL_STATE_FILING_PGM: addFormData.federal ? 'Y' : 'N',
        EIN_NAME2: '',
        CONTACT_NAME: '',
        CONTACT_FIRSTNAME: '',
        CONTACT_LASTNAME: '',
        contacT_MIDDLENAME: '',
        CONTACT_SUFFIX: '',
        TOTAL_1095B_SUBMITTED: '',
        SIGNATURE: '',
        TITLE: '',
        DATE: ''
        };
        this.entityservice
            .updateEntity(datas)
            .subscribe(
                (result) => {
                this.cookieService.set('TabValue','1');  
                    if (result.isValid) {
                        this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/modifysite_clientadmin']);
                    } else {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/modifysite_clientadmin']);
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                    this.router.navigate(['home/modifysite_clientadmin']);
                }
            );

    }


}


