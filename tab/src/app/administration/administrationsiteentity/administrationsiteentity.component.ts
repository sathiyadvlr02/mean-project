import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
    FormGroup,
    FormControl,
    FormBuilder,
    Validators,
    FormArray
} from '@angular/forms';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { NavService } from '../../nav.service';
import { FileValidators } from "ngx-file-drag-drop";
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

import { SiteManagementService } from '../../services/sitemanagement.service';
import { SpaceValidator } from '../../validator/spacevalidation.validator';

import { EntityService } from '../../services/ein.service';
import { PocService } from './../../services/poc.service';
import {StateService} from './../../services/state.service';

export interface TemplateElement {
    Data_Type_1098: String;
    Postage_Paid: string;
    Site_Id: any;
    Site_Type: string;
    Supplemental_Code_1098: string;
    TCRS_Merge: string;
    Tax_Year: string
}


@Component({
    selector: 'app-administrationsiteentity',
    templateUrl: './administrationsiteentity.component.html'
})
export class AdministrationSiteEntityComponent implements OnInit {
    createSiteForm: FormGroup;

    selectedRow: any;
    EntityselectedRow: any;
    POCselectedRow: any;

    fakeImagePath: any;
    selectedIndex: any =0;

    EntitydisplayedColumns: string[] = [ 'entity_name','address1','address2','city','state','zipcode', 'action'];
    POCdisplayedColumns: string[] = ['site_id', 'name', 'title', 'email', 'city', 'action'];

    EntitydataSource: MatTableDataSource<any>;
    POCdataSource: MatTableDataSource<any>;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    private CookieTab    = this.cookieService.get('TabValue');


    displayedColumns: string[] = ["Tax_Year", "Site_Type"];
    TemplatedataSource:  TemplateElement[];

    /* @ViewChild(MatPaginator) paginator: MatPaginator; */
    @ViewChild(MatSort) sort: MatSort;

    @ViewChild('entityPaginator',{read: MatPaginator}) entityPaginator: MatPaginator;
    @ViewChild('pocPaginator',{read: MatPaginator}) pocPaginator: MatPaginator;
    /* @ViewChild('entitySort',{read: MatSort}) entitySort: MatSort;
    @ViewChild('pocSort',{read: MatSort}) pocSort: MatSort; */




    fileControl = new FormControl([], FileValidators.required);


    maskSSNList = [
        {name: '1099-MISC', value: '1099-MISC'},
        {name: '1099-R', value: '1099-R'},
        {name: '1099-INT', value: '1099-INT'},
        {name: '1099-C', value: '1099-C'},
        {name: '1099-SA', value: '1099-SA'},
        {name: '5498-SA', value: '5498-SA'},
        {name: '3921', value: '3921'},
        {name: '3922', value: '3922'},
        {name: '1098T', value: '1098T'},
        {name: '1098E', value: '1098E'},
        {name: '1095-C', value: '1095-C'},
        {name: '1095-B', value: '1095-B'},
        {name: 'T4A', value: 'T4A'},
        {name: '1042-S', value: '1042-S'},
        {name: '1099-DIV', value: '1099-DIV'},
    ];
    
    stateList;
    constructor(
        public navService: NavService,
        private _siteManagementService: SiteManagementService,
        private fb: FormBuilder,
        public router: Router,
        private _entityService: EntityService,
        public toasterService: ToastrService,
        public pocservice : PocService,
        public StateServices : StateService,
        private cookieService: CookieService,
        private el: ElementRef
        ) {
            if(this.CookieTab != ''){
                this.selectedIndex= this.CookieTab;
                this.cookieService.set('TabValue','0');
            }
           
    }


    ngOnInit(): void {
        this.createSiteForm = this.fb.group({
            folderName: new FormControl('', [Validators.required, SpaceValidator.cannotContainSpace]),
            companyName: new FormControl('', [Validators.required, SpaceValidator.cannotContainSpace]),
            companyLogo: new FormControl(''),
            createSupportRequest: new FormControl(false),
            siteType: new FormControl('P'),
            efileService: new FormControl('Y'),
            showLogo: new FormControl('N'),
            address1: new FormControl(''),
            address2: new FormControl(''),
            address3: new FormControl(''),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            contactName: new FormControl(''),
            contactPhone: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            ext: new FormControl(''),
            contactFax: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            contactEmail: new FormControl('', [Validators.required, Validators.email, SpaceValidator.cannotContainSpace]),
            initialFile: new FormControl(''),
            initialJob: new FormControl(''),
            initialReceipt: new FormControl(''),
            directories: new FormArray([]),
            maskSSN: new FormArray([]),
            enableIndividualAccess: new FormControl(''),
            yearValues: new FormControl({}),
            //siteGroup: new FormControl(''),
        });

        this.getSiteDetails();

        this.getAllEntityDetails();
        this.getAllPOC();
        this.getstatelist();

    }

    get trrowErrors() { return this.createSiteForm.controls; }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    getSiteDetails(){
        //Edit Site Details
        this._siteManagementService
        .getSiteDetailsBasedOnSiteId(this.CookieSiteId)
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.createSiteForm.get('folderName').setValue(result.data.sitE_FOLDER_NAME_PATH);
                    this.createSiteForm.get('companyName').setValue(result.data.companY_NAME);
                    this.createSiteForm.get('companyLogo').setValue(result.data.companY_LOGO_PATH);
                    this.createSiteForm.get('siteType').setValue(result.data.sitE_TYPE);
                    this.createSiteForm.get('efileService').setValue(result.data.efilE_SERVICES);
                    this.createSiteForm.get('showLogo').setValue(result.data.shoW_LOGO_IND);
                    this.createSiteForm.get('address1').setValue(result.data.addresS1);
                    this.createSiteForm.get('address2').setValue(result.data.addresS2);
                    this.createSiteForm.get('address3').setValue(result.data.addresS3);
                    this.createSiteForm.get('city').setValue(result.data.city);
                    this.createSiteForm.get('state').setValue(result.data.state);
                    this.createSiteForm.get('zip').setValue(result.data.zip);
                    this.createSiteForm.get('contactName').setValue(result.data.contacT_NAME);
                    this.createSiteForm.get('contactPhone').setValue(result.data.contacT_PHONE);
                    this.createSiteForm.get('ext').setValue(result.data.contacT_PHONE_EXTENSION);
                    this.createSiteForm.get('contactFax').setValue(result.data.contacT_FAX);
                    this.createSiteForm.get('contactEmail').setValue(result.data.contacT_EMAIL);
                    this.createSiteForm.get('initialFile').setValue(result.data.INITIAL_FILE ? result.data.INITIAL_FILE : '');
                        this.createSiteForm.get('initialJob').setValue(result.data.INITIAL_JOB ? result.data.INITIAL_JOB : '');
                        this.createSiteForm.get('initialReceipt').setValue(result.data.INITIAL_RECIPIENT ? result.data.INITIAL_RECIPIENT : '');
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );

        //Get Tax Form Details
        this._siteManagementService
        .getTaxFormDetails(this.CookieSiteId)
        .subscribe(
            (result) => {
                //console.log('tax form result', result)
                if(result.isValid){
                    let maskSSNArray: FormArray = this.createSiteForm.get('maskSSN') as FormArray;
                    let directoriesArray: FormArray = this.createSiteForm.get('directories') as FormArray;
                    if(JSON.parse(result.data)){
                        JSON.parse(result.data).map((itm) => {
                            if(itm.Mask_SSN == 'Y'){
                                maskSSNArray.push(new FormControl(itm.tax_form_type));
                                directoriesArray.push(new FormControl(itm.tax_form_type));
                            }
                        })
                    }
                    
                }
                
            },
            (error) => {
                console.log('tax form error', error)
            }
        );

        //Get Year By Site Id
        this._siteManagementService
        .getSiteYear(this.CookieSiteId)
        .subscribe(
            (result) => {
                //console.log('tax form result', result)
                if(result.isValid){
                    //let yearValuesArray: FormArray = this.createSiteForm.get('yearValues') as FormArray;
                    if(JSON.parse(result.data)){
                        let tempData = {};
                        let tempArray = [];
                        JSON.parse(result.data).map((itm) => {
                            //console.log('itm', itm)
                            //yearValuesArray.push(new FormControl(itm));
                            tempData[itm.Tax_Year] = itm;
                            tempArray.push(itm);
                        })
                        this.createSiteForm.get('yearValues').setValue(tempData);
                        this.TemplatedataSource = tempArray;
                        console.log('this.TemplatedataSource', this.TemplatedataSource)
                    }
                    //this.TemplatedataSource = yearValuesArray;
                    
                }
                
            },
            (error) => {
                console.log('tax form error', error)
            }
        );
    }

    onCheckSSN(event){
        let formArray: FormArray = this.createSiteForm.get('maskSSN') as FormArray;
        if(event.checked){
          formArray.push(new FormControl(event.source.value));
        } else {
          let i: number = 0;
          formArray.controls.forEach((ctrl: FormControl) => {
            if(ctrl.value == event.source.value) {
              formArray.removeAt(i);
              return;
            }
            i++;
          });
        }
    }

    onChangeYearForm(year, field, event){
        let siteDetailsFormData = JSON.parse(JSON.stringify(this.createSiteForm.value.yearValues));
        //siteDetailsFormData.year.field = event.value;
        siteDetailsFormData[year][field] = event.value;
        //console.log('siteDetailsFormData', siteDetailsFormData[year][field])
        this.createSiteForm.get('yearValues').setValue(siteDetailsFormData);
        //console.log('year', year)
        //console.log('field', field)
        //console.log('event', event)
    }

    onSiteSubmit(){
        console.log(this.createSiteForm.value)
        
        if (this.createSiteForm.invalid) {
            for (const key of Object.keys(this.createSiteForm.controls)) {
                if (this.createSiteForm.controls[key].invalid) {
                  const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                  invalidControl.focus();
                  break;
                }
              }
            return;
        }

        let siteDetailsFormData = this.createSiteForm.value;

        //update Tax form

        let tempUpsData = [];

        if(siteDetailsFormData.maskSSN){
            siteDetailsFormData.maskSSN.map((itmData) => {
                tempUpsData.push({
                    id: 0,
                    site_id: parseInt(this.CookieSiteId),
                    tax_form_type: itmData,
                    mask_SSN: "Y",
                    mask_EIN: "N"
                })
            })
        }
        if(siteDetailsFormData.directories){
            siteDetailsFormData.directories.map((itmData) => {
                tempUpsData.push({
                    id: 0,
                    site_id: parseInt(this.CookieSiteId),
                    tax_form_type: itmData,
                    mask_SSN: "Y",
                    mask_EIN: "N"
                })
            })
          } 

          let tempUpsDataTemp = [...new Map(tempUpsData.map(item => [item['tax_form_type'], item])).values()]
        

        this._siteManagementService
        .updateTaxForm(parseInt(this.CookieSiteId), tempUpsDataTemp)
        .subscribe(
            (result) => {
                console.log('up result', result)
            },
            (error) => {
                console.log('up error', error)
            }
        );
           // return;

        //Update Year Form Details

        let tempYearArray = [];
        if(siteDetailsFormData.yearValues){
            Object.keys(siteDetailsFormData.yearValues).map(function(key, index) {
                tempYearArray.push({
                    site_Id: siteDetailsFormData.yearValues[key]['Site_Id'],
                    tax_Year: siteDetailsFormData.yearValues[key]['Tax_Year'],
                    postage_Paid: siteDetailsFormData.yearValues[key]['Postage_Paid'],
                    site_Type: siteDetailsFormData.yearValues[key]['Site_Type'],
                    data_Type_1098: siteDetailsFormData.yearValues[key]['Data_Type_1098'],
                    supplemental_Code_1098: siteDetailsFormData.yearValues[key]['Supplemental_Code_1098'],
                    tcrS_Merge: siteDetailsFormData.yearValues[key]['tcrS_Merge'],
                });
            });
        }


        this._siteManagementService
        .updateSiteYearForm(parseInt(this.CookieSiteId), tempYearArray)
        .subscribe(
            (result) => {
                console.log('up result', result)
            },
            (error) => {
                console.log('up error', error)
            }
        );



        //Update Other Details

        let createDatas = {
            SITE_ID: this.CookieSiteId,
            COMPANY_NAME: siteDetailsFormData.companyName,
            COMPANY_LOGO_PATH: siteDetailsFormData.companyLogo,
            SHOW_LOGO_IND: siteDetailsFormData.showLogo,
            STATUS: 'ONLINE',
            SITE_FOLDER_NAME_PATH: siteDetailsFormData.folderName,
            SITE_BROADCAST_MESSAGE: '',
            ADDRESS1: siteDetailsFormData.address1,
            ADDRESS2: siteDetailsFormData.address2,
            ADDRESS3: siteDetailsFormData.address3,
            CITY: siteDetailsFormData.city,
            STATE: siteDetailsFormData.state,
            ZIP: siteDetailsFormData.zip,
            CONTACT_NAME: siteDetailsFormData.contactName,
            CONTACT_PHONE: siteDetailsFormData.contactPhone,
            CONTACT_PHONE_EXTENSION: siteDetailsFormData.ext,
            CONTACT_FAX: siteDetailsFormData.contactFax,
            CONTACT_EMAIL: siteDetailsFormData.contactEmail,
            SITE_TYPE: siteDetailsFormData.siteType,
            EFILE_SERVICES: siteDetailsFormData.efileService,
            EFILE_CONTACT_NAME: siteDetailsFormData.contactName,
            EFILE_CONTACT_PHONE: siteDetailsFormData.contactPhone,
            EFILE_CONTACT_EMAIL: siteDetailsFormData.contactEmail,
            EMPLOYER_CONSENT_TEXT: '',
            PROOF_APPROVAL_TEXT: '',
            PAY_PROOF_APPROVAL_TEXT: 0,
            ADMIN_W2EMAIL_SUBJECT: '',
            ADMIN_W2EMAIL_MESSAGE: '',
            ADMIN_PAYSTUBEMAIL_SUBJECT: '',
            ADMIN_PAYSTUBEMAIL_MESSAGE: '',
            USER_W2EMAIL_SUBJECT: '',
            USER_W2EMAIL_MESSAGE: '',
            USER_EMAIL_NOTIFICATION_MESSAGE: '',
            USER_EMAIL_PROMPT_MESSAGE: '',
            PDF_OPEN_OPTIONS: 0,
            ENCRYPT_W2: 'N',
            VIEW_PAYPERIODS: 0,
            SHOW_UPDATE_W2: 'N',
            FORGOT_PASSWORD_TO_USER_IND: '',
            FORGOT_PASSWORD_AUTO_RESET: '',
            EXECUTIVE_EXCLUDE_FROM_PRINT_MAIL: '',
            HOME_PAGE_MESSAGE: '',
            ACCESS_ID: '',
            ENABLE_MENU_CONSENT_OPTION: '',
            EFILE_PROOF_APPROVAL_TEXT: null,
            SiteIDVerificationKey: null,
            SITE_RECIPIENT_XREF: '',
            AES_KEY: null,
            AES_IV: null,
            SITE_GROUP_ID: 0,
            DATA_TYPE_1098: null,
            SUPPLEMENTAL_CODE_1098T: null,
            CALL_CENTER_1098T: 'N',
            SR_OPTION: 'N',
            login_type: null,
            INITIAL_FILE: siteDetailsFormData.initialFile.toString(),
            INITIAL_JOB: siteDetailsFormData.initialJob.toString(),
            INITIAL_RECIPIENT: siteDetailsFormData.initialReceipt.toString(),
        };

        this._siteManagementService
        .updateSite(createDatas)
        .subscribe(
            (result) => {
            if(result.isValid){
                this.toasterService.success(
                result.message,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
                );
            } else {
                this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
                );
            }
            
            },
            (error) => {
            this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
            );
            
            }
        );
    }

    getAllEntityDetails(){
        this._entityService
        .getEntityListDetails('NIL')
        .subscribe(
            (result) => {
                //console.log('entity result', result)
                if(result.isValid){
                    
                    this.EntitydataSource = new MatTableDataSource(JSON.parse(result.data));
                    this.EntitydataSource.paginator = this.entityPaginator;
                    this.EntitydataSource.sort = this.sort;
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

    searchEntityData(searchValue: any) {
        this.EntitydataSource.filter = searchValue.trim().toLowerCase();
    }

    deleteEntity(einNumber){
        Swal.fire({
            title: 'Are you sure delete?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this._entityService
                .deleteEntity(einNumber)
                .subscribe(
                    (result) => {
                        if(result.isValid){
                            this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                            this.getAllEntityDetails();
                        } else {
                            this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                        }
                        // window.location.reload();
                    },
                    (error) => {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        // window.location.reload();
                    }
                );
            }
        })
    }


    submitPocs(): void {
        //if (this.username == 'admin' && this.password == 'admin') {
        this.router.navigate(["addpocs"]);
        // } else {
        //  alert("Invalid credentials");
        // }
    }


    submitentity(): void {
        //if (this.username == 'admin' && this.password == 'admin') {
        this.router.navigate(["addentity"]);
        // } else {
        //  alert("Invalid credentials");
        // }
    }


    onValueChange(file: File[]) {
        
        if(file.length > 0){
            this.createSiteForm.get('companyLogo').setValue(file[0])
            const reader = new FileReader();
            reader.readAsDataURL(file[0]); 
            reader.onload = (_event) => { 
                this.fakeImagePath = reader.result; 
            }
        } else {
            this.createSiteForm.get('companyLogo').setValue('')
            this.fakeImagePath = ''; 
        }
    }

    //For Poc

    getAllPOC(){
        this.pocservice
        .getPOCListDetails()
        .subscribe(
            (result) => {
                if(result.isValid){
                    
                    this.POCdataSource = new MatTableDataSource(JSON.parse(result.data));
                    this.POCdataSource.paginator = this.pocPaginator;
                    this.POCdataSource.sort = this.sort;
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

   
    searchPOCData(searchvalue:any){
        this.POCdataSource.filter = searchvalue.trim().toLowerCase();
    }

    deletePOC(sitepocid){
        Swal.fire({
            title: 'Are you sure delete?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.pocservice
                .deletePOC(sitepocid)
                .subscribe(
                    (result) => {
                            if(result.isValid){
                            this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                            this.getAllPOC();
                        } else {
                            this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                        }
                        // window.location.reload();
                    },
                    (error) => {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        // window.location.reload();
                    }
                );
            }
        })
    }

}



/* Static data */


export interface POCElement {
    site_id: string;
    name: string;
    title: string;
    email: string;
    city: string;
}




const POC_ELEMENT_DATA: POCElement[] = [
    {
        site_id: "11137", name: "	Roshelle Lethby", title: "	Cost Accountant", email: "csargerson1@baidu.com", city: "Palompon"
    },
    {
        site_id: "11137", name: "Wandis Dolling", title: "Assistant Manager", email: "wdolling2@e-recht24.de", city: "Palompon"
    },
    {
        site_id: "11137", name: "Christiano Sargerson", title: "Cost Accountant", email: "csargerson1@baidu.com", city: "Montes Velhos"
    },
    {
        site_id: "11137", name: "Darrin Bedborough", title: "Programmer Analyst III", email: "dbedborough4@ifeng.com", city: "Kota Kinabalu"
    }
];








