import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import {StateService} from './../../services/state.service';  
import { PocService } from './../../services/poc.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-pocs',
    templateUrl: './addpocs.component.html'
})
export class AddPOCsComponent implements OnInit {
    pocs: FormGroup;
    private CookieUserName = this.cookieService.get('username');
    public CookieSiteId = this.CookieUserName.split('_')[0];
    public checkboxvalue;
    stateList;

    get Errors() { return this.pocs.controls; }

    constructor(
        private el: ElementRef,
        public toasterService: ToastrService,
        private router: Router,
        public Pocservice: PocService,
        private cookieService: CookieService,
        public StateServices : StateService,
    ) { }

    ngOnInit(): void {
        this.pocs = new FormGroup({
            site: new FormControl(this.CookieSiteId ,[Validators.required]),
            contactname: new FormControl('', [Validators.required]),
            title: new FormControl('', [Validators.required]),
            address1: new FormControl('', [Validators.required]),
            address2: new FormControl(''),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            phn1: new FormControl('', [Validators.required,Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            phn2: new FormControl('', [Validators.required,Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            email: new FormControl('', [Validators.required, Validators.email]),
            recipient: new FormControl(false)
        });
        this. getstatelist();
    }

    getstatelist() {
      this.StateServices.getStatelist().subscribe(
          result => {
              this.stateList= JSON.parse(result.data);
          });
  }

  cancelButton(){
    this.cookieService.set('TabValue','2'); 
    this.router.navigate(['home/modifysite_clientadmin']);
  }


    Addpoc(){
        if (this.pocs.invalid) {
            for (const key of Object.keys(this.pocs.controls)) {
              if (this.pocs.controls[key].invalid) {
                const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                invalidControl.focus();
                break;
              }
            }
            return;
          }
          let addFormData = this.pocs.value;
          let datas = {
            "site_poc_id": 0,
            "sequence_id": 0,
            "sitE_ID": parseInt(this.CookieSiteId),
            "status": "ONLINE",
            "poC_Name": addFormData.contactname,
            "title": addFormData.title,
            "address1": addFormData.address1,
            "address2": addFormData.address2,
            "city": addFormData.city,
            "state": addFormData.state,
            "zip": addFormData.zip,
            "phone1": addFormData.phn1,
            "phone2": addFormData.phn2,
            "fax": "",
            "email": addFormData.email,
            "sR_Recipient": addFormData.recipient ? 'Y' : 'N'
         };
          
          this.Pocservice
          .addPOC(datas)
          .subscribe(
            (result) => {
              if(result.isValid){
                this.cookieService.set('TabValue','2');  
                this.toasterService.success(
                  result.message,
                  '',
                  {
                      positionClass: 'toast-top-center',
                      timeOut: 3000
                  }
                );
                this.router.navigate(['home/modifysite_clientadmin']);
              } else {
                this.toasterService.error(
                  'Please Try Again.',
                  '',
                  {
                      positionClass: 'toast-top-center',
                      timeOut: 3000
                  }
                );
                this.router.navigate(['home/modifysite_clientadmin']);
              }
            },
            (error) => {
              this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
              this.router.navigate(['home/modifysite_clientadmin']);
            }
          );
    
     }

}


