import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AdministrationManageSystemUserRoutes } from './administration.routing';

import { AdministrationManageSystemUserComponent } from './administrationmanagesystemuser/administrationmanagesystemuser.component';

import { ManageAdduserComponent } from './administrationmanagesystemuser/manage-adduser.component';
import { ManageEdituserComponent } from './administrationmanagesystemuser/manage-edituser.component';

import { AdministrationSiteEntityComponent } from './administrationsiteentity/administrationsiteentity.component';
import { AddPOCsComponent } from './administrationsiteentity/addpocs.component';
import { AddEntityComponent } from './administrationsiteentity/addentity.component';
import { EditEntityComponent } from './administrationsiteentity/editentity.component';
import { EditPOCsComponent } from './administrationsiteentity/editpocs.component';

import { AdministrationViewFormsAndReportsComponent } from './administrationviewformsandreports/administrationviewformsandreports.component';
import { ApproveDialogue } from './administrationviewformsandreports/approvedialogue.component';
import { BroadcastComponent } from './broadcastmessage/broadcast.component';
import { FAQComponent } from './faqcomponents/faq.component';

import { ChangePasswordComponent } from './change-password/change-password.component';
import { AdminUserTypeComponent } from './admin-user-type/admin-user-type.component';
import { AddAdminUserTypeComponent } from './admin-user-type/admin-adduser-type.component';
import { EditAdminUserTypeComponent } from './admin-user-type/admin-edituser-type.component';
import { EmailHistoryComponent } from './email-history/email-history.component';
import { NavService } from './../layouts/full/menu-list-item/nav.service';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FormsModule ,ReactiveFormsModule ,
    FlexLayoutModule,    
    
    RouterModule.forChild(AdministrationManageSystemUserRoutes),
  ],
  declarations: [
    AdministrationManageSystemUserComponent,
    AdministrationSiteEntityComponent,
    AdministrationViewFormsAndReportsComponent,
    ApproveDialogue,
    BroadcastComponent,
    FAQComponent,ManageAdduserComponent,ManageEdituserComponent,
    ChangePasswordComponent,
    AdminUserTypeComponent,
    AddAdminUserTypeComponent,
    EditAdminUserTypeComponent,
    EmailHistoryComponent,
    AddPOCsComponent,
    AddEntityComponent,
    EditEntityComponent,
    EditPOCsComponent
],
providers:[NavService]
})
export class AdministrationModule {}
