import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-email-history',
    templateUrl: './email-history.component.html',
    styleUrls: ['./email-history.component.css']
})
export class EmailHistoryComponent implements OnInit {
    selectedRow:any;
    displayedColumns: string[] = ['first_name', 'last_name', 'login_name', 'SSN', 'Status', 'Description', 'DateSent'];
    dataSource = ELEMENT_DATA;


    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    constructor() { }

    ngOnInit(): void {
    }

}


export interface PeriodicElement {
    first_name: string;
    last_name: string;
    SSN: string;
    login_name: string;
    Status: string;
    Description: string;
    DateSent: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { first_name: '11217', last_name: '2019', login_name: 'blambert@tabservice.com', SSN: 'TSC1099 Intent to Renew', Status: 'Queued', Description: 'Queued. Thank you', DateSent: '09/18/2019' },
    { first_name: '11217', last_name: '2019', login_name: 'blambert@tabservice.com', SSN: 'TSC1099 Intent to Renew', Status: 'Queued', Description: 'Queued. Thank you', DateSent: '09/18/2019' },
    { first_name: '11217', last_name: '2019', login_name: 'blambert@tabservice.com', SSN: 'TSC1099 Intent to Renew', Status: 'Queued', Description: 'Queued. Thank you', DateSent: '09/18/2019' },
    { first_name: '11217', last_name: '2019', login_name: 'blambert@tabservice.com', SSN: 'TSC1099 Intent to Renew', Status: 'Queued', Description: 'Queued. Thank you', DateSent: '09/18/2019' },

];

