import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import Swal from 'sweetalert2';
import { ToastrComponentlessModule, ToastrService } from 'ngx-toastr';


import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { CookieService } from 'ngx-cookie-service';
export interface PeriodicElement {
  USER_ID: string;
  LOGIN_NAME: string;
  PASSWORD: string;
  FIRST_NAME: string;
  LAST_NAME: string;
  ADDRESS1: string;
  ADDRESS2: string;
  ADDRESS3: string;
  CITY: string;
  STATE: string;
  ZIP: string;
  PHONE: string;
  PHONE_EXTENSION: string;
  FAX: string;
  EMAIL: string;
  SITE_ID: string;
  USER_SSN: string;
  USER_TYPE: string;
  
}

@Component({
  selector: 'app-administrationmanagesystemuser',
  templateUrl: './administrationmanagesystemuser.component.html'
})
export class AdministrationManageSystemUserComponent implements OnInit {
  homeForm: FormGroup;
  selectedRow:any;
  displayedColumns: string[] = ['FIRST_NAME', 'LAST_NAME', 'LOGIN_NAME', "action" ];
  dataSource: MatTableDataSource<any>;
  

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  
  private CookieUserName = this.cookieService.get('username');
  public CookieSiteId = this.CookieUserName.split('_')[0];
  constructor(
    private router: Router,
    private _userService: UserService,
    public toasterService: ToastrService,
     private cookieService: CookieService
  ) {
    this.getAllUserDetails();
   }

  ngOnInit(): void {
  }

  applyFilter(filterValue) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }


  getAllUserDetails(){
    this._userService
    .getAllUserUserDetails()
    .subscribe(
        (result) => {
            if(result.isValid){
                this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                if(this.CookieSiteId != '1'){
                    console.log('work');
                    this.applyFilter(this.CookieSiteId);
                }
               
            }
        },
        (error) => {
            //console.log('error', error)
        }
    );
  }

  deleteAction(user_id){
    Swal.fire({
        title: 'Are you sure delete?',
        //text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
          this._userService
          .deleteUserDetails(user_id)
          .subscribe(
              (result) => {
                  if(result.isValid){
                      this.toasterService.success(
                      result.message,
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                      );
                      this.getAllUserDetails();
                  } else {
                      this.toasterService.error(
                      'Please Try Again.',
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                      );
                  }
                 // window.location.reload();
              },
              (error) => {
                  this.toasterService.error(
                      'Please Try Again.',
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                  );
                 // window.location.reload();
              }
          );
        }
    });
  }

  searchData(searchValue: any) {

    
      this.dataSource.filter = searchValue.trim().toLowerCase();
  

   /*  this.dataSource = this.duplicateData.filter((item) => {
           
        return item.FIRST_NAME.toLowerCase().includes(searchValue.toLowerCase()) || item.LAST_NAME.toString().includes(searchValue.toLowerCase()) ? item : '';
            
  }); */
  
}

  submit(): void {
    //if (this.username == 'admin' && this.password == 'admin') {
    this.router.navigate(["addsystemUser"]);
    // } else {
    //  alert("Invalid credentials");
    // }
  }

}


/* Static data */



/* const ELEMENT_DATA: PeriodicElement[] = [
  {
    first_name: 'Ariana1', last_name: 'Arevalo', login_name: '969100', company_name: 'General Test Company'},
  { first_name: 'Ariana2', last_name: 'Arevalo', login_name: '969100', company_name: 'General Test Company' },
  { first_name: 'Ariana3', last_name: 'Arevalo', login_name: '969100', company_name: 'General Test Company' },
  { first_name: 'Ariana4', last_name: 'Arevalo', login_name: '969100', company_name: 'General Test Company' }
   
]; */