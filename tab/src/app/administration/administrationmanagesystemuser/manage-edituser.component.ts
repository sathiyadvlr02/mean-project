import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';

import { SpaceValidator } from '../../validator/spacevalidation.validator';
import { AdminmenuService } from '../../services/adminmenu.service';
import { ConfirmedValidator } from '../../login/confirmed.validator';
import { UserService } from '../../services/user.service';
import { StateService } from '../../services/state.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-manage-edituser',
    templateUrl: './manage-edituser.component.html'
})

export class ManageEdituserComponent implements OnInit {

    createUserForm: FormGroup;

    loading: boolean;
    newpasswordhide = true;
    confirmpasswordhide = true;

    siteForDropdown: any;
    userTypeForDropdown: any;

    bar: any = '0%';
    barLabel: string = 'WEAK';  

    id: any;
    private CookieUserName = this.cookieService.get('username');
    public CookieSiteId = this.CookieUserName.split('_')[0];

    stateList;

    constructor(
        private fb: FormBuilder,
        private _adminMenuService: AdminmenuService,
        private _userService: UserService,
        public toasterService: ToastrService,
        private router: Router,
        public StateServices : StateService,
        private _Activatedroute:ActivatedRoute,
        private el: ElementRef,
        private cookieService: CookieService
    ) { 
        this.id = this._Activatedroute.snapshot.paramMap.get("id");
        this.getAllSiteDropDown();
        this.getAllUserTypeDropDown();

        console.log('id', this.id)
    }

    ngOnInit(): void {
        let TempValue = (this.CookieSiteId == '1' ? '' : this.CookieSiteId);
        this.createUserForm = this.fb.group({
            relatedSite: new FormControl(TempValue, [Validators.required]),
            userType: new FormControl('', [Validators.required]),
            loginName: new FormControl('', [Validators.required]),
            newpassword: new FormControl('', [Validators.required]),
            confirmpassword: new FormControl('', [Validators.required]),
            email: new FormControl('', [Validators.required]),
            firstName: new FormControl('', [Validators.required]),
            lastName: new FormControl('', [Validators.required]),
            address1: new FormControl(''),
            address2: new FormControl(''),
            address3: new FormControl(''),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            contactName: new FormControl(''),
            contactPhone: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            ext: new FormControl(''),
            cantactFax: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            campusCode: new FormControl(''),
            programCode: new FormControl(''),
            startTime: new FormControl(''),
            endTime: new FormControl(''),
        }, {
          validator: ConfirmedValidator('newpassword', 'confirmpassword')
        })
       this. getstatelist();
        if(this.id){
            this._userService
            .getSingleUserDetails(this.id)
            .subscribe(
                (result) => {
                    //console.log('result', result)
                    if(result.isValid){
                        console.log('result', JSON.parse(result.data))
                        this.createUserForm.get('relatedSite').setValue(JSON.parse(result.data).SITE_ID.toString());
                        this.createUserForm.get('userType').setValue(JSON.parse(result.data).USER_TYPE);
                        this.createUserForm.get('loginName').setValue(JSON.parse(result.data).LOGIN_NAME);
                        this.createUserForm.get('newpassword').setValue(JSON.parse(result.data).PASSWORD);
                        this.createUserForm.get('confirmpassword').setValue(JSON.parse(result.data).PASSWORD);
                        this.createUserForm.get('email').setValue(JSON.parse(result.data).EMAIL);
                        this.createUserForm.get('firstName').setValue(JSON.parse(result.data).FIRST_NAME);
                        this.createUserForm.get('lastName').setValue(JSON.parse(result.data).LAST_NAME);
                        this.createUserForm.get('address1').setValue(JSON.parse(result.data).ADDRESS1);
                        this.createUserForm.get('address2').setValue(JSON.parse(result.data).ADDRESS2);
                        this.createUserForm.get('address3').setValue(JSON.parse(result.data).ADDRESS3);
                        this.createUserForm.get('city').setValue(JSON.parse(result.data).CITY);
                        this.createUserForm.get('state').setValue(JSON.parse(result.data).STATE);
                        this.createUserForm.get('zip').setValue(JSON.parse(result.data).ZIP);
                        this.createUserForm.get('contactPhone').setValue(JSON.parse(result.data).PHONE);
                        this.createUserForm.get('ext').setValue(JSON.parse(result.data).PHONE_EXTENSION);
                        this.createUserForm.get('cantactFax').setValue(JSON.parse(result.data).FAX);
                    }
                },
                (error) => {
                   // console.log('error', error)
                }
            );
        }

    }

    get trrowErrors() { return this.createUserForm.controls; }

    getstatelist() {
      this.StateServices.getStatelist().subscribe(
          result => {
              this.stateList= JSON.parse(result.data);
          });
  }

    onSubmit(){
        if (this.createUserForm.invalid) {
          for (const key of Object.keys(this.createUserForm.controls)) {
            if (this.createUserForm.controls[key].invalid) {
              const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
              invalidControl.focus();
              break;
            }
          }
          return;
        }
        let createDetailsFormData = this.createUserForm.value;
        let createDatas = {
          useR_ID: parseInt(this.id),
          logiN_NAME: createDetailsFormData.loginName,
          password: createDetailsFormData.newpassword,
          firsT_NAME: createDetailsFormData.firstName,
          lasT_NAME: createDetailsFormData.lastName,
          addresS1: createDetailsFormData.address1 ? createDetailsFormData.address1 : '',
          addresS2: createDetailsFormData.address2 ? createDetailsFormData.address2 : '',
          addresS3: createDetailsFormData.address3 ? createDetailsFormData.address3 : '',
          city: createDetailsFormData.city ? createDetailsFormData.city : '',
          state: createDetailsFormData.state ? createDetailsFormData.state : '',
          zip: createDetailsFormData.zip ? createDetailsFormData.zip : '',
          phone: createDetailsFormData.contactPhone ? createDetailsFormData.contactPhone : '',
          phonE_EXTENSION: createDetailsFormData.ext ? createDetailsFormData.ext : '',
          fax: createDetailsFormData.cantactFax ? createDetailsFormData.cantactFax : '',
          email: createDetailsFormData.email,
          sitE_ID: parseInt(createDetailsFormData.relatedSite),
          useR_SSN: '',
          passworD_EXP_DATE: '2021-05-14T12:27:23.746Z',
          useR_TYPE: createDetailsFormData.userType,
          useR_CONSENT_DATE: '2021-05-14T12:27:23.746Z',
          status: '',
          useR_STATUS: '',
          databasE_LOAD_ID: '',
          lasT_ONLINE_TAX_FORM_YEAR: '',
          useR_NBR: '',
          useR_FIELD01: '',
          useR_FIELD02: '',
          useR_FIELD03: '',
          proC_TMPFLAG: '',
          sitE_GROUP_ID: '',
          campus_Code: createDetailsFormData.campusCode ? createDetailsFormData.campusCode : '',
          program_Code: createDetailsFormData.programCode ? createDetailsFormData.programCode : '',
          access_Start_Time: createDetailsFormData.startTime ? createDetailsFormData.startTime : {},
          access_Stop_Time: createDetailsFormData.endTime ? createDetailsFormData.endTime : {},
          createddate: '2021-05-14T12:27:23.746Z',
          updateddate: '2021-05-14T12:27:23.746Z',
          createdby: '',
          updatedby: ''
        }
  
        this._userService
        .editUserDetails(createDatas)
        .subscribe(
          (result) => {
            if(result.isValid){
              this.toasterService.success(
                result.message,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
              this.router.navigate(['administration/manageuser']);
            } else {
              this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
            }
          },
          (error) => {
            this.toasterService.error(
              'Please Try Again.',
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
          }
        );
    }

    getAllUserTypeDropDown(){
        this._adminMenuService
        .getAllUserTypeDetails()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.userTypeForDropdown = JSON.parse(result.data);
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

    getAllSiteDropDown(){
        this._adminMenuService
        .getAllSiteDetails()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.siteForDropdown = JSON.parse(result.data);
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

    onChangePassword(value){
        let checkPasswordStrong = this.measureStrength(value)
        this.bar = checkPasswordStrong.toString()+'%';
        this.strongerMessage(checkPasswordStrong);
    }
    
    strongerMessage(value: any){
        value = parseInt(value)
        if(value >= 80){
          this.barLabel = 'STRONG';
        } else if(value < 80 && value >= 50){
          this.barLabel = 'MEDIUM';
        } else {
          this.barLabel = 'WEAK';
        }
    }
    
    measureStrength(pass: string) {  
        let score = 0;  
        // award every unique letter until 5 repetitions  
        let letters = {};  
        for (let i = 0; i< pass.length; i++) {  
          letters[pass[i]] = (letters[pass[i]] || 0) + 1;  
          score += 5.0 / letters[pass[i]];  
        }  
        // bonus points for mixing it up  
        let variations = {  
          digits: /\d/.test(pass),  
          lower: /[a-z]/.test(pass),  
          upper: /[A-Z]/.test(pass),  
          nonWords: /\W/.test(pass),  
        };  
        let variationCount = 0;  
        for (let check in variations) {  
          variationCount += (variations[check]) ? 1 : 0;  
        }  
        score += (variationCount - 1) * 10;  
        score = score < 0 ? 0 : score;
        return Math.trunc(score);  
    }  

}