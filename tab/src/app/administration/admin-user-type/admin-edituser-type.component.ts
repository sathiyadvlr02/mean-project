import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { UserTypeService } from '../../services/usertype.service';

@Component({
    selector: 'app-edit-admin-user-type',
    templateUrl: './admin-edituser-type.component.html'
})

export class EditAdminUserTypeComponent implements OnInit {

    createUserForm: FormGroup;
    id: any;

    accessLevelList = [
        {name: 'Select Access Level', value: '0'},
        {name: 'Super Admin', value: 'SA'},
        {name: 'Client Admin', value: 'CA'},
        {name: 'Recipient', value: 'RECIPIENT'},
    ];

    constructor(
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private router: Router,
        private _userTypeService: UserTypeService,
        private _Activatedroute:ActivatedRoute,
        private el: ElementRef,
    ) { 
        this.id = this._Activatedroute.snapshot.paramMap.get("id");
    }

    ngOnInit(): void {
        this.createUserForm = this.fb.group({
            userType: new FormControl('', [Validators.required]),
            accessLevel: new FormControl('', [Validators.required]),
            userDescription: new FormControl('', [Validators.required]),
        });

        if(this.id){
            this._userTypeService
            .getSingleUserTypeDetails(this.id)
            .subscribe(
                (result) => {
                    //console.log('result', result)
                    if(result.isValid){
                        this.createUserForm.get('userType').setValue(JSON.parse(result.data).user_type);
                        this.createUserForm.get('accessLevel').setValue(JSON.parse(result.data).Access_Level);
                        this.createUserForm.get('userDescription').setValue(JSON.parse(result.data).user_description);
                    }
                },
                (error) => {
                    // console.log('error', error)
                }
            );
        }
    }

    get trrowErrors() { return this.createUserForm.controls; }

    onSubmit(){
        if (this.createUserForm.invalid) {
            for (const key of Object.keys(this.createUserForm.controls)) {
                if (this.createUserForm.controls[key].invalid) {
                  const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                  invalidControl.focus();
                  break;
                }
            }
            return;
        }
        let createDetailsFormData = this.createUserForm.value;
        let createDatas = {
            id: parseInt(this.id),
            user_type: createDetailsFormData.userType,
            user_description: createDetailsFormData.userDescription,
            access_Level: createDetailsFormData.accessLevel,
        }

        this._userTypeService
        .updateUserTypeDetails(createDatas)
        .subscribe(
          (result) => {
            if(result.isValid){
              this.toasterService.success(
                result.message,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
              this.router.navigate(['/menuadminemployeetype']);
            } else {
              this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
            }
          },
          (error) => {
            this.toasterService.error(
              'Please Try Again.',
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
          }
        );

    }

}