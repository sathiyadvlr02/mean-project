import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { UserTypeService } from '../../services/usertype.service';

@Component({
    selector: 'app-add-admin-user-type',
    templateUrl: './admin-adduser-type.component.html'
})

export class AddAdminUserTypeComponent implements OnInit {

    createUserForm: FormGroup;

    accessLevelList = [
        {name: 'Select Access Level', value: '0'},
        {name: 'Super Admin', value: 'SA'},
        {name: 'Client Admin', value: 'CA'},
        {name: 'Recipient', value: 'RECIPIENT'},
    ];

    constructor(
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private router: Router,
        private _userTypeService: UserTypeService,
        private el: ElementRef,
    ) { }

    ngOnInit(): void {
        this.createUserForm = this.fb.group({
            userType: new FormControl('', [Validators.required]),
            accessLevel: new FormControl('', [Validators.required]),
            userDescription: new FormControl('', [Validators.required]),
        });
    }

    get trrowErrors() { return this.createUserForm.controls; }

    onSubmit(){
        if (this.createUserForm.invalid) {
            for (const key of Object.keys(this.createUserForm.controls)) {
                if (this.createUserForm.controls[key].invalid) {
                  const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                  invalidControl.focus();
                  break;
                }
              }
            return;
        }
        let createDetailsFormData = this.createUserForm.value;
        let createDatas = {
            id: 0,
            user_type: createDetailsFormData.userType,
            user_description: createDetailsFormData.userDescription,
            access_Level: createDetailsFormData.accessLevel,
        }

        this._userTypeService
            .addUserTypeDetails(createDatas)
            .subscribe(
                (result) => {
                if(result.isValid){
                    this.toasterService.success(
                    result.message,
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                    );
                    this.router.navigate(['/menuadminemployeetype']);
                } else {
                    this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                    );
                    this.router.navigate(['/addadminusertype']);
                }
                },
                (error) => {
                this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
                this.router.navigate(['/addadminusertype']);
                }
            );
    }


}