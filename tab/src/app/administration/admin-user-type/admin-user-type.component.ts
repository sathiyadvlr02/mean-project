import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

import { UserTypeService } from '../../services/usertype.service';

export interface PeriodicElement {
  Id: string;
  user_type: string;
  user_description: string;
  Access_Level: string;
}

@Component({
  selector: 'app-admin-user-type',
  templateUrl: './admin-user-type.component.html',
  styleUrls: ['./admin-user-type.component.css']
})
export class AdminUserTypeComponent implements OnInit {
    selectedRow:any;
    displayedColumns: string[] = ['user_type', 'user_description', 'Access_Level', 'action'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router: Router,
    private _userTypeService: UserTypeService,
    public toasterService: ToastrService,
  ) { 
    this.getAllUserTypeDetail();
  }

  ngOnInit(): void {
  }

  searchData(searchValue: any) {
    this.dataSource.filter = searchValue.trim().toLowerCase();
  }

  deleteAction(type_id){
    Swal.fire({
        title: 'Are you sure delete?',
        //text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
          this._userTypeService
          .deleteUserTypeDetails(type_id)
          .subscribe(
              (result) => {
                  if(result.isValid){
                      this.toasterService.success(
                      result.message,
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                      );
                      this.getAllUserTypeDetail();
                  } else {
                      this.toasterService.error(
                      'Please Try Again.',
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                      );
                  }
                // window.location.reload();
              },
              (error) => {
                  this.toasterService.error(
                      'Please Try Again.',
                      '',
                      {
                          positionClass: 'toast-top-center',
                          timeOut: 3000
                      }
                  );
                // window.location.reload();
              }
          );
        }
    });
  }

  getAllUserTypeDetail(){
    this._userTypeService
    .getAllUserTypeDetails()
    .subscribe(
        (result) => {
            //console.log('result', result)
            if(result.isValid){
                
                this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            }
        },
        (error) => {
            //console.log('error', error)
        }
    );
  }

  submit(): void {
    //if (this.username == 'admin' && this.password == 'admin') {
    this.router.navigate(["addadminusertype"]);
    // } else {
    //  alert("Invalid credentials");
    // }
  }

}



