import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { BroadcastService } from '../../services/broadcast.service';

declare var require: any;
 
@Component({
	selector: 'app-broadcast',
	templateUrl: './broadcast.component.html'
	 
})
export class BroadcastComponent  {
	broadcastForm:FormGroup;

	

	constructor(
		private router: Router,
		private fb: FormBuilder,
		private _broadCastService: BroadcastService,
		private el: ElementRef,
		public toasterService: ToastrService,
	) { 
		this.getBroadCastServiceMessage();
	}

	ngOnInit() {

		this.broadcastForm = this.fb.group({
			description: new FormControl(''),
		});
	}

	get trrowErrors() { return this.broadcastForm.controls; }

	getBroadCastServiceMessage(){
		this._broadCastService
            .getBroadCastMessage()
            .subscribe(
                (result) => {
					if(result.isValid){
						this.broadcastForm.get('description').setValue(result.data);
					}
				},
				(error) => {
					// console.log('error', error)
				}
			)
	}

	updateBroadCastMessage

	onSubmit(){
		if (this.broadcastForm.invalid) {
			for (const key of Object.keys(this.broadcastForm.controls)) {
			  if (this.broadcastForm.controls[key].invalid) {
				const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
				invalidControl.focus();
				break;
			  }
			}
			return;
		}
		let broadMesageForm = this.broadcastForm.value;
		
		this._broadCastService
        .updateBroadCastMessage(broadMesageForm.description)
        .subscribe(
          (result) => {
            if(result.isValid){
              this.toasterService.success(
                result.message,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
			  this.getBroadCastServiceMessage();
            } else {
              this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
            }
          },
          (error) => {
            this.toasterService.error(
              'Please Try Again.',
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
          }
        );
	}

}
