import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ApproveDialogue } from './approvedialogue.component';

@Component({
  selector: 'app-administrationviewformsandreports',
  templateUrl: './administrationviewformsandreports.component.html'
})
export class AdministrationViewFormsAndReportsComponent implements OnInit {
  homeForm: FormGroup;
  selectedRow:any;
  displayedColumns: string[] = [ 'description', 'proof_description', 'approval', 'date', 'time'];
  dataSource = ELEMENT_DATA;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog
  ){

  }

  ngOnInit(): void {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ApproveDialogue);

    dialogRef.afterClosed().subscribe(result => {
    console.log(`Dialog result: ${result}`);
    });
  }

}


/* Static data */

export interface PeriodicElement {
  description_link: string;
  description_link_title: string;
  description: string;
  proof_description: string;
  approval: string;
  date: string;
  time: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    description_link: '#', description_link_title: '2020 1098T Print and Mail Proof - 8.5 x 11 (Multi-Page)',
    description: '11137-1098T-2020PROOF-1098TBOXRPT-ALL-20200526104936ALL.PDF', proof_description: '0',
    approval: 'Approved', date: '2020-05-26',
    time: '11:02:03'
  },
  {
    description_link: '#', description_link_title: '2020 1098T Print and Mail Proof - 8.5 x 11 (Multi-Page)',
    description: '11137-1098T-2020PROOF-1098TBOXRPT-ALL-20200526104936ALL.PDF', proof_description: '0',
    approval: 'Approved', date: '2020-05-26',
    time: '11:02:03'
  }
];
