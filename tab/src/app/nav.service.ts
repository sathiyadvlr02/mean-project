import {EventEmitter, Injectable} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class NavService {
  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    this.appDrawer.open();
  }
}







// import { EventEmitter, Injectable } from '@angular/core';
// import { Event, NavigationEnd, Router } from '@angular/router';
// import { BehaviorSubject } from 'rxjs';

// @Injectable()
// export class NavService {
//   public appDrawer: any;
//   public currentUrl = new BehaviorSubject<string>(undefined);

//   constructor(private router: Router) {


//     this.router.events.subscribe((event: Event) => {

//       /*
//             this.router.routeReuseStrategy.shouldReuseRoute = function(){return false;};
      
//         let currentUrl = this.router.url + '?';
      
//         this.router.navigateByUrl(currentUrl)
//           .then(() => {
//             this.router.navigated = false;
//             this.router.navigate([this.router.url]);
//           });
//         */

//       if (event instanceof NavigationEnd) {
//         this.currentUrl.next(event.urlAfterRedirects);

//       }


//     });


//   }

//   public closeNav() {
//     this.appDrawer.close();
//   }

//   public openNav() {
//     this.appDrawer.open();
//   }
// }
