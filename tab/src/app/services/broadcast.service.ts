import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { Console } from 'console';

@Injectable({
    providedIn: 'root'
})

export class BroadcastService {

    private baseUrlGetBroadCastMessage: string = '/api/v1/Home/GetBroadcastMessage';
    private baseUrlUpdateBroadCastMessage: string = '/api/v1/Home/UpdateBroadcastMessage';
    

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    getBroadCastMessage(){
        return this.http
        .get<any>(this.baseUrlGetBroadCastMessage + '/' + this.CookieSiteId + '?site_id=' + this.CookieSiteId)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    updateBroadCastMessage(broadcast_Message){
        console.log('broad', broadcast_Message)
        return this.http
        .post<any>(this.baseUrlUpdateBroadCastMessage + '/' + this.CookieSiteId + '?site_id=' + this.CookieSiteId,
            JSON.stringify(broadcast_Message),
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }


}