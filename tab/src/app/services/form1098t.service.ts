import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class Form1098tService {

    private baseUrlGetAllData: string = '/api/v1/Taxform/GetAll1098TForms';
    private baseUrlCreateFormData: string = '/api/v1/Taxform/Add1098T';
    private baseUrlUpdateFormData: string = '/api/v1/Taxform/Update1098T';
    private baseUrlEmailFormData: string = '/api/v1/Taxform/Email1098T';
    private baseUrlDeleteFormData: string = '/api/v1/Taxform/Delete1098T';
    private baseUrlFederalIdentification: string = '/api/v1/EIN/GetAllEINsforDropdown';
    private baseUrlGetJobNames: string = '/api/v1/Taxform/GetJobNames';
    private baseUrlGetDatabaseLoadIds: string = '/api/v1/Taxform/GetDatabaseLoadIds'; 
    private baseUrlGetAllforms: string = '/api/v1/Taxform/Get1098TForm'
    private baseUrlGetTaxformversion: string = '/api/v1/Taxform/GetTaxformVersion'
    private baseUrlGetTaxformHistory: string = '/api/v1/Taxform/GetTaxformHistory'
    private baseUrlGetCountry: string = 'api/v1/Country/GetCountry';  
    private baseUrlAddTCRS: string = '/api/v1/Taxform/AddAll1098TTransaction';

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    createTCRSFormData(form_data) {
        return this.http
            .post<any>(this.baseUrlAddTCRS,
                form_data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    taxFormHistory(data){
        let tempURL = '?';
        for (const key of Object.keys(data)) {
            if(key == 'Student_Id'){
                tempURL = tempURL + key + '=' + data[key];
            } else {
                tempURL = tempURL + '&' + key + '=' + data[key];
            }
        }

        return this.http
        .get<any>(this.baseUrlGetTaxformHistory + tempURL)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    taxFormVersion(data){
        let tempURL = '?';
        for (const key of Object.keys(data)) {
            if(key == 'Student_Id'){
                tempURL = tempURL + key + '=' + data[key];
            } else {
                tempURL = tempURL + '&' + key + '=' + data[key];
            }
        }

        return this.http
        .get<any>(this.baseUrlGetTaxformversion + tempURL)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getDatabaseLoadIds(formType, taxYear){
        return this.http
        .get<any>(this.baseUrlGetDatabaseLoadIds + '?TaxformType=' + formType + '&TaxYear=' + taxYear)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getJobNames(formType, taxYear){
        return this.http
        .get<any>(this.baseUrlGetJobNames + '?TaxformType=' + formType + '&TaxYear=' + taxYear)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    deleteFormDatas(data){
        let tempURL = '?';
        for (const key of Object.keys(data)) {
            if(key == 'Student_Id'){
                tempURL = tempURL + key + '=' + data[key];
            } else {
                tempURL = tempURL + '&' + key + '=' + data[key];
            }
        }

        return this.http
        .delete<any>(this.baseUrlDeleteFormData + tempURL)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    emailFormDatas(data){
        let tempURL = '?';
        for (const key of Object.keys(data)) {
            if(key == 'Student_Id'){
                tempURL = tempURL + key + '=' + data[key];
            } else {
                tempURL = tempURL + '&' + key + '=' + data[key];
            }
        }

        return this.http
        .get<any>(this.baseUrlEmailFormData + tempURL)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getAllFormDatas(tax_year){
        return this.http
        .get<any>(this.baseUrlGetAllData + '/' + this.CookieSiteId + '/' + tax_year)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    createFormData(form_data) {
        return this.http
            .post<any>(this.baseUrlCreateFormData,
                form_data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    updateFormData(form_data) {
        return this.http
            .post<any>(this.baseUrlUpdateFormData,
                form_data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }


    getFederalIdentification() {
        return this.http
            .get<any>(this.baseUrlFederalIdentification + '/' + this.CookieSiteId)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }


    getAllForms(studentid,sqe,taxyr){
        return this.http
        .get<any>(this.baseUrlGetAllforms  + '?Student_Id=' + studentid + '&' + 'Sequence_Id=' + sqe + '&' + 'TaxYear='+ taxyr)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getCountry(){
        return this.http
        .get<any>(this.baseUrlGetCountry)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

 

}