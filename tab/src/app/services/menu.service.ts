import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { Menu } from '../interfaces/menu';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class MenuService {
    private menuListUrl: string = '/api/v1/Menu/GetMenusforHomePage';
    public menu$: Observable<Menu[]>;
    private UserName = new BehaviorSubject<string>(this.cookieService.get('username'));

    constructor(private http: HttpClient,
        private cookieService: CookieService,
        private router: Router) {}

    getMenus(userType: String): Observable<any>{
        let cookieUserName = this.cookieService.get('username');
        let cookieUserNameArray = cookieUserName.split('_');
        let params = new HttpParams().set('username', this.UserName.getValue());

        if(cookieUserNameArray[0]=="1") userType="AAM";
        if (params.get('username') !== null) {
            // if (!this.menu$) {
                this.menu$ = this.http.get<any>(this.menuListUrl + '/' + cookieUserNameArray[0] + '/' + userType,            
            {
                headers: { Accept: 'application/json', 
                'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN') ,
                'Authorization': this.cookieService.get('XSRF-TOKEN') 

            } 
            }
             )
                .pipe(
                    shareReplay(),
                    map(
                        (result) => {
                            return result;
                        },
                        (error) => {
                            return new Observable<Error>();
                        }
                    )
                );
            //}
            
            return this.menu$;
        } else {
            this.router.navigate(['/login']);
            return new Observable<Error>();
        }
    }
}