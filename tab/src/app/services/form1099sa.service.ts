import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class Form1099saService {
  baseUri: string = 'http://localhost:3000/Home';
  fileurl: string = 'http://localhost:3000/file/upload_file';
  getuploadfileurl : string = 'http://localhost:3000/file/getallfiles';
  downloadfilesurl : string = 'http://localhost:3000/file/getfiles';

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  createForm1099(data) {
    let url = this.baseUri + '/create';
    return this.http.post(url, data).pipe(
      catchError(this.errorMgmt)
    )
  }

 getformdata(){
  let url = this.baseUri + '/getdata';
  return this.http.get(url);
  
 }

 deletedata(id){
   let url = this.baseUri + '/deleteformdata/' +id;
   return this.http.delete(url)
 }

 updatedata(data){
   let url = this.baseUri + '/updateform';
   return this.http.put(url,data).pipe(
    catchError(this.errorMgmt)
  )
 }

 getbyid(id){
   let url = this.baseUri + '/getbyid/' +id
   return this.http.get(url);
 }


  addFiles(filetype: string,description:string, fileupload: File,) {
    var formData: any = new FormData();
    formData.append("filetype", filetype);
    formData.append("fileupload", fileupload);
    formData.append("description",description);
    const header = new HttpHeaders();
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: true,
      headers: header
    };
    return this.http.post(this.fileurl,formData);
    
    // const req = new HttpRequest('POST', this.fileurl, formData, options);
    // return this.http.request(req);
  }

Getuploadfiles(){
  return this.http.get(this.getuploadfileurl);
}

downloadfiles(file){
  return this.http.get(this.downloadfilesurl+ '/' + file);
}


  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}