import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthguradServiceService } from './authgurad-service.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private Authguardservice:AuthguradServiceService,
              private router:Router){
    }

  canActivate(): boolean {
    if(!this.Authguardservice.getLoggedInStatus())
      {
        this.router.navigateByUrl("/");
      }
      return this.Authguardservice.getLoggedInStatus();
  }
  
}
