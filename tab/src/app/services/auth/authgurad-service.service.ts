import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthguradServiceService {

  constructor(private cookieService: CookieService) { }

  getLoggedInStatus(){ 
    let loginCookie = this.cookieService.get('loginStatus');

    return loginCookie == '1';
  }
}
