import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    private baseUrlGetAllUsers: string = '/api/v1/Admin/GetAllAdminUser';
    private baseUrlDeleteUser: string = '/api/v1/Admin/DeleteSystemUser';
    private baseUrlAddUser: string = '/api/v1/Admin/AddAdminUser';
    private baseUrlEditUser: string = '/api/v1/Admin/UpdateAdminUser';
    private baseUrlSingleUser: string = '/api/v1/Admin/GetAdminUserById';
    private baseUrlEmailSupportRequest: string = '/api/v1/Admin/EmailSupportRequest';
    private baseUrlSubmitRunRequest: string = '/api/v1/Admin/ProofRunRequest';
    private baseUrlFormTypes: string = '/api/v1/Admin/GetFormTypes?userType=';
   
    
    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    submitRunRequest(submit_data){
        return this.http
        .post<any>(this.baseUrlSubmitRunRequest,
            submit_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    emailSupportRequest(support_data){
        return this.http
        .post<any>(this.baseUrlEmailSupportRequest,
            support_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    getSingleUserDetails(user_id){
        return this.http
        .get<any>(this.baseUrlSingleUser + '?userId=' + user_id)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    editUserDetails(user_details){
        return this.http
        .post<any>(this.baseUrlEditUser,
            user_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    addUserDetails(user_details){
        return this.http
        .post<any>(this.baseUrlAddUser,
            user_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    deleteUserDetails(user_id){
        return this.http
            .delete<any>(this.baseUrlDeleteUser + '?userId=' + user_id)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                       // console.log('service res', result)
                        return result;
                    },
                    (error) => {
                       // console.log('service error', error)
                        return new Observable<Error>();
                    }
                )
            );
    }
    
    getAllUserUserDetails(){
        return this.http
        .get<any>(this.baseUrlGetAllUsers)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getFormTypes(user_type){
        return this.http
        .get<any>(this.baseUrlFormTypes + user_type)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

}