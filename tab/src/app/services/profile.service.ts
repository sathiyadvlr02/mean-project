import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class PreofileService {

    private getProfileListUrl: string = '/api/v1/Profile/GetUserProfile';
    private baseUrlUpdateProfile: string = '/api/v1/profile/updateprofile';
    private baseUrlBroadcastMessage: string = '/api/v1/Home/GetBroadcastMessage';

    private profileDetails$: Observable<any>;
    private UserName = new BehaviorSubject<string>(this.cookieService.get('username'));

    constructor(private http: HttpClient,
        private cookieService: CookieService) {}

    getBroadcastMessage(site_id){
        return this.http
        .get<any>(this.baseUrlBroadcastMessage + '/' + site_id)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getProfileData(){
        if (!this.profileDetails$) {
            let cookieUserName = this.cookieService.get('username');
            let cookieUserNameArray = cookieUserName.split('_');
            this.profileDetails$ = this.http
            .get<any>(this.getProfileListUrl + '/' + cookieUserNameArray[1])
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
        }
        return this.profileDetails$;
    }

    upDateProfile(UpdateProfileDetails: any){
        const formdata = new FormData();
        /* let params = new HttpParams().set('username', this.UserName.getValue()); */

        for (const key of Object.keys(UpdateProfileDetails)) {
            const value = UpdateProfileDetails[key];
            formdata.append(key, value);
        }
        return this.http
            .post<any>(this.baseUrlUpdateProfile, formdata, {
                headers: { Accept: 'multipart/form-data', 'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN') },
                /* params: params */
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }
    
}
