import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class TCRSService {

    private baseUrlGetTcrs: string = 'api/v1/TCRSForm/GetTCRSSettings?Site_id=';
    private baseUrlGetprgmCode: string = 'api/v1/TCRSForm/GetProgramCodeDropDown?Site_id=';
    private baseUrlGeFinancialCode: string = 'api/v1/TCRSForm/GetTCRSFinancialDropDownByBoxNo?Site_id='
    private baseUrlGetAcademic: string = 'api/v1/TCRSForm/GetTCRSAcademicTerms?Site_id=';
    private baseUrlAllTCRS: string = 'api/v1/Taxform/GetAll1098TTransaction?student_id=';
    private baseUrlSingleTCRS: string = 'api/v1/Taxform/Get1098TTransaction?student_id=';
    private baseUrlAddTCRS: string = 'api/v1/Taxform/Add1098TTransaction';
    private baseUrlUpdateTCRS: string = 'api/v1/Taxform/Update1098TTransaction';
    private baseUrlDeleteTCRS: string = 'api/v1/Taxform/Delete1098TTransaction?student_id=';
    private baseUrlSequenceTCRS: string = 'api/v1/Taxform/GetNext1098TTransactionSequence?Student_Id=';

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) { }

    getSequence(stu_id, tax_year){
        return this.http
        .get<any>(this.baseUrlSequenceTCRS + stu_id + '&TaxYear=' + tax_year)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    deleteTCRSFormData(stu_id, sq_id, tax_year){
        return this.http
        .delete<any>(this.baseUrlDeleteTCRS + stu_id + '&SequenceId=' + sq_id + '&Tax_Year=' + tax_year)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    updateTCRSFormData(form_data) {
        return this.http
            .post<any>(this.baseUrlUpdateTCRS,
                form_data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    createTCRSFormData(form_data) {
        return this.http
            .post<any>(this.baseUrlAddTCRS,
                form_data,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    GetSingleTCRSData(stu_id, sq_id, taxyr) {
        return this.http
            .get<any>(this.baseUrlSingleTCRS + stu_id + '&SequenceId=' + sq_id + '&' + 'Tax_Year=' + taxyr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    GetAllTCRSData(stu_id, taxyr) {
        return this.http
            .get<any>(this.baseUrlAllTCRS + stu_id + '&' + 'Tax_Year=' + taxyr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    GetTCRSSettings(taxyr) {
        return this.http
            .get<any>(this.baseUrlGetTcrs + this.CookieSiteId + '&' + 'Tax_Year=' + taxyr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }
    GetProgramCode(taxyr) {
        return this.http
            .get<any>(this.baseUrlGetprgmCode + this.CookieSiteId + '&' + 'Tax_Year=' + taxyr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    GetFinancialCode(taxyr,box) {
        return this.http
            .get<any>(this.baseUrlGeFinancialCode + this.CookieSiteId + '&' + 'Tax_Year=' + taxyr + '&' + 'boxNo='+ box)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    GetAcademic(taxyr) {
        return this.http
            .get<any>(this.baseUrlGetAcademic + this.CookieSiteId + '&' + 'Tax_Year=' + taxyr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }
}
