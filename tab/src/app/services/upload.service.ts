import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { ObservableStore } from '@codewithdan/observable-store';

@Injectable({
    providedIn: 'root'
})

export class UploadService {
    private basegetuploadfiledetails: string = '/api/v1/UploadFile/GetUploadFiles';
    private basegetsinglefiledetails: string = '/api/v1/UploadFile/GetUploadedFile';
    private baseURLdownloadfile: string = '/api/v1/UploadFile/DownloadFile';
    private baseURLuploadfile: string = '/api/v1/UploadFile/UploadFile';

    private gettaxyear: string ='/api/v1/Site/GetSiteYearBySiteId?site_Id=';
    



    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    private Cokkieprofilename = this.CookieUserName.split('_')[1];


    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) { }

    getFiles(tax_yr) {
        return this.http
            .get<any>(this.basegetuploadfiledetails + '/' + this.CookieSiteId + '/' + tax_yr)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    getSingleFileDetails(fileid) {
        return this.http
            .get<any>(this.basegetsinglefiledetails + '/' + fileid)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    downloadfiles(fileid) {
        return this.http
            .get<any>(this.baseURLdownloadfile + '/' + fileid)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        console.log(result);
                        //return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    uploadfiles(uploadDatas: any) {
        const formdata = new FormData();

        for (const key of Object.keys(uploadDatas)) {
            const value = uploadDatas[key];
            formdata.append(key, value);
        }

        return this.http
            .post<any>(this.baseURLuploadfile, formdata, {
                headers: { Accept: 'multipart/form-data', 'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN') },
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }



    getTaxyear() {
        return this.http
            .get<any>(this.gettaxyear + this.CookieSiteId)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    getProfilename() {
        return this.http
            .get<any>('/api/v1/Profile/GetUserProfile' + '/' + this.Cokkieprofilename)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

}