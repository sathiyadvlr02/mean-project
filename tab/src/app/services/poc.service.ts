import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class PocService {

    private baseUrlGetAllPOC: string = '/api/v1/SitePOC/GetAllSitePOCs';
    private baseUrlDeletePOC: string = '/api/v1/SitePOC/DeleteSitePOC';
    private baseUrlAddPOC: string = '/api/v1/SitePOC/AddSitePOC';
    private baseUrlPOCEdit: string = '​/api/v1/SitePOC/UpdateSitePOC';
    private baseUrlSinglePOC: string = '/api/v1/SitePOC/GetSitePOCById';
   
    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];


    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    getPOCListDetails(){
        return this.http
        .get<any>(this.baseUrlGetAllPOC + '/' + this.CookieSiteId)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getSinglePOCDetails(sitepocid){
        return this.http
        .get<any>(this.baseUrlSinglePOC + '/' + this.CookieSiteId + '?site_POC_id=' + sitepocid)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    addPOC(poc_details){
        return this.http
        .post<any>(this.baseUrlAddPOC,
            poc_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }
 
    updatePOC(poc_details){
        return this.http
        .post<any>('/api/v1/SitePOC/UpdateSitePOC',
            poc_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    deletePOC(sitepocid){
        return this.http
            .delete<any>(this.baseUrlDeletePOC + '/' + sitepocid)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }
}