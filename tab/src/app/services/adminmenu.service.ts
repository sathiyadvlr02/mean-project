import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class AdminmenuService {

    private baseUrlGetAllMenu: string = '/api/v1/Menu/GetAllMenus';
    private baseUrlAllSiteDetails: string = '/api/v1/Site/GetAllSitesforDropdown';
    private baseUrlAllUserTypeDetails: string = '/api/v1/UserType/GetAllUserTypesforDropdown';
    private baseUrlCustomConfigMenu: string = '/api/v1/Menu/GetMenusforCustomConfiguration';
    private baseUrlDefaultConfigMenu: string = '/api/v1/Menu/GetMenusforDefaultConfiguration';
    private baseUrlUpdateCustomConfigMenu: string = '/api/v1/Menu/UpdateCustomMenuConfiguration';
    private baseUrlUpdateDefaultConfigMenu: string = '/api/v1/Menu/UpdateDefaultMenuConfiguration';
    private baseUrlUpdateMenuItem: string = '/api/v1/Menu/UpdateMenuItem';
    
    constructor(private http: HttpClient,
        private cookieService: CookieService,
        ) {}

    updateCustomConfig(site_id, user_type, menu_data){
        return this.http
        .post<any>(this.baseUrlUpdateCustomConfigMenu + '/' + site_id + '/' + user_type,
            menu_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    updateMenuItem(menu_data){
        return this.http
        .post<any>(this.baseUrlUpdateMenuItem,
            menu_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    updateDefaultConfig(user_type, menu_data){
        return this.http
        .post<any>(this.baseUrlUpdateDefaultConfigMenu + '/' + user_type,
            menu_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    getCustomConfigMenu(site_id, user_type){
        return this.http
        .get<any>(this.baseUrlCustomConfigMenu + '/' + site_id + '/' + user_type)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getDefaultConfigMenu(user_type){
        return this.http
        .get<any>(this.baseUrlDefaultConfigMenu + '/' + user_type)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getAllUserTypeDetails(){
        return this.http
        .get<any>(this.baseUrlAllUserTypeDetails)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getAllSiteDetails(){
        return this.http
        .get<any>(this.baseUrlAllSiteDetails)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getAllMenus(){
        return this.http
        .get<any>(this.baseUrlGetAllMenu)
        .pipe(
            shareReplay(),
            map(                
                (result) => {
                    console.log('result', result);
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

}