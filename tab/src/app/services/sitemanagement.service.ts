import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { ObservableStore } from '@codewithdan/observable-store';

@Injectable({
    providedIn: 'root'
})

export class SiteManagementService {

    private baseUrlCreateSite: string = '/api/v1/Site/AddSite';
    private baseUrlUpdateSite: string = '/api/v1/Site/UpdateSite';
    private baseUrlDeleteSite: string = '/api/v1/Site/DeleteSite';
    private baseUrlAllSite: string = '/api/v1/Site/GetAllSites';
    private baseUrlSingleSite: string = '/api/v1/Site/GetSiteById';
    private baseUrlGetTaxForm: string = '/api/v1/Site/GetSiteToTaxforms';
    private baseUrlUpdateTaxForm: string = '/api/v1/Site/UpdateSiteToTaxforms';
    private baseUrlGetSiteYear: string = '/api/v1/Site/GetSiteYearBySiteId';
    private baseUrlUpdateSiteYear: string = '/api/v1/Site/UpdateSiteYears';
    private baseUrlGetSiteYearForm: string = '/api/v1/Site/GetSiteYear';
    
    private siteDetails$: Observable<any>;
    private SingleSiteDetails$: Observable<any>;

    constructor(private http: HttpClient,
        private cookieService: CookieService) {}

    getSiteYearForForm(site_id, tax_year){
        return this.http
        .get<any>(this.baseUrlGetSiteYearForm + '?site_Id=' + site_id + '&Tax_Year=' + tax_year)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    updateSiteYearForm(site_id, update_data){
        return this.http
        .put<any>(this.baseUrlUpdateSiteYear,
        update_data,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .pipe(
            map((result) => {
                return result;
            })
        );
    }

    getSiteYear(site_id){
        return this.http
        .get<any>(this.baseUrlGetSiteYear + '?site_Id=' + site_id)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    updateTaxForm(site_id, update_data){
        return this.http
        .put<any>(this.baseUrlUpdateTaxForm + '?site_id=' + site_id,
        update_data,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    getTaxFormDetails(site_id){
        return this.http
        .get<any>(this.baseUrlGetTaxForm + '?site_Id=' + site_id)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    deleteSiteDetails(siteId){
        return this.http
            .delete<any>(this.baseUrlDeleteSite + '/' + siteId)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                       // console.log('service res', result)
                        return result;
                    },
                    (error) => {
                       // console.log('service error', error)
                        return new Observable<Error>();
                    }
                )
            );
    }

    getSiteDetailsBasedOnSiteId(siteId){
        return this.http.get<any>(this.baseUrlSingleSite + '/' + siteId)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    //console.log('service res', result)
                    return result;
                },
                (error) => {
                    //console.log('service error', error)
                    //return error;
                    return new Observable<Error>();
                }
            )
        );
    }

    allSiteDetails(){
        //if (!this.siteDetails$) {
            this.siteDetails$ = this.http
            .get<any>(this.baseUrlAllSite)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                       // console.log('result', result)
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
       // }
        return this.siteDetails$;
    }

    createSite(createDatas: any){
        const formdata = new FormData();
       // let params = new HttpParams().set('username', this.UserName.getValue());

        for (const key of Object.keys(createDatas)) {
            const value = createDatas[key];
            formdata.append(key, value);
        }

        return this.http
            .post<any>(this.baseUrlCreateSite, formdata, {
                headers: { Accept: 'multipart/form-data', 'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN') },
               // params: params
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );

        //console.log('create site service works')
    }

    updateSite(createDatas: any){
        const formdata = new FormData();
       // let params = new HttpParams().set('username', this.UserName.getValue());

        for (const key of Object.keys(createDatas)) {
            const value = createDatas[key];
            formdata.append(key, value);
        }

        return this.http
            .post<any>(this.baseUrlUpdateSite, formdata, {
                headers: { Accept: 'multipart/form-data', 'X-XSRF-TOKEN': this.cookieService.get('XSRF-TOKEN') },
               // params: params
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );

        //console.log('create site service works')
    }

}