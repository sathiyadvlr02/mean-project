import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class TemplateService {

    private baseUrlGetTemplateList: string = '/api/v1/TemplateFile/GetTemplateFiles';
    private baseUrlGetUserGuideList: string = '/api/v1/TemplateFile/GetManualFiles';

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    getTemplateList(taxYear){
        return this.http
        .get<any>(this.baseUrlGetTemplateList + '/' + taxYear)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getUserGuide(taxYear){
        return this.http
        .get<any>(this.baseUrlGetUserGuideList + '/' + taxYear)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }


}