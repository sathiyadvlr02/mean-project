import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class StudentService {

    private baseUrlGetAllStudent: string = '/api/v1/Student/GetAllStudents';
    private baseUrlDeleteStudent: string = '/api/v1/Student/DeleteStudent';
    private baseUrlAddStudent: string = '/api/v1/Student/AddStudent';
    private baseUrlUpdateStudent: string = '/api/v1/Student/UpdateStudent';
    private baseUrlGetStudentbyID: string = '/api/v1/Student/GetStudent';
     



    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];


    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) { }

    getStudentDetails() {
        return this.http
            .get<any>(this.baseUrlGetAllStudent + '/' + this.CookieSiteId)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    getStudentbyID(studentid) {
        return this.http
            .get<any>(this.baseUrlGetStudentbyID + '/' + studentid)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    deleteStudent(studentid) {
        return this.http
            .delete<any>(this.baseUrlDeleteStudent + '/' + studentid)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }

    addStudent(stu_details) {
        return this.http
            .post<any>(this.baseUrlAddStudent,
                stu_details,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    updateStudent(stu_details) {
        return this.http
            .put<any>(this.baseUrlUpdateStudent,
                stu_details,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

}