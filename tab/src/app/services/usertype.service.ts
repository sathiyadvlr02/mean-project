import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class UserTypeService {
    private baseUrlGetAllUsersType: string = '/api/v1/UserType/GetAllUserTypes';
    private baseUrlAddUsersType: string = '/api/v1/UserType/AddUserType';
    private baseUrlGetSingleUsersType: string = '/api/v1/UserType/GetUserTypeById';
    private baseUrlUpdateUsersType: string = '/api/v1/UserType/UpdateUserType';
    private baseUrlDeleteUsersType: string = '/api/v1/UserType/DeleteUserTypeById';
    
    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    deleteUserTypeDetails(type_id){
        return this.http
            .delete<any>(this.baseUrlDeleteUsersType + '?userTypeId=' + type_id)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                       // console.log('service res', result)
                        return result;
                    },
                    (error) => {
                       // console.log('service error', error)
                        return new Observable<Error>();
                    }
                )
            );
    }

    updateUserTypeDetails(type_details){
        return this.http
        .post<any>(this.baseUrlUpdateUsersType,
            type_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    getSingleUserTypeDetails(type_id){
        return this.http
        .get<any>(this.baseUrlGetSingleUsersType + '?userTypeId=' + type_id)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    addUserTypeDetails(user_type_details){
        return this.http
        .post<any>(this.baseUrlAddUsersType,
            user_type_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    getAllUserTypeDetails(){
        return this.http
        .get<any>(this.baseUrlGetAllUsersType)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }
}