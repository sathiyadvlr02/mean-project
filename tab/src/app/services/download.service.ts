import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class DownloadService {

    private baseUrlGetDownloadList: string = '/api/v1/DownloadFile/GetFiles';

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    getDownloadList(){
        return this.http
        .get<any>(this.baseUrlGetDownloadList + '/' + this.CookieSiteId)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

}