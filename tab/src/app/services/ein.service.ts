import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})

export class EntityService {

    private baseUrlGetAllEntity: string = '/api/v1/EIN/GetAllEINsBySiteId';
    private baseUrlDeleteEntity: string = '/api/v1/EIN/DeleteEIN';
    private baseUrlAddEntity: string = '/api/v1/EIN/AddEIN';
    private baseUrlEditEntity: string = '/api/v1/EIN/UpdateEIN';
    private baseUrlSingleEntity: string = '/api/v1/EIN/GetEINByEINNo';
   

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];


    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

    getEntityListDetails(EinNumber){
        return this.http
        .get<any>(this.baseUrlGetAllEntity + '/' + this.CookieSiteId + '/' + EinNumber)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getSingleEntityListDetails(EinNumber){
        return this.http
        .get<any>(this.baseUrlSingleEntity + '/' + this.CookieSiteId + '/' + EinNumber)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    addEntity(entity_details){
        return this.http
        .post<any>(this.baseUrlAddEntity,
            entity_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }

    updateEntity(entity_details){
        return this.http
        .post<any>(this.baseUrlEditEntity,
            entity_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
            );
    }
  

    deleteEntity(EinNumber){
        return this.http
            .delete<any>(this.baseUrlDeleteEntity + '/' + this.CookieSiteId + '/' + EinNumber)
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    },
                    (error) => {
                        return new Observable<Error>();
                    }
                )
            );
    }
}