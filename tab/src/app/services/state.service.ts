import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root'
})

export class StateService {

    
    private baseUrlGetStates: string = 'api/v1/Country/GetStatesDropDown';
    private baseUrlGetCountry: string = 'api/v1/Country/GetCountry';  
    

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    constructor(private http: HttpClient,
        private cookieService: CookieService,
    ) {}

 
  
 getStatelist(){
        return this.http
        .get<any>(this.baseUrlGetStates)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

    getCountry(){
        return this.http
        .get<any>(this.baseUrlGetCountry)
        .pipe(
            shareReplay(),
            map(
                (result) => {
                    return result;
                },
                (error) => {
                    return new Observable<Error>();
                }
            )
        );
    }

}