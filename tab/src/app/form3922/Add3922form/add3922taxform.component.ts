import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add3922taxform',
  templateUrl: './add3922taxform.component.html',
})
export class Add3922taxformComponent implements OnInit {
  Form3922add: FormGroup;

  constructor(
    public router: Router,
    private location: Location,
  ) { }

  ngOnInit(): void {
    this.Form3922add = new FormGroup({
      Transaction_Id: new FormControl('', Validators.required),
      box1datepicker: new FormControl('', Validators.required),
      box2datepicker: new FormControl('', Validators.required),
      Box3: new FormControl('', Validators.required),
      Box4: new FormControl('', Validators.required),
      Box5: new FormControl('', Validators.required),
      Box6: new FormControl('', Validators.required),
      box7datepicker: new FormControl('', Validators.required),
      box8datepicker: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required),


    });

  }
  get trrowErrors() { return this.Form3922add.controls; }

  Backto3922(): void {
    this.location.back();
    this.router.navigate(["create3922"]);
  }

  AddTransaction() {
    console.log(this.Form3922add.value);
  }


}
