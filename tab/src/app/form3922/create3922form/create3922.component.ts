import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Add3922taxformComponent } from '../Add3922form/add3922taxform.component';


@Component({
    selector: 'app-create3922',
    templateUrl: './create3922.component.html',
    styleUrls: ['./create3922.component.scss'],

})
export class Create3922Component implements OnInit {

    createAppForm: FormGroup;
    stateList;
    country;
    taxyear;

    selectedRow: any;
    displayedColumns: string[] = ['transID', 'box1', 'box2', 'box3', 'box4', 'box5', 'box6', 'box7', 'box8', 'action'];
    dataSource = ELEMENT_DATA;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices: StateService,
    ) { }

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            corporationName: new FormControl('', [Validators.required]),
            corporationAddress1: new FormControl('', [Validators.required]),
            corporationAddress2: new FormControl(''),
            corporationAddress3: new FormControl(''),
            corporationCity: new FormControl('', [Validators.required]),
            corporationState: new FormControl('', [Validators.required]),
            corporationZip: new FormControl('', [Validators.required]),
            corporationTelephoneNo: new FormControl('', [Validators.required]),
            corporationTelephoneNo1: new FormControl(''),
            corporationFederalIdentificationNumber: new FormControl('', [Validators.required]),
            employeeName: new FormControl('', [Validators.required]),
            employeeAddress1: new FormControl('', [Validators.required]),
            employeeAddress2: new FormControl(''),
            employeeAddress3: new FormControl(''),
            employeeCity: new FormControl('', [Validators.required]),
            employeeState: new FormControl('', [Validators.required]),
            employeeZip: new FormControl('', [Validators.required]),
            employeeIdentificationNumber: new FormControl('', [Validators.required]),
        });

        this.taxyear = 2005;
        this.getstatelist();
        this.getCountry();
    }

    get trrowErrors() { return this.createAppForm.controls; }

    onSubmit() {

    }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
    }

    openInsertDialog() {
        const dialogRef = this.dialog.open(Add3922taxformComponent)

        dialogRef.afterClosed().subscribe(result => {
            console.log(result)
        })

    }


}


/* Static data */

export interface PeriodicElement {
    transID: string;
    box1: string;
    box2: string;
    box3: string;
    box4: string;
    box5: string;
    box6: string;
    box7: string;
    box8: string;

}

const ELEMENT_DATA: PeriodicElement[] = [

    { transID: '654', box1: '10/01/2016', box2: '12/31/2016', box3: '26.42', box4: '34.82', box5: '33.08', box6: '9.00', box7: '12/31/2016', box8: '25.00' },
    { transID: '654', box1: '10/01/2016', box2: '12/31/2016', box3: '26.42', box4: '34.82', box5: '33.08', box6: '9.00', box7: '12/31/2016', box8: '25.00' },
];
