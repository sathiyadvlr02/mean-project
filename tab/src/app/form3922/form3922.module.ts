import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form3922Routes } from './form3922.routing';
import { ChartistModule } from 'ng-chartist';
import { Create3922Component } from './create3922form/create3922.component'; 
import { Update3922Component } from './update3922form/update3922.component'; 
import { History3922Component } from './history3922form/history3922.component'; 
import { FormEmail3922 } from './email3922form/email3922.component'; 
import { SetPassword } from './email3922form/setpassword.component';

import { Add3922taxformComponent } from './Add3922form/add3922taxform.component'; 



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    FormsModule ,
    ReactiveFormsModule,
    ChartistModule,
    RouterModule.forChild(Form3922Routes)
  ],
  declarations: [Create3922Component,Update3922Component ,History3922Component,Add3922taxformComponent,FormEmail3922,SetPassword]
})
export class Form3922Module {}
