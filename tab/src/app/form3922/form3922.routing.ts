import { Routes } from '@angular/router';
 

import { Create3922Component } from './create3922form/create3922.component'; 
import { Update3922Component } from './update3922form/update3922.component'; 
import { History3922Component } from './history3922form/history3922.component'; 
import { FormEmail3922 } from './email3922form/email3922.component'
import { Add3922taxformComponent } from './Add3922form/add3922taxform.component'; 


export const Form3922Routes: Routes = [
    {
        path: 'add3922',
        component: Add3922taxformComponent  
      },
  {
    path: 'create3922',
    component: Create3922Component  
  },
  {
    path: 'form3922create',
    component: Create3922Component  
  },
  {
    path: 'update3922',
    component: Update3922Component 
  },
  {
    path: 'history3922',
    component: History3922Component 
  },
  {
    path: 'email3922',
    component: FormEmail3922 
  },
];
