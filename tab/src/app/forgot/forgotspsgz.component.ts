import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup,FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { AccountService } from '../services/account.service';
import { ToastrService } from 'ngx-toastr';

@Component(
  {
    selector: 'app-forgotspsgz',
    templateUrl: './forgotspsgz.component.html',
    styleUrls:['./forgotspsgz.component.scss'],
    encapsulation: ViewEncapsulation.None
  })

export class ForgotSPSGZComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean;
  Site_id: FormControl;
  Email: FormControl;

  constructor(private acct: AccountService, 
    private fb: FormBuilder, 
    private toasterService: ToastrService,
    private router: Router) {}


  ngOnInit() {
    this.loginForm = new FormGroup({
        Site_id: new FormControl('',Validators.required),
        username: new FormControl('',Validators.required),               
      });
  }

  onSubmit() {
    let userInfo = this.loginForm.value;
    this.acct.sendForgotPasswordEmail(userInfo.Site_id, userInfo.username, 'SPSGZ').subscribe(
        (result) => {
            if (result && result.message == 'Success') {
                /* $('#forgotPassCard').html('');
                $('#forgotPassCard').append(
                    "<div class='alert alert-success show'>" +
                        '<strong>Success!</strong> Please check your email for password reset instructions.' +
                        '</div>'
                ); */
                this.toasterService.success(
                  'Please check your email and chgange the password',
                  '',
                  {
                      positionClass: 'toast-top-center',
                      timeOut: 3000
                  }
                );
                this.router.navigate(['/loginspsgz']);
            }
        },
        (error) => {
            this.toasterService.error('An error occured while processing this request.', '', { positionClass: 'toast-top-right' });
        }
    );
}

}
