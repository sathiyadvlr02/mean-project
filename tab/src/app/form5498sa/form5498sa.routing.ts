import { Routes } from '@angular/router';

import { Create5498SAComponent } from './create5498saform/create5498sa.component';
import { Update5498SAComponent } from './update5498saform/update5498sa.component';
import { History5498SAComponent } from './history5498saform/history5498sa.component';
import { FormEmail5498SAComponent } from './email5498sa/email5498sa.component';

export const Form5498SARoutes: Routes = [

    {
        path: 'create5498sa',
        component: Create5498SAComponent
    },
    {
        path: 'form5498sacreate',
        component: Create5498SAComponent
    },
    {
        path: 'update5498sa',
        component: Update5498SAComponent
    },

    {
        path: 'history5498sa',
        component: History5498SAComponent
    },
    {
        path: 'email5498sa',
        component: FormEmail5498SAComponent 
      },
    


];
