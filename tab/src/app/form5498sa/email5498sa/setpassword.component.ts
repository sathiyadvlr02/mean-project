import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'form-dialogue-5498SA-email',
    templateUrl: './setpassword.component.html',
    
})

export class SetPassword implements OnInit {

    password = '';
    defaultPassword = false;

    constructor(){
    }

    ngOnInit(): void {

    }

    onChangeDefault(){
        this.password = '';
        this.defaultPassword = !this.defaultPassword;
    }

    onChangePassword(event){
        this.password = event.target.value
    }
    
}