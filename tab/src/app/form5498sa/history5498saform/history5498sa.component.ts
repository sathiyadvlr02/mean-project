import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, } from '@angular/forms';
import { Router } from '@angular/router';
import { StateService } from './../../services/state.service';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
    selector: 'app-history5498sa',
    templateUrl: './history5498sa.component.html',
    styleUrls: ['./history5498sa.component.scss'],

})
export class History5498SAComponent implements OnInit {
    homeForm: FormGroup;
    stateList;
    taxyear;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        public StateServices: StateService,
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.homeForm = this.fb.group({
            filerName: new FormControl('', [Validators.required]),
            filerAddress1: new FormControl('', [Validators.required]),
            filerAddress2: new FormControl(''),
            filerAddress3: new FormControl(''),
            filerCity: new FormControl('', [Validators.required]),
            filerState: new FormControl('', [Validators.required]),
            filerZip: new FormControl('', [Validators.required]),
            filerTelephoneNo1: new FormControl('', [Validators.required]),
            filerTelephoneNo2: new FormControl(''),
            filerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            RecipientIdentificationNumber: new FormControl('', [Validators.required]),
            RecipientName: new FormControl('', [Validators.required]),
            RecipientName1: new FormControl('', [Validators.required]),
            RecipientAddress1: new FormControl('', [Validators.required]),
            RecipientAddress2: new FormControl(''),
            RecipientAddress3: new FormControl(''),
            RecipientCity: new FormControl('', [Validators.required]),
            RecipientState: new FormControl('', [Validators.required]),
            RecipientZip: new FormControl('', [Validators.required]),
            Account: new FormControl('', [Validators.required]),
        });

        this.taxyear = 2005;
        this.getstatelist();
    }

    get trrowErrors() { return this.homeForm.controls; }


    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }


}

