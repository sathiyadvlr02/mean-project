import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form5498SARoutes } from './form5498sa.routing';
import { ChartistModule } from 'ng-chartist';

import { Create5498SAComponent } from './create5498saform/create5498sa.component'; 
import { Update5498SAComponent } from './update5498saform/update5498sa.component'; 
import { History5498SAComponent } from './history5498saform/history5498sa.component'; 
import { FormEmail5498SAComponent} from './email5498sa/email5498sa.component';
import { SetPassword } from './email5498sa/setpassword.component';


@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule ,ReactiveFormsModule,
    RouterModule.forChild(Form5498SARoutes)
  ],

  declarations: [Create5498SAComponent,Update5498SAComponent,History5498SAComponent, FormEmail5498SAComponent,
    SetPassword]

})
export class Form5498SAModule {}
