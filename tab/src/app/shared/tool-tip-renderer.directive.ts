import {
    Directive,
    Input,
    TemplateRef,
    ElementRef,
    OnInit,
    HostListener,
    ComponentRef,
    OnDestroy,
    Renderer2
  } from "@angular/core";
  import {
    Overlay,
    OverlayPositionBuilder,
    OverlayRef
  } from "@angular/cdk/overlay";
  import { Observable, Subject } from "rxjs";
  import { take, takeUntil } from "rxjs/operators";
  import { CustomToolTipComponent } from "./custom-tool-tip/custom-tool-tip.component";
  import { ComponentPortal } from "@angular/cdk/portal";
  
  @Directive({
    selector: "[customToolTip]"
  })
  export class ToolTipRendererDirective {
    /**
     * This will be used to show tooltip or not
     * This can be used to show the tooltip conditionally
     */
    @Input() showToolTip: boolean = true;
  
    //If this is specified then specified text will be showin in the tooltip
    @Input(`customToolTip`) text: string;
  
    //If this is specified then specified template will be rendered in the tooltip
    @Input() contentTemplate: TemplateRef<any>;
  
    private _overlayRef: OverlayRef;
    private _tooltipInstance;
    private _mouseInTooltip: boolean = false;
    private _hasListeners: boolean = false;
  
    constructor(
      private _overlay: Overlay,
      private _overlayPositionBuilder: OverlayPositionBuilder,
      private _elementRef: ElementRef,
      private _r2: Renderer2
    ) {}
  
    /**
     * Init life cycle event handler
     */
    ngOnInit() {
      if (!this.showToolTip) {
        return;
      }
  
      const positionStrategy = this._overlayPositionBuilder
        .flexibleConnectedTo(this._elementRef)
        .withPositions([
          {
            originX: "center",
            originY: "bottom",
            overlayX: "center",
            overlayY: "top",
            offsetY: -10
          }
        ]);
  
      this._overlayRef = this._overlay.create({ positionStrategy });
    }
  
    /**
     * This method will be called whenever mouse enters in the Host element
     * i.e. where this directive is applied
     * This method will show the tooltip by instantiating the McToolTipComponent and attaching to the overlay
     */
    @HostListener("mouseenter")
    show(e) {
      if (this._overlayRef && !this._overlayRef.hasAttached()) {
        //set tooltip instance
        this._tooltipInstance = this._overlayRef.attach(
          new ComponentPortal(CustomToolTipComponent)
        ).instance;
  
        //set CustomToolTipComponenet content/inputs
        this._tooltipInstance.text = this.text;
        this._tooltipInstance.contentTemplate = this.contentTemplate;
  
        //render tooltip
        this._tooltipInstance!.show(0);
  
        //sub to detach after hide anitmation is complete
        this._tooltipInstance
          .afterHidden()
          .pipe(take(1))
          .subscribe(() => {
            this._overlayRef.detach();
          });
        if (!this._hasListeners) {
          this._hasListeners = true;
          //attach mouseleave listener to detach when mouseleave on tooltip
          this._r2.listen(this._overlayRef.overlayElement, "mouseleave", () => {
            //call hide function in this directive
            this._mouseInTooltip = false;
            this.hide();
          });
  
          this._r2.listen(this._overlayRef.overlayElement, "mouseenter", () => {
            //call hide function in this directive
            this._mouseInTooltip = true;
          });
        }
      }
    }
    /**
     * This method will be called when mouse goes out of the host element
     * i.e. where this directive is applied
     * This method will close the tooltip by detaching the overlay from the view
     */
    @HostListener("mouseleave")
    hide(buttonClicked = null) {
      if(buttonClicked)
        this._mouseInTooltip = false;
      setTimeout(() => {
        if (!this._mouseInTooltip) this._tooltipInstance!._onHide.next();
      }, 20);
    }
  }
  