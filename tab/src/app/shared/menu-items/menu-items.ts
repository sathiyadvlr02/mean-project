import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuService } from '../../services/menu.service';

export interface Menu {
    Menu_Comments: string;
    Menu_Group: string;
    Menu_Group_Id: string;
    MenuItems: [{
        menu_id: Number,
        menu_description: String,
        menu_function: String,
        menu_comments: String,
        side_menu: Number,
        menu_group_id: Number,
        access_level: String
    }]
}
  
@Injectable()
export class MenuItems {
    private apiMenuItem = [];
    private apiAdminMenuItem = [];

    constructor(
        private mcct: MenuService,
    ){}

    getMenuitem(): Menu[]{
        
        this.mcct.getMenus('1098A')
        .subscribe(
            (result) => {
             
                if(result.isValid){
                    //console.log('result', result);
                    //console.log( result.data);
                    this.apiMenuItem = JSON.parse(result.data);
                }
                //console.log('subs result', result)
            },
            (error) => {
               // console.log('subs error', error)
            }
        );
        return this.apiMenuItem;
        //return MENUITEMS;
        
    }
    getAdminMenuitem(): Menu[] {
        this.mcct.getMenus('AAM')
        .subscribe(
            (result) => {
               // console.log('subs result', result)
                if(result.isValid){
                    this.apiAdminMenuItem = JSON.parse(result.data);
                }
                //console.log('subs result', result)
            },
            (error) => {
                //console.log('subs error', error)
            }
        )
        return this.apiAdminMenuItem;
        //return ADMINMENUITEMS;
        
    }

}


