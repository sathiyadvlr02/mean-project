import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-report-manager',
    templateUrl: './report-manager.component.html',
    styleUrls: ['./report-manager.component.css']
})
export class ReportManagerComponent implements OnInit {
    displayedColumns: string[] = ['first_name', 'last_name', 'login_name', 'SSN', 'TaxFormType', 'action'];
    dataSource = ELEMENT_DATA;
    selectedRow:any;
    displayedColumnsAWT: string[] = ['first_name', 'last_name', 'login_name', 'SSN', 'TaxFormType', 'Date','action'];
    dataSourceAWT = AwatingJobs_DATA;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;


    constructor() { }

    ngOnInit(): void {
    }
    //ApproveJobs() {
    //    this.appJobs = true;
    //    this.awatingJobs = false;
    //    this.proofJobs = false;
    //}

    //AwaitingJobs() {
    //    this.appJobs = false;
    //    this.awatingJobs = true;
    //    this.proofJobs = false;
    //}

    //ProofStatus() {
    //    this.appJobs = false;
    //    this.awatingJobs = false;
    //    this.proofJobs = true;
    //}
}


/* Static data */

export interface PeriodicElement {
    first_name: string;
    last_name: string;
    login_name: string;
    SSN: string;
    TaxFormType: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { first_name: '11137', last_name: '11137-General Test Company 1-20200422121350 2020 1099-NEC Print and Mail Proof - 2UP 8.5x11', login_name: '', SSN: '2020', TaxFormType: 'Waiting' },
    { first_name: '11137', last_name: '11137-General Test Company 1-20200422121350 2020 1099-NEC Print and Mail Proof - 2UP 8.5x11', login_name: '', SSN: '2020', TaxFormType: 'Waiting' },
];


/* Static data */

export interface AwatingJobs {
    first_name: string;
    last_name: string;
    login_name: string;
    SSN: string;
    TaxFormType: string;
    Date: string;
}

const AwatingJobs_DATA: AwatingJobs[] = [
    { first_name: '11137', last_name: 'General Test Company 1', login_name: '1099MISC', SSN: '2020 1099-MISC Print and Mail Proof - 2UP 8.5x11-Window Envelope', TaxFormType: '', Date:'May-15th-2020'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: '1098T', SSN: '	2020 1098-T Print and Mail Proof- 1UP 8.5 x 11 � All Students', TaxFormType: '', Date:'May-26th-2020' },
];