import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';



@Component({
    selector: 'app-manage-proofs',
    templateUrl: './manage-proofs.component.html',
    styleUrls: ['./manage-proofs.component.css']
})
export class ManageProofsComponent implements OnInit {
    selectedRow: any;
    select_email_notify: string;
    description: string;
    file_name: string;
    proof_description: string;
    approval: string;
    date: string;
    time: string;
    email_notified: string;
    Initial_notified: string;

    displayedColumns: string[] = ['select_email_notify', 'description',
        'proof_description', 'approval', 'date', 'time', 'email_notified', 'Initial_notified'];

    dataSource = ELEMENT_DATA;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor() { }

    ngOnInit(): void {
    }

}

/* Static data */

export interface PeriodicElement {
    select_email_notify: string;
    description: string;
    file_name: string;
    proof_description: string;
    approval: string;
    date: string;
    time: string;
    email_notified: string;
    Initial_notified: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        select_email_notify: '', description: '2020 1099MISC BOX TOTALS REPORT', file_name: '11137-1099MISC-2020PROOF-1099MISCBOXRPT-ALL-20200402170020ALL.PDF',
        proof_description: '', approval: 'Approved', date: '08-31-2020', time: '1.20 AM', email_notified: '08-01-2020', Initial_notified: '08-01-2020'
    },

    {
        select_email_notify: '', description: '2020 1099MISC BOX TOTALS REPORT', file_name: '11137-1099MISC-2020PROOF-1099MISCBOXRPT-ALL-20200402170020ALL.PDF',
        proof_description: '', approval: 'Approved', date: '08-31-2020', time: '1.20 AM', email_notified: '08-01-2020', Initial_notified: '08-01-2020'
    },

];
