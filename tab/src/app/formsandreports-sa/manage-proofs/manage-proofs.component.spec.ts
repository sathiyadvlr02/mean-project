import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageProofsComponent } from './manage-proofs.component';

describe('ManageProofsComponent', () => {
  let component: ManageProofsComponent;
  let fixture: ComponentFixture<ManageProofsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageProofsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageProofsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
