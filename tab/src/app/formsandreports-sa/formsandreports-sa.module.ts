import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FormsandreportsSARoutes } from './formsandreports-sa-routing.module';
import { FormReportGeneratorComponent } from './form-report-generator/form-report-generator.component';
import { ManageProofsComponent } from './manage-proofs/manage-proofs.component';
import { ReportManagerComponent } from './report-manager/report-manager.component';
import { ApproveProofsComponent } from './approve-proof/approveproof.component';


@NgModule({
  declarations: [FormReportGeneratorComponent, ManageProofsComponent, ReportManagerComponent,ApproveProofsComponent],
  imports: [
      CommonModule,
      AppMaterialModule,
      FormsModule, ReactiveFormsModule,
      FlexLayoutModule,
      RouterModule.forChild(FormsandreportsSARoutes)
  ]
})
export class FormsandreportsSAModule { }
