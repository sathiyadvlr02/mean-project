import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormReportGeneratorComponent } from './form-report-generator.component';

describe('FormReportGeneratorComponent', () => {
  let component: FormReportGeneratorComponent;
  let fixture: ComponentFixture<FormReportGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormReportGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormReportGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
