import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormReportGeneratorComponent } from './form-report-generator/form-report-generator.component';
import { ManageProofsComponent } from './manage-proofs/manage-proofs.component';
import { ReportManagerComponent } from './report-manager/report-manager.component';
import { ApproveProofsComponent } from './approve-proof/approveproof.component';

export const FormsandreportsSARoutes: Routes = [
    {
        path: 'form-report-generator',
        component: FormReportGeneratorComponent
    },

    {
        path: 'manageproofs',
        component: ManageProofsComponent
    },
    {
        path: 'approve-proofs',
        component: ApproveProofsComponent
    },
    {
        path: 'reportmanageremployee',
        component: ReportManagerComponent
    }, 
]

