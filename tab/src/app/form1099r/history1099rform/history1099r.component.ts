import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';

import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-history1099r',
    templateUrl: './history1099r.component.html',

})
export class History1099RComponent implements OnInit {
    historyForm: FormGroup;
    stateList;
    country;
    taxyear;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public stateservice: StateService) {
    }

    ngOnInit(): void {
        this.historyForm = this.fb.group({
            corrected : new FormControl(''),
            payers_federalno : new FormControl(''),
            payersname: new FormControl('', [Validators.required]),
            address1: new FormControl('', [Validators.required]),
            address2: new FormControl('', [Validators.required]),
            address3: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            phone1: new FormControl('', [Validators.required]),
            phone2: new FormControl('', [Validators.required]),
            recipientno:  new FormControl('', [Validators.required]),
            recipient_name: new FormControl('', [Validators.required]),
            streetaddress1: new FormControl('', [Validators.required]),
            streetaddress2: new FormControl('', [Validators.required]),
            streetaddress3: new FormControl('', [Validators.required]),
            recipientcity: new FormControl('', [Validators.required]),
            recipientstate: new FormControl('', [Validators.required]),
            recipientzip: new FormControl('', [Validators.required]),
            distributioncode: new FormControl('', [Validators.required]),
            rothcontrib: new FormControl(''),
            grossdistribution: new FormControl(''),
            taxableamount: new FormControl(''), 
            taxamt_notdetermined:new FormControl(''),
            totaldistribution: new FormControl(''),
            capitalgain:new FormControl(''),
            federaltax:new FormControl(''),
            insuranceperemiums: new FormControl(''),
            ra_sep_simple: new FormControl(''),
            per_totodistribution: new FormControl(''),
            employersecurities:new FormControl(''),
            other: new FormControl(''),
            percentage: new FormControl(''),
            totalcontributions: new FormControl(''),
            amountallocable: new FormControl(''),
            FATCA: new FormControl(''),
            statetax: new FormControl(''),
            stateno: new FormControl(''),
            statedistribution: new FormControl(''),
            acctno:new FormControl(''),
            dateofpayment :new FormControl(''),
            locality:new FormControl(''),
            localtax: new FormControl(''),
            localdistribution:new FormControl(''), 
            forignCountry: new FormControl(false),
            Country: new FormControl(),
            releasedate: new FormControl(),
            consentdate: new FormControl(),
            memo: new FormControl(),
        });
         this.taxyear=2020;
    }

    get trrowErrors() { return this.historyForm.controls; }
    
}

