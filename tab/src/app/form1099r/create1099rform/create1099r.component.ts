import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Router,ActivatedRoute} from '@angular/router';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';

import {StateService} from './../../services/state.service'; 

@Component({
    selector: 'app-create1099r',
    templateUrl: './create1099r.component.html',
    styleUrls: ['./create1099r.component.scss'],

})
export class Create1099RComponent implements OnInit {
    createForm: FormGroup;
    stateList;
    country;
    taxyear;
    foreign=false;
    FATCA=false;
    amount=false;
    dateofpayment=false;
    

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;
    

    constructor(public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        public stateservice:StateService) {
    }

    ngOnInit(): void {
        this.createForm = this.fb.group({
            corrected: new FormControl(''),
            payers_federalno : new FormControl(''),
            payersname: new FormControl('', [Validators.required]),
            address1: new FormControl('', [Validators.required]),
            address2: new FormControl('', [Validators.required]),
            address3: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            phone1: new FormControl('', [Validators.required]),
            phone2: new FormControl('', [Validators.required]),
            recipientno:  new FormControl('', [Validators.required]),
            recipient_name: new FormControl('', [Validators.required]),
            streetaddress1: new FormControl('', [Validators.required]),
            streetaddress2: new FormControl('', [Validators.required]),
            streetaddress3: new FormControl('', [Validators.required]),
            recipientcity: new FormControl('', [Validators.required]),
            recipientstate: new FormControl('', [Validators.required]),
            recipientzip: new FormControl('', [Validators.required]),
            distributioncode: new FormControl('', [Validators.required]),
            rothcontrib: new FormControl(''),
            grossdistribution: new FormControl(''),
            taxableamount: new FormControl(''), 
            taxamt_notdetermined:new FormControl(''),
            totaldistribution: new FormControl(''),
            capitalgain:new FormControl(''),
            federaltax:new FormControl(''),
            insuranceperemiums: new FormControl(''),
            ra_sep_simple: new FormControl(''),
            per_totodistribution: new FormControl(''),
            employersecurities:new FormControl(''),
            other: new FormControl(''),
            percentage: new FormControl(''),
            totalcontributions: new FormControl(''),
            amountallocable: new FormControl(''),
            FATCA: new FormControl(''),
            statetax: new FormControl(''),
            stateno: new FormControl(''),
            statedistribution: new FormControl(''),
            acctno:new FormControl(''),
            dateofpayment :new FormControl(''),
            locality:new FormControl(''),
            localtax: new FormControl(''),
            localdistribution:new FormControl(''), 
            forignCountry: new FormControl(false),
            Country: new FormControl(),
        });
        this.taxyear =2020;

          this.getstatelist();
    }

    get trrowErrors() { return this.createForm.controls; }

    

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    getstatelist() {
        this.stateservice.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
            this.getCountry();
    }

    getCountry() {
        this.stateservice.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
        }

    AddFiler() {}
}

