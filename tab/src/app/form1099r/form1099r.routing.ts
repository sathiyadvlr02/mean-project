import { Routes } from '@angular/router';
 
import { Create1099RComponent } from './create1099rform/create1099r.component'; 
import { Update1099RComponent } from './update1099rform/update1099r.component'; 
import { History1099RComponent } from './history1099rform/history1099r.component'; 
import {FormEmail1099R } from './email1099rform/email1099r.component';

export const Form1099RRoutes: Routes = [
   
  {
    path: 'create1099r',
    component: Create1099RComponent  
  },
  {
    path: 'form1099rcreate',
    component: Create1099RComponent  
  },
  {
    path: 'form1099rupdate',
    component: Update1099RComponent 
  },
  {
    path: 'form1099rhistory',
    component: History1099RComponent 
  },
  {
    path: 'form1099remail',
    component: FormEmail1099R 
  },
];
