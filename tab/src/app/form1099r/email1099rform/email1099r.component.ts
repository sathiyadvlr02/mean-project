import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { SetPassword } from './setpassword.component';

@Component({
    selector: 'app-email1099r',
    templateUrl: './email1099r.component.html'
})
export class FormEmail1099R implements OnInit {

    emailForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private el: ElementRef,
        public dialog: MatDialog,
        private router: Router,
        private route: ActivatedRoute,
        public toaster: ToastrService,
    ){}

    ngOnInit(): void {
        this.emailForm = this.fb.group({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('')
        });
    }

    get trrowErrors() { return this.emailForm.controls; }

    onSubmit(){
    }

    openDialog() {
        const dialogRef = this.dialog.open(SetPassword);

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.emailForm.get('password').setValue(result);
            }
        });
    }

}