import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
 
import { Form1099RRoutes } from './form1099r.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099RComponent } from './create1099rform/create1099r.component'; 
import { Update1099RComponent } from './update1099rform/update1099r.component'; 
import { History1099RComponent } from './history1099rform/history1099r.component'; 
import { FormEmail1099R } from './email1099rform/email1099r.component';
import {SetPassword } from './email1099rform/setpassword.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(Form1099RRoutes)
  ],
  declarations: [Create1099RComponent,Update1099RComponent ,History1099RComponent,FormEmail1099R,SetPassword]
})
export class Form1099RModule {}
