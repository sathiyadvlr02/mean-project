import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../services/account.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { Subscription, timer } from 'rxjs';
import { takeWhile, tap } from 'rxjs/operators';


@Component({
    selector: 'app-validate-codeSPSGZ',
    templateUrl: './validate-codeSPSGZ.component.html',
    styleUrls:['./validate-codeSPSGZ.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ValidateCodeSPSGZComponent implements OnInit {
    imageUrl: string = '/assets/images/bg-register.jpeg';
    insertForm: FormGroup;
    code: FormControl;
    returnUrl: string;
    ErrorMessage: string;
    invalidLogin: boolean;
    timerCountMessage: string;
    attemptsRemaining: string = '3';
    private subscription: Subscription;
    formsubmitted: boolean;

    constructor(
        private fb: FormBuilder,
        private acct: AccountService,
        private router: Router,
        private route: ActivatedRoute,
        public toasterService: ToastrService
    ) { }

    ngOnInit(): void {
        this.code = new FormControl('', [Validators.required]);
        this.timerCountMessage = "";
        this.observerTimer();

        if ((localStorage.getItem('codeExpiry')) == null) {
            this.router.navigate(['/loginspsgz']);
        }

        this.insertForm = this.fb.group({
            code: this.code
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }


    /* Observable timer */
    observerTimer() {
        let endTime = new Date(localStorage.getItem('codeExpiry')).getTime();

        let currentTime = new Date().getTime();
        let difference = endTime - currentTime;
        let seconds = Math.floor(difference / 1000);

        this.subscription = timer(1000, 1000) //Initial delay 1 seconds and interval countdown also 1 second
            .pipe(
                takeWhile(() => seconds > 0),
                tap(() => seconds--)
            )
            .subscribe(() => {

                this.timerCountMessage = "Time remaining: " + seconds + "s ";

                //  $('#countDown').text(seconds + 's');
                let isSessionActive = localStorage.getItem('isSessionActive');
                if (seconds <= 0 || isSessionActive == '0' || isSessionActive == undefined || !(isSessionActive == '1')) {

                    this.timerCountMessage = "EXPIRED";
                    if (!this.formsubmitted) {
                        this.stopTimer();
                        this.acct.sendExpiryNotification();
                    }
                }
            });
    }

    private stopTimer() {
        this.subscription.unsubscribe();
    }


    onSubmit() {

        this.formsubmitted = true;
        let validateCodeDetails = this.insertForm.value;
        this.acct.validateTwoFactorCode(validateCodeDetails.code).
        subscribe(
            (result) => {
                //debugger;

                this.invalidLogin = false;
                this.router.navigateByUrl('/home');
            },
            (error) => {
                this.formsubmitted = false;
                //debugger;
                this.invalidLogin = true;

                console.log(error.message);
                this.ErrorMessage = error.error;
                console.log("2fa error : " + (<any>this.ErrorMessage));


                if ((<any>this.ErrorMessage).loginError == 'Code-InValid') {

                    //debugger;

                    if ((<any>this.ErrorMessage).attemptsRemaining == 0) {
                      //  this.acct.sendExpiryNotification();
                        let timerInterval;

                        Swal.fire({
                            title: 'Your Account is Locked!',
                            html: 'You will be redirected in <b></b> seconds.',
                            timer: 10000,
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                timerInterval = setInterval(() => {
                                    const content = Swal.getContent();
                                    if (content) {
                                        const b = content.querySelector('b');
                                        if (b) {
                                            b.textContent = String((Swal.getTimerLeft() / 1000).toFixed());
                                        }
                                    }
                                }, 100);
                            },
                            onClose: () => {
                                clearInterval(timerInterval);
                            }
                        }).then((result) => {
                            if (result.dismiss === Swal.DismissReason.timer) {
                            }
                           // window.location.href = '/login';
                        });


                    } else {
                        this.attemptsRemaining = (<any>this.ErrorMessage).attemptsRemaining;
                        this.toasterService.warning('Invalid Code', '', { positionClass: 'toast-top-right' });
                    }
                } else {
                    this.toasterService.error('Request cannot be processed at the moment.  Try again later.', '', {
                        positionClass: 'toast-top-center'
                    });
                }
            }
        );
    }

    onResend() {

        let provider = localStorage.getItem("TwoFactorProvider");

        this.acct.ResendTwoFactorProvider(provider, "", false).subscribe(
            (result) => {
                window.location.reload();
            },
            (error) => {
                this.toasterService.error(
                    'Invalid data. Please check and try again',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );

              //  this.router.navigate(['/login']);

            }
        );
    }

}

