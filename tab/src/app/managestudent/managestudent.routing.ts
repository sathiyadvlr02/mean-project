import { Routes } from '@angular/router';

import { ManageStudentModule } from './managestudent.module';
import { ManageStudentComponent } from './managestudent.component';
import { ManageStudentAddComponent } from './managestudentadd.component';
import { EditManageStudentComponent } from './editmanagestudent.component';

export const ManageStudentRoutes: Routes = [
  {
  path: 'managestudents',
  component: ManageStudentComponent
},
{
  path: 'managestudentadd',
  component: ManageStudentAddComponent
},
{
    path: 'editstudent/:id',
    component: EditManageStudentComponent
}

];
