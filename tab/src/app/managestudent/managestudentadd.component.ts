import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';

import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StateService } from '../services/state.service';    
import { Title } from '@angular/platform-browser';
import { StudentService } from './../services/student.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
    selector: 'app-managestudentadd',
    templateUrl: './managestudentadd.component.html'
})
export class ManageStudentAddComponent implements OnInit {
    studentForm: FormGroup;
    loading: boolean;
    stateList;
    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    

    constructor(
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public studentservice: StudentService,
        private cookieService: CookieService,
         public StateServices : StateService,
    ) { }

    ngOnInit() {

        this.studentForm = new FormGroup({
            first_name: new FormControl('', Validators.required),
            last_name: new FormControl('', Validators.required),
            social_security: new FormControl('', [Validators.required, Validators.pattern(/^\d{3}-\d{2}-\d{4}$/)]),
            school_student_id: new FormControl(''),
            student_const_date: new FormControl(''),
            user_name: new FormControl('', Validators.required),
            email: new FormControl('', Validators.required),
            address1: new FormControl('', Validators.required),
            address2: new FormControl(''),
            address3: new FormControl(''),
            city: new FormControl('', Validators.required),
            state: new FormControl('', Validators.required),
            zip: new FormControl('', Validators.required),
            phone: new FormControl('', [Validators.required, Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            ext: new FormControl(''),
            // printSuppression: new FormControl(false),
            // eFileSuppression: new FormControl(false),
        });
        this.getstatelist();
    }

    get trrowErrors() { return this.studentForm.controls; }
    
    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    AddStudent() {
        if (this.studentForm.invalid) {
            for (const key of Object.keys(this.studentForm.controls)) {
                if (this.studentForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }

        let addFormData = this.studentForm.value;
        let datas = {
            "STUDENT_ID": "",
            "LOGIN_NAME": addFormData.user_name,
            "PASSWORD": "",
            "TOKEN": "",
            "FIRST_NAME": addFormData.first_name,
            "MIDDLE_NAME": "",
            "LAST_NAME": addFormData.last_name,
            "ADDRESS1": addFormData.address1,
            "ADDRESS2": addFormData.address2,
            "ADDRESS3": "",
            "CITY": addFormData.city,
            "STATE": addFormData.state,
            "ZIP": addFormData.zip,
            "PHONE": addFormData.phone,
            "PHONE_EXTENSION": addFormData.ext,
            "FAX": "",
            "EMAIL": addFormData.email,
            "SITE_ID": parseInt(this.CookieSiteId),
            "SCHOOL_STUDENT_ID": addFormData.school_student_id,
            "STUDENT_SSN": addFormData.social_security,
            "PASSWORD_EXP_DATE": "",
            "STUDENT_TYPE": "STUD",
            "STUDENT_CONSENT_DATE": addFormData.student_const_date,
            "EMAIL_INDICATOR": "",
            "STATUS": "ONLINE",
            "DATABASE_LOAD_ID": "",
            "PROC_TMPFLAG": "",
            "CAMPUS_CODE": "",
            "PROGRAM_CODE": "",
            "MOTHERS_MAIDEN_NAME": "",
            "STUDENT_BIRTH_DATE": "",
            "TIN_SOLICITATION_FLAG": "",
            "COUNTRY": "",
            "HALF_TIME_STUDENT_IND": "",
            "GRADUATE_STUDENT_IND": "",
            "TRANSACTION_CODE": "",
            "DOUBTFUL_ADDRESSES": ""
        };
        this.studentservice
            .addStudent(datas)
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/managestudents']);
                    } else {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/managestudents']);
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                    this.router.navigate(['home/managestudents']);
                }
            );

    }

    onCancelButton(){
        this.router.navigate(['home/managestudents']);
    }


}