import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import Swal from 'sweetalert2';

import { StudentService } from './.././services/student.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-managestudent',
  templateUrl: './managestudent.component.html'
})
export class ManageStudentComponent implements OnInit {
  homeForm: FormGroup;
  selectedRow:any;
  displayedColumns: string[] = ['first_name', 'last_name', 'SSN', 'login_name','action'];

  dataSource : MatTableDataSource<any>;;
   

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static:true }) sort: MatSort;

  constructor(
    public router: Router,
    public toasterService: ToastrService,
    private cookieService: CookieService,
    private el: ElementRef,
    public studentservice:StudentService) { }

  private CookieUserName = this.cookieService.get('username');
  private CookieSiteId = this.CookieUserName.split('_')[0];

  ngOnInit() {
     
    this.homeForm = new FormGroup({
    });

    this.getAllEntityDetails();
  }

 

   
  submit(): void {
    this.router.navigate(["managestudentadd"]);
  }

  getAllEntityDetails(){
    this.studentservice
    .getStudentDetails()
    .subscribe(
        (result) => {
            if(result.isValid){   
                this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            }
        },
        (error) => {
            //console.log('error', error)
        }
    );
}

searchEntityData(searchValue: any) {
    this.dataSource.filter = searchValue.trim().toLowerCase();
}

deleteStudent(stu_id){
    Swal.fire({
        title: 'Are you sure delete?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            this.studentservice
            .deleteStudent(stu_id)
            .subscribe(
                (result) => {
                    if(result.isValid){
                        this.toasterService.success(
                        result.message,
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                        );
                        this.getAllEntityDetails();
                    } else {
                        this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                        );
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                }
            );
        }
    })
}


}

/* Static data */

export interface PeriodicElement {
  first_name: string;
  last_name: string;
  SSN: string;
  login_name: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    first_name: 'Ariana', last_name: 'Arevalo', login_name: '969100', SSN: '634-48-7535'
  },
  { first_name: 'Ariana', last_name: 'Arevalo', login_name: '969100', SSN: '634-48-7535' },
  { first_name: 'Ariana', last_name: 'Arevalo', login_name: '969100', SSN: '634-48-7535' },
  { first_name: 'Ariana', last_name: 'Arevalo', login_name: '969100', SSN: '634-48-7535' }
   
];
