import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ManageStudentRoutes } from './managestudent.routing';
import { ChartistModule } from 'ng-chartist';
import { ManageStudentComponent } from './managestudent.component';
import { ManageStudentAddComponent } from './managestudentadd.component';
import { EditManageStudentComponent } from './editmanagestudent.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule ,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(ManageStudentRoutes)
  ],
  declarations: [ManageStudentComponent,ManageStudentAddComponent,EditManageStudentComponent]
})
export class ManageStudentModule {}
