import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { StudentService } from '../services/student.service';
import { StateService } from '../services/state.service';

@Component({
    selector: 'app-editmanagestudent',
    templateUrl: './editmanagestudent.component.html'
  })
  export class EditManageStudentComponent implements OnInit {
    studentForm: FormGroup;
    private CookieUserName = this.cookieService.get('username');
    public CookieSiteId = this.CookieUserName.split('_')[0];
    public checkboxvalue="Y";
    stateList;
    id:any;
     get Errors() { return this.studentForm.controls; }
    
     constructor(
      private el: ElementRef,
      public toasterService: ToastrService,
      private router: Router,
      public StudentService: StudentService,
      public StateServices : StateService,
      private cookieService: CookieService,
      private _Activatedroute:ActivatedRoute
  ) { 
    this.id = this._Activatedroute.snapshot.paramMap.get("id");
  }
    ngOnInit(){
     
    this.studentForm = new FormGroup({
      first_name: new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required),      
      social_security: new FormControl('', Validators.required),
      school_student_id: new FormControl(''),
      student_const_date: new FormControl(''),
      user_name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      address1: new FormControl('', Validators.required),
      address2: new FormControl(''),
      address3: new FormControl(''),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zip: new FormControl('', Validators.required),
      phone: new FormControl('',[Validators.required,Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
      ext: new FormControl(''),
      student_id: new FormControl(''),
      site_id: new FormControl(''),
      // printSuppression: new FormControl(false),
      // eFileSuppression: new FormControl(false),
    });
    this.  getstatelist();
    if(this.id){
      this.StudentService
      .getStudentbyID(this.id)
      .subscribe(
          (result) => {
              if(result.isValid){
                  this.studentForm.get('student_id').setValue(result.data.studenT_ID);
                  this.studentForm.get('first_name').setValue(result.data.firsT_NAME);
                  this.studentForm.get('last_name').setValue(result.data.lasT_NAME);
                  this.studentForm.get('social_security').setValue(result.data.studenT_SSN);
                  this.studentForm.get('school_student_id').setValue(result.data.schooL_STUDENT_ID);
                  this.studentForm.get('student_const_date').setValue(result.data.studenT_CONSENT_DATE);
                  this.studentForm.get('user_name').setValue(result.data.logiN_NAME);
                  this.studentForm.get('email').setValue(result.data.email);
                  this.studentForm.get('address1').setValue(result.data.addresS1);
                  this.studentForm.get('address2').setValue(result.data.addresS2);
                  this.studentForm.get('address3').setValue(result.data.addresS3);
                  this.studentForm.get('city').setValue(result.data.city);
                  this.studentForm.get('state').setValue(result.data.state);
                  this.studentForm.get('zip').setValue(result.data.zip);
                  this.studentForm.get('phone').setValue(result.data.phone);
                  this.studentForm.get('ext').setValue(result.data.phonE_EXTENSION);
                  this.studentForm.get('site_id').setValue(result.data.sitE_ID);
                  //this.studentForm.get('email').setValue(result.data.email);
                 // this.studentForm.get('printSuppression').setValue(result.data.printSuppression == 'Y' ? true : false);
                  //this.studentForm.get('eFileSuppression').setValue(result.data.eFileSuppression == 'Y' ? true : false)
               }
          },
          (error) => {
             // console.log('error', error)
          }
      );
  }
   }

   

   get trrowErrors() { return this.studentForm.controls; }

   getstatelist() {
    this.StateServices.getStatelist().subscribe(
        result => {
            this.stateList= JSON.parse(result.data);
        });
}
   

   EditStudent(){
     console.log("Praveen");
    console.log(this.studentForm)
      if (this.studentForm.invalid) {
          for (const key of Object.keys(this.studentForm.controls)) {
            if (this.studentForm.controls[key].invalid) {
              const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
              invalidControl.focus();
              break;
            }
          }
          return;
        }
        let addFormData = this.studentForm.value;
        let datas = {
          "studenT_ID":addFormData.student_id,
          "firsT_NAME": addFormData.first_name,
          "middlE_NAME": "",
          "lasT_NAME": addFormData.last_name,
          "addresS1": addFormData.address1,
          "addresS2": addFormData.address2,
          "addresS3": addFormData.address3,
          "city": addFormData.city,
          "state": addFormData.state,
          "zip": addFormData.zip,
          "phone": addFormData.phone,
          "phonE_EXTENSION": addFormData.ext,
         "fax": "",
          "email": addFormData.email,
          "sitE_ID":addFormData.site_id,
          "schooL_STUDENT_ID": addFormData.school_student_id,
          "studenT_SSN":addFormData.social_security,
          "passworD_EXP_DATE": "",
          "studenT_TYPE": "",
          "studenT_CONSENT_DATE": addFormData.student_const_date,
        
        };
        this.StudentService
            .updateStudent(datas)
            .subscribe(
              
                (result) => {
                  console.log(datas);
                    if (result.isValid) {
                        this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/managestudents']);
                    } else {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['home/managestudents']);
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                    this.router.navigate(['home/managestudents']);
                }
            );
  
   }

   onCancelButton(){
    this.router.navigate(['home/managestudents']);
  }

  }