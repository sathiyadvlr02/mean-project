import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';

import { Formt4ARoutes } from './formt4a-routing.module';
import { Createformt4aComponent } from './createformt4a/createformt4a.component';
import { Updateformt4aComponent } from './updateformt4a/updateformt4a.component';
import { Historyformt4aComponent } from './historyformt4a/historyformt4a.component';


@NgModule({
  declarations: [Createformt4aComponent,Updateformt4aComponent,Historyformt4aComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Formt4ARoutes)
  ]
})
export class Formt4aModule { }
