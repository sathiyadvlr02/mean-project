import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Historyformt4aComponent } from './historyformt4a.component';

describe('Historyformt4aComponent', () => {
  let component: Historyformt4aComponent;
  let fixture: ComponentFixture<Historyformt4aComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Historyformt4aComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Historyformt4aComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
