import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartistModule } from 'ng-chartist';

import { Historyformt4aRoutes } from './historyformt4a-routing.module';
import { Historyformt4aComponent } from './historyformt4a.component';


@NgModule({
  declarations: [Historyformt4aComponent],
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Historyformt4aRoutes)

  ]
})
export class Historyformt4aModule { }
