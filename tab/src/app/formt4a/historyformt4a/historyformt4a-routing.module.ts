import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Historyformt4aComponent } from './historyformt4a.component';


export const Historyformt4aRoutes: Routes = [
  {
      path: '/historyformt4a',
      component: Historyformt4aComponent
  },
];

