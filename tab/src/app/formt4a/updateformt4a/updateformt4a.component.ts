import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updateformt4a',
  templateUrl: './updateformt4a.component.html', 
})
export class Updateformt4aComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  btn_history(){
    this.router.navigate(["/historyformt4a"]);
  }
}
