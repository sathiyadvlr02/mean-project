import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Createformt4aComponent } from './createformt4a/createformt4a.component';
import { Updateformt4aComponent } from './updateformt4a/updateformt4a.component';
import { Historyformt4aComponent } from './historyformt4a/historyformt4a.component';


export const Formt4ARoutes: Routes = [
  {
      path: 'createformt4a',
      component: Createformt4aComponent
  },
  {
    path: 'updateformt4a',
    component: Updateformt4aComponent
  },
  {
    path: 'historyformt4a',
    component: Historyformt4aComponent
  },
]