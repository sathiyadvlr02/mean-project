"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardRoutes = void 0;
var dashboard_component_1 = require("./dashboard.component");
exports.DashboardRoutes = [
    {
        path: 'home',
        component: dashboard_component_1.DashboardComponent
    }
];
//# sourceMappingURL=dashboard.routing.js.map