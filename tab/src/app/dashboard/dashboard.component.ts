import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatStepper } from '@angular/material';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';
import { MenuService } from '../services/menu.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, AfterViewInit {
    LoginStatus$ = new BehaviorSubject<boolean>(null);
    Username$: Observable<string>;
    @ViewChild('stepper') stepper: MatStepper;

    constructor(private acct: AccountService, private router: Router,public menu:MenuService) {}

    ngOnInit(): void {
        this.acct.globalStateChanged.subscribe((state) => {
            this.LoginStatus$.next(state.loggedInStatus);
        });

        this.Username$ = this.acct.currentUserName;
    }

    onLogout() {
        this.acct.logout().subscribe((result) => {
        });

        this.router.navigate(['/login']);
    }
    
    menuunsubscribe(){
        this.menu.menu$ = new Observable<[]>();
    }

    ngAfterViewInit() {
        this.move(1);
     }


    move(index: number) {
        this.stepper.selectedIndex = index;
    }

}
