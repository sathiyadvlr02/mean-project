import { Component, OnInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

import { MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';

import { Form1098tService } from '../services/form1098t.service';
import { UserService } from '../services/user.service';

interface  FormType{
    "useR_TYPE": string,
    "forM_TYPE": string
}
@Component({
    selector: 'app-formsmanagement',
    templateUrl: './formsmanagement.component.html'
})

export class FormsManagementComponent implements OnInit {

    createAppForm: FormGroup;
    selectedYear;
    formType: FormType;

    yearList = [
        { name: 'Select Year', value: '' },
        { name: '2020', value: '2020' },
        { name: '2019', value: '2019' },
        { name: '2018', value: '2018' },
        { name: '2017', value: '2017' },
        { name: '2016', value: '2016' },
        { name: '2015', value: '2015' },
        { name: '2014', value: '2014' },
        { name: '2013', value: '2013' },
        { name: '2012', value: '2012' },
        { name: '2011', value: '2011' },
        { name: '2010', value: '2010' },
        { name: '2005', value: '2005' },
    ];

    // formType = [
    //     { name: '1098-T', value: '1098-T' }
    // ];

    homeForm: FormGroup;
    selectedRow: any;
    public show: boolean = false;
    public showlbl: boolean = true;
    public showcirclebtn: boolean = false;
    displayedColumns: string[] = ['function', 'SEQUENCE_ID', 'STUDENT_FIRST_NAME', 'STUDENT_LAST_NAME', 'BOX_STUDENT_SSN', 'STUDENT_ID'];
    dataSource: MatTableDataSource<any>;
    radioStatus: boolean;
    public selectedValue: any;
    currentCheckedValue = null;

    selectedObject = {};

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private router: Router,
        private ren: Renderer2,
        private el: ElementRef,
        private fb: FormBuilder,
        public toaster: ToastrService,
        private _form1098tService: Form1098tService,
        private cookieService: CookieService,
        public userservice: UserService
    ) { }

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            formYear: new FormControl('2020', [Validators.required]),
            formType: new FormControl('', [Validators.required]),          
        });

        if (this.selectedYear != '' && this.cookieService.get('formYear')) {
            this.selectedyear(this.cookieService.get('formYear'))
            this.createAppForm.get('formYear').setValue(this.cookieService.get('formYear'));
            this.cookieService.set('formYear', '');
        }

        this.getformTtpe();
    }

    get trrowErrors() { return this.createAppForm.controls; }


    getformTtpe() {
        this.userservice.getFormTypes('1098A').subscribe(result => {
            if (result.isValid) {
                this.formType= result.data;
                this.createAppForm.get('formType').setValue(this.formType[0].forM_TYPE);              
            }
            this.selectedyear('2020');
        });
    }
    selectedyear(value) {
        this.selectedYear = value;
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.get1098tData(this.selectedYear);
            default:
                break;

        }
    }

    get1098tData(year) {
        this._form1098tService
            .getAllFormDatas(year)
            .subscribe(
                (result) => {
                    //console.log('result', result)
                    if (result.isValid) {

                        let tempData = [];
                        JSON.parse(result.data) && JSON.parse(result.data).map((itm, i) => {
                            let tembObj = itm;
                            tembObj['selected'] = false;
                            tempData.push(tembObj);
                        })

                        this.dataSource = new MatTableDataSource(tempData);
                        console.log('this.dataSource', this.dataSource);
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

    createButtonClick() {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.router.navigate(["create1098t"], { queryParams: { data: this.selectedYear } });
                break;
            default:
                this.toaster.error(
                    'No Form',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
                break;
        }
    }

    onSubmit() {
        console.log(this.createAppForm)
        if (this.createAppForm.invalid) {
            for (const key of Object.keys(this.createAppForm.controls)) {
                if (this.createAppForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }

        let siteDetailsFormData = this.createAppForm.value;

        switch (siteDetailsFormData.formType) {
            case '1098-T':
                this.router.navigate(["create1098t"], { queryParams: { data: this.selectedYear } });
                break;
            default:
                this.toaster.error(
                    'No Form',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
                break;
        }

    }

    radioChange(e1, element) {
        this.selectedObject = element;
        this.show = true;
        this.showlbl = false;
        this.showcirclebtn = true;
        /* this.dataSource.forEach(p => p.selected = false);
        element.selected = !element.selected;
        this.show = true;
        this.showlbl = false;
        this.showcirclebtn = true; */
    }

    onSelectedChecked(element) {
        return JSON.stringify(element) == JSON.stringify(this.selectedObject) ? true : false
    }

    searchData(searchValue: any) {
        this.dataSource.filter = searchValue.trim().toLowerCase();
    }

    create(): void {
        this.router.navigate(["formsmanagementcreatenew"]);
    }

    ViewForm(): void {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                let tempData = {
                    Student_Id: this.selectedObject['STUDENT_ID'],
                    Sequence_Id: this.selectedObject['SEQUENCE_ID'],
                    TaxYear: this.selectedYear
                };
                this._form1098tService.taxFormVersion(tempData).subscribe(
                    result => {
                        if (result.isValid && result.data) {
                            this.router.navigate(["view1098thistory"], { queryParams: { student: this.selectedObject['STUDENT_ID'], sequence: this.selectedObject['SEQUENCE_ID'], tax: this.selectedYear } });
                        } else {
                            this.toaster.error(
                                'View History Not Found',
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                        }
                    });

                break;
            default:
                break;
        }
    }

    UpdateForm(): void {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.router.navigate(["update1098t"], { queryParams: { data: this.selectedYear, seq: this.selectedObject['SEQUENCE_ID'], stuid: this.selectedObject['STUDENT_ID'] } });
                break;
            default:
                break;
        }
    }

    PrintForm(): void {
        const link = document.createElement('a');
        link.setAttribute('target', '_blank');
        let tempLink = '';

        switch (this.createAppForm.value.formType) {
            case '1098-T':
                tempLink = '/api/v1/Taxform/Print1098T?Student_Id=' + this.selectedObject['STUDENT_ID'] + '&Sequence_Id=' + this.selectedObject['SEQUENCE_ID'] + '&TaxYear=' + this.selectedYear;
            default:
                break;

        }

        link.setAttribute('href', tempLink);
        document.body.appendChild(link);
        link.click();
        link.remove();
    }

    EmailForm(): void {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.router.navigate(["email1098t"], { queryParams: { student: this.selectedObject['STUDENT_ID'], sequence: this.selectedObject['SEQUENCE_ID'], tax: this.selectedYear } });
                break;
            default:
                break;
        }
    }

    DeleteForm(): void {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.delete1098tData();
                break;
            default:
                break;
        }
        this.selectedObject = {};
        this.show = false;
        this.showlbl = true;
        this.showcirclebtn = false;
    }

    UpdateForms(): void {
        switch (this.createAppForm.value.formType) {
            case '1098-T':
                this.UpdateForm();
                break;
            default:
                break;
        }
    }

    delete1098tData() {
        let data = {
            Student_Id: this.selectedObject['STUDENT_ID'],
            Sequence_Id: this.selectedObject['SEQUENCE_ID'],
            TaxYear: this.selectedYear,
        }
        Swal.fire({
            title: 'Are you sure delete?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this._form1098tService
                    .deleteFormDatas(data)
                    .subscribe(
                        (result) => {
                            //console.log('result', result)
                            if (result.isValid) {
                                this.toaster.success(
                                    result.message,
                                    '',
                                    {
                                        positionClass: 'toast-top-center',
                                        timeOut: 3000
                                    }
                                );
                                this.get1098tData(this.selectedYear);
                            }
                        },
                        (error) => {
                            this.toaster.error(
                                'Please Try Again',
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                        }
                    );
            }
        })
    }


    click_circle(): void {
        this.selectedObject = {};
        this.show = false;
        this.showlbl = true;
        this.showcirclebtn = false;
        /* this.show = false;
        this.showlbl = true;
        this.showcirclebtn = false;
        this.dataSource.forEach(p => p.selected = false); */
    }
}


/* Static data */

export interface PeriodicElement {
    function: string;
    SEQUENCE_ID: string;
    STUDENT_FIRST_NAME: string;
    STUDENT_LAST_NAME: string;
    BOX_STUDENT_SSN: string;
    STUDENT_ID: string;
    selected: boolean;

}

/* const ELEMENT_DATA: PeriodicElement[] = [
    {
        function: 'Create 1098', seq: '0',
        first_name: 'Michela', last_name: 'BRUNELLI',
        ssn: '999-99-9999', school_student_id: '21691638', selected: false
    },
    {
        function: 'Create 1098', seq: '0',
        first_name: 'Michela', last_name: 'BRUNELLI',
        ssn: '999-99-9999', school_student_id: '21691639', selected: false
    },
    {
        function: 'Create 1098', seq: '0',
        first_name: 'Michela', last_name: 'BRUNELLI',
        ssn: '999-99-9999', school_student_id: '21691637', selected: false
    }

]; */
