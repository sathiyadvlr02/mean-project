import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { FormsManagementRoutes } from './formsmanagement.routing';
import { ChartistModule } from 'ng-chartist';
import { FormsManagementComponent } from './formsmanagement.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule ,ReactiveFormsModule ,
    RouterModule.forChild(FormsManagementRoutes)
  ],
  declarations: [FormsManagementComponent]
})
export class FormsManagementModule {}
