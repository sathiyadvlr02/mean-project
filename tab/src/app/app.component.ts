import { Component, HostListener } from '@angular/core';
import { interval } from 'rxjs';
import { AccountService } from './services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { SpinnerService } from './services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private _acct: AccountService,
    private cookieService: CookieService,
    public spinner: SpinnerService
  ){
    //console.log('app component working');
    setInterval(() => {
     // console.log('get refresh token')
      if(this.cookieService.get('loginStatus') == '1'){
        let tempData = {
          grantType: "refresh_token",
          siteid: this.cookieService.get('site_id'),
          userName: this.cookieService.get('username').split('_')[1],
          refreshToken: "string",
          password: "string",
          rememberMe: true
        };
        //this.getToken(tempData)
      }
    }, 10000);
    
    
  }

  getToken(tempData){
    this._acct
      .getRefreshToken(tempData)
      .subscribe(
        (result) => {
          console.log('token result', result)
        },
        (error) => {
          console.log('token error', error)
        }
      );
  }
  
}
