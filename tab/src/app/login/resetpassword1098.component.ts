import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormGroup, FormBuilder ,ReactiveFormsModule } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { ConfirmedValidator } from './confirmed.validator';
import { AccountService } from '../services/account.service';

@Component(
  {
    selector: 'app-forgot',
    templateUrl: './resetpassword1098.component.html',
    styleUrls:['./login1098.component.scss'],
    encapsulation: ViewEncapsulation.None
  })

export class ResetPassword1098Component implements OnInit {
  
  loginForm: FormGroup = new FormGroup({});
  
  loading: boolean;
  newpasswordhide = true; 
  confirmpasswordhide = true; 
  /* oldpasswordhide = true; */
  bar: any = '0%';
  barLabel: string = 'WEAK';  

  code = '';
  userId = '';


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public toasterService: ToastrService,
    private acct: AccountService,
  ) { 

    this.loginForm = fb.group({    
      newpassword: new FormControl('', Validators.required),
      confirmpassword: new FormControl('', Validators.required),
      /* oldpassword: new FormControl('', Validators.required), */
    }, {
      validator: ConfirmedValidator('newpassword', 'confirmpassword')
    });

    this.activatedRoute.queryParams.subscribe( params => {
      this.code = params['code'];
      this.userId = params['userId'];
     });
  } 

  ngOnInit() {
    
  };
    
   
  get passwordInput() { return this.loginForm.get('newpassword'); }  
  
  get confirmpasswordInput() { return this.loginForm.get('confirmpassword'); }  

  /* get oldpasswordInput() { return this.loginForm.get('oldpassword'); } */  


  onSubmit() {
    let userInfo = this.loginForm.value;
    if((userInfo.newpassword && userInfo.confirmpassword && userInfo.newpassword == userInfo.confirmpassword) && this.barLabel != 'WEAK'){
      this.acct
      .onboardResetPassword(this.code, this.userId, userInfo.newpassword, userInfo.confirmpassword)
      .subscribe(
        (result) => {
          this.toasterService.success(
            'Your Password Reset Successful. Please Login.',
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
          this.router.navigate(['/login1098']);
        },
        (error) => {
          this.toasterService.error(
            'Please Try Again.',
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
          return false;
        }
      );
    } else {
      let message = '';
      /* if(!userInfo.oldpassword){
        message = 'Old Password is Required'
      } else */ if(!userInfo.newpassword && !userInfo.confirmpassword){
        message = 'New and Confirm Password is Required'
      } else if(userInfo.newpassword != userInfo.confirmpassword){
        message = 'Password Not Matched'
      } else if(this.barLabel == 'WEAK'){
        message = 'Your Password is Weak'
      }
      this.toasterService.error(
        message,
        '',
        {
            positionClass: 'toast-top-center',
            timeOut: 3000
        }
      );
      return false;
    }
  }

  onChangePassword(value){
    let checkPasswordStrong = this.measureStrength(value)
    this.bar = checkPasswordStrong.toString()+'%';
    this.strongerMessage(checkPasswordStrong);
  }

  strongerMessage(value: any){
    value = parseInt(value)
    if(value >= 80){
      this.barLabel = 'STRONG';
    } else if(value < 80 && value >= 50){
      this.barLabel = 'MEDIUM';
    } else {
      this.barLabel = 'WEAK';
    }
  }

  measureStrength(pass: string) {  
    let score = 0;  
    // award every unique letter until 5 repetitions  
    let letters = {};  
    for (let i = 0; i< pass.length; i++) {  
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;  
      score += 5.0 / letters[pass[i]];  
    }  
    // bonus points for mixing it up  
    let variations = {  
      digits: /\d/.test(pass),  
      lower: /[a-z]/.test(pass),  
      upper: /[A-Z]/.test(pass),  
      nonWords: /\W/.test(pass),  
    };  
    let variationCount = 0;  
    for (let check in variations) {  
      variationCount += (variations[check]) ? 1 : 0;  
    }  
    score += (variationCount - 1) * 10;  
    score = score < 0 ? 0 : score;
    return Math.trunc(score);  
  }  

}
