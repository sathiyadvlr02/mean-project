import { Component, OnInit ,ViewEncapsulation } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { AccountService } from '../services/account.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component(
  {
    selector: 'app-loginspsgz',
    templateUrl: './loginspsgz.component.html',
    styleUrls:['./loginspsgz.component.scss'],
    encapsulation: ViewEncapsulation.None

  })
export class LoginspsgzComponent implements OnInit {
  

    loginForm: FormGroup;
    Siteid: FormControl;
    Username: FormControl;
    Password: FormControl;
    RememberMe: FormControl;
    returnUrl: string;
    ErrorMessage: string;
    invalidLogin: boolean;
  
    LoginStatus$ = new BehaviorSubject<boolean>(null);
  
    constructor(
      private acct: AccountService,
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
      public toasterService: ToastrService
    ) { }
  
    ngOnInit(): void {
      this.acct.globalStateChanged.subscribe((state) => {
        this.LoginStatus$.next(state.loggedInStatus);
      });
  
      if (this.LoginStatus$.getValue()) {
        this.router.navigate(['/']);
      }
  
      // Initialize Form Controls
      this.Siteid = new FormControl('', [Validators.required]);
      this.Username = new FormControl('', [Validators.required]);
      this.Password = new FormControl('', [Validators.required]);
      this.RememberMe = new FormControl(false);
  
      // Initialize FormGroup using FormBuilder
      this.loginForm = this.fb.group({
        Siteid: this.Siteid,
        Username: this.Username,
        Password: this.Password,
        RememberMe: this.RememberMe
      });
  
      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  
      this.setbackgroundImage();
    }
  
    onSubmit() {
      let userlogin = this.loginForm.value;
      this.acct
        .login(userlogin.Siteid, userlogin.Username, userlogin.Password, false )
        .subscribe(
          (result) => {
            this.invalidLogin = false;
            //  $('body').css({ 'background-image': 'none' });
            this.router.navigateByUrl('/home');
          },
          (error) => {
            this.invalidLogin = true;
            this.ErrorMessage = error;
            if (error.status == 500) {
              this.toasterService.info(
                'Our Team is working to fix this error. Try again later.',
                '',
                { positionClass: 'toast-top-center' }
              );
            }
            if (error.status == 401) {
              if (error.error.loginError) {
                if (error.error.loginError == 'Auth Code Required') 
                {
                  localStorage.setItem(
                    'codeExpiry',
                    error.error.expiry
                  );
                  localStorage.setItem(
                    'twoFactorToken',
                    error.error.twoFactorToken
                  );
                  localStorage.setItem('isSessionActive', '1');
                  localStorage.setItem(
                    'user_id',
                    error.error.userId
                  );
                  localStorage.setItem('makedEmail',
                      error.error.makedEmail
                    );
                    localStorage.setItem(
                      'maskedPhone',
                      error.error.maskedPhone
                    );
  
                    console.log( error.error);
  
  
                  this.router.navigate(['/send-codespsgz']);
                  return false;
                }
                if (error.error.loginError == 'Account Locked') {
  
                    Swal.fire({
                        title:
                            'Your account is locked, please contact support.',
                        icon: 'error',
                        showClass: {
                            popup:
                                'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup:
                                'animate__animated animate__fadeOutUp'
                        }
                    });
  
  
                  return false;
                }
                if (
                  error.error.loginError == 'Email Not Confirmed'
                ) {
  
                    Swal.fire({
                        title:
                            'Your Email is not Verified. Please Verify Email',
                        icon: 'error',
                        showClass: {
                            popup:
                                'animate__animated animate__fadeInDown'
                        },
                        hideClass: {
                            popup:
                                'animate__animated animate__fadeOutUp'
                        }
                    });
  
  
                  return false;
                } else {
                  this.toasterService.error(
                    error.error.loginError,
                    '',
                    { positionClass: 'toast-top-center' }
                  );
                  return false;
                }
              } else {
                this.toasterService.error(
                  'Invalid Username/Password. Please check credentials and try again',
                  '',
                  {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                  }
                );
                return false;
              }
            } else {
              this.toasterService.warning(
                'An error occured while processing this request.',
                '',
                { positionClass: 'toast-top-center' }
              );
              return false;
            }
          }
        );
    }
  
    setbackgroundImage() {
      //$('body').css({
      //    'background-image': 'url(' + this.imageUrl + ')',
      //    'background-repeat': 'no-repeat',
      //    'background-size': 'cover'
      //});
    }
  }
  