import { Component, ElementRef, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FileValidators } from "ngx-file-drag-drop";
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrComponentlessModule, ToastrService } from 'ngx-toastr';

import { UploadService } from '../../services/upload.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import 'rxjs/add/operator/map';

import { Form1099saService } from './../../services/form1099sa.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';

@Component({
    selector: 'app-upload-file',
    templateUrl: './uploadfile.component.html'
})

export class UploadFile implements OnInit {
    uploadForm: FormGroup;
    loading: boolean;
    selectedfile: File = null;
    fileName = '';
    fileerror = '';
    filesize = '';
    currentdatatime;
    cValue;
    taxyear;
    profileemail;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];



    constructor(
        private el: ElementRef,
        public toasterService: ToastrService,
        public uploadservice: UploadService,
        private cookieService: CookieService,
        private router: Router,
        private _Activatedroute: ActivatedRoute,
        public fileservice:Form1099saService
    ) {
        this.currentdatatime = new Date();
        this.cValue = formatDate(this.currentdatatime, 'yyyy-MM-dd:hh:mm a', 'en-US');

    }

    fileControl = new FormControl([], FileValidators.required);


    ngOnInit() {

        this.uploadForm = new FormGroup({
            uploadfile: new FormControl(''),
            filedata: new FormControl(''),
            filepath: new FormControl(''),
            email: new FormControl(this.profileemail),
            description: new FormControl('', [Validators.required]),
            file_type: new FormControl('', [Validators.required]),
            formYear: new FormControl('2020', [Validators.required]),
        });

        this.gettaxyear();
        this.getprofile();
    }
    get f() {
        return this.uploadForm.controls;
    }


    gettaxyear() {
        this.uploadservice.getTaxyear().subscribe(res => {
            this.taxyear = JSON.parse(res.data);
        });
    }

    getprofile() {
        this.uploadservice.getProfilename().subscribe(res => {
            this.profileemail = res.email;
        });
    }

    selectedtaxyear(e) {
        this.uploadForm.get('formYear').setValue(e);
    }


    public onFileSelected(f) {
        this.fileName = f.target.files[0].name;
        const file: File = f.target.files[0];
        this.filesize = f.target.files[0].size;

        if (f.target.files.length > 0) {
            const type = f.target.files[0].type;

            if (type == 'text/plain') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else if (type == 'application/x-zip-compressed') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else if (type == 'application/vnd.ms-excel') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else {
                this.fileerror = 'Please upload correct format file';
            }

        }
    }



    public uploadfile() {
        if (this.fileName) {
            if (this.uploadForm.invalid) {
                for (const key of Object.keys(this.uploadForm.controls)) {
                    if (this.uploadForm.controls[key].invalid) {
                        const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                        invalidControl.focus();
                        break;
                    }
                }
                return;
            }
            let FormData = this.uploadForm.value;
            let createDatas = {
                    "UPLOAD_FILE_ID": 0,
                    "FILE_NAME": this.fileName,
                    "FILE_PATH": FormData.filedata,
                    "FILE_SIZE": this.filesize,
                    "RECORD_COUNT": 0,
                    "UPLOAD_DATE_TIME": this.cValue,
                    "FILE_STATUS": "",
                    "EMPLOYEE_ID": "",
                    "SITE_ID": this.CookieSiteId,
                    "TAX_YEAR": FormData.formYear,
                    "DESCRIPTION": FormData.description,
                    "FILE_USE": "",
                    "FILE_ENCRYPTION": "",
                    "DOWNLOAD_STATUS": "",
                    "FILE_TYPE": FormData.file_type,
                    "IP_ADDRESS": "",
                    "DATA_ANALYST": "",
                    "USER_TYPE": "",
                    "PROCESSING_NOTES": "",
                    "DATE_MAILED": "",
                    "PRESORT_CODE": "",
                    "FILE_NAME2": "",
                    "FILE_PATH2": "",
                    "FILE_SIZE2": 0,
                    "RECORD_COUNT2": 0
            };
            this.uploadservice
                .uploadfiles(createDatas)
                .subscribe(
                    (result) => {
                        if (result.isValid) {
                            this.toasterService.success(
                                result.message,
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                            this.uploadForm.reset();
                            this.selectedfile = null;
                            this.fileName = '';
                            this.filesize = '';
                            this.router.navigate(['filemanagement/uploadfile']);
                        } else {
                            this.toasterService.error(
                                'Please Try Again.',
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                            this.router.navigate(['filemanagement/uploadfile']);
                        }
                    },
                    (error) => {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['filemanagement/uploadfile']);
                    }
                );
        }
        else {
            this.fileerror = '* Plese Upload Document'
        }
    }


    public fileupload(){
        //   /console.log(this.uploadForm.value.uploadfile)
        this.fileservice.addFiles(this.uploadForm.value.file_type,this.uploadForm.value.description,
            this.uploadForm.value.filedata).subscribe(res => {
                alert('FileUploaded');
              })
    };

}