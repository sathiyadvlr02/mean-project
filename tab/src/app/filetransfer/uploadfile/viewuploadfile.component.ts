import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Form1099saService } from './../../services/form1099sa.service';


@Component({
    selector: 'app-viewupload',
    templateUrl: './viewuploadfile.component.html'
})
export class ViewfileComponent implements OnInit {
    
    selectedRow: any;
    displayedColumns: string[] = ['FileType', 'Description', 'download'];
    dataSource: MatTableDataSource<any>;


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private router: Router,
        private Form1099sa: Form1099saService,
        public toasterService:ToastrService
    ) {
        this.getAllDetails();
    }

    ngOnInit(): void {
    }

    applyFilter(filterValue) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }
    
    Addnew(){
        console.log('Hai');
        this.router.navigate(['/uploadfile'])
    }

    getAllDetails() {
        this.Form1099sa
            .Getuploadfiles()
            .subscribe(
                (result) => {
                    if (result) {
                        this.dataSource = new MatTableDataSource(<any>result);
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

    searchData(searchValue: any) {
        this.dataSource.filter = searchValue.trim().toLowerCase();
    }

    download(file){
        const link = document.createElement('a');
		link.setAttribute('target', '_blank');
		link.setAttribute('href', 'http://localhost:3000/file/getfiles/' + file);
		document.body.appendChild(link);
		link.click();
		link.remove();
    }

}