import { Routes } from '@angular/router';

import { FileManagementUploadFileComponent } from './filemanagementuploadfile/filemanagementuploadfile.component';
import { FileManagementDownloadFileComponent } from './filemanagementdownloadfile/filemanagementdownloadfile.component';
import { UploadFile } from './uploadfile/uploadfile.component';
import { DownloadFile } from './downloadfile/downloadfile.component';
import { ViewfileComponent } from './uploadfile/viewuploadfile.component';

export const FileTransferRoutes: Routes = [
    {
        path: 'filemanagementuploadfile/:id',
        component: FileManagementUploadFileComponent
    },
    {
        path: 'filemanagementdownloadfile',
        component: FileManagementDownloadFileComponent
    },
    {
        path: 'viewuploadfile',
        component: ViewfileComponent
    },
    {
        path: 'uploadfile',
        component: UploadFile
    },
    {
        path: 'downloadfile',
        component: DownloadFile
    },

];
