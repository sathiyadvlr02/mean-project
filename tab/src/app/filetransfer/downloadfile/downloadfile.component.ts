import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FileValidators } from "ngx-file-drag-drop";
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrComponentlessModule, ToastrService } from 'ngx-toastr';

import { DownloadService } from '../../services/download.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-download-file',
    templateUrl: './downloadfile.component.html'
})

export class DownloadFile implements OnInit {

    displayedColumns: string[] = ['tracking_number', 'client', 'file_name', 'notes', 'date_uploaded', 'download_status', 'action'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(
        public router: Router,
        public toasterService: ToastrService,
        private cookieService: CookieService,
        private el: ElementRef,
        public _downloadservice: DownloadService
    ){}

    ngOnInit() {
        this.getFileDetails();
    }

    searchData(searchValue: any) {
        this.dataSource.filter = searchValue.trim().toLowerCase();

    }

    getFileDetails() {
        this._downloadservice
            .getDownloadList()
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

    downloadfile(fileid) {
        const link = document.createElement('a');
		link.setAttribute('target', '_blank');
		link.setAttribute('href', '/api/v1/DownloadFile/DownloadFile/' + fileid);
		document.body.appendChild(link);
		link.click();
		link.remove();
    }

}