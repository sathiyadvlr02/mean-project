import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UploadService } from './.././../services/upload.service';


@Component({
    selector: 'app-filemanagementdownloadfile',
    templateUrl: './filemanagementdownloadfile.component.html'
})
export class FileManagementDownloadFileComponent implements OnInit {
    taxyear: FormGroup;
    selectedRow: any;
    displayedColumns: string[] = ['tracking_number', 'client', 'file_name', 'notes', 'date_uploaded', 'download_status', 'action'];
    dataSource: MatTableDataSource<any>;


    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    ngOnInit() {
        this.taxyear = new FormGroup({
            formYear: new FormControl('', [Validators.required]),
        });
    }

    constructor(
        private fb: FormBuilder,
        public router: Router,
        public toasterService: ToastrService,
        private cookieService: CookieService,
        private el: ElementRef,
        public uploadservice: UploadService
    ) { }

    get errors() {
        return this.taxyear.controls;
    }

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    yearList = [
        { name: 'Select State', value: '' },
        { name: '2020', value: '2020' },
        { name: '2019', value: '2019' },
        { name: '2018', value: '2018' },
        { name: '2017', value: '2017' },
        { name: '2016', value: '2016' },
        { name: '2015', value: '2015' },
        { name: '2014', value: '2014' },
        { name: '2013', value: '2013' },
        { name: '2012', value: '2012' },
        { name: '2011', value: '2011' },
        { name: '2010', value: '2010' },
        { name: '2005', value: '2005' },
    ];


    gettaxyear(event) {
        this.getFileDetails(event);
    }

    searchData(searchValue: any) {
        this.dataSource.filter = searchValue.trim().toLowerCase();

    }

    getFileDetails(taxyr) {
        this.uploadservice
            .getFiles(taxyr)
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

    downloadfile(fileid) {
        const link = document.createElement('a');
		link.setAttribute('target', '_blank');
		link.setAttribute('href', '/api/v1/UploadFile/DownloadFile/' + fileid);
		document.body.appendChild(link);
		link.click();
		link.remove();
    }

}

/* Static data */

export interface PeriodicElement {
    tracking_number: string;
    client: string;
    notes: string;
    date_uploaded: string;
    download_status: string;
    file_name: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        tracking_number: '11137-1', client: 'General Test Company 1',
        file_name: '11137-1099R-2012PROOF-ALL-J20140903153932ALL.pdf', notes: 'test notes',
        date_uploaded: '26/03/2015 15:26:37', download_status: 'Yes'
    },
    {
        tracking_number: '21345-2', client: 'General Test Company 1',
        file_name: '11137-1099R-2012PROOF-ALL-J20140903153932ALL.pdf', notes: 'test notes',
        date_uploaded: '26/03/2015 15:26:37', download_status: 'Yes'
    },
    {
        tracking_number: '34567-3', client: 'General Test Company 1',
        file_name: '11137-1099R-2012PROOF-ALL-J20140903153932ALL.pdf', notes: 'test notes',
        date_uploaded: '26/03/2015 15:26:37', download_status: 'Yes'
    }
];