import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { FileTransferRoutes } from './filetransfer.routing';
import { FileManagementUploadFileComponent } from './filemanagementuploadfile/filemanagementuploadfile.component';
import { FileManagementDownloadFileComponent } from './filemanagementdownloadfile/filemanagementdownloadfile.component';
import { UploadFile } from './uploadfile/uploadfile.component';
import { DownloadFile } from './downloadfile/downloadfile.component';
import { ViewfileComponent } from './uploadfile/viewuploadfile.component';
@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,    
    FormsModule ,
    ReactiveFormsModule ,
    RouterModule.forChild(FileTransferRoutes)
  ],
  declarations: [
    FileManagementUploadFileComponent,
    FileManagementDownloadFileComponent,
    UploadFile,
    DownloadFile,
    ViewfileComponent
  ]
})
export class FileTransferModule {}
