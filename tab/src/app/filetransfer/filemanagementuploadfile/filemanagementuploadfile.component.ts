import { Component, ElementRef, OnInit } from '@angular/core';
import { validateBasis } from '@angular/flex-layout';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { FileValidators } from "ngx-file-drag-drop";
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToastrComponentlessModule, ToastrService } from 'ngx-toastr';

import { UploadService } from '../../services/upload.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';




@Component({
    selector: 'app-filemanagementuploadfile',
    templateUrl: './filemanagementuploadfile.component.html'
})
export class FileManagementUploadFileComponent implements OnInit {
    uploadForm: FormGroup;
    loading: boolean;
    selectedfile: File = null;
    fileName = '';
    fileerror = '';
    filesize = '';
    currentdatatime;
    cValue;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    id: any;

    constructor(
        private el: ElementRef,
        public toasterService: ToastrService,
        public uploadservice: UploadService,
        private cookieService: CookieService,
        private router: Router,
        private _Activatedroute: ActivatedRoute,
    ) {
        this.id = this._Activatedroute.snapshot.paramMap.get("id");
        this.currentdatatime = new Date();
        this.cValue = formatDate(this.currentdatatime, 'yyyy-MM-dd:hh:mm a', 'en-US');       
    }


    file: string;
    email: string;
    description: string;
    file_type: string;


    fileControl = new FormControl([], FileValidators.required);



    ngOnInit() {

        this.uploadForm = new FormGroup({
            uploadfile: new FormControl(''),
            filedata : new FormControl(''),
            filepath: new FormControl(''),
            //email: new FormControl('', [Validators.required, Validators.email]),
            description: new FormControl('', [Validators.required]),
            file_type: new FormControl('', [Validators.required]),
            FILE_SIZE: new FormControl('', [Validators.required]),
            FILE_STATUS: new  FormControl(''),
            EMPLOYEE_ID:new  FormControl(''),
            TAX_YEAR: new  FormControl(''),
            FILE_USE: new  FormControl(''),
            FILE_ENCRYPTION: new  FormControl(''),
            DOWNLOAD_STATUS: new  FormControl(''),
            DATA_ANALYST:  new  FormControl(''),
            USER_TYPE: new  FormControl(''),
            PROCESSING_NOTES: new  FormControl(''),
            DATE_MAILED: new  FormControl(''),
            PRESORT_CODE: new  FormControl(''),
            FILE_NAME2: new  FormControl(''),
            FILE_PATH2: new  FormControl(''),
            FILE_SIZE2: new  FormControl(''),
            RECORD_COUNT2: new  FormControl(''),
            RECORD_COUNT: new  FormControl('')
        })

        if (this.id) {
            this.uploadservice
                .getSingleFileDetails(this.id)
                .subscribe(
                    (result) => {
                        console.log(result.data);
                        if (result.isValid) {
                            this.fileName = JSON.parse(result.data).FILE_NAME;
                            this.uploadForm.get('filedata').setValue(JSON.parse(result.data).FILE_PATH)
                            this.uploadForm.get('description').setValue(JSON.parse(result.data).DESCRIPTION);
                            this.uploadForm.get('file_type').setValue(JSON.parse(result.data).FILE_TYPE);
                            this.uploadForm.get('FILE_STATUS').setValue(JSON.parse(result.data).FILE_STATUS);
                            this.uploadForm.get('FILE_SIZE').setValue(JSON.parse(result.data).FILE_SIZE);
                            this.uploadForm.get('EMPLOYEE_ID').setValue(JSON.parse(result.data).EMPLOYEE_ID);
                            this.uploadForm.get('TAX_YEAR').setValue(JSON.parse(result.data).TAX_YEAR);
                            this.uploadForm.get('FILE_USE').setValue(JSON.parse(result.data).FILE_USE);
                            this.uploadForm.get('FILE_ENCRYPTION').setValue(JSON.parse(result.data).FILE_ENCRYPTION);
                            this.uploadForm.get('DOWNLOAD_STATUS').setValue(JSON.parse(result.data).DOWNLOAD_STATUS);
                            this.uploadForm.get('DATA_ANALYST').setValue(JSON.parse(result.data).DATA_ANALYST);
                            this.uploadForm.get('USER_TYPE').setValue(JSON.parse(result.data).USER_TYPE);
                            this.uploadForm.get('PROCESSING_NOTES').setValue(JSON.parse(result.data).PROCESSING_NOTES);
                            this.uploadForm.get('DATE_MAILED').setValue(JSON.parse(result.data).DATE_MAILED);
                            this.uploadForm.get('PRESORT_CODE').setValue(JSON.parse(result.data).PRESORT_CODE);
                            this.uploadForm.get('FILE_NAME2').setValue(JSON.parse(result.data).FILE_NAME2);
                            this.uploadForm.get('FILE_PATH2').setValue(JSON.parse(result.data).FILE_PATH2);
                            this.uploadForm.get('FILE_SIZE2').setValue(JSON.parse(result.data).FILE_SIZE2);
                            this.uploadForm.get('RECORD_COUNT2').setValue(JSON.parse(result.data).RECORD_COUNT2);
                            this.uploadForm.get('RECORD_COUNT').setValue(JSON.parse(result.data).RECORD_COUNT);
                        }
                    },
                    (error) => {
                        // console.log('error', error)
                    }
                );
        }


    }
    get f() {
        return this.uploadForm.controls;
    }
  
    

    public onFileSelected(f) {
        this.fileName = f.target.files[0].name;
        const file: File = f.target.files[0];
        this.filesize = f.target.files[0].size;

        if (f.target.files.length > 0) {
            const type = f.target.files[0].type;

            if (type == 'text/plain') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
                console.log(this.uploadForm.value.filedata);
            }
            else if (type == 'application/x-zip-compressed') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else if (type == 'application/vnd.ms-excel') {
                this.fileerror = '';
                this.uploadForm.get('filedata').setValue(file)
            }
            else {
                this.fileerror = 'Please upload correct format file';
            }

        }
    }



    public uploadfile() {
        if (this.fileName) {
            if (this.uploadForm.invalid) {
                for (const key of Object.keys(this.uploadForm.controls)) {
                    if (this.uploadForm.controls[key].invalid) {
                        const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                        invalidControl.focus();
                        break;
                    }
                }
                return;
            }
            let FormData = this.uploadForm.value;
            let createDatas = {
                "UPLOAD_FILE_ID": 0,
                "FILE_NAME": this.fileName,
                "FILE_PATH": FormData.filedata,
                "FILE_SIZE": FormData.FILE_SIZE ,
                "RECORD_COUNT": FormData.RECORD_COUNT,
                "UPLOAD_DATE_TIME": this.cValue,
                "FILE_STATUS": FormData.FILE_STATUS,
                "EMPLOYEE_ID": FormData.EMPLOYEE_ID,
                "SITE_ID": this.CookieSiteId,
                "TAX_YEAR": FormData.TAX_YEAR,
                "DESCRIPTION": FormData.description,
                "FILE_USE": FormData.FILE_USE,
                "FILE_ENCRYPTION": FormData.FILE_ENCRYPTION,
                "DOWNLOAD_STATUS": FormData.DOWNLOAD_STATUS,
                "FILE_TYPE": FormData.file_type,
                "IP_ADDRESS": "",
                "DATA_ANALYST": FormData.DATA_ANALYST,
                "USER_TYPE": FormData.DATA_ANALYST,
                "PROCESSING_NOTES": FormData.PROCESSING_NOTES,
                "DATE_MAILED": FormData.DATE_MAILED,
                "PRESORT_CODE": FormData.PRESORT_CODE,
                "FILE_NAME2": FormData.FILE_NAME2,
                "FILE_PATH2": FormData.FILE_PATH2,
                "FILE_SIZE2": FormData.FILE_SIZE2,
                "RECORD_COUNT2": FormData.RECORD_COUNT2
            };
           console.log(createDatas);
            // this.uploadservice
            //     .uploadfiles(createDatas)
            //     .subscribe(
            //         (result) => {
            //             if (result.isValid) {
            //                 this.toasterService.success(
            //                     result.message,
            //                     '',
            //                     {
            //                         positionClass: 'toast-top-center',
            //                         timeOut: 3000
            //                     }
            //                 );
            //                 this.router.navigate(['/filemanagementdownloadfile']);
            //             } else {
            //                 this.toasterService.error(
            //                     'Please Try Again.',
            //                     '',
            //                     {
            //                         positionClass: 'toast-top-center',
            //                         timeOut: 3000
            //                     }
            //                 );
            //                 this.router.navigate(['/filemanagementdownloadfile']);
            //             }
            //         },
            //         (error) => {
            //             this.toasterService.error(
            //                 'Please Try Again.',
            //                 '',
            //                 {
            //                     positionClass: 'toast-top-center',
            //                     timeOut: 3000
            //                 }
            //             );
            //             this.router.navigate(['/filemanagementdownloadfile']);
            //         }
            //     );
        }
        else {
            this.fileerror = '* Plese Upload Document'
        }
    }

}
