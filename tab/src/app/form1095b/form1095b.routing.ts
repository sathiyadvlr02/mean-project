import { Routes } from '@angular/router';

import { Create1095BComponent } from './create1095bform/create1095b.component';
import { Update1095BComponent } from './update1095bform/update1095b.component';
import { Histroy1095BComponent } from './history1095bform/history1095b.component';
import { FormEmail1095B } from './email1095bform/email1095b.component';
 
export const Form1095BRoutes: Routes = [
    
    {
        path: 'print1095b',
        component: Create1095BComponent
    }, 
    {
        path: 'create1095b',
        component: Create1095BComponent
    }, 
    {
        path: 'update1095b',
        component: Update1095BComponent
    }, 
    {
        path: 'history1095b',
        component: Histroy1095BComponent
    },
    {
        path: 'Email1095B',
        component: FormEmail1095B
    }

];
