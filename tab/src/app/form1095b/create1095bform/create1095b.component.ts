import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';

@Component({
  selector: 'app-create1095b',
  templateUrl: './create1095b.component.html',
  styleUrls: ['./create1095b.component.scss']

})
export class Create1095BComponent implements OnInit {
  create1095bForm: FormGroup;
  CIselectedRow: any;
  stateList;
  country;
  stringDate

  CIdisplayedColumns: string[] = ['Name', 'SSN', 'DOB', 'Allmonths', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'action'];
  CIdataSource = CI_ELEMENT_DATA;

  @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

  constructor(
    public navService: NavService,
    public router: Router,
    private fb: FormBuilder,
    public toasterService: ToastrService,
    private el: ElementRef,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    public StateServices: StateService,
  ) { }
  private CookieUserName = this.cookieService.get('username');
  private CookieSiteId = this.CookieUserName.split('_')[0];

  ngOnInit(): void {
    this.create1095bForm = this.fb.group({
      IssuerName: new FormControl('',Validators.required),
      issuerAddress1: new FormControl('',Validators.required),
      issuerAddress2 : new FormControl(), 
      issuerCity: new FormControl('',Validators.required),
      issuerState: new FormControl('',Validators.required),
      issuerzip: new FormControl('',Validators.required),
      ForeignCountry: new FormControl(false),
      Country: new FormControl(),
      provinceCode: new FormControl(),
      postalcode: new FormControl(),
      firstName: new FormControl('',Validators.required),
      Middlename: new FormControl(),
      Lastname: new FormControl('',Validators.required),
      suffix: new FormControl(),
      VOID: new FormControl(),
      corrected: new FormControl(),
      federalID: new FormControl(),
      telephone: new FormControl('',Validators.required),
      policyID: new FormControl('',Validators.required),
      policyfirstname: new FormControl('',Validators.required),
      policymiddlename: new FormControl(),
      policylastname: new FormControl('',Validators.required),
      policysuffix: new FormControl(),
      policyaddress: new FormControl('',Validators.required),
      policyaddress2: new FormControl(),
      policycity: new FormControl('',Validators.required),
      policystate: new FormControl('',Validators.required),
      policyzip: new FormControl(''),
      policyForeigncountry: new FormControl(false),
      policyCountry: new FormControl(),
      policyProvinceCode: new FormControl(),
      policypostalcode: new FormControl(),
      Reserved: new FormControl(),
      EmployerName: new FormControl(),
      employeeEIN: new FormControl('',Validators.required),
      employeeAddress: new FormControl(),
      employeeeAddress2: new FormControl(),
      employeeCity: new FormControl(),
      employeestate: new FormControl(),
      employeeZip: new FormControl(),
      employeeForeignCountry: new FormControl(false),
      employeeCountry: new FormControl(),
      employeeProvinceCode: new FormControl(),
      provincePostalCode: new FormControl(),
      PolicyOriginCode: new FormControl('',Validators.required),
      SSN: new FormControl('',Validators.required),
      DOB: new FormControl('',Validators.required)
    });

    this.getstatelist();
  }
  get trrowErrors() { return this.create1095bForm.controls; }

  getstatelist() {
    this.StateServices.getStatelist().subscribe(
        result => {
            this.stateList = JSON.parse(result.data);
        });
        this.getCountry();
}

getCountry() {
    this.StateServices.getCountry().subscribe(
        result => {
            this.country = JSON.parse(result.data);
        });
}
dateChangeHandler(date: Date){
   this.stringDate = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
  //this.create1095bForm.get('DOB').setValue(stringDate);
}

AddForm(){
  this.create1095bForm.get("DOB").setValue(this.stringDate);
  console.log(this.create1095bForm.value);
}

}



export interface CIElement {
  Name: string;
  SSN: string;
  DOB: string;
  Allmonths: string;
  Jan: string;
  Feb: string;
  Mar: string;
  Apr: string;
  May: string;
  Jun: string;
  Jul: string;
  Aug: string;
  Sep: string;
  Oct: string;
  Nov: string;
  Dec: string;
}

const CI_ELEMENT_DATA: CIElement[] = [
  { Name: '', SSN: '', DOB: '', Allmonths: '', Jan: '', Feb: '', Mar: '', Apr: '', May: '', Jun: '', Jul: '', Aug: '', Sep: '', Oct: '', Nov: '', Dec: '' },
  { Name: '', SSN: '', DOB: '', Allmonths: '', Jan: '', Feb: '', Mar: '', Apr: '', May: '', Jun: '', Jul: '', Aug: '', Sep: '', Oct: '', Nov: '', Dec: '' },
  { Name: '', SSN: '', DOB: '', Allmonths: '', Jan: '', Feb: '', Mar: '', Apr: '', May: '', Jun: '', Jul: '', Aug: '', Sep: '', Oct: '', Nov: '', Dec: '' }

];
