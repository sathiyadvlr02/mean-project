import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1095BRoutes } from './form1095b.routing';
import { ChartistModule } from 'ng-chartist';

import { Create1095BComponent } from './create1095bform/create1095b.component'; 
import { Update1095BComponent } from './update1095bform/update1095b.component';
import { Histroy1095BComponent } from './history1095bform/history1095b.component';
import { SetPassword } from './email1095bform/setpassword.component';
import { FormEmail1095B } from './email1095bform/email1095b.component';


//import { Update1099SAComponent } from './update1099saform/update1099sa.component'; 
//import { History1099SAComponent } from './history1099saform/history1099sa.component'; 


@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(Form1095BRoutes)
  ],

  declarations: [Create1095BComponent,Update1095BComponent,
    Histroy1095BComponent,SetPassword,FormEmail1095B]

})
export class Form1095BModule {}
