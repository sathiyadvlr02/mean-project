export interface Menu {
    state: string;
    name: string;
    type: string;
    icon: string;
    children?: Menu[];
}