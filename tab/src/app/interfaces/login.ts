export interface Login {
  siteId:string;
    username: string;
    password: string;
    rememberMe: boolean;
    grant_type: string;
}
