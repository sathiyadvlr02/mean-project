import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';
import { Form1099saService } from './../../services/form1099sa.service';

@Component({
    selector: 'app-update1099sa',
    templateUrl: './update1099sa.component.html',
    styleUrls: ['./update1099sa.component.scss'],

})
export class Update1099SAComponent implements OnInit {
    createSAForm: FormGroup;
    stateList;
    taxyear;
    dataid;
    data: any;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices: StateService,
        public formservice: Form1099saService,
    ) { }

    ngOnInit(): void {
        this.dataid = this.route.snapshot.paramMap.get("id");

        this.getdatabyid(this.dataid)

        this.createSAForm = this.fb.group({
            _id: new FormControl(''),
            corrected: new FormControl(''),
            payerName: new FormControl('', [Validators.required]),
            payeraddress1: new FormControl('', [Validators.required]),
            payeraddress3: new FormControl(''),
            payeraddress2: new FormControl('', [Validators.required]),
            payercity: new FormControl('', [Validators.required]),
            payerState: new FormControl('', [Validators.required]),
            payerzip: new FormControl('', [Validators.required]),
            payerphone: new FormControl('', [Validators.required]),
            payerphone2: new FormControl(''),
            payerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            recipientno: new FormControl('', [Validators.required]),
            recipientname: new FormControl('', [Validators.required]),
            recipientname2: new FormControl(''),
            recipientaddress1: new FormControl('', [Validators.required]),
            recipientaddress2: new FormControl('', [Validators.required]),
            recipientaddress3: new FormControl(''),
            recipientcity: new FormControl('', [Validators.required]),
            recipientState: new FormControl('', [Validators.required]),
            recipientzip: new FormControl('', [Validators.required]),
            acctno: new FormControl('', [Validators.required]),
            grossdistribution: new FormControl(''),
            earningsonexcess: new FormControl(''),
            distributioncode: new FormControl(''),
            FMW: new FormControl(''),
            HSA: new FormControl(''),
            archerMSA: new FormControl(''),
            MAMSA: new FormControl(''),
            releaseDate: new FormControl(''),
            consentDate: new FormControl(''),
            memo: new FormControl('')
        });
        this.taxyear = 2005;
        this.getstatelist();
    }

    get trrowErrors() { return this.createSAForm.controls; }


    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }



    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    getdatabyid(id) {
        this.formservice.getbyid(id).subscribe(res => {
            this.data = res;
            if (this.data) {
                this.createSAForm.get('_id').setValue(this.data._id);
                this.createSAForm.get('corrected').setValue(this.data.corrected);
                this.createSAForm.get('payerName').setValue(this.data.payerName);
                this.createSAForm.get('payeraddress1').setValue(this.data.payeraddress1);
                this.createSAForm.get('payeraddress2').setValue(this.data.payeraddress2);
                this.createSAForm.get('payercity').setValue(this.data.payercity);
                this.createSAForm.get('payerState').setValue(this.data.payerState);
                this.createSAForm.get('payerzip').setValue(this.data.payerzip);
                this.createSAForm.get('payerphone').setValue(this.data.payerphone);
                this.createSAForm.get('payerFederalIdentificationNumber').setValue(this.data.payerFederalIdentificationNumber);
                this.createSAForm.get('recipientno').setValue(this.data.recipientno);
                this.createSAForm.get('recipientname').setValue(this.data.recipientname);
                this.createSAForm.get('recipientaddress1').setValue(this.data.recipientaddress1);
                this.createSAForm.get('recipientState').setValue(this.data.recipientState);
                this.createSAForm.get('recipientcity').setValue(this.data.recipientcity);
                this.createSAForm.get('recipientzip').setValue(this.data.recipientzip);
                this.createSAForm.get('acctno').setValue(this.data.acctno);
                this.createSAForm.get('distributioncode').setValue(this.data.distributioncode);
                this.createSAForm.get('earningsonexcess').setValue(this.data.earningsonexcess);
                this.createSAForm.get('archerMSA').setValue(this.data.archerMSA);
                this.createSAForm.get('grossdistribution').setValue(this.data.grossdistribution);
                this.createSAForm.get('releaseDate').setValue(this.data.releaseDate);
                this.createSAForm.get('consentDate').setValue(this.data.consentDate);
                this.createSAForm.get('memo').setValue(this.data.memo);
            }
        });
    }

    onSubmit() {
        let data = this.createSAForm.value;
        this.formservice.updatedata(data).subscribe(res => {
            this.toasterService.success(
                <any>res,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
            );
        });
        this.router.navigate(['/viewform1099sa']);
    }
}


