import { Routes } from '@angular/router';
 
import { Create1099SAComponent } from './create1099saform/create1099sa.component'; 
import { Update1099SAComponent } from './update1099saform/update1099sa.component'; 
import { History1099SAComponent } from './history1099saform/history1099sa.component'; 
import { FormEmail1099SA } from './email1099saform/email1099sa.component';
import { Form1099saComponent } from './form1099samanagement';

export const Form1099SARoutes: Routes = [
   
  {
    path: 'viewform1099sa',
    component: Form1099saComponent  
  },
  {
    path: 'form1099sacreate',
    component: Create1099SAComponent  
  },
  {
    path: 'form1099saupdate/:id',
    component: Update1099SAComponent 
  },
  {
    path: 'form1099sahistory',
    component: History1099SAComponent 
  },
  {
    path: 'email1099sa',
    component: FormEmail1099SA  
  }

];
