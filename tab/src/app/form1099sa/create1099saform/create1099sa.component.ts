import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';
import { Form1099saService } from './../../services/form1099sa.service';

@Component({
    selector: 'app-create1099sa',
    templateUrl: './create1099sa.component.html',
    styleUrls: ['./create1099sa.component.scss'],

})
export class Create1099SAComponent implements OnInit {
    createSAForm: FormGroup;
    stateList;
    taxyear;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices: StateService,
        public form1099sa:Form1099saService
    ) { }

    ngOnInit(): void {
        this.createSAForm = this.fb.group({
            corrected: new FormControl(''),
            payerName: new FormControl('', [Validators.required]),
            payeraddress1: new FormControl('', [Validators.required]),
            payeraddress3: new FormControl(''),
            payeraddress2: new FormControl('', [Validators.required]),
            payercity: new FormControl('', [Validators.required]),
            payerState: new FormControl('', [Validators.required]),
            payerzip: new FormControl('', [Validators.required]),
            payerphone: new FormControl('', [Validators.required]),
            payerphone2: new FormControl(''),
            payerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            recipientno: new FormControl('', [Validators.required]),
            recipientname: new FormControl('', [Validators.required]),
            recipientname2: new FormControl(''),
            recipientaddress1: new FormControl('', [Validators.required]),
            recipientaddress2: new FormControl('', [Validators.required]),
            recipientaddress3: new FormControl(''),
            recipientcity: new FormControl('', [Validators.required]),
            recipientState: new FormControl('', [Validators.required]),
            recipientzip: new FormControl('', [Validators.required]),
            acctno: new FormControl('', [Validators.required]),
            grossdistribution: new FormControl(''),
            earningsonexcess: new FormControl(''),
            distributioncode: new FormControl(''),
            FMW: new FormControl(''),
            HSA: new FormControl(''),
            archerMSA: new FormControl(''),
            MAMSA: new FormControl(''),
        });
        this.taxyear = 2005;
        this.getstatelist();
    }

    get trrowErrors() { return this.createSAForm.controls; }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList = JSON.parse(result.data);
            });
    }



    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }
    onSubmit() {
        let data = this.createSAForm.value;
        this.form1099sa.createForm1099(data).subscribe(res=>{
            this.toasterService.success(
                <any>res,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
              );
        });
        this.router.navigate(['/viewform1099sa']);
    }
}

