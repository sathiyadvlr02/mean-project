import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule} from '@angular/forms';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1099SARoutes } from './form1099sa.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099SAComponent } from './create1099saform/create1099sa.component'; 
import { Update1099SAComponent } from './update1099saform/update1099sa.component'; 
import { History1099SAComponent } from './history1099saform/history1099sa.component'; 
import { FormEmail1099SA } from './email1099saform/email1099sa.component';
import { SetPassword } from './email1099saform/setpassword.component';
import { Form1099saComponent } from './form1099samanagement';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Form1099SARoutes)
  ],
  declarations: [Create1099SAComponent,Update1099SAComponent ,
    History1099SAComponent,FormEmail1099SA,SetPassword,Form1099saComponent]
})
export class Form1099SAModule {}
