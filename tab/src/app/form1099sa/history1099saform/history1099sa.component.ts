import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';
@Component({
  selector: 'app-history1099sa',
  templateUrl: './history1099sa.component.html',
  
})
export class History1099SAComponent implements OnInit {
    createSAForm: FormGroup;
    stateList;
    taxyear;
  
  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(
    public navService: NavService,
    public router: Router,
    private fb: FormBuilder,
    public toasterService: ToastrService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    public StateServices: StateService,
) { }

  ngOnInit(): void {
    this.createSAForm = this.fb.group({
        corrected: new FormControl(''),
        payerName: new FormControl('', [Validators.required]),
        payeraddress1: new FormControl('', [Validators.required]),
        payeraddress3: new FormControl(''),
        payeraddress2: new FormControl('', [Validators.required]),
        payercity: new FormControl('', [Validators.required]),
        payerState: new FormControl('', [Validators.required]),
        payerzip: new FormControl('', [Validators.required]),
        payerphone: new FormControl('', [Validators.required]),
        payerphone2: new FormControl(''),
        payerFederalIdentificationNumber: new FormControl('', [Validators.required]),
        recipientno: new FormControl('', [Validators.required]),
        recipientname: new FormControl('', [Validators.required]),
        recipientname2: new FormControl(''),
        recipientaddress1: new FormControl('', [Validators.required]),
        recipientaddress2: new FormControl('', [Validators.required]),
        recipientaddress3: new FormControl(''),
        recipientcity: new FormControl('', [Validators.required]),
        recipientState: new FormControl('', [Validators.required]),
        recipientzip: new FormControl('', [Validators.required]),
        acctno: new FormControl('', [Validators.required]),
        grossdistribution: new FormControl(''),
        earningsonexcess: new FormControl(''),
        distributioncode: new FormControl(''),
        FMW: new FormControl(''),
        HSA: new FormControl(''),
        archerMSA: new FormControl(''),
        MAMSA: new FormControl(''),
        releaseDate: new FormControl(''),
        eConsentDate: new FormControl(''),
        memo: new FormControl('')
    });
    this.getstatelist();
    this.taxyear=2020;
}

get trrowErrors() { return this.createSAForm.controls; }


getstatelist() {
    this.StateServices.getStatelist().subscribe(
        result => {
            this.stateList = JSON.parse(result.data);
        });
}

}

