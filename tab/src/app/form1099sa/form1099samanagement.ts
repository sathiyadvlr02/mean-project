import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Form1099saService } from './../services/form1099sa.service';


@Component({
    selector: 'app-form1099sa',
    templateUrl: './form1099samanagement.html'
})
export class Form1099saComponent implements OnInit {
    
    selectedRow: any;
    displayedColumns: string[] = ['PAYER_NAME', 'PAYER_ADDRESS', 'PAYER_STATE', "ACCOUNTNO","RECIPIENTNAME","action"];
    dataSource: MatTableDataSource<any>;


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private router: Router,
        private Form1099sa: Form1099saService,
        public toasterService:ToastrService
    ) {
        this.getAllDetails();
    }

    ngOnInit(): void {
    }

    applyFilter(filterValue) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }
    
    Addnew(){
        console.log('Hai');
        this.router.navigate(['/form1099sacreate'])
    }

    getAllDetails() {
        this.Form1099sa
            .getformdata()
            .subscribe(
                (result) => {
                    if (result) {
                        this.dataSource = new MatTableDataSource(<any>result);
                        this.dataSource.paginator = this.paginator;
                        this.dataSource.sort = this.sort;
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );
    }

    searchData(searchValue: any) {
        this.dataSource.filter = searchValue.trim().toLowerCase();
    }

    deleteAction(id){
     this.Form1099sa.deletedata(id).subscribe(res=>{
         if(res){
             this.getAllDetails();
        this.toasterService.success(
            <any>res,
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
        }
     });
    
    }
}