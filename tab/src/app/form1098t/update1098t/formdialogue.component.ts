import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { TCRSService } from './../../services/TCRS.service';
import { CookieService } from 'ngx-cookie-service';

import moment from 'moment';

export interface DialogData {
    tax_year: any,
    box_value: any,
    stu_id: any,
    stu_name: any;
}

@Component({
    selector: 'form-dialogue',
    templateUrl: './formdialogue.component.html',
    
})

export class UpdateFormDialogue implements OnInit {

    formMethod = 'list';
    formSubMethod = '';
    createSiteForm: FormGroup;
    displayedColumns: string[] = ['Transaction_Date', 'Campus_Code', 'Program_Name', 'Student_LastName',
     'Financial_Type','Academic_Term','Academic_Year','Amount','action'];
     dataSource: MatTableDataSource<any>;

     editValue = '';
     sqUid = 0;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];
    programcode;
    Financialcode;
    accTerm;
    accYear;

    constructor(
        private fb: FormBuilder,
        public tcrs_service: TCRSService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private el: ElementRef,
        private cookieService: CookieService,
    ){
        this.formMethod = 'list';
    }

    ngOnInit(): void {
        this.createSiteForm = this.fb.group({
           
            tdate: new FormControl(''),
            pname: new FormControl('', [Validators.required]),
            ccode: new FormControl('', [Validators.required]),
            sname: new FormControl(this.data.stu_name, [Validators.required]),
            financialtype: new FormControl('', [Validators.required]),
            financialdesp: new FormControl('', [Validators.required]),
            academic: new FormControl('', [Validators.required]),
            academicterm: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
          });
          this.getTCRSsetting();
          this.getAllData();
          this.getSequeceId();
    }
    get trrowErrors() { return this.createSiteForm.controls; }

    getSequeceId(){
        this.tcrs_service.getSequence(this.data.stu_id, this.data.tax_year).subscribe(
            (result) => {
                if(result.isValid){
                    this.sqUid = parseInt(JSON.parse(result.data).sequence_id)
                }
                console.log('this.sqUid', this.sqUid)
            },
            (error) => {
                console.log('error', error)
            }
        );
    }

    formDataEmpty(){
        this.createSiteForm.get('tdate').setValue('');
        this.createSiteForm.get('pname').setValue('');
        this.createSiteForm.get('financialtype').setValue('');
        this.createSiteForm.get('financialdesp').setValue('');
        this.createSiteForm.get('academic').setValue('');
        this.createSiteForm.get('academicterm').setValue('');
        this.createSiteForm.get('amount').setValue('');
    }

    deleteTCRS(ele){
        this.tcrs_service.deleteTCRSFormData(ele.STUDENT_ID, ele.SEQUENCE_ID, parseInt(ele.TAX_YEAR)).subscribe(
            (result) => {
                console.log('result', result)
                this.getAllData();
            },
            (error) => {
                console.log('error', error)
            }
        );
    }

    mainSubmit(){
        let value = 0;
        if(this.dataSource){
            if(this.dataSource.filteredData){
                let tempData1 = this.dataSource.filteredData
                tempData1.map((itm, i) => {
                    value = value + parseFloat(itm.AMOUNT)
                })
                let tempData = {
                    total: value,
                    data: this.dataSource.filteredData,
                }
                return JSON.stringify(tempData);
            }
        }
    }

    actionSubmit(){
        if (this.createSiteForm.invalid) {
            for (const key of Object.keys(this.createSiteForm.controls)) {
                if (this.createSiteForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }
        let tempFormData = this.createSiteForm.value;
        
        if(this.formSubMethod == 'add'){
            let typeVal = tempFormData.financialtype.VALUE;
            let tempData = {
                studenT_ID: this.data.stu_id,
                sequencE_ID: this.sqUid,
                databasE_LOAD_ID: "",
                sitE_ID: parseInt(this.CookieSiteId),
                status: "ONLINE",
                type: "",
                onlinE_UPDATE_IND: "",
                taX_YEAR: this.data.tax_year,
                controL_NUMBER: "",
                campuS_CODE: tempFormData.ccode,
                prograM_CODE: tempFormData.pname,
                studenT_SSN: "",
                transactioN_DATE: moment(tempFormData.tdate).format('MM-DD-YYYY'),
                transactioN_TYPE: "",
                studenT_LAST_NAME: tempFormData.sname,
                studenT_REGISTRATION_ID: "",
                transactioN_ID: "",
                financiaL_TYPE_CODE: typeVal.slice(0, 1),
                financiaL_SUB_TYPE_CODE: typeVal.substring(1),
                academiC_TERM: tempFormData.academicterm,
                academiC_YEAR: tempFormData.academic,
                description: tempFormData.financialdesp,
                amount: parseFloat(tempFormData.amount).toFixed(2),
                namE_VALIDATION: "",
                vieweD_INDICATOR: "",
                printeD_INDICATOR: "",
                efileD_INDICATOR: "",
                doubtfuL_ADDRESSES: ""
            };
            this.addTCRS(tempData);
            this.formDataEmpty();
        } else {
            let typeVal = tempFormData.financialtype.VALUE;
            let tempData = {
                studenT_ID: this.data.stu_id,
                sequencE_ID: this.editValue['SEQUENCE_ID'] ? this.editValue['SEQUENCE_ID'] : 0,
                databasE_LOAD_ID: "",
                sitE_ID: parseInt(this.CookieSiteId),
                status: "ONLINE",
                type: "",
                onlinE_UPDATE_IND: "",
                taX_YEAR: this.data.tax_year,
                controL_NUMBER: "",
                campuS_CODE: tempFormData.ccode,
                prograM_CODE: tempFormData.pname,
                studenT_SSN: "",
                transactioN_DATE: moment(tempFormData.tdate).format('MM-DD-YYYY'),
                transactioN_TYPE: "",
                studenT_LAST_NAME: tempFormData.sname,
                studenT_REGISTRATION_ID: "",
                transactioN_ID: "",
                financiaL_TYPE_CODE: typeVal.slice(0, 1),
                financiaL_SUB_TYPE_CODE: typeVal.substring(1),
                academiC_TERM: tempFormData.academicterm,
                academiC_YEAR: tempFormData.academic,
                description: tempFormData.financialdesp,
                amount: parseFloat(tempFormData.amount).toFixed(2),
                namE_VALIDATION: "",
                vieweD_INDICATOR: "",
                printeD_INDICATOR: "",
                efileD_INDICATOR: "",
                doubtfuL_ADDRESSES: ""
            };
            this.updateTCRS(tempData);
            this.editValue = '';
            this.formDataEmpty();
        }
        this.changePopUpMode('list', '')
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    updateTCRS(tempData){
        this.tcrs_service.updateTCRSFormData(tempData).subscribe(
            (result) => {
                this.getAllData();
                console.log('result', result)
            },
            (error) => {
                console.log('error', error)
            }
        );
    }

    addTCRS(tempData){
        this.tcrs_service.createTCRSFormData(tempData).subscribe(
            (result) => {
                this.getAllData();
                this.getSequeceId();
                console.log('result', result)
            },
            (error) => {
                console.log('error', error)
            }
        );
    }

    onClickEdit(ele) {
        this.editValue = ele;

        this.tcrs_service.GetSingleTCRSData(this.data.stu_id, ele.SEQUENCE_ID, this.data.tax_year).subscribe(
            result => {
                if(result.isValid){
                    let tempFinancial = {};
                    this.Financialcode.map((itm, i) => {
                        if(itm.VALUE == JSON.parse(result.data).FINANCIAL_TYPE_CODE + JSON.parse(result.data).FINANCIAL_SUB_TYPE_CODE){
                            tempFinancial = itm;
                        }
                    })
                    this.createSiteForm.get('tdate').setValue(new Date(JSON.parse(result.data).TRANSACTION_DATE));
                    this.createSiteForm.get('pname').setValue(JSON.parse(result.data).PROGRAM_CODE);
                    this.createSiteForm.get('ccode').setValue(JSON.parse(result.data).CAMPUS_CODE);
                    this.createSiteForm.get('sname').setValue(JSON.parse(result.data).STUDENT_LAST_NAME);
                    this.createSiteForm.get('financialtype').setValue(tempFinancial);
                    this.createSiteForm.get('financialdesp').setValue(JSON.parse(result.data).DESCRIPTION);
                    this.createSiteForm.get('academic').setValue(JSON.parse(result.data).ACADEMIC_YEAR);
                    this.createSiteForm.get('academicterm').setValue(JSON.parse(result.data).ACADEMIC_TERM);
                    this.createSiteForm.get('amount').setValue(JSON.parse(result.data).AMOUNT);
                }
            });

        this.formMethod = 'action';
        this.formSubMethod = 'edit';
    }

    getAllData(){
        this.tcrs_service.GetAllTCRSData(this.data.stu_id, this.data.tax_year).subscribe(
            result => {
                if(result.isValid){
                    //this.sqUid = JSON.parse(result.data).length - 1;
                    
                    let tempFinanceArray = [];
                    this.Financialcode.map((itm, i) => {
                        tempFinanceArray.push(itm.VALUE.slice(0, 1))
                    })
                    var newArray = JSON.parse(result.data).filter(function (el) {
                        return tempFinanceArray.includes(el.FINANCIAL_TYPE_CODE);
                    });
                    
                    this.dataSource = new MatTableDataSource(newArray);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                }
            });
    }


    changePopUpMode(type, sub_type){
        this.formMethod = type;
        this.formSubMethod = sub_type;
        if(type == 'list'){
            this.editValue = '';
            this.formDataEmpty()
        }
    }

    getTCRSsetting() {
        this.tcrs_service.GetTCRSSettings(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    let a = JSON.parse(result.data);
                    this.createSiteForm.get('ccode').setValue(a[0].CAMPUS_CODE);
                }
            });

        this.getProgramcode();
    }

    getProgramcode() {
        this.tcrs_service.GetProgramCode(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    this.programcode = JSON.parse(result.data);
                }
            });   
            this.getFinancialCode();    
    }
    getFinancialCode() {
        this.tcrs_service.GetFinancialCode(this.data.tax_year,this.data.box_value).subscribe(
            result => {
                if (result.isValid) {
                    this.Financialcode = JSON.parse(result.data);
                }
            });  
            this.getAcademicterm();    
    }

    selectedType(e){
     this.createSiteForm.get('financialdesp').setValue(e.value.DISPLAY_NAME);
     //console.log(this.createSiteForm);   
    }

    getAcademicterm() {
        this.tcrs_service.GetAcademic(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    this.accTerm = JSON.parse(result.data)
                    //this.accYear = JSON.parse(result.data)
                    this.accYear = [...new Map(JSON.parse(result.data).map(item => [item['ACADEMIC_YEAR'], item])).values()]
                }
                console.log('acc', this.accTerm)
            });      
    }
    
}

export interface PeriodicElement {
    Transaction_Date: string;
    Campus_Code: string;
    Program_Name: string;
    Student_LastName: string;
    Financial_Type: string;
    Academic_Term: string;
    Academic_Year: string;
    Financial_Description: string;
    Amount: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'ALL PAYMENTS', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION AND FEES (a)', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'SCHOLARSHIP', Amount: '100.00'
    },

];