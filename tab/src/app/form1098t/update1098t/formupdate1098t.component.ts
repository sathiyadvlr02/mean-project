import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateFormDialogue } from './../update1098t/formdialogue.component';
import { EntityService } from './../../services/ein.service';
import { StateService } from './../../services/state.service';
import { Form1098tService } from './../../services/form1098t.service';
import { CookieService } from 'ngx-cookie-service';
import { SiteManagementService } from '../../services/sitemanagement.service';
import moment from 'moment';



@Component({
    selector: 'app-formupdate1098t',
    templateUrl: './formupdate1098t.component.html',
    styleUrls: ['./formupdate1098t.component.scss'],
})
export class FormUpdate1098T implements OnInit {

    createAppForm: FormGroup;
    taxyear;
    stud_id;
    seq_id;
    solicited = false;
    federalno;
    country;
    viewHistory = false;
    stateList;
    tcrsStatus = false;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        public StateServices : StateService,
        public EntityService: EntityService,
        public Form1098tService: Form1098tService,
        private cookieService: CookieService,
        private _siteManagementService: SiteManagementService,
    ) {}

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

   
    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            studenT_ID : new FormControl(),
            SEQUENCE_ID : new FormControl(),
            corrected: new FormControl(false),
            filerName: new FormControl('', [Validators.required]),
            filerAddress1: new FormControl('', [Validators.required]),
            filerAddress2: new FormControl(''),
            filerAddress3: new FormControl(''),
            filerCity: new FormControl('', [Validators.required]),
            filerState: new FormControl('', [Validators.required]),
            filerZip: new FormControl('', [Validators.required]),
            filerTelephoneNo1: new FormControl('', [Validators.required]),
            filerTelephoneNo2: new FormControl(''),
            filerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            studentSocialSecurityNumber: new FormControl('', [Validators.required]),
            ssnSocialized: new FormControl(false),
            studentFirstName: new FormControl('', [Validators.required]),
            studentLastName: new FormControl('', [Validators.required]),
            studentAddress1: new FormControl('', [Validators.required]),
            studentAddress2: new FormControl(''),
            studentAddress3: new FormControl(''),
            studentCity: new FormControl(''),
            studentState: new FormControl(''),
            studentZip: new FormControl(''),
            forignCountry: new FormControl(false),
            studentCountry: new FormControl(''),
            serviceProviderAccNumber: new FormControl('', [Validators.required]),
            printSuppession: new FormControl(false),
            eFileSuppession: new FormControl(false),
            tutionAndRelatedExpenses: new FormControl(''),
            adjustmentMade: new FormControl(''),
            scholarshipsOrGrants: new FormControl(''),
            adjustmentScholarshipsOrGrants: new FormControl(''),
            acadamicPeriodsBegin: new FormControl(''),
            leastHalfTimeStudent: new FormControl(''),
            graduateStudent: new FormControl(''),
            reimbursementOrRefunds: new FormControl(''),
            amountsBuildForQualified: new FormControl(''),
            educationalInstitution: new FormControl(false),
            releaseDate: new FormControl(''),
            eContentDate: new FormControl(''),
            memo: new FormControl('')
        });
        // this.route.paramMap.subscribe(params => { 
        //     this.taxyear = params.get('data'); 
        // });
        this.taxyear = this.route.snapshot.queryParams.data;
        this.seq_id = this.route.snapshot.queryParams.seq;
        this.stud_id = this.route.snapshot.queryParams.stuid;

        if (this.taxyear >= 2015) {
            this.solicited = true;
        }
        this.getformdata();
        this.getVersionList();
        this.checkTCRSCondition();
    }

    get trrowErrors() { return this.createAppForm.controls; }

    checkTCRSCondition(){
        this._siteManagementService.getSiteYearForForm(this.CookieSiteId, this.taxyear).subscribe(
            result => {
                if(result.isValid && result.data){
                    if(JSON.parse(result.data).Data_Type_1098 == 'D'){
                        this.tcrsStatus = true;
                    }
                }
            });
    }

    getVersionList(){
        let tempData = {
            Student_Id: this.stud_id,
            Sequence_Id: this.seq_id,
            TaxYear: this.taxyear
        };
        this.Form1098tService.taxFormVersion(tempData).subscribe(
            result => {
                if(result.isValid && result.data){
                    this.viewHistory = true;
                }
            });
    }

    onClickViewHistory(){
        this.router.navigate(["view1098thistory"], { queryParams: { student: this.stud_id, sequence: this.seq_id, tax: this.taxyear} });
    }

    openDialog(b_val): void {
        const dialogRef = this.dialog.open(UpdateFormDialogue, {
            data: {tax_year: this.taxyear, box_value: b_val, stu_id: this.createAppForm.value.studenT_ID, stu_name: this.createAppForm.value.studentLastName}
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                let tempVal = JSON.parse(result)
                if(b_val == '01'){
                    this.createAppForm.get('tutionAndRelatedExpenses').setValue(parseFloat(tempVal.total).toFixed(2));
                } else if(b_val == '04'){
                    this.createAppForm.get('adjustmentMade').setValue(parseFloat(tempVal.total).toFixed(2));
                } else if(b_val == '05'){
                    this.createAppForm.get('scholarshipsOrGrants').setValue(parseFloat(tempVal.total).toFixed(2));
                } else if(b_val == '06'){
                    this.createAppForm.get('adjustmentScholarshipsOrGrants').setValue(parseFloat(tempVal.total).toFixed(2));
                }
            }
        });
    }

    AddFiler() {
        if (this.createAppForm.invalid) {
            for (const key of Object.keys(this.createAppForm.controls)) {
                if (this.createAppForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }
        let addFormData = this.createAppForm.value;
        let datas = {
            studenT_ID: addFormData.studenT_ID,
            sitE_ID: parseInt(this.CookieSiteId),
            sequencE_ID:addFormData.SEQUENCE_ID, 
            taX_YEAR: this.taxyear,
            boX_FILER_FEDERAL_ID_NUMBER: addFormData.filerFederalIdentificationNumber,
            boX_STUDENT_SSN: addFormData.studentSocialSecurityNumber,
            boX_FILER_NAME: addFormData.filerName,
            boX_FILER_PHONE: addFormData.filerTelephoneNo1,
            boX_FILER_PHONE2: addFormData.filerTelephoneNo2,
            fileR_ADDRESS1: addFormData.filerAddress1,
            fileR_ADDRESS2: addFormData.filerAddress2,
            fileR_ADDRESS3: addFormData.filerAddress3,
            fileR_CITY: addFormData.filerCity,
            fileR_STATE: addFormData.filerState,
            fileR_ZIP: addFormData.filerZip,
            studenT_FIRST_NAME: addFormData.studentFirstName,
            studenT_LAST_NAME: addFormData.studentLastName,
            boX_STUDENT_NAME: addFormData.studentFirstName +' '+addFormData.studentLastName,
            studenT_ADDRESS1: addFormData.studentAddress1,
            studenT_ADDRESS2: addFormData.studentAddress2,
            studenT_ADDRESS3: "",
            studenT_CITY: addFormData.studentCity,
            studenT_STATE: addFormData.studentState,
            studenT_ZIP: addFormData.studentZip,
            boX_ACCOUNT_NUMBER: addFormData.serviceProviderAccNumber,
            boX1_PAYMENTS_RECEIVED: addFormData.tutionAndRelatedExpenses.toString(),
            boX2_AMOUNTS_BILLED: addFormData.amountsBuildForQualified.toString(),
            boX3_RPT_MTHD_CHG_IND: addFormData.educationalInstitution ? 'X' : '',
            boX4_ADJ_MADE_PRIOR_YR: addFormData.adjustmentMade.toString(),
            boX5_SCHOLARSHIP_GRANTS: addFormData.scholarshipsOrGrants.toString(),
            boX6_ADJ_SCHOLARSHIP_PRIOR_YR: addFormData.adjustmentScholarshipsOrGrants.toString(),
            boX7_ACADEMIC_PERIOD_IND: addFormData.acadamicPeriodsBegin ? 'X' : '',
            boX8_HALF_TIME_STUDENT_IND: addFormData.leastHalfTimeStudent ? 'X' : '',
            boX9_GRADUATE_STUDENT_IND: addFormData.graduateStudent ? 'X' : '',
            boX10_REIMBURSEMENT_OR_REFUND: addFormData.reimbursementOrRefunds.toString(),
            boX_CORRECTED_IND: addFormData.corrected ? 'X' : '',
            ssN_SOLICITED: addFormData.ssnSocialized ? 'X' : '',
            memo: addFormData.memo,
            foreigN_COUNTRY_INDICATOR: addFormData.forignCountry ? 'X' : '',
            foreigN_COUNTRY: addFormData.studentCountry,
            prinT_SUPPRESSION: addFormData.printSuppession ? 'Y': "N",
            efilE_SUPPRESSION: addFormData.eFileSuppession ? 'Y': "N",
            onlinE_ACCESS: "",
            campuS_CODE: "",
            prograM_CODE: "",
            financiaL_TYPE_CODE: "",
            financiaL_SUB_TYPE_CODE: "",
            academiC_TERM: "",
            description: "",
            doubtfuL_ADDRESSES: "",
            forM_RELEASE_DATE: moment(addFormData.releaseDate).format('YYYY-MM-DD'),
            version: 0,
            createD_BY: "",
            ipaddress: "",
            comments: "",
            modifieD_BY: "",
            sTatus: "ONLINE"
        }
        //console.log(datas);
        this.Form1098tService
            .updateFormData(datas)
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        this.cookieService.set('formYear', this.taxyear);
                        this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['/formsmanagement/create1099search']);
                    } else {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        this.router.navigate(['/formsmanagement/create1099search']);
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                    this.router.navigate(['/formsmanagement/create1099search']);
                }
            );

    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    onYearAdd(year) {
        return parseInt(year) + 1;
    }

    getFederalIdentification() {
        this.Form1098tService.getFederalIdentification().subscribe(
            result => {
                this.federalno = result.data;
            });
            
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
    }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    getformdata() {
        this.getCountry();
        this.getstatelist();
        
        this.Form1098tService.getAllForms( this.stud_id,this.seq_id,this.taxyear) .subscribe(
            (result) => {
                if(result.isValid){
                    this.createAppForm.get('studenT_ID').setValue(JSON.parse(result.data).STUDENT_ID);
                    this.createAppForm.get('SEQUENCE_ID').setValue(JSON.parse(result.data).SEQUENCE_ID);
                    this.createAppForm.get('corrected').setValue(JSON.parse(result.data).BOX_CORRECTED_IND== 'X' ? true : false);
                    this.createAppForm.get('filerName').setValue(JSON.parse(result.data).BOX_FILER_NAME);
                    this.createAppForm.get('filerAddress1').setValue(JSON.parse(result.data).FILER_ADDRESS1);
                    this.createAppForm.get('filerAddress2').setValue(JSON.parse(result.data).FILER_ADDRESS2);
                    this.createAppForm.get('filerAddress3').setValue(JSON.parse(result.data).FILER_ADDRESS3);
                    this.createAppForm.get('filerCity').setValue(JSON.parse(result.data).FILER_CITY);
                    this.createAppForm.get('filerState').setValue(JSON.parse(result.data).FILER_STATE);
                    this.createAppForm.get('filerZip').setValue(JSON.parse(result.data).FILER_ZIP);
                    this.createAppForm.get('filerTelephoneNo1').setValue(JSON.parse(result.data).BOX_FILER_PHONE);
                    this.createAppForm.get('filerTelephoneNo2').setValue(JSON.parse(result.data).BOX_FILER_PHONE2);
                    this.createAppForm.get('filerFederalIdentificationNumber').setValue(JSON.parse(result.data).BOX_FILER_FEDERAL_ID_NUMBER);
                    this.createAppForm.get('studentSocialSecurityNumber').setValue(JSON.parse(result.data).BOX_STUDENT_SSN);
                    this.createAppForm.get('ssnSocialized').setValue(JSON.parse(result.data).SSN_SOLICITED);
                    this.createAppForm.get('studentFirstName').setValue(JSON.parse(result.data).STUDENT_FIRST_NAME);
                    this.createAppForm.get('studentLastName').setValue(JSON.parse(result.data).STUDENT_LAST_NAME);
                    this.createAppForm.get('studentAddress1').setValue(JSON.parse(result.data).STUDENT_ADDRESS1);
                    this.createAppForm.get('studentAddress2').setValue(JSON.parse(result.data).STUDENT_ADDRESS2);
                    this.createAppForm.get('studentAddress3').setValue(JSON.parse(result.data).STUDENT_ADDRESS3);
                    this.createAppForm.get('studentCity').setValue(JSON.parse(result.data).STUDENT_CITY);
                    this.createAppForm.get('studentState').setValue(JSON.parse(result.data).STUDENT_STATE);
                    this.createAppForm.get('studentZip').setValue(JSON.parse(result.data).STUDENT_ZIP);
                    this.createAppForm.get('forignCountry').setValue(JSON.parse(result.data).FOREIGN_COUNTRY_INDICATOR == 'X' ? true : false);
                    this.createAppForm.get('studentCountry').setValue(JSON.parse(result.data).FOREIGN_COUNTRY);
                    this.createAppForm.get('serviceProviderAccNumber').setValue(JSON.parse(result.data).BOX_ACCOUNT_NUMBER);
                    this.createAppForm.get('printSuppession').setValue(JSON.parse(result.data).PRINT_SUPPRESSION  == 'Y' ? true : false);
                    this.createAppForm.get('eFileSuppession').setValue(JSON.parse(result.data).EFILE_SUPPRESSION  == 'Y' ? true : false);
                    this.createAppForm.get('tutionAndRelatedExpenses').setValue(JSON.parse(result.data).BOX1_PAYMENTS_RECEIVED);
                    this.createAppForm.get('adjustmentMade').setValue(JSON.parse(result.data).BOX4_ADJ_MADE_PRIOR_YR);
                    this.createAppForm.get('scholarshipsOrGrants').setValue(JSON.parse(result.data).BOX5_SCHOLARSHIP_GRANTS);
                    this.createAppForm.get('adjustmentScholarshipsOrGrants').setValue(JSON.parse(result.data).BOX6_ADJ_SCHOLARSHIP_PRIOR_YR);
                    this.createAppForm.get('acadamicPeriodsBegin').setValue(JSON.parse(result.data).BOX7_ACADEMIC_PERIOD_IND == 'X' ? true : false);
                    this.createAppForm.get('leastHalfTimeStudent').setValue(JSON.parse(result.data).BOX8_HALF_TIME_STUDENT_IND  == 'X' ? true : false);
                    this.createAppForm.get('graduateStudent').setValue(JSON.parse(result.data).BOX9_GRADUATE_STUDENT_IND  == 'x' ? true : false);
                    this.createAppForm.get('reimbursementOrRefunds').setValue(JSON.parse(result.data).BOX10_REIMBURSEMENT_OR_REFUND);
                    this.createAppForm.get('amountsBuildForQualified').setValue(JSON.parse(result.data).BOX2_AMOUNTS_BILLED);
                    this.createAppForm.get('educationalInstitution').setValue(JSON.parse(result.data).BOX3_RPT_MTHD_CHG_IND  == 'X' ? true : false); 
                    this.createAppForm.get('releaseDate').setValue(moment(JSON.parse(result.data).FORM_RELEASE_DATE).format('MM-DD-YYYY'));  
                    this.createAppForm.get('eContentDate').setValue('');  
                    this.createAppForm.get('memo').setValue(JSON.parse(result.data).Memo);   
                      
                }
            });    
            this.getFederalIdentification();
    }


    selectedFno(no) {
        this.EntityService
            .getSingleEntityListDetails(no)
            .subscribe(
                (result) => {
                    if (result.isValid) {

                        this.createAppForm.get('filerFederalIdentificationNumber').setValue(result.data.eiN_NUMBER);
                        this.createAppForm.get('filerName').setValue(result.data.eiN_NAME);
                        this.createAppForm.get('filerAddress1').setValue(result.data.addresS1);
                        this.createAppForm.get('filerAddress2').setValue(result.data.addresS2);
                        this.createAppForm.get('filerAddress3').setValue(result.data.addresS3);
                        this.createAppForm.get('filerCity').setValue(result.data.city);
                        this.createAppForm.get('filerState').setValue(result.data.state);
                        this.createAppForm.get('filerZip').setValue(result.data.zip);
                        this.createAppForm.get('filerTelephoneNo1').setValue(result.data.phone);
                        this.createAppForm.get('filerTelephoneNo2').setValue(result.data.phonE2);
                    }
                },
                (error) => {
                    // console.log('error', error)
                }
            );
    }

    TooltipMouseenter(event: Event, boxtooltip: any): void {
        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.show();
        }

    }
    TooltipMouseout(event: Event, boxtooltip: any): void {
        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.hide();
        }

    }


    Showtooltip(event: Event, boxtooltip: any): void {

        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.show();
        }
    }


    Hidetooltip(event: Event, boxtooltip: any): void {

        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.hide();
        }
    }


}
