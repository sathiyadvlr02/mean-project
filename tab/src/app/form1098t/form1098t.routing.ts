import { Routes } from '@angular/router';
import { FormCreate1098T } from './create1098t/formcreate1098t.component';
import { FormUpdate1098T } from './update1098t/formupdate1098t.component';
import { FormViewHistory1098TComponent } from './view1098thistory/formviewhistory1098t.component';
import { FormEmail1098T } from './email1098t/email1098t.component';

export const Form1098TRoutes: Routes = [

  {
    path: 'formsmanagementcreatenew',
    component: FormCreate1098T
  },
  {
    path: 'create1098t',
    component: FormCreate1098T
  },
  {
    path: 'email1098t',
    component: FormEmail1098T
  },

  {
    path: 'update1098t',
    component: FormUpdate1098T
  },


  {
    path: 'formsmanagementupdatemodel',
    component: FormUpdate1098T
  },
  {
    path: 'view1098thistory',
    component: FormViewHistory1098TComponent
  },


];
