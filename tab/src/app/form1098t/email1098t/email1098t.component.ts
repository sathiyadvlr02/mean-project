import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { SetPassword } from './setpassword.component';

import { Form1098tService } from '../../services/form1098t.service';

@Component({
    selector: 'app-email1098t',
    templateUrl: './email1098t.component.html'
})

export class FormEmail1098T implements OnInit {

    emailForm: FormGroup;
    taxyear;
    student;
    sequence;

    constructor(
        private fb: FormBuilder,
        private el: ElementRef,
        public dialog: MatDialog,
        private _form1098tService: Form1098tService,
        private router: Router,
        private route: ActivatedRoute,
        public toaster: ToastrService,
    ){}

    ngOnInit(): void {
        this.emailForm = this.fb.group({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('')
        });
        this.taxyear = this.route.snapshot.queryParams.tax;
        this.student = this.route.snapshot.queryParams.student;
        this.sequence = this.route.snapshot.queryParams.sequence;
    }

    get trrowErrors() { return this.emailForm.controls; }

    onSubmit(){
        if (this.emailForm.invalid) {
            for (const key of Object.keys(this.emailForm.controls)) {
              if (this.emailForm.controls[key].invalid) {
                const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                invalidControl.focus();
                break;
              }
            }
            return;
        }

        let emailFormData = this.emailForm.value;
        let tempData = {
            Student_Id: this.student,
            Sequence_Id: this.sequence,
            TaxYear: this.taxyear,
            email: emailFormData.email,
        }
        if(emailFormData.password){
            tempData['password'] = emailFormData.password
        }

        this._form1098tService
        .emailFormDatas(tempData)
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.toaster.success(
                        result.message,
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                    this.router.navigate(["filemanagement"]);
                }
            },
            (error) => {
                this.toaster.error(
                    'Please Try Again',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
            }
        );
    }

    openDialog() {
        const dialogRef = this.dialog.open(SetPassword);

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.emailForm.get('password').setValue(result);
            }
        });
    }

}