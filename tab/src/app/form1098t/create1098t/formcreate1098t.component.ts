import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormDialogue } from './formdialogue.component';
import { EntityService } from './../../services/ein.service';
import { StateService } from './../../services/state.service';
import { Form1098tService } from './../../services/form1098t.service';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { SiteManagementService } from '../../services/sitemanagement.service';

@Component({
    selector: 'app-create1098t',
    templateUrl: './formcreate1098t.component.html',
    styleUrls: ['./formcreate1098t.component.scss'],
})
export class FormCreate1098T implements OnInit {

    createAppForm: FormGroup;
    taxyear;
    solicited = false;
    federalno
    country;
    stateList;
    tcrsStatus = false;
    tempPopupData = [];


    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        public StateServices : StateService,
        public EntityService: EntityService,
        public Form1098tService: Form1098tService,
        private cookieService: CookieService,
        private _siteManagementService: SiteManagementService,
    ) {}

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            corrected: new FormControl(false),
            filerName: new FormControl('', [Validators.required]),
            filerAddress1: new FormControl('', [Validators.required]),
            filerAddress2: new FormControl(''),
            filerAddress3: new FormControl(''),
            filerCity: new FormControl('', [Validators.required]),
            filerState: new FormControl('', [Validators.required]),
            filerZip: new FormControl('', [Validators.required]),
            filerTelephoneNo1: new FormControl('', [Validators.required]),
            filerTelephoneNo2: new FormControl(''),
            filerFederalIdentificationNumber: new FormControl('', [Validators.required]),
            studentSocialSecurityNumber: new FormControl('', [Validators.required]),
            ssnSocialized: new FormControl(false),
            studentFirstName: new FormControl('', [Validators.required]),
            studentLastName: new FormControl('', [Validators.required]),
            studentAddress1: new FormControl('', [Validators.required]),
            studentAddress2: new FormControl(''),
            studentAddress3: new FormControl(''),
            studentCity: new FormControl(''),
            studentState: new FormControl(''),
            studentZip: new FormControl(''),
            forignCountry: new FormControl(false),
            studentCountry: new FormControl(''),
            serviceProviderAccNumber: new FormControl('', [Validators.required]),
            printSuppession: new FormControl(false),
            eFileSuppession: new FormControl(false),
            tutionAndRelatedExpenses: new FormControl(''),
            adjustmentMade: new FormControl(''),
            scholarshipsOrGrants: new FormControl(''),
            adjustmentScholarshipsOrGrants: new FormControl(''),
            acadamicPeriodsBegin: new FormControl(''),
            leastHalfTimeStudent: new FormControl(''),
            graduateStudent: new FormControl(''),
            reimbursementOrRefunds: new FormControl(''),
            amountsBuildForQualified: new FormControl(''),
            educationalInstitution: new FormControl(false),
            Box01:  new FormControl([]),
            Box04:  new FormControl([]),
            Box05:  new FormControl([]),
            Box06:  new FormControl([]),
        });
        this.taxyear = this.route.snapshot.queryParams.data;
        if (this.taxyear >= 2015) {
            this.solicited = true;
        }
        this.getFederalIdentification();
        this.checkTCRSCondition();
    }

    get trrowErrors() { return this.createAppForm.controls; }

    checkTCRSCondition(){
        this._siteManagementService.getSiteYearForForm(this.CookieSiteId, this.taxyear).subscribe(
            result => {
                if(result.isValid && result.data){
                    if(JSON.parse(result.data).Data_Type_1098 == 'D'){
                        this.tcrsStatus = true;
                    }
                }
            });
    }

    openDialog(b_val): void {
        const dialogRef = this.dialog.open(FormDialogue, {
            data: {tax_year: this.taxyear, box_value: b_val, box_data: this.createAppForm.value['Box' + b_val], stu_name: this.createAppForm.value.studentLastName}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(result)
            if(result){
                let tempVal = JSON.parse(result)
                if(b_val == '01'){
                    this.createAppForm.get('tutionAndRelatedExpenses').setValue(parseFloat(tempVal.total).toFixed(2));
                    this.createAppForm.get('Box01').setValue(tempVal.data);
                    tempVal.data.map((itm, i) => {
                        this.tempPopupData.push(itm);
                    })
                } else if(b_val == '04'){
                    this.createAppForm.get('adjustmentMade').setValue(parseFloat(tempVal.total).toFixed(2));
                    this.createAppForm.get('Box04').setValue(tempVal.data);
                    tempVal.data.map((itm, i) => {
                        this.tempPopupData.push(itm);
                    })
                } else if(b_val == '05'){
                    this.createAppForm.get('scholarshipsOrGrants').setValue(parseFloat(tempVal.total).toFixed(2));
                    this.createAppForm.get('Box05').setValue(tempVal.data);
                    tempVal.data.map((itm, i) => {
                        this.tempPopupData.push(itm);
                    })
                } else if(b_val == '06'){
                    this.createAppForm.get('adjustmentScholarshipsOrGrants').setValue(parseFloat(tempVal.total).toFixed(2));
                    this.createAppForm.get('Box06').setValue(tempVal.data);
                    tempVal.data.map((itm, i) => {
                        this.tempPopupData.push(itm);
                    })
                }
                console.log('this.tempPopupData', this.tempPopupData)
            }
        });
    }

    AddFiler() {
        if (this.createAppForm.invalid) {
            for (const key of Object.keys(this.createAppForm.controls)) {
                if (this.createAppForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }
        let addFormData = this.createAppForm.value;
        let datas = {
            studenT_ID: "",
            sitE_ID: parseInt(this.CookieSiteId),
            taX_YEAR: this.taxyear,
            boX_FILER_FEDERAL_ID_NUMBER: addFormData.filerFederalIdentificationNumber,
            boX_STUDENT_SSN: addFormData.studentSocialSecurityNumber,
            boX_FILER_NAME: addFormData.filerName,
            boX_FILER_PHONE: addFormData.filerTelephoneNo1,
            boX_FILER_PHONE2: addFormData.filerTelephoneNo2,
            fileR_ADDRESS1: addFormData.filerAddress1,
            fileR_ADDRESS2: addFormData.filerAddress2,
            fileR_ADDRESS3: addFormData.filerAddress3,
            fileR_CITY: addFormData.filerCity,
            fileR_STATE: addFormData.filerState,
            fileR_ZIP: addFormData.filerZip,
            studenT_FIRST_NAME: addFormData.studentFirstName,
            studenT_LAST_NAME: addFormData.studentLastName,
            boX_STUDENT_NAME: addFormData.studentFirstName +' '+ addFormData.studentLastName,
            studenT_ADDRESS1: addFormData.studentAddress1,
            studenT_ADDRESS2: addFormData.studentAddress2,
            studenT_ADDRESS3: "",
            studenT_CITY: addFormData.studentCity,
            studenT_STATE: addFormData.studentState,
            studenT_ZIP: addFormData.studentZip,
            boX_ACCOUNT_NUMBER: addFormData.serviceProviderAccNumber,
            boX1_PAYMENTS_RECEIVED: addFormData.tutionAndRelatedExpenses.toString(),
            boX2_AMOUNTS_BILLED: addFormData.amountsBuildForQualified.toString(),
            boX3_RPT_MTHD_CHG_IND: addFormData.educationalInstitution ? 'X' : '',
            boX4_ADJ_MADE_PRIOR_YR: addFormData.adjustmentMade.toString(),
            boX5_SCHOLARSHIP_GRANTS: addFormData.scholarshipsOrGrants.toString(),
            boX6_ADJ_SCHOLARSHIP_PRIOR_YR: addFormData.adjustmentScholarshipsOrGrants.toString(),
            boX7_ACADEMIC_PERIOD_IND: addFormData.acadamicPeriodsBegin ? 'X' : '',
            boX8_HALF_TIME_STUDENT_IND: addFormData.leastHalfTimeStudent ? 'X' : '',
            boX9_GRADUATE_STUDENT_IND: addFormData.graduateStudent ? 'X' : '',
            boX10_REIMBURSEMENT_OR_REFUND: addFormData.reimbursementOrRefunds.toString(),
            boX_CORRECTED_IND: addFormData.corrected ? 'X' : '',
            ssN_SOLICITED: addFormData.ssnSocialized ? 'X' : '',
            memo: "",
            foreigN_COUNTRY_INDICATOR: addFormData.forignCountry ? 'X' : '',
            foreigN_COUNTRY: addFormData.studentCountry,
            prinT_SUPPRESSION: addFormData.printSuppession ? 'Y' : "N",
            efilE_SUPPRESSION: addFormData.eFileSuppession ? 'Y' : "N",
            onlinE_ACCESS: "",
            campuS_CODE: "",
            prograM_CODE: "",
            financiaL_TYPE_CODE: "",
            financiaL_SUB_TYPE_CODE: "",
            academiC_TERM: "",
            description: "",
            doubtfuL_ADDRESSES: "",
            forM_RELEASE_DATE: "2021-06-30T16:57:27.429Z",
            version: 0,
            createD_BY: "",
            ipaddress: "",
            comments: "",
            modifieD_BY: "",
            sTatus: "ONLINE"
        }
        //console.log(JSON.stringify(datas));
        this.Form1098tService
            .createFormData(datas)
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        this.cookieService.set('formYear', this.taxyear);
                        this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                        if(this.tcrsStatus){
                            this.updateTCRS(result.data.student_Id);
                        } else {
                            this.router.navigate(['/formsmanagement/create1099search']);
                        }
                    } else {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                    }
                },
                (error) => {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                    );
                }
            );

    }

    updateTCRS(stu_id){
        if(this.tempPopupData){
            let upData = [];
            this.tempPopupData.map((itm, i) => {
                let tempData = {
                    studenT_ID: stu_id,
                    sequencE_ID: i,
                    databasE_LOAD_ID: "",
                    sitE_ID: parseInt(this.CookieSiteId),
                    status: "ONLINE",
                    type: "",
                    onlinE_UPDATE_IND: "",
                    taX_YEAR: this.taxyear,
                    controL_NUMBER: "",
                    campuS_CODE: itm.Campus_Code,
                    prograM_CODE: itm.Program_Name,
                    studenT_SSN: this.createAppForm.value.studentSocialSecurityNumber,
                    transactioN_DATE: itm.Transaction_Date,
                    transactioN_TYPE: "",
                    studenT_LAST_NAME: itm.Student_LastName,
                    studenT_REGISTRATION_ID: this.createAppForm.value.serviceProviderAccNumber,
                    transactioN_ID: "",
                    financiaL_TYPE_CODE: itm.Financial_Type.slice(0, 1),
                    financiaL_SUB_TYPE_CODE: itm.Financial_Type.substring(1),
                    academiC_TERM: itm.Academic_Term,
                    academiC_YEAR: itm.Academic_Year,
                    description: itm.Financial_Description,
                    amount: itm.Amount,
                    namE_VALIDATION: "",
                    vieweD_INDICATOR: "",
                    printeD_INDICATOR: "",
                    efileD_INDICATOR: "",
                    doubtfuL_ADDRESSES: ""
                }
                upData.push(tempData);
            })
            console.log(JSON.stringify(upData))
            this.Form1098tService
            .createTCRSFormData(upData)
            .subscribe(
                (result) => {
                    if (result.isValid) {
                        console.log('result', result)
                    }
                },
                (error) => {
                },
            )
        } 
        this.router.navigate(['/formsmanagement/create1099search']);
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    onYearAdd(year) {
        return parseInt(year) + 1;
    }

    getFederalIdentification() {
        this.Form1098tService.getFederalIdentification().subscribe(
            result => {
                this.federalno = result.data;
            });
            this.getCountry();
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
            this.getstatelist();
        }
            

        getstatelist() {
            this.StateServices.getStatelist().subscribe(
                result => {
                    this.stateList= JSON.parse(result.data);
                });
        }

    selectedFno(no) {
        this.EntityService
            .getSingleEntityListDetails(no)
            .subscribe(
                (result) => {
                    if (result.isValid) {

                        this.createAppForm.get('filerFederalIdentificationNumber').setValue(result.data.eiN_NUMBER);
                        this.createAppForm.get('filerName').setValue(result.data.eiN_NAME);
                        this.createAppForm.get('filerAddress1').setValue(result.data.addresS1);
                        this.createAppForm.get('filerAddress2').setValue(result.data.addresS2);
                        this.createAppForm.get('filerAddress3').setValue(result.data.addresS3);
                        this.createAppForm.get('filerCity').setValue(result.data.city);
                        this.createAppForm.get('filerState').setValue(result.data.state);
                        this.createAppForm.get('filerZip').setValue(result.data.zip);
                        this.createAppForm.get('filerTelephoneNo1').setValue(result.data.phone);
                        this.createAppForm.get('filerTelephoneNo2').setValue(result.data.phonE2);
                    }
                },
                (error) => {
                    // console.log('error', error)
                }
            );
    }

    TooltipMouseenter(event: Event, boxtooltip: any): void {
        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.show();
        }

    }
    TooltipMouseout(event: Event, boxtooltip: any): void {
        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.hide();
        }

    }


    Showtooltip(event: Event, boxtooltip: any): void {

        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.show();
        }
    }


    Hidetooltip(event: Event, boxtooltip: any): void {

        event.stopImmediatePropagation();
        event.preventDefault();

        if (!boxtooltip._isTooltipVisible()) {
            boxtooltip.hide();
        }
    }


}

