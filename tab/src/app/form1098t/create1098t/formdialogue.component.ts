import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { TCRSService } from './../../services/TCRS.service';

import moment from 'moment';

export interface DialogData {
    tax_year: any,
    box_value: any,
    box_data: any,
    stu_name: any,
}

@Component({
    selector: 'form-dialogue',
    templateUrl: './formdialogue.component.html',

})

export class FormDialogue implements OnInit {

    formMethod = 'list';
    formSubMethod = '';
    createSiteForm: FormGroup;/* 'Financial_Description', */
    displayedColumns: string[] = ['Transaction_Date', 'Campus_Code', 'Program_Name', 'Student_LastName',
     'Financial_Type','Academic_Term','Academic_Year', 'Amount','action'];
    //dataSource = ELEMENT_DATA;
    dataSourceData: MatTableDataSource<any>;
    dataSource = [];
    editValue = '';

    programcode;
    Financialcode;
    accTerm;
    accYear;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private fb: FormBuilder,
        public tcrs_service: TCRSService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private el: ElementRef,
    ){
        this.dataSource = data.box_data;
        this.dataSourceData = new MatTableDataSource(data.box_data);
        this.dataSourceData.paginator = this.paginator;
        this.dataSourceData.sort = this.sort;
        this.formMethod = 'list';
    }

    ngOnInit(): void {

        this.createSiteForm = this.fb.group({

            tdate: new FormControl('', [Validators.required]),
            pname: new FormControl('', [Validators.required]),
            ccode: new FormControl('', [Validators.required]),
            sname: new FormControl(this.data.stu_name, [Validators.required]),
            financialtype: new FormControl('', [Validators.required]),
            financialdesp: new FormControl('', [Validators.required]),
            academic: new FormControl('', [Validators.required]),
            academicterm: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
        });

        this.getTCRSsetting();
    }



    get trrowErrors() { return this.createSiteForm.controls; }

    formDataEmpty(){
        this.createSiteForm.get('tdate').setValue('');
        this.createSiteForm.get('pname').setValue('');
        this.createSiteForm.get('financialtype').setValue('');
        this.createSiteForm.get('financialdesp').setValue('');
        this.createSiteForm.get('academic').setValue('');
        this.createSiteForm.get('academicterm').setValue('');
        this.createSiteForm.get('amount').setValue('');
    }

    onClickDelete(index){
        let tempArray = this.dataSource;
        tempArray.splice(index,  1);
        this.dataSource = tempArray;
        this.dataSourceData = new MatTableDataSource(tempArray);
        this.dataSourceData.paginator = this.paginator;
        this.dataSourceData.sort = this.sort;
    }

    onClickEdit(index){
        this.editValue = index;
        let tempValue = this.dataSource[index];
        let tempFinancial = {};
        this.Financialcode.map((itm, i) => {
            if(itm.VALUE == tempValue.Financial_Type){
                tempFinancial = itm;
            }
        })
        this.createSiteForm.get('tdate').setValue(new Date(tempValue.Transaction_Date));
        this.createSiteForm.get('pname').setValue(tempValue.Program_Name);
        this.createSiteForm.get('ccode').setValue(tempValue.Campus_Code);
        this.createSiteForm.get('sname').setValue(tempValue.Student_LastName);
        this.createSiteForm.get('financialtype').setValue(tempFinancial);
        this.createSiteForm.get('financialdesp').setValue(tempValue.Financial_Description);
        this.createSiteForm.get('academic').setValue(tempValue.Academic_Year);
        this.createSiteForm.get('academicterm').setValue(tempValue.Academic_Term);
        this.createSiteForm.get('amount').setValue(tempValue.Amount);
        this.formMethod = 'action';
        this.formSubMethod = 'edit';
    }

    actionSubmit(){
        if (this.createSiteForm.invalid) {
            for (const key of Object.keys(this.createSiteForm.controls)) {
                if (this.createSiteForm.controls[key].invalid) {
                    const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                    invalidControl.focus();
                    break;
                }
            }
            return;
        }
        let tempDataPlus = this.dataSource;
        let tempFormData = this.createSiteForm.value;
        let tempData = {
            Transaction_Date: moment(tempFormData.tdate).format('MM-DD-YYYY'),
            Campus_Code: tempFormData.ccode,
            Program_Name: tempFormData.pname,
            Student_LastName: tempFormData.sname,
            Financial_Type: tempFormData.financialtype.VALUE,
            Academic_Term: tempFormData.academicterm,
            Academic_Year: tempFormData.academic,
            Financial_Description: tempFormData.financialdesp,
            Amount: parseFloat(tempFormData.amount).toFixed(2),
        };
        if(this.formSubMethod == 'add'){
            tempDataPlus.push(tempData);
            this.formDataEmpty();
        } else {
            tempDataPlus[this.editValue] = tempData;
            this.editValue = '';
            this.formDataEmpty();
        }
        this.dataSource = tempDataPlus;
        this.dataSourceData = new MatTableDataSource(tempDataPlus);
        this.dataSourceData.paginator = this.paginator;
        this.dataSourceData.sort = this.sort;
        this.changePopUpMode('list', '')
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

    changePopUpMode(type, sub_type){
        this.formMethod = type;
        this.formSubMethod = sub_type;
        if(type == 'list'){
            this.editValue = '';
            this.formDataEmpty()
        }
    }

    getTCRSsetting() {
        this.tcrs_service.GetTCRSSettings(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    let a = JSON.parse(result.data);
                    this.createSiteForm.get('ccode').setValue(a[0].CAMPUS_CODE);
                }
            });

        this.getProgramcode();
    }

    getProgramcode() {
        this.tcrs_service.GetProgramCode(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    this.programcode = JSON.parse(result.data);
                }
            });   
            this.getFinancialCode();    
    }
    getFinancialCode() {
        this.tcrs_service.GetFinancialCode(this.data.tax_year,this.data.box_value).subscribe(
            result => {
                if (result.isValid) {
                    this.Financialcode = JSON.parse(result.data);
                }
            });  
            
            this.getAcademicterm();    
    }

    selectedType(e){
     this.createSiteForm.get('financialdesp').setValue(e.value.DISPLAY_NAME);     
    }

    getAcademicterm() {
        this.tcrs_service.GetAcademic(this.data.tax_year).subscribe(
            result => {
                if (result.isValid) {
                    this.accTerm = JSON.parse(result.data)
                    //this.accYear = JSON.parse(result.data)
                    this.accYear = [...new Map(JSON.parse(result.data).map(item => [item['ACADEMIC_YEAR'], item])).values()]
                }
                //console.log('acc', this.accYear)
            });      
    }
    

    mainSubmit(){
        let value = 0;
        let tempData1 = JSON.parse(JSON.stringify(this.dataSource))
        tempData1.map((itm, i) => {
            value = value + parseFloat(itm.Amount)
        })
        let tempData = {
            total: value,
            data: this.dataSource,
        }
        return JSON.stringify(tempData);
    }
}

export interface PeriodicElement {
    Transaction_Date: string;
    Campus_Code: string;
    Program_Name: string;
    Student_LastName: string;
    Financial_Type: string;
    Academic_Term: string;
    Academic_Year: string;
    Financial_Description: string;
    Amount: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
    /* {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'ALL PAYMENTS', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION AND FEES (a)', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'SCHOLARSHIP', Amount: '100.00'
    }, */

];