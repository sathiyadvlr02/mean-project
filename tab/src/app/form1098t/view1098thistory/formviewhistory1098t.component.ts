import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewFormDialogue } from './formdialogue.component';
import { Form1098tService } from '../../services/form1098t.service';

import moment from 'moment';
@Component({
  selector: 'app-formviewhistory1098t',
  templateUrl: './formviewhistory1098t.component.html'
})
export class FormViewHistory1098TComponent implements OnInit {

    public selectedversion;

    createAppForm: FormGroup;
    taxyear;
    student;
    sequence;
    solicited = false;

    taxVersions = [];
    selectedVersion = 0;

    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    stateList = [
        {name: 'Select State', value: ''},
        {name: 'AA', value: 'AA'},
        {name: 'AB', value: 'AB'},
        {name: 'AE', value: 'AE'},
        {name: 'AK', value: 'AK'},
        {name: 'AL', value: 'AL'},
        {name: 'AP', value: 'AP'},
        {name: 'AR', value: 'AR'},
        {name: 'AZ', value: 'AZ'},
        {name: 'BC', value: 'BC'},
        {name: 'CA', value: 'CA'},
        {name: 'CO', value: 'CO'},
        {name: 'CT', value: 'CT'},
        {name: 'DE', value: 'DE'},
        {name: 'FL', value: 'FL'},
        {name: 'GA', value: 'GA'},
        {name: 'HI', value: 'HI'},
        {name: 'IA', value: 'IA'},
        {name: 'ID', value: 'ID'},
        {name: 'IL', value: 'IL'},
        {name: 'IN', value: 'IN'},
        {name: 'KS', value: 'KS'},
        {name: 'KY', value: 'KY'},
        {name: 'LA', value: 'LA'},
        {name: 'MA', value: 'MA'},
        {name: 'MB', value: 'MB'},
        {name: 'MD', value: 'MD'},
        {name: 'ME', value: 'ME'},
        {name: 'MI', value: 'MI'},
        {name: 'MN', value: 'MN'},
        {name: 'MO', value: 'MO'},
        {name: 'MS', value: 'MS'},
        {name: 'MT', value: 'MT'},
        {name: 'NB', value: 'NB'},
        {name: 'NC', value: 'NC'},
        {name: 'ND', value: 'ND'},
        {name: 'NE', value: 'NE'},
        {name: 'NH', value: 'NH'},
        {name: 'NJ', value: 'NJ'},
        {name: 'NL', value: 'NL'},
        {name: 'NM', value: 'NM'},
        {name: 'NS', value: 'NS'},
        {name: 'NT', value: 'NT'},
        {name: 'NU', value: 'NU'},
        {name: 'NV', value: 'NV'},
        {name: 'NY', value: 'NY'},
        {name: 'OH', value: 'OH'},
        {name: 'OK', value: 'OK'},
        {name: 'ON', value: 'ON'},
        {name: 'OR', value: 'OR'},
        {name: 'PA', value: 'PA'},
        {name: 'PE', value: 'PE'},
        {name: 'QC', value: 'QC'},
        {name: 'RI', value: 'RI'},
        {name: 'SC', value: 'SC'},
        {name: 'SD', value: 'SD'},
        {name: 'SK', value: 'SK'},
        {name: 'TN', value: 'TN'},
        {name: 'TX', value: 'TX'},
        {name: 'UT', value: 'UT'},
        {name: 'VA', value: 'VA'},
        {name: 'VT', value: 'VT'},
        {name: 'WA', value: 'WA'},
        {name: 'WI', value: 'WI'},
        {name: 'WV', value: 'WV'},
        {name: 'WY', value: 'WY'},
        {name: 'YT', value: 'YT'},
    ];


    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private _form1098tService: Form1098tService,
    ) { }

    ngOnInit(): void {
        this.createAppForm = this.fb.group({
            corrected: new FormControl(false),
            filerName: new FormControl(''),
            filerAddress1: new FormControl(''),
            filerAddress2: new FormControl(''),
            filerAddress3: new FormControl(''),
            filerCity: new FormControl(''),
            filerState: new FormControl(''),
            filerZip: new FormControl(''),
            filerTelephoneNo1: new FormControl(''),
            filerTelephoneNo2: new FormControl(''),
            filerFederalIdentificationNumber: new FormControl(''),
            studentSocialSecurityNumber: new FormControl(''),
            ssnSocialized: new FormControl(false),
            studentFirstName: new FormControl(''),
            studentLastName: new FormControl(''),
            studentAddress1: new FormControl(''),
            studentAddress2: new FormControl(''),
            studentAddress3: new FormControl(''),
            studentCity: new FormControl(''),
            studentState: new FormControl(''),
            studentZip: new FormControl(''),
            forignCountry: new FormControl(false),
            studentCountry: new FormControl(''),
            serviceProviderAccNumber: new FormControl(''),
            printSuppession: new FormControl(''),
            eFileSuppession: new FormControl(''),
            tutionAndRelatedExpenses: new FormControl(''),
            adjustmentMade: new FormControl(''),
            scholarshipsOrGrants: new FormControl(''),
            adjustmentScholarshipsOrGrants: new FormControl(''),
            acadamicPeriodsBegin: new FormControl(''),
            leastHalfTimeStudent: new FormControl(''),
            graduateStudent: new FormControl(''),
            reimbursementOrRefunds: new FormControl(''),
            releaseDate: new FormControl(''),
            eContentDate: new FormControl(''),
            memo: new FormControl(''),
            amountsBuildForQualified: new FormControl(''),
            educationalInstitution: new FormControl(false),
            modify_by: new FormControl(''),
            modify_date: new FormControl(''),
            ip_address: new FormControl(''),
        });

        this.taxyear = this.route.snapshot.queryParams.tax;
        this.student = this.route.snapshot.queryParams.student;
        this.sequence = this.route.snapshot.queryParams.sequence;

        if (this.taxyear >= 2015) {
            this.solicited = true;
        }

        this.getVersionList();
    }

    getHistoryList(){
        let tempData = {
            Student_Id: this.student,
            Sequence_Id: this.sequence,
            TaxYear: this.taxyear,
            FormVersion: this.taxVersions[this.selectedVersion].VALUE
        };
        this._form1098tService.taxFormHistory(tempData).subscribe(
            result => {
                if(result.isValid){
                    this.createAppForm.get('corrected').setValue(JSON.parse(result.data).BOX_CORRECTED_IND== 'X' ? true : false);
                    this.createAppForm.get('filerName').setValue(JSON.parse(result.data).BOX_FILER_NAME);
                    this.createAppForm.get('filerAddress1').setValue(JSON.parse(result.data).FILER_ADDRESS1);
                    this.createAppForm.get('filerAddress2').setValue(JSON.parse(result.data).FILER_ADDRESS2);
                    this.createAppForm.get('filerAddress3').setValue(JSON.parse(result.data).FILER_ADDRESS3);
                    this.createAppForm.get('filerCity').setValue(JSON.parse(result.data).FILER_CITY);
                    this.createAppForm.get('filerState').setValue(JSON.parse(result.data).FILER_STATE);
                    this.createAppForm.get('filerZip').setValue(JSON.parse(result.data).FILER_ZIP);
                    this.createAppForm.get('filerTelephoneNo1').setValue(JSON.parse(result.data).BOX_FILER_PHONE);
                    this.createAppForm.get('filerTelephoneNo2').setValue(JSON.parse(result.data).BOX_FILER_PHONE2);
                    this.createAppForm.get('filerFederalIdentificationNumber').setValue(JSON.parse(result.data).BOX_FILER_FEDERAL_ID_NUMBER);
                    this.createAppForm.get('studentSocialSecurityNumber').setValue(JSON.parse(result.data).BOX_STUDENT_SSN);
                    this.createAppForm.get('ssnSocialized').setValue(JSON.parse(result.data).SSN_SOLICITED);
                    this.createAppForm.get('studentFirstName').setValue(JSON.parse(result.data).STUDENT_FIRST_NAME);
                    this.createAppForm.get('studentLastName').setValue(JSON.parse(result.data).STUDENT_LAST_NAME);
                    this.createAppForm.get('studentAddress1').setValue(JSON.parse(result.data).STUDENT_ADDRESS1);
                    this.createAppForm.get('studentAddress2').setValue(JSON.parse(result.data).STUDENT_ADDRESS2);
                    this.createAppForm.get('studentAddress3').setValue(JSON.parse(result.data).STUDENT_ADDRESS3);
                    this.createAppForm.get('studentCity').setValue(JSON.parse(result.data).STUDENT_CITY);
                    this.createAppForm.get('studentState').setValue(JSON.parse(result.data).STUDENT_STATE);
                    this.createAppForm.get('studentZip').setValue(JSON.parse(result.data).STUDENT_ZIP);
                    this.createAppForm.get('forignCountry').setValue(JSON.parse(result.data).FOREIGN_COUNTRY_INDICATOR == 'X' ? true : false);
                    this.createAppForm.get('studentCountry').setValue(JSON.parse(result.data).FOREIGN_COUNTRY);
                    this.createAppForm.get('serviceProviderAccNumber').setValue(JSON.parse(result.data).BOX_ACCOUNT_NUMBER);
                    this.createAppForm.get('printSuppession').setValue(JSON.parse(result.data).PRINT_SUPPRESSION  == 'Y' ? true : false);
                    this.createAppForm.get('eFileSuppession').setValue(JSON.parse(result.data).EFILE_SUPPRESSION  == 'Y' ? true : false);
                    this.createAppForm.get('tutionAndRelatedExpenses').setValue(JSON.parse(result.data).BOX1_PAYMENTS_RECEIVED);
                    this.createAppForm.get('adjustmentMade').setValue(JSON.parse(result.data).BOX4_ADJ_MADE_PRIOR_YR);
                    this.createAppForm.get('scholarshipsOrGrants').setValue(JSON.parse(result.data).BOX5_SCHOLARSHIP_GRANTS);
                    this.createAppForm.get('adjustmentScholarshipsOrGrants').setValue(JSON.parse(result.data).BOX6_ADJ_SCHOLARSHIP_PRIOR_YR);
                    this.createAppForm.get('acadamicPeriodsBegin').setValue(JSON.parse(result.data).BOX7_ACADEMIC_PERIOD_IND == 'X' ? true : false);
                    this.createAppForm.get('leastHalfTimeStudent').setValue(JSON.parse(result.data).BOX8_HALF_TIME_STUDENT_IND  == 'X' ? true : false);
                    this.createAppForm.get('graduateStudent').setValue(JSON.parse(result.data).BOX9_GRADUATE_STUDENT_IND  == 'x' ? true : false);
                    this.createAppForm.get('reimbursementOrRefunds').setValue(JSON.parse(result.data).BOX10_REIMBURSEMENT_OR_REFUND);
                    this.createAppForm.get('amountsBuildForQualified').setValue(JSON.parse(result.data).BOX2_AMOUNTS_BILLED);
                    this.createAppForm.get('educationalInstitution').setValue(JSON.parse(result.data).BOX3_RPT_MTHD_CHG_IND  == 'X' ? true : false);    
                    this.createAppForm.get('releaseDate').setValue(moment(JSON.parse(result.data).FORM_RELEASE_DATE).format('MM-DD-YYYY'));    
                    this.createAppForm.get('memo').setValue(JSON.parse(result.data).Memo);    
                    this.createAppForm.get('modify_by').setValue(JSON.parse(result.data).MODIFIED_BY);    
                    this.createAppForm.get('modify_date').setValue(moment(JSON.parse(result.data).Last_Change_Date).format('MM-DD-YYYY'));    
                    this.createAppForm.get('ip_address').setValue(JSON.parse(result.data).IPADDRESS);    
                }
            });
    }


    getVersionList(){
        let tempData = {
            Student_Id: this.student,
            Sequence_Id: this.sequence,
            TaxYear: this.taxyear
        };
        this._form1098tService.taxFormVersion(tempData).subscribe(
            result => {
                if(result.isValid && result.data){
                    this.taxVersions = JSON.parse(result.data);
                    this.selectedVersion = this.taxVersions.length - 1;
                    this.getHistoryList();
                }
            });
    }

    onYearAdd(year) {
        return parseInt(year) + 1;
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(ViewFormDialogue);

        dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        });
    }

    onClickBacktoCurrent(){
        this.router.navigate(["update1098t"],{ queryParams: { data: this.taxyear,seq:this.sequence,stuid:this.student } });
    }

    onClickPrev(){
        if(this.selectedVersion != 0){
            this.selectedVersion = this.selectedVersion - 1;
            this.getHistoryList();
        }
    }

    onClickNext(){
        if(this.selectedVersion != this.taxVersions.length - 1){
            this.selectedVersion = this.selectedVersion + 1;
            this.getHistoryList();
        }
    }

    onBlurFloat(event){
        if(event.target.value !== ''){
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }

}
