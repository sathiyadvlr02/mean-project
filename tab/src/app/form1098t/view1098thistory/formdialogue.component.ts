import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'form-dialogue',
    templateUrl: './formdialogue.component.html',
    
})

export class ViewFormDialogue implements OnInit {

    formMethod = 'list';

    displayedColumns: string[] = ['Transaction_Date', 'Campus_Code', 'Program_Name', 'Student_LastName',
     'Financial_Type','Academic_Term','Academic_Year','Financial_Description','action'];
    dataSource = ELEMENT_DATA;

    constructor(){
        this.formMethod = 'list';
    }

    ngOnInit(): void {

    }

    changePopUpMode(type){
        this.formMethod = type;
    }
}

export interface PeriodicElement {
    Transaction_Date: string;
    Campus_Code: string;
    Program_Name: string;
    Student_LastName: string;
    Financial_Type: string;
    Academic_Term: string;
    Academic_Year: string;
    Financial_Description: string;
    Amount: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'ALL PAYMENTS', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'TUITION AND FEES (a)', Amount: '100.00'
    },
    {
        Transaction_Date: '20180701', Campus_Code: 'MAIN', Program_Name: 'MAIN', Student_LastName: 'LAMBERT', Financial_Type: 'Q01', Academic_Term: 'FA', Academic_Year: '2020', Financial_Description: 'SCHOLARSHIP', Amount: '100.00'
    },

];