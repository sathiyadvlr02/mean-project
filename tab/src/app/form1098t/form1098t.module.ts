import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Form1098TRoutes } from './form1098t.routing';
import { ChartistModule } from 'ng-chartist';

import { FormCreate1098T } from './create1098t/formcreate1098t.component';
import { FormDialogue } from './create1098t/formdialogue.component';
import { UpdateFormDialogue } from './update1098t/formdialogue.component';
import { ViewFormDialogue } from './view1098thistory/formdialogue.component';
import { FormEmail1098T } from './email1098t/email1098t.component';
import { SetPassword } from './email1098t/setpassword.component';
import { FormUpdate1098T } from './update1098t/formupdate1098t.component';
import { FormViewHistory1098TComponent } from './view1098thistory/formviewhistory1098t.component';


@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(Form1098TRoutes)
  ],
  declarations: [
    FormCreate1098T,
    FormUpdate1098T,
    FormViewHistory1098TComponent,
    FormDialogue,
    UpdateFormDialogue,
    ViewFormDialogue,
    FormEmail1098T,
    SetPassword,

  ]
})
export class Form1098TModule { }
