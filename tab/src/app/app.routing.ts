import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';


import { LoginComponent } from './login/login.component';
import { Login1098Component } from './login/login1098.component';
import { LoginspsgzComponent } from './login/loginspsgz.component';

import { SendCodeComponent } from './send-code/send-code.component';
import { ValidateCodeComponent } from './validate-code/validate-code.component';

import { SendCode1098TComponent } from './send-code/send-code1098t.component';
import { ValidateCode1098TComponent } from './validate-code/validate-code1098t.component';

import { SendCodeSPSGZComponent } from './send-code/send-codeSPSGZ.component';
import { ValidateCodeSPSGZComponent } from './validate-code/validate-codeSPSGZ.component';


import { ForgotComponent } from './forgot/forgot.component';
import { ForgotSPSGZComponent } from './forgot/forgotspsgz.component';
import { Forgot1098Component } from './forgot/forgot1098.component';

import { ResetPasswordComponent } from './login/resetpassword.component';
import { ResetPassword1098Component } from './login/resetpassword1098.component';
import { ResetPasswordspsgzComponent } from './login/resetpasswordspsgz.component';

import { StudentViewComponent } from './studentforms/viewforms/studentviewform.component';
import { OnlineConsentComponent } from './studentforms/onlineconsent/onlineconsent.component';
import { AdminLayoutComponent } from './layouts/full/adminlayout.component';

import { DragandDropComponent } from './test/draganddrop.component';
import { AuthenticationGuard } from './services/auth/authentication.guard';

export const AppRoutes: Routes = [
    { path: '', component: LoginComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'login1098', component: Login1098Component },
    { path: 'loginspsgz', component: LoginspsgzComponent },
    { path: 'validate-code', component: ValidateCodeComponent},
    { path: 'send-code', component: SendCodeComponent},
    { path: 'validate-code1098t', component: ValidateCode1098TComponent},
    { path: 'send-code1098t', component: SendCode1098TComponent},
    { path: 'validate-codespsgz', component: ValidateCodeSPSGZComponent},
    { path: 'send-codespsgz', component: SendCodeSPSGZComponent},
    { path: 'forgot', component: ForgotComponent },
    { path: 'forgotspsgz', component: ForgotSPSGZComponent },
    { path: 'forgot1098', component: Forgot1098Component },
    { path: 'resetpassword', component: ResetPasswordComponent },
    { path: 'resetpassword1098t', component: ResetPassword1098Component },
    { path: 'resetpasswordspsgz', component: ResetPasswordspsgzComponent },
    { path: 'studentaccess', component: StudentViewComponent },
    { path: 'onlineconsent', component: OnlineConsentComponent },
    { path: 'draganddrop', component: DragandDropComponent },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            }
        ]
    },
    {
        path: 'home',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./managestudent/managestudent.module').then(m => m.ManageStudentModule)
            }

        ]
    },
    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./managestudent/managestudent.module').then(m => m.ManageStudentModule)
            }

        ]
    },
    {
        path: 'managesites',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./managesites/managesites.module')
                    .then(m => m.ManagesitesModule)
            }
        ]
    },

    /* {
        path: 'administration',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./managesites/managesites.module')
                    .then(m => m.ManagesitesModule)
            }
        ]
    }, */


    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./managesites/managesites.module')
                    .then(m => m.ManagesitesModule)
            }
        ]
    },
    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099misc/form109misc.module').then(m => m.Form1099MISCModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1098t/form1098t.module').then(m => m.Form1098TModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099nec/form1099nec.module').then(m => m.Form1099NecModule)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099int/form109int.module').then(m => m.Form1099INTModule)
            }
        ]
    },
    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099r/form1099r.module').then(m => m.Form1099RModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099c/form1099c.module').then(m => m.Form1099CModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099sa/form1099sa.module').then(m => m.Form1099SAModule)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form5498sa/form5498sa.module').then(m => m.Form5498SAModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1099div/form109div.module').then(m => m.Form1099DIVModule)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form3922/form3922.module').then(m => m.Form3922Module)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form3921/form3921.module').then(m => m.Form3921Module)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./formt4a/formt4a.module').then(m => m.Formt4aModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1042s/form1042s.module').then(m => m.Form1042sModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1094b/form1094b.module').then(m => m.Form1094BModule)
            }
        ]
    },

    {
        path: '1095bmanagement',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1095b/form1095b.module').then(m => m.Form1095BModule)
            }
        ]
    },
    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1095b/form1095b.module').then(m => m.Form1095BModule)
            }
        ]
    },
    

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1094c/form1094c.module').then(m => m.Form1094CModule)
            }
        ]
    },


    {
        path: 'employeemanagement',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./form1095c/form1095c.module').then(m => m.Form1095CModule)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./changepassword/changepassword.module').then(m => m.changepasswordModule)
            }

        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./profile/profile.module').then(m => m.profileModule)
            }

        ]
    },

    {
        path: 'reports/statisticalreports',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./statisticalreports/statisticalreports.module').then(m => m.StatisticalReportsModule)
            }
        ]
    },


    {
        path: 'formsandreports',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./statisticalreports/statisticalreports.module').then(m => m.StatisticalReportsModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./statisticalreports/statisticalreports.module').then(m => m.StatisticalReportsModule)
            }
        ]
    },

    {
        path: 'downloads',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./downloadsupport/downloadsupport.module')
                    .then(m => m.DownloadSupportModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./downloadsupport/downloadsupport.module')
                    .then(m => m.DownloadSupportModule)
            }
        ]
    },



    {
        path: 'formsmanagement',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./formsmanagement/formsmanagement.module').then(m => m.FormsManagementModule)
            }
        ]
    },

    {
        path: 'filemanagement',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./filetransfer/filetransfer.module')
                    .then(m => m.FileTransferModule)
            }
        ]
    },


    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./filetransfer/filetransfer.module')
                    .then(m => m.FileTransferModule)
            }
        ]
    },

    {
        path: '',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./administration/administration.module')
                    .then(m => m.AdministrationModule)
            }
        ]
    },

    {
        path: 'home',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./administration/administration.module')
                    .then(m => m.AdministrationModule)
            }
        ]
    },

    {
        path: 'administration',
        component: FullComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./administration/administration.module')
                    .then(m => m.AdministrationModule)
            }
        ]
    },

    {
        path: 'menumanagement',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./menumanagement/menumanagement.module')
                    .then(m => m.MenumanagementModule)
            }
        ]
    },

    {
        path: 'filemanagement',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./menumanagement/menumanagement.module')
                    .then(m => m.MenumanagementModule)
            }
        ]
    },

    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./menumanagement/menumanagement.module')
                    .then(m => m.MenumanagementModule)
            }
        ]
    },

    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./create-efile/create-efile.module')
                    .then(m => m.CreateEfileModule)
            }
        ]
    },
    {
        path: 'filetransfermanagement',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./filetransfermanagement/filetransfermanagement.module')
                    .then(m => m.FiletransfermanagementModule)
            }
        ]
    },
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./filetransfermanagement/filetransfermanagement.module')
                    .then(m => m.FiletransfermanagementModule)
            }
        ]
    },

    {
        path: 'formsandreports',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./formsandreports-sa/formsandreports-sa.module')
                    .then(m => m.FormsandreportsSAModule)
            }
        ]
    },
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./formsandreports-sa/formsandreports-sa.module')
                    .then(m => m.FormsandreportsSAModule)
            }
        ]
    },

    {
        path: 'formsandreports/statisticalreports',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./statisticalreports/statisticalreports.module').then(m => m.StatisticalReportsModule)
            }
        ]
    },
    {
        path: 'systemmanagement',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./systemmanagement/systemmanagement.module')
                    .then(m => m.SystemmanagementModule)
            }
        ]
    },
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./systemmanagement/systemmanagement.module')
                    .then(m => m.SystemmanagementModule)
            }
        ]
    },


    {
        path: 'administrationsa',
        component: AdminLayoutComponent,
        canActivate:[AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./administration/administration.module')
                    .then(m => m.AdministrationModule)
            }
        ]
    },


];
