import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MenumanagementRoutes } from './menumanagement-routing.module';
import { AdminMenuItemComponent } from './admin-menu-item/admin-menu-item.component';
import { DialogOverviewExampleDialog } from './admin-menu-item/admin-menu-item.component';
import { AdminCustomMenuComponent } from './admin-custom-menu/admin-custom-menu.component';
import { AdminDefaultMenuComponent } from './admin-default-menu/admin-default-menu.component';


@NgModule({
  declarations: [DialogOverviewExampleDialog, AdminMenuItemComponent, AdminCustomMenuComponent, AdminDefaultMenuComponent],
  imports: [
      CommonModule,
      AppMaterialModule,
      FormsModule, ReactiveFormsModule,
      FlexLayoutModule,
      RouterModule.forChild(MenumanagementRoutes)
  ]
})
export class MenumanagementModule { }
