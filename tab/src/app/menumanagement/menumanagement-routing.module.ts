import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminMenuItemComponent } from './admin-menu-item/admin-menu-item.component';
import { AdminCustomMenuComponent } from './admin-custom-menu/admin-custom-menu.component';
import { AdminDefaultMenuComponent } from './admin-default-menu/admin-default-menu.component';



export const MenumanagementRoutes: Routes = [
    {
        path: 'menuadminmenuitem',
        component: AdminMenuItemComponent
    },

    {
        path: 'menuadmincustommenu',
        component: AdminCustomMenuComponent
    },

    {
        path: 'menuadmindefaultmenu',
        component: AdminDefaultMenuComponent
    },
]
