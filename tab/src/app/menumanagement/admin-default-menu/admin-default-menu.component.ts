import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

import { Contact } from "../../interfaces/menum";
//import { contacts as sampleContacts } from "../../interfaces/menum";
import { AdminmenuService } from '../../services/adminmenu.service';
import { ToastrService } from 'ngx-toastr';

import {
    FormGroup,
    FormControl,
    FormBuilder,
    Validators,
    FormArray
  } from '@angular/forms';

interface PendingSelection {
	[key: number] : Array<any>;
}


@Component({
    selector: 'app-admin-default-menu',
    styleUrls: [ "./admin-default-menu.component.scss" ],
    templateUrl: './admin-default-menu.component.html'
})
export class AdminDefaultMenuComponent implements OnInit {

    public contacts: Contact[];
	public pendingSelection: PendingSelection;
	public selectedContacts: Contact[];
	public unselectedContacts: Contact[];
    userTypeForDropdown: any;
    findForm: FormGroup;
    displayDragAndDrop = false;

    items = [
        'Change Password',
        'Online Consent',
        'Create Employee User',
        'Modify Employee User',
        'Delete Employee User'
    ];

    basket = [
        'Manage System User',
        'View Forms and Reports',
        'Site/Entity Manager',
        'Contracts/Service Agreements'
    ];

    constructor(
        private _adminMenuService: AdminmenuService,
        private fb: FormBuilder,
        public toasterService: ToastrService,
    ) {

        this.getAllSiteDetails();
        this.getAllUserTypeDropDown();
        this.pendingSelection = Object.create( null );

    }

    ngOnInit(): void {
        this.findForm = this.fb.group({
            usertype: new FormControl('', [Validators.required]),
        });
    }

    get trrowErrors() { return this.findForm.controls; }

    onFind(){
        console.log(this.findForm)
        if (this.findForm.invalid) {
            return;
        }
        this.getAllSiteDetails();
        let formDataValues = this.findForm.value;
        this._adminMenuService
        .getDefaultConfigMenu(formDataValues.usertype)
        .subscribe(
            (result) => {
                if(result.isValid){
                    //console.log('selected')
                    console.log('result', JSON.parse(result.data))
                    this.selectedContacts = JSON.parse(result.data);
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
        this.displayDragAndDrop = true;

    }

    getAllUserTypeDropDown(){
        this._adminMenuService
        .getAllUserTypeDetails()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.userTypeForDropdown = JSON.parse(result.data);
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

    getAllSiteDetails(){
        this._adminMenuService
        .getAllMenus()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    this.contacts = JSON.parse(result.data);
                    this.unselectedContacts = this.contacts;
		            //this.selectedContacts = [];
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

    public addAllToSelectedContacts() : void {
        var unSelectValues = this.unselectedContacts;
        unSelectValues.map((itm) => {
            this.togglePendingSelection(itm, itm.MenuItems ? itm.MenuItems : [] , true);
        })
        this.addToSelectedContacts();
    }


    public addToSelectedContacts( contact?: Contact ) : void {

        if(contact){
            var changeContacts = ( contact )
                ? [ contact ]
                : this.getPendingSelectionFromCollection( this.unselectedContacts )
            ;

            this.pendingSelection = Object.create( null );

            this.unselectedContacts = this.removeContactsFromCollection( this.unselectedContacts, changeContacts );

            this.selectedContacts = changeContacts.concat( this.selectedContacts );
        } else {
            var selectContent = [];
            var unSelectContent = [];

            this.unselectedContacts.map((itm) => {
                if(itm.Menu_Group_Id in this.pendingSelection){
                    if(itm.MenuItems){
                        var tempArray = JSON.parse(JSON.stringify(itm));
                        let tempSelectedChild = [];
                        let tempUnSelectedChild = [];

                        itm.MenuItems.map((chi) => {
                            if(this.pendingSelection[itm.Menu_Group_Id].includes(chi.menu_id)){
                                tempSelectedChild.push(chi);
                            } else {
                                tempUnSelectedChild.push(chi);
                            }
                        });
                        
                        if(tempUnSelectedChild.length > 0){
                            tempArray.MenuItems = tempUnSelectedChild;
                            unSelectContent.push(tempArray);
                        }
                        if(tempSelectedChild.length > 0){
                            itm.MenuItems = tempSelectedChild;
                            selectContent.push(itm);
                        } 
                        
                        
                    } else {
                        selectContent.push(itm);
                    }
                } else {
                    unSelectContent.push(itm);
                }
               // //console.log('itm', itm)
            });

            var tempSelected = JSON.parse(JSON.stringify(selectContent));
            var tempAlSelected = JSON.parse(JSON.stringify(this.selectedContacts));

            let arrIndex = [];
            tempSelected.map((itm, i) => {
                if(itm.Menu_Group_Id in this.pendingSelection){
                    let index = tempAlSelected.findIndex(x => x.Menu_Group_Id === itm.Menu_Group_Id);
                    if(index >= 0){
                        tempAlSelected[index].MenuItems = itm.MenuItems.concat(tempAlSelected[index].MenuItems);
                        arrIndex.push(i)
                        //tempSelected.splice(i, 1);
                    }
                }
            })

            for (var i = arrIndex.length -1; i >= 0; i--){
                tempSelected.splice(arrIndex[i],1);
            }
                
            //console.log('tempSelected', tempSelected)
            //console.log('tempAlSelected', tempAlSelected)


            this.unselectedContacts = unSelectContent;
            this.selectedContacts = tempSelected.concat( tempAlSelected );
            this.pendingSelection = Object.create( null );
        }
		

	}

    public removeAllFromSelectedContacts() : void {
        var selectValues = this.selectedContacts;
        selectValues.map((itm) => {
            this.togglePendingSelection(itm, itm.MenuItems ? itm.MenuItems : [], true);
        })
        this.removeFromSelectedContacts();
    }


	public removeFromSelectedContacts( contact?: Contact ) : void {

        if(contact){
            var changeContacts = ( contact )
                ? [ contact ]
                : this.getPendingSelectionFromCollection( this.selectedContacts )
            ;

            this.pendingSelection = Object.create( null );

            this.selectedContacts = this.removeContactsFromCollection( this.selectedContacts, changeContacts );

            this.unselectedContacts = changeContacts
                .concat( this.unselectedContacts )
                
            ;
        } else {
            var selectContent = [];
            var unSelectContent = [];

            this.selectedContacts.map((itm) => {
                if(itm.Menu_Group_Id in this.pendingSelection){
                    if(itm.MenuItems){
                        var tempArray = JSON.parse(JSON.stringify(itm));
                        let tempSelectedChild = [];
                        let tempUnSelectedChild = [];

                        itm.MenuItems.map((chi) => {
                            if(this.pendingSelection[itm.Menu_Group_Id].includes(chi.menu_id)){
                                tempSelectedChild.push(chi);
                            } else {
                                tempUnSelectedChild.push(chi);
                            }
                        });
                        
                        if(tempUnSelectedChild.length > 0){
                            tempArray.MenuItems = tempUnSelectedChild;
                            unSelectContent.push(tempArray);
                        }
                        if(tempSelectedChild.length > 0){
                            itm.MenuItems = tempSelectedChild;
                            selectContent.push(itm);
                        } 
                        
                        
                    } else {
                        selectContent.push(itm);
                    }
                } else {
                    unSelectContent.push(itm);
                }
               // //console.log('itm', itm)
            });

            var tempSelected = JSON.parse(JSON.stringify(selectContent));
            var tempAlSelected = JSON.parse(JSON.stringify(this.unselectedContacts));

            let arrIndex = [];
            tempSelected.map((itm, i) => {
                if(itm.Menu_Group_Id in this.pendingSelection){
                    let index = tempAlSelected.findIndex(x => x.Menu_Group_Id === itm.Menu_Group_Id);
                    if(index >= 0){
                        tempAlSelected[index].MenuItems = itm.MenuItems.concat(tempAlSelected[index].MenuItems);
                        arrIndex.push(i)
                        //tempSelected.splice(i, 1);
                    }
                }
            })

            for (var i = arrIndex.length -1; i >= 0; i--){
                tempSelected.splice(arrIndex[i],1);
            }
                
            //console.log('tempSelected', tempSelected)
            //console.log('tempAlSelected', tempAlSelected)


            this.unselectedContacts = tempSelected.concat( tempAlSelected );
            this.selectedContacts = unSelectContent;
            this.pendingSelection = Object.create( null );

        }

	}

	public togglePendingSelection( contact: Contact, child, event ) : void {
        console.log('contact', contact)
        console.log('child', child)
        let childIds = this.pendingSelection[ contact.Menu_Group_Id ] ? this.pendingSelection[ contact.Menu_Group_Id ] : [];
        
        if(event){
            if(child.length > 0){
                child.map((itm) => {
                    if (childIds.includes(itm.menu_id) === false) childIds.push(itm.menu_id);
                })
            }
            console.log('childIds', childIds)
            this.pendingSelection[ contact.Menu_Group_Id ] = childIds;
        } else {
            if(child.length > 0){
                child.map((itm) => {
                    let index = childIds.indexOf(itm.menu_id);
                    if (index > -1) {
                        childIds.splice(index, 1);
                    }
                })
            }
            if(!childIds.length){
                delete this.pendingSelection[ contact.Menu_Group_Id ];
            }
        }
        //console.log('this.pendingSelection', this.pendingSelection)

	}

	private getPendingSelectionFromCollection( collection: Contact[] ) : Contact[] {

        

		var selectionFromCollection = collection.filter(
			( contact ) => {
               
				return( contact.Menu_Group_Id in this.pendingSelection );

			}
		);
        
		return( selectionFromCollection );

	}


	private removeContactsFromCollection(
		collection: Contact[],
		contactsToRemove: Contact[]
		) : Contact[] {

		var collectionWithoutContacts = collection.filter(
			( contact ) => {

				return( ! contactsToRemove.includes( contact ) );

			}
		);

		return( collectionWithoutContacts );

	}


	private sortContactOperator( a: Contact, b: Contact ) : number {

		return( a.Menu_Group.localeCompare( b.Menu_Group ) );

	}

    updateMenuSettings(){
        let tempData = [];
        let formDataValues = this.findForm.value;
        let tempSelectedData = JSON.parse(JSON.stringify(this.selectedContacts));
        if(tempSelectedData){
            tempSelectedData.map((itm, i) => {
                let tempObj = {
                    menu_id: 0,
                    menu_group_id: itm.Menu_Group_Id,
                    menu_sequence_id: i + 1,
                }
                tempData.push(tempObj);
                if(itm.MenuItems){
                    itm.MenuItems.map((childitm, j) => {
                        let tempObj = {
                            menu_id: childitm.menu_id,
                            menu_group_id: childitm.menu_group_id,
                            menu_sequence_id: j + 1,
                        }
                        tempData.push(tempObj);
                    });
                }
                
            })
        }
        this._adminMenuService
        .updateDefaultConfig(formDataValues.usertype, tempData)
        .subscribe(
            (result) => {
                if(result.isValid){
                    this.toasterService.success(
                        result.message,
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                      );
                    
                } else {
                    this.toasterService.error(
                        'Please Try Again.',
                        '',
                        {
                            positionClass: 'toast-top-center',
                            timeOut: 3000
                        }
                      );
                }
            },
            (error) => {
                this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                  );
            }
        );
    }


    drop(event: CdkDragDrop<string[]>, lType: any) {

        let extData = JSON.parse(JSON.stringify(event.previousContainer.data));
        if(extData[event.previousIndex].menu_id){
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            } else {
                let parentId = extData[event.previousIndex].menu_group_id;
                

                if(lType == 'sl'){
                    let tempList = JSON.parse(JSON.stringify(this.unselectedContacts));
                    let obj = tempList.find(o => o.Menu_Group_Id == parentId);
                    this.togglePendingSelection(obj, [obj.MenuItems[event.previousIndex]], true);
                    this.addToSelectedContacts();
                } else {
                    let tempList = JSON.parse(JSON.stringify(this.selectedContacts));
                    let obj = tempList.find(o => o.Menu_Group_Id == parentId);
                    this.togglePendingSelection(obj, [obj.MenuItems[event.previousIndex]], true);
                    this.removeFromSelectedContacts();
                }
            }
        } else {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            } else {
                transferArrayItem(event.previousContainer.data,
                    event.container.data,
                    event.previousIndex,
                    event.currentIndex);
            } 
        }
        


    }



}
