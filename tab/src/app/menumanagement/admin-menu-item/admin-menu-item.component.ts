import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { AdminmenuService } from '../../services/adminmenu.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

export interface PeriodicElement {
    menu_id: string;
    menu_description: string;
    menu_function: string;
    menu_comments: string;
    side_menu: string;
    menu_group_id: string;
    access_level: string;
}

@Component({
    selector: 'app-admin-menu-item',
    templateUrl: './admin-menu-item.component.html',
    styleUrls: ['./admin-menu-item.component.css']
})

export class AdminMenuItemComponent implements OnInit {
    homeForm: FormGroup;

    displayedColumns: string[] = ['menu_id', 'menu_description', 'menu_function', 'access_level', 'menu_comments', 'action'];
    dataSource: MatTableDataSource<any>;
    menu_id: any;
    menu_description: any;


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private _adminMenuService: AdminmenuService,
        public dialog: MatDialog,
        public toasterService: ToastrService,
    ) {
        this.getAllSiteDetails();
     }

    ngOnInit(): void {

    }

    searchData(searchValue: any) {

        this.dataSource.filter = searchValue.trim().toLowerCase();
      
   }

    openDialog(element): void {
        let tempData = element.menu_description;
        const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
            width: '500px',
          data: element
        });
    
        dialogRef.afterClosed().subscribe(result => {
            if(result && tempData != result){
                this._adminMenuService
                .updateMenuItem({menu_id: element.menu_id, menu_description: result})
                .subscribe(
                    (result) => {
                        if(result.isValid){
                            this.toasterService.success(
                                result.message,
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                            
                        } else {
                            this.toasterService.error(
                                'Please Try Again.',
                                '',
                                {
                                    positionClass: 'toast-top-center',
                                    timeOut: 3000
                                }
                            );
                        }
                    },
                    (error) => {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                    }
                );
            }
        });
    }

    getAllSiteDetails(){
        this._adminMenuService
        .getAllMenus()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                    let tempArray = [];
                    JSON.parse(result.data).map((itm) => {
                        if(itm.MenuItems){
                            tempArray = tempArray.concat(itm.MenuItems);
                        }
                    })
                    console.log(tempArray)
                    this.dataSource = new MatTableDataSource(tempArray);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
    }

}


@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog-overview-example-dialog.html',
})

export class DialogOverviewExampleDialog {
    tempData: any;
    constructor(
      public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
      @Inject(MAT_DIALOG_DATA) public data: PeriodicElement) {
        this.tempData = data.menu_description;
          console.log('data', data)
      }
  
    onNoClick(): void {
        this.data.menu_description = this.tempData;
      this.dialogRef.close();
    }
  
  }


/* Static data */



/* const ELEMENT_DATA: PeriodicElement[] = [
    {
        tracking_number: '1', client: 'Upload File',
        file_name: 'uploadfile.aspx', notes: 'ALL',
        date_uploaded: 'Upload file to a logged in site', download_status: 'Edit', delete_status: 'Delete'
    },
    {
        tracking_number: '2', client: 'Change Password',
        file_name: 'ChangeTmpPass.aspx', notes: 'CA',
        date_uploaded: 'Change Recipient Password', download_status: 'Edit', delete_status: 'Delete'
    },
    {
        tracking_number: '3', client: 'Change Password',
        file_name: 'changePassword.aspx', notes: 'SA',
        date_uploaded: 'Change current user password', download_status: 'Edit', delete_status:'Delete'
    }
]; */
