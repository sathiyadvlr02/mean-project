import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { StatisticalReportsRoutes } from './statisticalreports.routing';
import { ChartistModule } from 'ng-chartist';
import { StatisticalReportsComponent } from './statisticalreports.component';
import { SubmitRunRequest } from './submitrunrequest.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(StatisticalReportsRoutes)
  ],
  declarations: [StatisticalReportsComponent, SubmitRunRequest]
})
export class StatisticalReportsModule {}
