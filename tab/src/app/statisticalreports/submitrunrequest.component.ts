import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

import { Form1098tService } from '../services/form1098t.service';
import { UserService } from '../services/user.service';

@Component({
    selector: 'app-submitrunrequest',
    templateUrl: './submitrunrequest.component.html'
})

export class SubmitRunRequest implements OnInit {

    submitRunRequestForm: FormGroup;

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    yearList = [
        { name: 'Select Year', value: '' },
        { name: '2020', value: '2020' },
        { name: '2019', value: '2019' },
        { name: '2018', value: '2018' },
        { name: '2017', value: '2017' },
        { name: '2016', value: '2016' },
        { name: '2015', value: '2015' },
        { name: '2014', value: '2014' },
        { name: '2013', value: '2013' },
        { name: '2012', value: '2012' },
        { name: '2011', value: '2011' },
        { name: '2010', value: '2010' },
        { name: '2005', value: '2005' },
    ];

    formType = [
        { name: '1098-T', value: '1098-T' }
    ];

    jobNameList = [];
    databaseLoadIdList = [];

    EINList = [];

    constructor(
        private _form1098tService: Form1098tService,
        private _userService: UserService,
        public toasterService: ToastrService,
        private el: ElementRef,
        private cookieService: CookieService,
    ) { }

    ngOnInit(): void {
        this.submitRunRequestForm = new FormGroup({
            formTypeData: new FormControl(this.formType[0].value, [Validators.required]),
            taxYear: new FormControl('2020', [Validators.required]),
            jobName: new FormControl('', [Validators.required]),
            EIN: new FormControl('', [Validators.required]),
            databaseLoadId: new FormControl('', [Validators.required]),
            comments: new FormControl(''),
        });
        this.getEINList();
    }

    getEINList(){
        this._form1098tService.getFederalIdentification().subscribe(
            result => {
                this.EINList = result.data;
            });
    }

    get trrowErrors() { return this.submitRunRequestForm.controls; }

    onChangeTaxYear(){
        if(this.submitRunRequestForm.value.taxYear){
            this.getJobNamesList();
            this.getDatabaseLoadIdList();
        }
        this.submitRunRequestForm.get('jobName').setValue('');
        this.submitRunRequestForm.get('databaseLoadId').setValue('');
    }

    getDatabaseLoadIdList(){
        this._form1098tService
        .getDatabaseLoadIds(this.submitRunRequestForm.value.formTypeData, this.submitRunRequestForm.value.taxYear)
        .subscribe(
            (result) => {
                if(result.isValid && result.data){
                    this.databaseLoadIdList = JSON.parse(result.data);
                } else {
                    this.databaseLoadIdList = [];
                }
            },
            (error) => {
            }
        )
    }

    getJobNamesList(){
        this._form1098tService
        .getJobNames(this.submitRunRequestForm.value.formTypeData, this.submitRunRequestForm.value.taxYear)
        .subscribe(
            (result) => {
                if(result.isValid && result.data){
                    this.jobNameList = JSON.parse(result.data);
                } else {
                    this.jobNameList = [];
                }
            },
            (error) => {
            }
        )
    }

    onSubmit(){
        if (this.submitRunRequestForm.invalid) {
            for (const key of Object.keys(this.submitRunRequestForm.controls)) {
              if (this.submitRunRequestForm.controls[key].invalid) {
                const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                invalidControl.focus();
                break;
              }
            }
            return;
        }
        let supportFormData = this.submitRunRequestForm.value;

        let datas = {
            sitE_ID: this.CookieSiteId.toString(),
            forM_TYPE: supportFormData.formTypeData,
            taX_YEAR: supportFormData.taxYear,
            joB_NAME: supportFormData.jobName,
            eiN_NUMBER: supportFormData.EIN,
            databasE_LOADID: supportFormData.databaseLoadId,
            description: supportFormData.comments
        };

        this._userService
        .submitRunRequest(datas)
        .subscribe(
            (result) => {
                if(result.isValid){
                    this.toasterService.success(
                    result.message,
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    });
                    this.submitRunRequestForm.get('formTypeData').setValue(this.formType[0].value);
                    this.submitRunRequestForm.get('taxYear').setValue('');
                    this.submitRunRequestForm.get('jobName').setValue('');
                    this.submitRunRequestForm.get('EIN').setValue('');
                    this.submitRunRequestForm.get('databaseLoadId').setValue('');
                    this.submitRunRequestForm.get('comments').setValue('');
                    this.jobNameList = [];
                    this.databaseLoadIdList = [];
                } else {
                    this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    });
                }
            },
            (error) => {
                this.toasterService.error(
                    'Please Try Again.',
                    '',
                    {
                        positionClass: 'toast-top-center',
                        timeOut: 3000
                    }
                );
            }
        );
    }

}