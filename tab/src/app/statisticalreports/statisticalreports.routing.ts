import { Routes } from '@angular/router';

import { StatisticalReportsModule } from './statisticalreports.module';
import { StatisticalReportsComponent } from './statisticalreports.component';
import { SubmitRunRequest } from './submitrunrequest.component';


export const StatisticalReportsRoutes: Routes = [
  {
  path: '',
  component: StatisticalReportsComponent
},
{
  path: '1098tfinancialreports',
  component: StatisticalReportsComponent
},
{
  path: '1099proofrequest',
  component: SubmitRunRequest
}

];
