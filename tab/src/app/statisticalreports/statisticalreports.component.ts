import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-statisticalreports',
  templateUrl: './statisticalreports.component.html'
})
export class StatisticalReportsComponent implements OnInit {
    staticreport: FormGroup;
  constructor() { }
  get Errors() { return this.staticreport.controls; }
  ngOnInit(): void {
    this.staticreport = new FormGroup({
        sitetype : new FormControl('', [Validators.required]),
        school :   new FormControl('', [Validators.required]),
        campus : new FormControl('', [Validators.required]),
        program : new FormControl('', [Validators.required]),
        selectreport: new FormControl('', [Validators.required]),
        datatype: new FormControl('', [Validators.required])

    })
  }

}
