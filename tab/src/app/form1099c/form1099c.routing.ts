import { Routes } from '@angular/router';
 
import { Create1099CComponent } from './create1099cform/create1099c.component'; 
import { Update1099CComponent } from './update1099cform/update1099c.component'; 
import { History1099CComponent } from './history1099cform/history1099c.component'; 

export const Form1099CRoutes: Routes = [
   
  {
    path: 'create1099c',
    component: Create1099CComponent  
  },
  {
    path: 'form1099ccreate',
    component: Create1099CComponent  
  },
  {
    path: 'form1099cupdate',
    component: Update1099CComponent 
  },
  {
    path: 'form1099chistory',
    component: History1099CComponent 
  },
];
