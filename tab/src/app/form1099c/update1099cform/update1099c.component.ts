import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-update1099c',
  templateUrl: './update1099c.component.html',
  
})
export class Update1099CComponent implements OnInit {
  homeForm: FormGroup;
  
  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }
  
  TooltipMouseenter(event: Event, boxtooltip: any): void {
    event.stopImmediatePropagation();
    event.preventDefault();

    if (!boxtooltip._isTooltipVisible()) {
        boxtooltip.show();
    }

}
TooltipMouseout(event: Event, boxtooltip: any): void {
    event.stopImmediatePropagation();
    event.preventDefault();

    if (!boxtooltip._isTooltipVisible()) {
        boxtooltip.hide();
    }

}


Showtooltip(event: Event, boxtooltip: any): void {

    event.stopImmediatePropagation();
    event.preventDefault();

    if (!boxtooltip._isTooltipVisible()) {
        boxtooltip.show();
    }
}


Hidetooltip(event: Event, boxtooltip: any): void {

    event.stopImmediatePropagation();
    event.preventDefault();

    if (!boxtooltip._isTooltipVisible()) {
        boxtooltip.hide();
    }
}


}

