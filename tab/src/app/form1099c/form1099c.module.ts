import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1099CRoutes } from './form1099c.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099CComponent } from './create1099cform/create1099c.component'; 
import { Update1099CComponent } from './update1099cform/update1099c.component'; 
import { History1099CComponent } from './history1099cform/history1099c.component'; 

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Form1099CRoutes)
  ],
  declarations: [Create1099CComponent,Update1099CComponent ,History1099CComponent]
})
export class Form1099CModule {}
