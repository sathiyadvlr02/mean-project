import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SystemmanagementRoutes } from './systemmanagement-routing.module';
import { WatchedFolderComponent } from './watched-folder/watched-folder.component';
import { CustomApprovalTextComponent } from './custom-approval-text/custom-approval-text.component';
import { ViewAuditLogComponent } from './view-audit-log/view-audit-log.component';
import { EditSystemMessagesComponent } from './edit-system-messages/edit-system-messages.component';
import { ListSystemMessagesComponent } from './edit-system-messages/list-system-messages.component';


@NgModule({
  declarations: [WatchedFolderComponent, CustomApprovalTextComponent, ViewAuditLogComponent, EditSystemMessagesComponent,ListSystemMessagesComponent],
  imports: [
      CommonModule,
      AppMaterialModule,
      FormsModule, ReactiveFormsModule,
      FlexLayoutModule,
      RouterModule.forChild(SystemmanagementRoutes)
  ]
})
export class SystemmanagementModule { }
