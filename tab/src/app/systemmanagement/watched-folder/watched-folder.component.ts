import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-watched-folder',
  templateUrl: './watched-folder.component.html',
  styleUrls: ['./watched-folder.component.css']
})

export class WatchedFolderComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    FPselectedRow:any;
    FDselectedRow:any;
    displayedColumns: string[] = ['first_name', 'last_name', 'SSN', 'login_name', 'TaxFormType', 'ElapsedTime','Status'];
    dataSource = ELEMENT_DATA;
    dataSource_Pending = ELEMENT_DATA_Pending;
    dataSource_Failed = ELEMENT_DATA_Failed;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor() { }

  ngOnInit(): void {
  }

}
export interface PeriodicElement {
    first_name: string;
    last_name: string;
    SSN: string;
    login_name: string;
    TaxFormType: string;
    ElapsedTime: string;
    Status: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'SUCCESS'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'SUCCESS'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'SUCCESS'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020', TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'SUCCESS'}

];


const ELEMENT_DATA_Pending: PeriodicElement[] = [
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'Processing'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Pending'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Pending'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020', TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Pending'}

];


const ELEMENT_DATA_Failed: PeriodicElement[] = [
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:01',Status:'Failed'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Failed'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020' ,TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Failed'},
    { first_name: '11137', last_name: 'General Test Company 1', login_name: 'Batch Type: OutputManager ; Job Number :20200720150350', SSN: '07/20/2020', TaxFormType:'3922',ElapsedTime:'00:00:00',Status:'Failed'}

];


