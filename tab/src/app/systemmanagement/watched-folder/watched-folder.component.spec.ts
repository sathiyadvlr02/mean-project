import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchedFolderComponent } from './watched-folder.component';

describe('WatchedFolderComponent', () => {
  let component: WatchedFolderComponent;
  let fixture: ComponentFixture<WatchedFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchedFolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchedFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
