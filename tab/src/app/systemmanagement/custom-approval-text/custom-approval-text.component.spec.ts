import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomApprovalTextComponent } from './custom-approval-text.component';

describe('CustomApprovalTextComponent', () => {
  let component: CustomApprovalTextComponent;
  let fixture: ComponentFixture<CustomApprovalTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomApprovalTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomApprovalTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
