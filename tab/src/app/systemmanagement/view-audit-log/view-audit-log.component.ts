import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-view-audit-log',
  templateUrl: './view-audit-log.component.html',
  styleUrls: ['./view-audit-log.component.css']
})
export class ViewAuditLogComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    displayedColumns: string[] = ['first_name', 'last_name', 'SSN', 'login_name', 'TaxFormType', 'ElapsedTime', 'Status', 'Before','After'];
    dataSource = ELEMENT_DATA;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor() { }

  ngOnInit(): void {
  }

}

export interface PeriodicElement {
    first_name: string;
    last_name: string;
    SSN: string;
    login_name: string;
    TaxFormType: string;
    ElapsedTime: string;
    Status: string;
    Before: string;
    After: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { first_name: '1', last_name: '1-100013', login_name: 'igs1099', SSN: '2020-08-13', TaxFormType: '02:09:37', ElapsedTime: '10001', Status: 'Successful Login',Before:'',After:'' },
    { first_name: '1', last_name: '1-100013', login_name: 'igs1099', SSN: '2020-08-13', TaxFormType: '02:09:37', ElapsedTime: '10001', Status: 'Successful Login',Before:'',After:'' },
    { first_name: '1', last_name: '1-100013', login_name: 'igs1099', SSN: '2020-08-13', TaxFormType: '02:09:37', ElapsedTime: '10001', Status: 'Successful Login',Before:'',After:'' },
    { first_name: '1', last_name: '1-100013', login_name: 'igs1099', SSN: '2020-08-13', TaxFormType: '02:09:37', ElapsedTime: '10001', Status: 'Successful Login',Before:'',After:'' }

];

