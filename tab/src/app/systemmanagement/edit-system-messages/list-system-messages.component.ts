import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { EditSystemMessagesComponent } from '../edit-system-messages/edit-system-messages.component';



@Component({
  selector: 'app-list-system-messages',
  templateUrl: './list-system-messages.component.html' 
})
export class ListSystemMessagesComponent implements OnInit {
    displayedColumns: string[] = ['first_name', 'last_name', 'login_name', 'action'];
    dataSource = ELEMENT_DATA;
    selectedRow:any;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
  
    constructor(public router: Router ,public dialog: MatDialog) {
    }
  

    ngOnInit(): void {
        
  }

  openInsertDialog() {
    const dialogRef = this.dialog.open(EditSystemMessagesComponent, {        
        width: '700px',
      }
      );

    dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
    });
}



}

/* Static data */

export interface PeriodicElement {
    first_name: string;
    last_name: string;
    login_name: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        first_name: '10001', last_name: 'I', login_name: 'Successful Login' 
    },
    { first_name: '10002', last_name: 'I', login_name: 'Password Changed' },
    { first_name: '10003', last_name: 'E', login_name: 'Consent Accepted' },
    { first_name: '10004', last_name: 'E', login_name: 'Consent Withdrawn' }

];