import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-edit-system-messages',
  templateUrl: './edit-system-messages.component.html'
})
export class EditSystemMessagesComponent implements OnInit {
    displayedColumns: string[] = ['first_name', 'last_name', 'login_name', 'action'];
    dataSource = ELEMENT_DATA;


    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor() { }

    ngOnInit(): void {
        
  }

}

/* Static data */

export interface PeriodicElement {
    first_name: string;
    last_name: string;
    login_name: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    {
        first_name: '10001', last_name: 'I', login_name: 'Successful Login' 
    },
    { first_name: '10002', last_name: 'I', login_name: 'Password Changed' },
    { first_name: '10003', last_name: 'E', login_name: 'Consent Accepted' },
    { first_name: '10004', last_name: 'E', login_name: 'Consent Withdrawn' }

];