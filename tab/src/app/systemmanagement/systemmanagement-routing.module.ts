import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WatchedFolderComponent } from './watched-folder/watched-folder.component';
import { CustomApprovalTextComponent } from './custom-approval-text/custom-approval-text.component';
import { ViewAuditLogComponent } from './view-audit-log/view-audit-log.component';
import { EditSystemMessagesComponent } from './edit-system-messages/edit-system-messages.component';
import { ListSystemMessagesComponent } from './edit-system-messages/list-system-messages.component';

export const SystemmanagementRoutes: Routes = [
    {
        path: 'watched-folder',
        component: WatchedFolderComponent
    },

    {
        path: 'custom-approval-text',
        component: CustomApprovalTextComponent
    },
    {
        path: 'system-messages',
        component: ListSystemMessagesComponent
    },
    {
        path: 'edit-system-messages',
        component: EditSystemMessagesComponent
    },
    {
        path: 'view-audit-log',
        component: ViewAuditLogComponent
    },
]
