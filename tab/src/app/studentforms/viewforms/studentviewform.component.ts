import { Component, OnInit, ViewEncapsulation ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatRadioChange } from '@angular/material';

declare var require: any;
 
@Component({
	selector: 'app-studentview',
	templateUrl: './studentviewform.component.html'
	 
})
export class StudentViewComponent  {
    studentForm: FormGroup;
    selectedRow:any;
	loading: boolean;
	public show: boolean = false;
	public showlbl: boolean = true;
	public showcirclebtn: boolean = false;
	displayedColumns: string[] = ['function', 'seq', 'first_name', 'last_name', 'ssn', 'school_student_id'];
  dataSource = ELEMENT_DATA;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
}
	radioChange(e1,element:PeriodicElement) {
		this.dataSource.forEach(p=>p.selected = false);
        element.selected = !element.selected;
		this.show = true;
		this.showcirclebtn = true;
		this.showlbl = false;

	}

	click_circle():void{  
        this.show = false;
        this.showlbl = true;
        this.showcirclebtn = false;
        this.dataSource.forEach(p=>p.selected = false);
    }   
}



/* Static data */

/* Static data */

export interface PeriodicElement {
	function: string;
	seq: string;
	first_name: string;
	last_name: string;
	ssn: string;
	school_student_id: string;
	selected: boolean;
  }
  
  const ELEMENT_DATA: PeriodicElement[] = [
	{
	  function: 'View/Print', seq: '0',
	  first_name: 'Michela', last_name: 'BRUNELLI',
	  ssn: '999-99-9999', school_student_id: '21691639',selected: false
	},
	{
	  function: 'View/Print', seq: '0',
	  first_name: 'Michela', last_name: 'BRUNELLI',
	  ssn: '999-99-9999', school_student_id: '21691639',selected: false
	},
	{
	  function: 'View/Print', seq: '0',
	  first_name: 'Michela', last_name: 'BRUNELLI',
	  ssn: '999-99-9999', school_student_id: '21691639',selected: false
	}
  ];
  