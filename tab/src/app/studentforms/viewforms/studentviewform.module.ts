import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { StudentViewRoutes } from './studentviewform.routing';

import { StudentViewComponent } from './studentviewform.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    
    RouterModule.forChild(StudentViewRoutes)
  ],
  declarations: [StudentViewComponent]
})
export class StudentViewModule {}
