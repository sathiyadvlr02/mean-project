import { Routes } from '@angular/router';

import { StudentViewModule } from './studentviewform.module';
import { StudentViewComponent } from './studentviewform.component';


export const StudentViewRoutes: Routes = [
  {
  path: '',
  component: StudentViewComponent
},


];
