import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BatchProcessingComponent } from './batch-processing/batch-processing.component';
import { ClientRetrievalComponent } from './client-retrieval/client-retrieval.component';
import { FileTrackingSAComponent } from './file-tracking-sa/file-tracking-sa.component';
import { AddfilesComponent } from './addfiles/addfiles.component';

export const FiletransfermanagementRoutes: Routes = [
    {
        path: 'batch-processing',
        component: BatchProcessingComponent
    },

    {
        path: 'client-retrieval',
        component: ClientRetrievalComponent
    },
    {
        path: 'client-retrieval-add',
        component: AddfilesComponent
    },
    {
        path: 'file-tracking-sa',
        component: FileTrackingSAComponent
    },
]
