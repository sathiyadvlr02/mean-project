import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FiletransfermanagementRoutes } from './filetransfermanagement-routing.module';
import { BatchProcessingComponent } from './batch-processing/batch-processing.component';
import { ClientRetrievalComponent } from './client-retrieval/client-retrieval.component';
import { FileTrackingSAComponent } from './file-tracking-sa/file-tracking-sa.component';
import { EditFileTrackingSAComponent } from './file-tracking-sa/Edit-file-tracking.component';
import { AddfilesComponent } from './addfiles/addfiles.component';

@NgModule({
    declarations: [
        BatchProcessingComponent,
        ClientRetrievalComponent, AddfilesComponent,
        FileTrackingSAComponent,
        EditFileTrackingSAComponent],
    imports: [
        CommonModule,
        AppMaterialModule,
        FormsModule, ReactiveFormsModule,
        FlexLayoutModule,
        RouterModule.forChild(FiletransfermanagementRoutes)
    ]
})
export class FiletransfermanagementModule { }
