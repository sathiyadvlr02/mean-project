import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FileValidators } from "ngx-file-drag-drop";
@Component({
  selector: 'app-batch-processing',
  templateUrl: './batch-processing.component.html',
  styleUrls: ['./batch-processing.component.css']
})
export class BatchProcessingComponent implements OnInit {

    onValueChange(file: File[]) {
        console.log(file)
    } 

    fileControl = new FormControl([], FileValidators.required);
  constructor() { }

    ngOnInit(){

        this.fileControl.valueChanges.subscribe((files: File[]) => console.log('value changed!!'))
  }

}
