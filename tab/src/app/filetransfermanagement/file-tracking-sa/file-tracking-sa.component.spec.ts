import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileTrackingSAComponent } from './file-tracking-sa.component';

describe('FileTrackingSAComponent', () => {
  let component: FileTrackingSAComponent;
  let fixture: ComponentFixture<FileTrackingSAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileTrackingSAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileTrackingSAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
