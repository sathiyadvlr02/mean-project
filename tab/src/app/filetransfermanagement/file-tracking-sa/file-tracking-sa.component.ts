import { Component, OnInit ,ViewChild } from '@angular/core';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';



@Component({
  selector: 'app-file-tracking-sa',
  templateUrl: './file-tracking-sa.component.html',
  styleUrls: ['./file-tracking-sa.component.css']
})
export class FileTrackingSAComponent implements OnInit {
    selectedRow:any;
    displayedColumns: string[] = ['file_id', 'site_id', 'client_name', 'date_upload','file_type', 'file_status', 'filename','notes',  'action'];
    dataSource = ELEMENT_DATA;
    
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(   ) { } 

  ngOnInit(): void {
  }

}

/* Static data */

export interface PeriodicElement {
    file_id: string;
    site_id: string;
    client_name:string;
    date_upload:string;
    file_type:string;
    file_status:string;
    filename: string;
    notes: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { file_id: '1110', site_id: '11137', client_name: 'Tab Service Company' , date_upload:'08-31-2020 11:20 AM' , file_type:'1099 Data File', file_status:'Printed and Mailed', filename: 'TSC1099_Test_file.txt', notes: 'Test file upload' },
    { file_id: '1110', site_id: '11137', client_name: 'Tab Service Company' , date_upload:'08-31-2020 11:20 AM' , file_type:'1099 Data File', file_status:'Printed and Mailed', filename: 'TSC1099_Test_file.txt', notes: 'Test file upload' },
    { file_id: '1110', site_id: '11137', client_name: 'Tab Service Company' , date_upload:'08-31-2020 11:20 AM' , file_type:'1099 Data File', file_status:'Printed and Mailed', filename: 'TSC1099_Test_file.txt', notes: 'Test file upload' },
    { file_id: '1110', site_id: '11137', client_name: 'Tab Service Company' , date_upload:'08-31-2020 11:20 AM' , file_type:'1099 Data File', file_status:'Printed and Mailed', filename: 'TSC1099_Test_file.txt', notes: 'Test file upload' },
    
];
