import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FileValidators } from "ngx-file-drag-drop";

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { AddfilesComponent } from '../addfiles/addfiles.component';


@Component({
    selector: 'app-client-retrieval',
    templateUrl: './client-retrieval.component.html',
    styleUrls: ['./client-retrieval.component.css']
})

export class ClientRetrievalComponent implements OnInit {

    selectedRow: any;
    displayedColumns: string[] = ['tracking_no', 'client', 'filename', 'downloadstatus', 'action'];
    dataSource = ELEMENT_DATA;


    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor(private router: Router, public dialog: MatDialog) { }


    onValueChange(file: File[]) {
        console.log(file)
    }
    fileControl = new FormControl([], FileValidators.required);


    ngOnInit() {
        this.fileControl.valueChanges.subscribe((files: File[]) => console.log('value changed!!'))
    }

    Addclientfile(): void {
        this.router.navigate(["client-retrieval-add"]);
    }


    openInsertDialog() {
        const dialogRef = this.dialog.open(AddfilesComponent, {
            
            width: '700px',
        }
        );

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }


}

/* Static data */

export interface PeriodicElement {
    tracking_no: string;
    client: string;
    filename: string;
    downloadstatus: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
    { tracking_no: '1110', client: 'Tab Service Company', filename: '11137-1099R-2020PROOF-ALL-J202008310000000ALL.PDF', downloadstatus: 'Downloaded' },
    { tracking_no: '1110', client: 'Tab Service Company', filename: '11137-1099R-2020PROOF-ALL-J202008310000000ALL.PDF', downloadstatus: 'Downloaded' },
    { tracking_no: '1110', client: 'Tab Service Company', filename: '11137-1099R-2020PROOF-ALL-J202008310000000ALL.PDF', downloadstatus: 'Downloaded' },
    { tracking_no: '1110', client: 'Tab Service Company', filename: '11137-1099R-2020PROOF-ALL-J202008310000000ALL.PDF', downloadstatus: 'Downloaded' }

];
