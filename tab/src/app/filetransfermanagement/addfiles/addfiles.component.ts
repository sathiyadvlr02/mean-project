import { Component, OnInit } from '@angular/core';

import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FileValidators } from "ngx-file-drag-drop";



@Component({
  selector: 'app-addfiles',
  templateUrl: './addfiles.component.html'  
})
export class AddfilesComponent implements OnInit {

    

    onValueChange(file: File[]) {
        console.log(file)
    }
    fileControl = new FormControl([], FileValidators.required);


    ngOnInit() {
        this.fileControl.valueChanges.subscribe((files: File[]) => console.log('value changed!!'))
    }


     

}
