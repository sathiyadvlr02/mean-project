"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRoutes = void 0;
var full_component_1 = require("./layouts/full/full.component");
var login_component_1 = require("./login/login.component");
var login1098_component_1 = require("./login/login1098.component");
var loginspsgz_component_1 = require("./login/loginspsgz.component");
var send_code_component_1 = require("./send-code/send-code.component");
var validate_code_component_1 = require("./validate-code/validate-code.component");
var send_code1098t_component_1 = require("./send-code/send-code1098t.component");
var validate_code1098t_component_1 = require("./validate-code/validate-code1098t.component");
var send_codeSPSGZ_component_1 = require("./send-code/send-codeSPSGZ.component");
var validate_codeSPSGZ_component_1 = require("./validate-code/validate-codeSPSGZ.component");
var forgot_component_1 = require("./forgot/forgot.component");
var forgotspsgz_component_1 = require("./forgot/forgotspsgz.component");
var forgot1098_component_1 = require("./forgot/forgot1098.component");
var resetpassword_component_1 = require("./login/resetpassword.component");
var resetpassword1098_component_1 = require("./login/resetpassword1098.component");
var resetpasswordspsgz_component_1 = require("./login/resetpasswordspsgz.component");
var studentviewform_component_1 = require("./studentforms/viewforms/studentviewform.component");
var onlineconsent_component_1 = require("./studentforms/onlineconsent/onlineconsent.component");
var adminlayout_component_1 = require("./layouts/full/adminlayout.component");
var draganddrop_component_1 = require("./test/draganddrop.component");
var authentication_guard_1 = require("./services/auth/authentication.guard");
exports.AppRoutes = [
    { path: '', component: login_component_1.LoginComponent, pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'login1098', component: login1098_component_1.Login1098Component },
    { path: 'loginspsgz', component: loginspsgz_component_1.LoginspsgzComponent },
    { path: 'validate-code', component: validate_code_component_1.ValidateCodeComponent },
    { path: 'send-code', component: send_code_component_1.SendCodeComponent },
    { path: 'validate-code1098t', component: validate_code1098t_component_1.ValidateCode1098TComponent },
    { path: 'send-code1098t', component: send_code1098t_component_1.SendCode1098TComponent },
    { path: 'validate-codespsgz', component: validate_codeSPSGZ_component_1.ValidateCodeSPSGZComponent },
    { path: 'send-codespsgz', component: send_codeSPSGZ_component_1.SendCodeSPSGZComponent },
    { path: 'forgot', component: forgot_component_1.ForgotComponent },
    { path: 'forgotspsgz', component: forgotspsgz_component_1.ForgotSPSGZComponent },
    { path: 'forgot1098', component: forgot1098_component_1.Forgot1098Component },
    { path: 'resetpassword', component: resetpassword_component_1.ResetPasswordComponent },
    { path: 'resetpassword1098t', component: resetpassword1098_component_1.ResetPassword1098Component },
    { path: 'resetpasswordspsgz', component: resetpasswordspsgz_component_1.ResetPasswordspsgzComponent },
    { path: 'studentaccess', component: studentviewform_component_1.StudentViewComponent },
    { path: 'onlineconsent', component: onlineconsent_component_1.OnlineConsentComponent },
    { path: 'draganddrop', component: draganddrop_component_1.DragandDropComponent },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./dashboard/dashboard.module'); }).then(function (m) { return m.DashboardModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./managestudent/managestudent.module'); }).then(function (m) { return m.ManageStudentModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099misc/form109misc.module'); }).then(function (m) { return m.Form1099MISCModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1098t/form1098t.module'); }).then(function (m) { return m.Form1098TModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099nec/form1099nec.module'); }).then(function (m) { return m.Form1099NecModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099int/form109int.module'); }).then(function (m) { return m.Form1099INTModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099r/form1099r.module'); }).then(function (m) { return m.Form1099RModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099c/form1099c.module'); }).then(function (m) { return m.Form1099CModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099sa/form1099sa.module'); }).then(function (m) { return m.Form1099SAModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form5498sa/form5498sa.module'); }).then(function (m) { return m.Form5498SAModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1099div/form109div.module'); }).then(function (m) { return m.Form1099DIVModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form3922/form3922.module'); }).then(function (m) { return m.Form3922Module; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form3921/form3921.module'); }).then(function (m) { return m.Form3921Module; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./formt4a/formt4a.module'); }).then(function (m) { return m.Formt4aModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1042s/form1042s.module'); }).then(function (m) { return m.Form1042sModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1094b/form1094b.module'); }).then(function (m) { return m.Form1094BModule; }); }
            }
        ]
    },
    {
        path: '1095bmanagement',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1095b/form1095b.module'); }).then(function (m) { return m.Form1095BModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1095b/form1095b.module'); }).then(function (m) { return m.Form1095BModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1094c/form1094c.module'); }).then(function (m) { return m.Form1094CModule; }); }
            }
        ]
    },
    {
        path: 'employeemanagement',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./form1095c/form1095c.module'); }).then(function (m) { return m.Form1095CModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./changepassword/changepassword.module'); }).then(function (m) { return m.changepasswordModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./profile/profile.module'); }).then(function (m) { return m.profileModule; }); }
            }
        ]
    },
    {
        path: 'reports/statisticalreports',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./statisticalreports/statisticalreports.module'); }).then(function (m) { return m.StatisticalReportsModule; }); }
            }
        ]
    },
    {
        path: 'statisticalreports',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./statisticalreports/statisticalreports.module'); }).then(function (m) { return m.StatisticalReportsModule; }); }
            }
        ]
    },
    {
        path: 'downloads',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./downloadsupport/downloadsupport.module'); }).then(function (m) { return m.DownloadSupportModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./downloadsupport/downloadsupport.module'); }).then(function (m) { return m.DownloadSupportModule; }); }
            }
        ]
    },
    {
        path: 'filemanagement',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./formsmanagement/formsmanagement.module'); }).then(function (m) { return m.FormsManagementModule; }); }
            }
        ]
    },
    {
        path: 'filemanagement',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./filetransfer/filetransfer.module'); }).then(function (m) { return m.FileTransferModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./filetransfer/filetransfer.module'); }).then(function (m) { return m.FileTransferModule; }); }
            }
        ]
    },
    {
        path: '',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./administration/administration.module'); }).then(function (m) { return m.AdministrationModule; }); }
            }
        ]
    },
    {
        path: 'administration',
        component: full_component_1.FullComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./administration/administration.module'); }).then(function (m) { return m.AdministrationModule; }); }
            }
        ]
    },
    {
        path: 'managesites',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./managesites/managesites.module'); }).then(function (m) { return m.ManagesitesModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./managesites/managesites.module'); }).then(function (m) { return m.ManagesitesModule; }); }
            }
        ]
    },
    {
        path: 'menumanagement',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./menumanagement/menumanagement.module'); }).then(function (m) { return m.MenumanagementModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./menumanagement/menumanagement.module'); }).then(function (m) { return m.MenumanagementModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./create-efile/create-efile.module'); }).then(function (m) { return m.CreateEfileModule; }); }
            }
        ]
    },
    {
        path: 'filetransfermanagement',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./filetransfermanagement/filetransfermanagement.module'); }).then(function (m) { return m.FiletransfermanagementModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./filetransfermanagement/filetransfermanagement.module'); }).then(function (m) { return m.FiletransfermanagementModule; }); }
            }
        ]
    },
    {
        path: 'formsandreports',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./formsandreports-sa/formsandreports-sa.module'); }).then(function (m) { return m.FormsandreportsSAModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./formsandreports-sa/formsandreports-sa.module'); }).then(function (m) { return m.FormsandreportsSAModule; }); }
            }
        ]
    },
    {
        path: 'formsandreports/statisticalreports',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./statisticalreports/statisticalreports.module'); }).then(function (m) { return m.StatisticalReportsModule; }); }
            }
        ]
    },
    {
        path: 'systemmanagement',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./systemmanagement/systemmanagement.module'); }).then(function (m) { return m.SystemmanagementModule; }); }
            }
        ]
    },
    {
        path: '',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./systemmanagement/systemmanagement.module'); }).then(function (m) { return m.SystemmanagementModule; }); }
            }
        ]
    },
    {
        path: 'administrationsa',
        component: adminlayout_component_1.AdminLayoutComponent,
        canActivate: [authentication_guard_1.AuthenticationGuard],
        children: [
            {
                path: '',
                loadChildren: function () { return Promise.resolve().then(function () { return require('./administration/administration.module'); }).then(function (m) { return m.AdministrationModule; }); }
            }
        ]
    },
];
//# sourceMappingURL=app.routing.js.map