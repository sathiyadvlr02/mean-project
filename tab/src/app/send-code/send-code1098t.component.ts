import { Component, OnInit ,ViewEncapsulation } from '@angular/core';
import { AccountService } from '../services/account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Subscription, timer } from 'rxjs';
import { takeWhile, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-send-code1098',
    templateUrl: './send-code1098t.component.html',  
    styleUrls:['./send-code1098t.component.scss'],
    encapsulation: ViewEncapsulation.None,
  })
  
  export class SendCode1098TComponent implements OnInit {
  loginForm: FormGroup;  
  Provider: FormControl;
  returnUrl: string;
  maskedEmail:string;
  maskedPhone:string;  
  timerCountMessage : string;  
  private subscription: Subscription;

  public ProviderOptions: any = [
    { label: 'Email', value: 'Email', checked: true },
    { label: 'Phone', value: 'Phone', checked: false }
  ];

  constructor(private acct: AccountService, 
    private route: ActivatedRoute, 
    private fb: FormBuilder, 
    private router: Router,
    public toasterService: ToastrService) 
    { }

  ngOnInit(): void {
    this.setBackgroundImage();
    this.timerCountMessage="";
    this.observerTimer();

    if ((localStorage.getItem('twoFactorToken') && localStorage.getItem('codeExpiry')) == null) {
        this.router.navigate(['/login1098']);
    }

    this.maskedEmail= localStorage.getItem('makedEmail') ;
    this.maskedPhone= localStorage.getItem('maskedPhone') ;
    console.log(this.maskedEmail+' phone -'+this.maskedPhone);

    this.ProviderOptions[0].label = "Email : " + this.maskedEmail;
    this.ProviderOptions[1].label ="Phone :" + this.maskedPhone;

   
    // Initialize Form Controls
    this.Provider  = new FormControl('Email', [Validators.required]);
    

     // Initialize FormGroup using FormBuilder
     this.loginForm = this.fb.group({        
        Provider:this.Provider 
     });   

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  /* Set the background image when page loads */
  setBackgroundImage() {
    //$('body').css({
    //    'background-image': 'url(' + this.imageUrl + '),  linear-gradient(rgba(255, 0, 0, 0.5), rgba(255, 0, 0, 0.5))',
    //    'background-repeat': 'no-repeat',
    //    'background-size': 'cover'
    //});
  }

  /* Observable timer */
  observerTimer() {
    let endTime = new Date(localStorage.getItem('codeExpiry')).getTime();

    let currentTime = new Date().getTime();
    let difference = endTime - currentTime;
    let seconds = Math.floor(difference / 1000);

    this.subscription = timer(1000, 1000) //Initial delay 1 seconds and interval countdown also 1 second
      .pipe(
        takeWhile(() => seconds > 0),
        tap(() => seconds--)
      )
      .subscribe(() => {
        this.timerCountMessage = "Time remaining: " + seconds + "s";
        //  $('#countDown').text(seconds + 's');
        let isSessionActive = localStorage.getItem('isSessionActive');
        if (seconds <= 0 || isSessionActive == '0' || isSessionActive == undefined || !(isSessionActive == '1')) {

            this.timerCountMessage ="EXPIRED";
          //$('#Provider').attr('disabled', '');
          //$('#countDown').text('EXPIRED');
          //$('#btnLogin').removeAttr('hidden');
          this.stopTimer();
          this.acct.sendExpiryNotification();
        }
      });
  }

  private stopTimer() {
    this.subscription.unsubscribe();
  }

  onSubmit() {

    // debugger;
     let sendCodeDetails = this.loginForm.value;


    this.acct.sendTwoFactorProvider(sendCodeDetails.Provider, "", false).subscribe(
      (result) => {
        localStorage.setItem("TwoFactorProvider", sendCodeDetails.Provider);
        this.router.navigate(['/validate-code1098t']);
      },
      (error) => {

        this.toasterService.error(
            'Invalid data. Please check and try again',
            '',
            {
              positionClass: 'toast-top-center',
              timeOut: 3000
            }
          );
          return false;

      }
    );
  }
}
