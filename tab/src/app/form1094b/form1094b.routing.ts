import { Routes } from '@angular/router';

import { Create1094BComponent } from './create1094bform/create1094b.component';
import { Form1094BComponent } from './form1094bmanagement/form1094b.component';
 
export const Form1094BRoutes: Routes = [
    {
        path: 'form1094b',
        component: Form1094BComponent
    },
    {
        path: 'create1094b',
        component: Create1094BComponent
    }, 

];
