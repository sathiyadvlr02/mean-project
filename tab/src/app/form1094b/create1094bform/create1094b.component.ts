import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NavService } from  '../../nav.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-create1094b',
  templateUrl: './create1094b.component.html',
  
})
export class Create1094BComponent implements OnInit {
  homeForm: FormGroup;
  
  @ViewChild('sidenav', {static: false}) public sidenav: MatSidenav;

  constructor(public navService: NavService,
    public router: Router) {
  }

  ngOnInit(): void {
   // this.navService.closeNav();
  }

}

