import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Form1094BRoutes } from './form1094b.routing';
import { ChartistModule } from 'ng-chartist';

import { Create1094BComponent } from './create1094bform/create1094b.component'; 
import {Form1094BComponent } from './form1094bmanagement/form1094b.component';

//import { Update1099SAComponent } from './update1099saform/update1099sa.component'; 
//import { History1099SAComponent } from './history1099saform/history1099sa.component'; 


@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    RouterModule.forChild(Form1094BRoutes)
  ],

  declarations: [Create1094BComponent,Form1094BComponent]

})
export class Form1094BModule {}
