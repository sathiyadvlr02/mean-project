import { Routes } from '@angular/router';
import { CreateEfileComponent } from './create-efile.component';


export const CerateEfileRoutes: Routes = [
    {
        path: 'create-efile',
        component: CreateEfileComponent
    },
];
