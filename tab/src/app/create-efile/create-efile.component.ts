import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';



@Component({
  selector: 'app-create-efile',
  templateUrl: './create-efile.component.html',
  styleUrls: ['./create-efile.component.css']
})
export class CreateEfileComponent  {

   items = [
        '11137 - General Test Company 1 - IRS 1042S = 1',
        '11138 - General Test Company 2 - IRS 1042S = 1',
        '11139 - General Test Company 3 - IRS 1042S = 1',        
      ];
    
      basket = [
        '11140 - General Test Company 4 - IRS 1042S = 1',
        '11141 - General Test Company 5 - IRS 1042S = 1',
        '11142 - General Test Company 6 - IRS 1042S = 1',
      ];
 
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }


}
