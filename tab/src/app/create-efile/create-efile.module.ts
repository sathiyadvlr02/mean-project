import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';



import { CerateEfileRoutes } from './create-efile-routing.module';
import { CreateEfileComponent } from './create-efile.component';

@NgModule({
  declarations: [CreateEfileComponent],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      AppMaterialModule,
      FlexLayoutModule,            
      RouterModule.forChild(CerateEfileRoutes)
  ]
})
export class CreateEfileModule { }
