import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { NavService } from '../../nav.service';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { formatDate } from '@angular/common';
import { StateService } from './../../services/state.service';

@Component({
    selector: 'app-history1099nec',
    templateUrl: './history1099nec.component.html',

})
export class History1099NecComponent implements OnInit {
    
    updateAppForm: FormGroup;
    stateList;
    country;
    taxyear;
  
    @ViewChild('sidenav', { static: false }) public sidenav: MatSidenav;

    constructor(
        public navService: NavService,
        public router: Router,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private el: ElementRef,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private cookieService: CookieService,
        public StateServices : StateService,
    ) {}

    private CookieUserName = this.cookieService.get('username');
    private CookieSiteId = this.CookieUserName.split('_')[0];

    ngOnInit(): void {
        this.updateAppForm = this.fb.group({
            corrected: new FormControl(false),
            payerName: new FormControl(''),
            payerAddress1: new FormControl(''),
            payerAddress2: new FormControl(''),
            payerAddress3: new FormControl(''),
            payerCity: new FormControl(''),
            payerState: new FormControl(''),
            payerZip: new FormControl(''),
            payerTelephoneNo1: new FormControl(''),
            payerFederalIdentificationNumber: new FormControl(''),
            receiptIdentificationNumber: new FormControl(''),
            receiptName1: new FormControl(''),
            receiptName2: new FormControl(''),
            receiptAddress1: new FormControl(''),
            receiptAddress2: new FormControl(''),
            receiptAddress3: new FormControl(''),
            receiptCity: new FormControl(''),
            receiptState: new FormControl(''),
            receiptZip: new FormControl(''),
            forignCountry: new FormControl(false),
            receiptCountry: new FormControl(''),
            accountNumber: new FormControl(''),
            fatca: new FormControl(false),
            nonemployeeCompensation: new FormControl(''),
            federalIncomeTaxWithheld: new FormControl(''),
            stateTaxWithheld1: new FormControl(''),
            stateTaxWithheld2: new FormControl(''),
            statePayersStateNo1: new FormControl(''),
            statePayersStateNo2: new FormControl(''),
            stateIncome1: new FormControl(''),
            stateIncome2: new FormControl(''),
        })
        this.taxyear=2020;
        this.getstatelist();
        this.getCountry();
    }

    getstatelist() {
        this.StateServices.getStatelist().subscribe(
            result => {
                this.stateList= JSON.parse(result.data);
            });
    }

    getCountry() {
        this.StateServices.getCountry().subscribe(
            result => {
                this.country = JSON.parse(result.data);
            });
    }

    onBlurFloat(event) {
        if (event.target.value !== '') {
            event.target.value = parseFloat(event.target.value).toFixed(2)
        }
    }



}

