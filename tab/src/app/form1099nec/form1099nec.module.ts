import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { Form1099NecRoutes } from './form1099nec.routing';
import { ChartistModule } from 'ng-chartist';
import { Create1099NecComponent } from './create1099nec/create1099nec.component';
import { Update1099NecComponent } from './update1099nec/update1099nec.component';
import { History1099NecComponent } from './history1099nec/history1099nec.component';
import { FormEmail1099NEC } from './email1099nec/email1099nec.component';
import { SetPassword } from './email1099nec/setpassword.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,
    ChartistModule,
    FormsModule ,ReactiveFormsModule,
    RouterModule.forChild(Form1099NecRoutes)
  ],
  declarations: [
    Create1099NecComponent,
    Update1099NecComponent,
    History1099NecComponent,
    FormEmail1099NEC,
    SetPassword
  ]
})
export class Form1099NecModule {}
