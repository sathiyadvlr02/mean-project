import { Routes } from '@angular/router';

import { Form1099NecModule } from './form1099nec.module';
import { Create1099NecComponent } from './create1099nec/create1099nec.component';
import { Update1099NecComponent } from './update1099nec/update1099nec.component';
import { History1099NecComponent } from './history1099nec/history1099nec.component';
import { FormEmail1099NEC } from './email1099nec/email1099nec.component';

export const Form1099NecRoutes: Routes = [
   
  {
    path: 'create1099nec',
    component: Create1099NecComponent  
  },
   
  {
    path: 'update1099nec',
    component: Update1099NecComponent 
  },
  {
    path: 'history1099nec',
    component: History1099NecComponent 
  },
  {
    path: 'email1099nec',
    component: FormEmail1099NEC 
  },


];
