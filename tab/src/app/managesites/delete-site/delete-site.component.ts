import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-delete-site',
    templateUrl: './delete-site.component.html'
})
export class DeleteSiteComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    displayedColumns: string[] = ['file_link','form_type', 'description'];
    TemplatedataSource = Template_ELEMENT_DATA;
    constructor() { }

    ngOnInit(): void {
        this.homeForm = new FormGroup({
        });
    }

}

/* Static data */


export interface TemplateElement {
    form_type: string;
    description: string;
    file_link: string;
}

const Template_ELEMENT_DATA: TemplateElement[] = [
    { form_type: '11137', description: 'Test Site 1', file_link: 'Delete'},
    { form_type: '11138', description: 'Test Site 2', file_link: 'Delete'},
    { form_type: '11139', description: 'Test Site 3', file_link: 'Delete'},

];
 