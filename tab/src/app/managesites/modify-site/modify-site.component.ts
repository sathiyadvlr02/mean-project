import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SiteManagementService } from '../../services/sitemanagement.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { MatSort } from '@angular/material';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
    SITE_ID: any;
    COMPANY_NAME: any;
}

@Component({
    selector: 'app-modify-site',
    templateUrl: './modify-site.component.html'
})

export class ModifySiteComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    displayedColumns: string[] = ['SITE_ID', 'COMPANY_NAME', 'Action'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    constructor(
        private _siteManagementService: SiteManagementService,
        public toasterService: ToastrService,
    ) {
    }

    ngOnInit(): void {
        this.homeForm = new FormGroup({
        });

        this.getAllSiteList();
        
    }

    getAllSiteList(){
        this._siteManagementService
        .allSiteDetails()
        .subscribe(
            (result) => {
                console.log('cons result', result)
                if(result.isValid){
                    this.dataSource = new MatTableDataSource(JSON.parse(result.data));
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    //this.TemplatedataSource = JSON.parse(result.data);
                    //this.OriginalData = JSON.parse(result.data);
                }
            },
            (error) => {
                console.log('error', error)
            }
        );
    }

    deleteAction(actionId: any){
        console.log('actionId', actionId)
        Swal.fire({
            title: 'Are you sure delete?',
            //text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this._siteManagementService
                .deleteSiteDetails(actionId)
                .subscribe(
                    (result) => {
                        if(result.isValid){
                            this.toasterService.success(
                            result.message,
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                            this.getAllSiteList();
                        } else {
                            this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                            );
                        }
                       // window.location.reload();
                    },
                    (error) => {
                        this.toasterService.error(
                            'Please Try Again.',
                            '',
                            {
                                positionClass: 'toast-top-center',
                                timeOut: 3000
                            }
                        );
                       // window.location.reload();
                    }
                );
                //window.location.reload();
            }
        });
    }

    searchData(searchValue: any) {
        /* this.TemplatedataSource = this.OriginalData.filter((item) => {
               
            return item.COMPANY_NAME.toLowerCase().includes(searchValue.toLowerCase()) || item.SITE_ID.toString().includes(searchValue.toLowerCase()) ? item : '';
                
        }); */

        this.dataSource.filter = searchValue.trim().toLowerCase();
      
   }
}
/* Static data */


let Template_ELEMENT_DATA = [     
    { form_type: '11137', file_link: 'Test Site 1'},
    { form_type: '11138', file_link: 'Test Site 2'},
    { form_type: '11139', file_link: 'Test Site 3'},
    { form_type: '11139', file_link: 'Test Site 4'},
    { form_type: '11139', file_link: 'Test Site 5'},
];
