import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-archived-sites',
  templateUrl: './archived-sites.component.html'
})
export class ArchivedSitesComponent implements OnInit {
    homeForm: FormGroup;
    selectedRow:any;
    displayedColumns: string[] = ['form_type', 'description', 'file_link'];
    TemplatedataSource = Template_ELEMENT_DATA;
  constructor() { }

    ngOnInit(): void {
        this.homeForm = new FormGroup({
        });
  }

}

/* Static data */


export interface TemplateElement {
    form_type: string;
    description: string;
    file_link: string;
}

const Template_ELEMENT_DATA: TemplateElement[] = [
    { form_type: '11137', description: 'Test Site Id 1', file_link: 'ONLINE' },
    { form_type: '11137', description: 'Test Site Id 2', file_link: 'ONLINE' },
    { form_type: '11137', description: 'Test Site Id 3', file_link: 'ONLINE' },

];
