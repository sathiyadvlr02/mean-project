import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatesiteComponent } from './createsite/createsite.component';
import { ModifySiteComponent } from './modify-site/modify-site.component';
import { EditSiteComponent } from './editSite/editsite.component';
import { DeleteSiteComponent } from './delete-site/delete-site.component';
import { ArchivedSitesComponent } from './archived-sites/archived-sites.component';
import { AddSitesYearComponent } from './add-sites-year/add-sites-year.component';
import { ManagePOCEmailComponent } from './manage-pocemail/manage-pocemail.component';

export const ManageSitesRoutes: Routes = [
    {
        path: 'createsite',
        component: CreatesiteComponent
    },

    {
        path: 'modifysite',
        component: ModifySiteComponent
    },
    {
        path: 'edit-site',
        component: EditSiteComponent
    },
    {
        path: 'edit-site/:id',
        component: EditSiteComponent
    },
    /* {
        path: 'deletesite',
        component: DeleteSiteComponent
    }, */
    {
        path: 'deletesite',
        component: ModifySiteComponent
    },
    {
        path: 'archived-sites',
        component: ArchivedSitesComponent
    },
    {
        path: 'addsitesyear',
        component: AddSitesYearComponent
    },
    {
        path: 'manage-pocemail',
        component: ManagePOCEmailComponent
    },

];
