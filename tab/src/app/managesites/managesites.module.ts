import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ManageSitesRoutes } from './managesites-routing.module';
import { CreatesiteComponent } from './createsite/createsite.component';
import { ModifySiteComponent } from './modify-site/modify-site.component';
import { EditSiteComponent } from './editSite/editsite.component';
import { DeleteSiteComponent } from './delete-site/delete-site.component';
import { ArchivedSitesComponent } from './archived-sites/archived-sites.component';
import { AddSitesYearComponent } from './add-sites-year/add-sites-year.component';
import { ManagePOCEmailComponent } from './manage-pocemail/manage-pocemail.component';


@NgModule({
    imports: [
        CommonModule,
        AppMaterialModule,
        FormsModule, ReactiveFormsModule,
        FlexLayoutModule,
        RouterModule.forChild(ManageSitesRoutes)
    ],
    declarations: [CreatesiteComponent, ModifySiteComponent,EditSiteComponent, DeleteSiteComponent, ArchivedSitesComponent, AddSitesYearComponent, ManagePOCEmailComponent],
})
export class ManagesitesModule { }
