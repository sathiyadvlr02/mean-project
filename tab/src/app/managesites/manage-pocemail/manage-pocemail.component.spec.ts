import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePOCEmailComponent } from './manage-pocemail.component';

describe('ManagePOCEmailComponent', () => {
  let component: ManagePOCEmailComponent;
  let fixture: ComponentFixture<ManagePOCEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePOCEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePOCEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
