import { Component, OnInit, ElementRef } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {
    FormGroup,
    FormControl,
    FormBuilder,
    Validators,
    FormArray
} from '@angular/forms';
import { SiteManagementService } from '../../services/sitemanagement.service';
import { ToastrService } from 'ngx-toastr';

import { SpaceValidator } from '../../validator/spacevalidation.validator';

export interface TemplateElement {
    Data_Type_1098: String;
    Postage_Paid: string;
    Site_Id: any;
    Site_Type: string;
    Supplemental_Code_1098: string;
    TCRS_Merge: string;
    Tax_Year: string
}

@Component({
    selector: 'app-editsite',
    templateUrl: './editsite.component.html',
    styleUrls: ['./editsite.component.scss'],

})
export class EditSiteComponent implements OnInit {

    selectedRow: any;
    displayedColumns: string[] = ["Tax_Year", "Site_Type", "Postage_Paid", "Supplemental_Code_1098", "TCRS_Merge"];
    TemplatedataSource: TemplateElement[];
    id: any;
    fetchedData: any;

    fakeImagePath: any;

    createSiteForm: FormGroup;
    submitted = false;
    fileName = '';

    directoriesList = [
        {name: '1099-MISC (creates "print1099" folder)', value: '1099-MISC'},
        {name: '1099-R (creates "print1099r" folder)', value: '1099-R'},
        {name: '1099-INT', value: '1099-INT'},
        {name: '1099-C (creates "print1099c" folder)', value: '1099-C'},
        {name: '1099-SA (creates "print1099sa" folder)', value: '1099-SA'},
        {name: '5498-SA (creates "print5498sa" folder)', value: '5498-SA'},
        {name: '1095-C (creates "print1095C" folder)', value: '1095-C'},
        {name: '3921', value: '3921'},
        {name: '3922', value: '3922'},
        {name: '1098T', value: '1098T'},
        {name: '1098E', value: '1098E'},
        {name: '1095-B', value: '1095-B'},
        {name: 'T4A', value: 'T4A'},
        //{name: 'EIN Mask', value: 'EIN Mask'},
        {name: '1042-S', value: '1042-S'},
        {name: '1099', value: '1099-DIV'},
    ];
    
    maskSSNList = [
        {name: '1099-MISC', value: '1099-MISC'},
        {name: '1099-R', value: '1099-R'},
        {name: '1099-INT', value: '1099-INT'},
        {name: '1099-C', value: '1099-C'},
        {name: '1099-SA', value: '1099-SA'},
        {name: '5498-SA', value: '5498-SA'},
        {name: '3921', value: '3921'},
        {name: '3922', value: '3922'},
        {name: '1098T', value: '1098T'},
        {name: '1098E', value: '1098E'},
        {name: '1095-C', value: '1095-C'},
        {name: '1095-B', value: '1095-B'},
        {name: 'T4A', value: 'T4A'},
        {name: '1042-S', value: '1042-S'},
        {name: '1099-DIV', value: '1099-DIV'},
    ];
    
    stateList = [
        {name: 'Select State', value: ''},
        {name: 'AA', value: 'AA'},
        {name: 'AB', value: 'AB'},
        {name: 'AE', value: 'AE'},
        {name: 'AK', value: 'AK'},
        {name: 'AL', value: 'AL'},
        {name: 'AP', value: 'AP'},
        {name: 'AR', value: 'AR'},
        {name: 'AZ', value: 'AZ'},
        {name: 'BC', value: 'BC'},
        {name: 'CA', value: 'CA'},
        {name: 'CO', value: 'CO'},
        {name: 'CT', value: 'CT'},
        {name: 'DE', value: 'DE'},
        {name: 'FL', value: 'FL'},
        {name: 'GA', value: 'GA'},
        {name: 'HI', value: 'HI'},
        {name: 'IA', value: 'IA'},
        {name: 'ID', value: 'ID'},
        {name: 'IL', value: 'IL'},
        {name: 'IN', value: 'IN'},
        {name: 'KS', value: 'KS'},
        {name: 'KY', value: 'KY'},
        {name: 'LA', value: 'LA'},
        {name: 'MA', value: 'MA'},
        {name: 'MB', value: 'MB'},
        {name: 'MD', value: 'MD'},
        {name: 'ME', value: 'ME'},
        {name: 'MI', value: 'MI'},
        {name: 'MN', value: 'MN'},
        {name: 'MO', value: 'MO'},
        {name: 'MS', value: 'MS'},
        {name: 'MT', value: 'MT'},
        {name: 'NB', value: 'NB'},
        {name: 'NC', value: 'NC'},
        {name: 'ND', value: 'ND'},
        {name: 'NE', value: 'NE'},
        {name: 'NH', value: 'NH'},
        {name: 'NJ', value: 'NJ'},
        {name: 'NL', value: 'NL'},
        {name: 'NM', value: 'NM'},
        {name: 'NS', value: 'NS'},
        {name: 'NT', value: 'NT'},
        {name: 'NU', value: 'NU'},
        {name: 'NV', value: 'NV'},
        {name: 'NY', value: 'NY'},
        {name: 'OH', value: 'OH'},
        {name: 'OK', value: 'OK'},
        {name: 'ON', value: 'ON'},
        {name: 'OR', value: 'OR'},
        {name: 'PA', value: 'PA'},
        {name: 'PE', value: 'PE'},
        {name: 'QC', value: 'QC'},
        {name: 'RI', value: 'RI'},
        {name: 'SC', value: 'SC'},
        {name: 'SD', value: 'SD'},
        {name: 'SK', value: 'SK'},
        {name: 'TN', value: 'TN'},
        {name: 'TX', value: 'TX'},
        {name: 'UT', value: 'UT'},
        {name: 'VA', value: 'VA'},
        {name: 'VT', value: 'VT'},
        {name: 'WA', value: 'WA'},
        {name: 'WI', value: 'WI'},
        {name: 'WV', value: 'WV'},
        {name: 'WY', value: 'WY'},
        {name: 'YT', value: 'YT'},
    ];

    constructor(
        private route: ActivatedRoute,
        private _siteManagementService: SiteManagementService,
        private fb: FormBuilder,
        public toasterService: ToastrService,
        private router: Router,
        private el: ElementRef
    ) { 
        this.id = '';
        this.route.params.subscribe( 
            params => {
                let let_id = params['id'];
                this.id = let_id;
            }
        );

        
        

        console.log('id', this.id)
    }

    ngOnInit(): void {

        this.createSiteForm = this.fb.group({
            folderName: new FormControl('', [Validators.required, SpaceValidator.cannotContainSpace]),
            companyName: new FormControl('', [Validators.required, SpaceValidator.cannotContainSpace]),
            companyLogo: new FormControl(''),
            createSupportRequest: new FormControl(false),
            siteType: new FormControl('P'),
            efileService: new FormControl('Y'),
            showLogo: new FormControl('N'),
            address1: new FormControl(''),
            address2: new FormControl(''),
            address3: new FormControl(''),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            zip: new FormControl('', [Validators.required]),
            contactName: new FormControl(''),
            contactPhone: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            ext: new FormControl(''),
            contactFax: new FormControl('', [Validators.pattern(/^\d{3}-\d{3}-\d{4}$/)]),
            contactEmail: new FormControl('', [Validators.required, Validators.email, SpaceValidator.cannotContainSpace]),
            initialFile: new FormControl(''),
            initialJob: new FormControl(''),
            initialReceipt: new FormControl(''),
            directories: new FormArray([]),
            maskSSN: new FormArray([]),
            enableIndividualAccess: new FormControl(''),
            yearValues: new FormControl({}),
            //siteGroup: new FormControl(''),
        });

        if(this.id){
            //Edit Site Details
            this._siteManagementService
            .getSiteDetailsBasedOnSiteId(this.id)
            .subscribe(
                (result) => {
                    console.log('const result', result)
                    if(result.isValid){
                        this.createSiteForm.get('folderName').setValue(result.data.sitE_FOLDER_NAME_PATH);
                        this.createSiteForm.get('companyName').setValue(result.data.companY_NAME);
                        //this.createSiteForm.get('companyLogo').setValue(result.data.companY_LOGO_PATH);
                        this.createSiteForm.get('siteType').setValue(result.data.sitE_TYPE);
                        this.createSiteForm.get('efileService').setValue(result.data.efilE_SERVICES);
                        this.createSiteForm.get('showLogo').setValue(result.data.shoW_LOGO_IND);
                        this.createSiteForm.get('address1').setValue(result.data.addresS1);
                        this.createSiteForm.get('address2').setValue(result.data.addresS2);
                        this.createSiteForm.get('address3').setValue(result.data.addresS3);
                        this.createSiteForm.get('city').setValue(result.data.city);
                        this.createSiteForm.get('state').setValue(result.data.state);
                        this.createSiteForm.get('zip').setValue(result.data.zip);
                        this.createSiteForm.get('contactName').setValue(result.data.contacT_NAME);
                        this.createSiteForm.get('contactPhone').setValue(result.data.contacT_PHONE);
                        this.createSiteForm.get('ext').setValue(result.data.contacT_PHONE_EXTENSION);
                        this.createSiteForm.get('contactFax').setValue(result.data.contacT_FAX);
                        this.createSiteForm.get('contactEmail').setValue(result.data.contacT_EMAIL);
                        this.createSiteForm.get('initialFile').setValue(result.data.INITIAL_FILE ? result.data.INITIAL_FILE : '');
                        this.createSiteForm.get('initialJob').setValue(result.data.INITIAL_JOB ? result.data.INITIAL_JOB : '');
                        this.createSiteForm.get('initialReceipt').setValue(result.data.INITIAL_RECIPIENT ? result.data.INITIAL_RECIPIENT : '');
                    }
                },
                (error) => {
                    //console.log('error', error)
                }
            );

            //Get Tax Form Details
            this._siteManagementService
            .getTaxFormDetails(this.id)
            .subscribe(
                (result) => {
                    //console.log('tax form result', result)
                    if(result.isValid){
                        let maskSSNArray: FormArray = this.createSiteForm.get('maskSSN') as FormArray;
                        let directoriesArray: FormArray = this.createSiteForm.get('directories') as FormArray;
                        if(JSON.parse(result.data)){
                            JSON.parse(result.data).map((itm) => {
                                if(itm.Mask_SSN == 'Y'){
                                    maskSSNArray.push(new FormControl(itm.tax_form_type));
                                    directoriesArray.push(new FormControl(itm.tax_form_type));
                                }
                            })
                        }
                        
                    }
                    
                },
                (error) => {
                    console.log('tax form error', error)
                }
            );

            //Get Year By Site Id
            this._siteManagementService
            .getSiteYear(this.id)
            .subscribe(
                (result) => {
                    //console.log('tax form result', result)
                    if(result.isValid){
                        //let yearValuesArray: FormArray = this.createSiteForm.get('yearValues') as FormArray;
                        if(JSON.parse(result.data)){
                            let tempData = {};
                            let tempArray = [];
                            JSON.parse(result.data).map((itm) => {
                                //console.log('itm', itm)
                                //yearValuesArray.push(new FormControl(itm));
                                tempData[itm.Tax_Year] = itm;
                                tempArray.push(itm);
                            })
                            this.createSiteForm.get('yearValues').setValue(tempData);
                            this.TemplatedataSource = tempArray;
                            console.log('this.TemplatedataSource', this.TemplatedataSource)
                        }
                        //this.TemplatedataSource = yearValuesArray;
                        
                    }
                    
                },
                (error) => {
                    console.log('tax form error', error)
                }
            );
        }

    }

    get trrowErrors() { return this.createSiteForm.controls; }

    onFileSelected(event) {
        const file:File = event.target.files[0];
        if (file) {
            this.fileName = file.name;
            this.createSiteForm.get('companyLogo').setValue(file)
        }
    }

    onValueChange(file: File[]) {
        console.log('file', file)
        if(file.length > 0){
            this.createSiteForm.get('companyLogo').setValue(file[0])
            const reader = new FileReader();
            reader.readAsDataURL(file[0]); 
            reader.onload = (_event) => { 
                this.fakeImagePath = reader.result; 
            }
        } else {
          this.createSiteForm.get('companyLogo').setValue('')
          this.fakeImagePath = ''; 
        }
      }

    onCheckSSN(event){
        let formArray: FormArray = this.createSiteForm.get('maskSSN') as FormArray;
        if(event.checked){
          formArray.push(new FormControl(event.source.value));
        } else {
          let i: number = 0;
          formArray.controls.forEach((ctrl: FormControl) => {
            if(ctrl.value == event.source.value) {
              formArray.removeAt(i);
              return;
            }
            i++;
          });
        }
    }
    
    onCheckDirectory(event){
        let formArray: FormArray = this.createSiteForm.get('directories') as FormArray;
        if(event.checked){
          formArray.push(new FormControl(event.source.value));
        } else {
          let i: number = 0;
          formArray.controls.forEach((ctrl: FormControl) => {
            if(ctrl.value == event.source.value) {
              formArray.removeAt(i);
              return;
            }
            i++;
          });
        }
    }

    onChangeYearForm(year, field, event){
        let siteDetailsFormData = JSON.parse(JSON.stringify(this.createSiteForm.value.yearValues));
        //siteDetailsFormData.year.field = event.value;
        siteDetailsFormData[year][field] = event.value;
        //console.log('siteDetailsFormData', siteDetailsFormData[year][field])
        this.createSiteForm.get('yearValues').setValue(siteDetailsFormData);
        //console.log('year', year)
        //console.log('field', field)
        //console.log('event', event)
    }

    onSubmit(){
        console.log(this.createSiteForm)
        this.submitted = true;
        
        if (this.createSiteForm.invalid) {
            for (const key of Object.keys(this.createSiteForm.controls)) {
                if (this.createSiteForm.controls[key].invalid) {
                  const invalidControl = this.el.nativeElement.querySelector('[formcontrolname="' + key + '"]');
                  invalidControl.focus();
                  break;
                }
              }
            return;
        }

        let siteDetailsFormData = this.createSiteForm.value;
       
        //update Tax form

        let tempUpsData = [];

        if(siteDetailsFormData.maskSSN){
            siteDetailsFormData.maskSSN.map((itmData) => {
                tempUpsData.push({
                    site_id: parseInt(this.id),
                    tax_form_type: itmData,
                    mask_SSN: "Y",
                    mask_EIN: "N"
                })
            })
        }
        if(siteDetailsFormData.directories){
            siteDetailsFormData.directories.map((itmData) => {
                tempUpsData.push({
                    site_id: parseInt(this.id),
                    tax_form_type: itmData,
                    mask_SSN: "Y",
                    mask_EIN: "N"
                })
            })
          } 
        
          let tempUpsDataTemp = [...new Map(tempUpsData.map(item => [item['tax_form_type'], item])).values()]

        this._siteManagementService
        .updateTaxForm(parseInt(this.id), tempUpsDataTemp)
        .subscribe(
            (result) => {
                console.log('up result', result)
            },
            (error) => {
                console.log('up error', error)
            }
        );
           // return;

        //Update Year Form Details

        let tempYearArray = [];
        if(siteDetailsFormData.yearValues){
            Object.keys(siteDetailsFormData.yearValues).map(function(key, index) {
                tempYearArray.push({
                    site_Id: siteDetailsFormData.yearValues[key]['Site_Id'],
                    tax_Year: siteDetailsFormData.yearValues[key]['Tax_Year'],
                    postage_Paid: siteDetailsFormData.yearValues[key]['Postage_Paid'],
                    site_Type: siteDetailsFormData.yearValues[key]['Site_Type'],
                    data_Type_1098: siteDetailsFormData.yearValues[key]['Data_Type_1098'],
                    supplemental_Code_1098: siteDetailsFormData.yearValues[key]['Supplemental_Code_1098'],
                    tcrS_Merge: siteDetailsFormData.yearValues[key]['TCRS_Merge'],
                });
            });
        }


        this._siteManagementService
        .updateSiteYearForm(parseInt(this.id), tempYearArray)
        .subscribe(
            (result) => {
                console.log('up result', result)
            },
            (error) => {
                console.log('up error', error)
            }
        );



        //Update Other Details

        let createDatas = {
            SITE_ID: this.id,
            COMPANY_NAME: siteDetailsFormData.companyName,
            COMPANY_LOGO_PATH: siteDetailsFormData.companyLogo,
            SHOW_LOGO_IND: siteDetailsFormData.showLogo,
            STATUS: 'ONLINE',
            SITE_FOLDER_NAME_PATH: siteDetailsFormData.folderName,
            SITE_BROADCAST_MESSAGE: '',
            ADDRESS1: siteDetailsFormData.address1,
            ADDRESS2: siteDetailsFormData.address2,
            ADDRESS3: siteDetailsFormData.address3,
            CITY: siteDetailsFormData.city,
            STATE: siteDetailsFormData.state,
            ZIP: siteDetailsFormData.zip,
            CONTACT_NAME: siteDetailsFormData.contactName,
            CONTACT_PHONE: siteDetailsFormData.contactPhone,
            CONTACT_PHONE_EXTENSION: siteDetailsFormData.ext,
            CONTACT_FAX: siteDetailsFormData.contactFax,
            CONTACT_EMAIL: siteDetailsFormData.contactEmail,
            SITE_TYPE: siteDetailsFormData.siteType,
            EFILE_SERVICES: siteDetailsFormData.efileService,
            EFILE_CONTACT_NAME: siteDetailsFormData.contactName,
            EFILE_CONTACT_PHONE: siteDetailsFormData.contactPhone,
            EFILE_CONTACT_EMAIL: siteDetailsFormData.contactEmail,
            EMPLOYER_CONSENT_TEXT: '',
            PROOF_APPROVAL_TEXT: '',
            PAY_PROOF_APPROVAL_TEXT: 0,
            ADMIN_W2EMAIL_SUBJECT: '',
            ADMIN_W2EMAIL_MESSAGE: '',
            ADMIN_PAYSTUBEMAIL_SUBJECT: '',
            ADMIN_PAYSTUBEMAIL_MESSAGE: '',
            USER_W2EMAIL_SUBJECT: '',
            USER_W2EMAIL_MESSAGE: '',
            USER_EMAIL_NOTIFICATION_MESSAGE: '',
            USER_EMAIL_PROMPT_MESSAGE: '',
            PDF_OPEN_OPTIONS: 0,
            ENCRYPT_W2: 'N',
            VIEW_PAYPERIODS: 0,
            SHOW_UPDATE_W2: 'N',
            FORGOT_PASSWORD_TO_USER_IND: '',
            FORGOT_PASSWORD_AUTO_RESET: '',
            EXECUTIVE_EXCLUDE_FROM_PRINT_MAIL: '',
            HOME_PAGE_MESSAGE: '',
            ACCESS_ID: '',
            ENABLE_MENU_CONSENT_OPTION: '',
            EFILE_PROOF_APPROVAL_TEXT: null,
            SiteIDVerificationKey: null,
            SITE_RECIPIENT_XREF: '',
            AES_KEY: null,
            AES_IV: null,
            SITE_GROUP_ID: 0,
            DATA_TYPE_1098: null,
            SUPPLEMENTAL_CODE_1098T: null,
            CALL_CENTER_1098T: 'N',
            SR_OPTION: 'N',
            login_type: null,
            INITIAL_FILE: siteDetailsFormData.initialFile.toString(),
            INITIAL_JOB: siteDetailsFormData.initialJob.toString(),
            INITIAL_RECIPIENT: siteDetailsFormData.initialReceipt.toString(),
        };

        this._siteManagementService
        .updateSite(createDatas)
        .subscribe(
            (result) => {
            if(result.isValid){
                this.toasterService.success(
                result.message,
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
                );
            } else {
                this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
                );
            }
            this.router.navigate(['/modifysite']);
            },
            (error) => {
            this.toasterService.error(
                'Please Try Again.',
                '',
                {
                    positionClass: 'toast-top-center',
                    timeOut: 3000
                }
            );
            this.router.navigate(['/modifysite']);
            }
        );

    }

}


/* Static data */
export interface TemplateElement {
    tax_year: String;
    site_type: string;
    postal_status: string;
    tcrs_settings: string;
    tcrs_merge: boolean;
}

/* const Template_ELEMENT_DATA: TemplateElement[] = [
    { tax_year: '2020', site_type: 'P', postal_status: 'PAID', tcrs_settings: 'A', tcrs_merge: true },
    { tax_year: '2019', site_type: 'T', postal_status: 'PAID', tcrs_settings: 'A', tcrs_merge: true },
    { tax_year: '2018', site_type: 'T', postal_status: 'PAID', tcrs_settings: 'A', tcrs_merge: true },

]; */

