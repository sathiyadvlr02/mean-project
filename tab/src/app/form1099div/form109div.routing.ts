import { Routes } from '@angular/router';


import { Create1099DIVComponent } from './create1099divform/create1099div.component';
import { Update1099DIVComponent } from './update1099divform/update1099div.component';
import { History1099DIVComponent } from './history1099divform/history1099div.component';
import { FormEmail1099DIV } from './email1099divform/email1099div.component';


export const Form1099DIVRoutes: Routes = [
   
  {
    path: 'create1099div',
    component: Create1099DIVComponent  
  },
  {
    path: 'form1099divcreate',
    component: Create1099DIVComponent  
  },
  {
    path: 'form1099divupdate',
    component: Update1099DIVComponent 
  },
  {
    path: 'form1099divhistory',
    component: History1099DIVComponent 
  },
  {
    path: 'email1099div',
    component: FormEmail1099DIV  
  }


];
