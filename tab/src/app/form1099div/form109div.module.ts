import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material-module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule ,ReactiveFormsModule  } from '@angular/forms';

import { Form1099DIVRoutes } from './form109div.routing';

import { Create1099DIVComponent } from './create1099divform/create1099div.component';
import { Update1099DIVComponent } from './update1099divform/update1099div.component';
import { History1099DIVComponent } from './history1099divform/history1099div.component';
import { FormEmail1099DIV } from './email1099divform/email1099div.component';
import { SetPassword } from './email1099divform/setpassword.component';



@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexLayoutModule,    
    FormsModule ,ReactiveFormsModule,
    RouterModule.forChild(Form1099DIVRoutes)
  ],
  declarations: [Create1099DIVComponent,Update1099DIVComponent,History1099DIVComponent,FormEmail1099DIV,SetPassword]
})
export class Form1099DIVModule {}
