import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: []
})
export class AppHeaderComponent {
    LoginStatus$ = new BehaviorSubject<boolean>(null);
    Username$: Observable<string>;
    DisplayName$: Observable<string>;
    nameOfUser: any

    constructor(private acct: AccountService, private router: Router,
        private cookieService: CookieService,
        ) { }

    ngOnInit(): void {
        this.acct.globalStateChanged.subscribe((state) => {
            this.LoginStatus$.next(state.loggedInStatus);
        });

        //debugger;
        this.Username$ = this.acct.currentUserName;
        this.DisplayName$ = this.acct.GetDisplayName;
        this.nameOfUser = this.cookieService.get('displayName');
    }

    profileOnClick(): void{
        this.router.navigate(["profile"]);
    }


    onLogout() {
        this.acct.logout().subscribe((result) => {
            console.log('Logged Out Successfully');
        });

        this.router.navigate(['/login']);
    }

    submit(): void {

        this.router.navigate(["changepassword"]);

    }

}
