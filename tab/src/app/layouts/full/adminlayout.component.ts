import * as $ from 'jquery';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { AppHeaderComponent } from './header/header.component';
import { AppSidebarComponent } from './sidebar/sidebar.component';
import { PreofileService } from '../../services/profile.service';
import { CookieService } from 'ngx-cookie-service';
import { BroadcastService } from '../../services/broadcast.service'

/** @title Responsive sidenav */
@Component({
  selector: 'app-admin-layout',
  templateUrl: 'adminlayout.component.html',
  styleUrls: []
})
export class AdminLayoutComponent implements OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;
  broadcastMessage: any;

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    private _profileService: PreofileService,
    private cookieService: CookieService,
    private _broadcastService: BroadcastService,
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.broadcastMessage = this.cookieService.get('broadcastMessage');
    
    this.getBroadCastMessageData();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  ngAfterViewInit() {}

  getBroadCastMessageData(){
    this._broadcastService
        .getBroadCastMessage()
        .subscribe(
            (result) => {
                //console.log('result', result)
                if(result.isValid){
                  this.cookieService.set('broadcastMessage', result.data);
                  this.broadcastMessage = result.data;
                }
            },
            (error) => {
                //console.log('error', error)
            }
        );
  }
}
