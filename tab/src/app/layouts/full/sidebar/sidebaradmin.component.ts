import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';
import { Observable } from 'rxjs';
import { MenuService } from './../../../services/menu.service';

export interface MenuItem {
    menu_id: number;
    menu_description: string;
    menu_function: string;
    menu_comments: string;
    side_menu: number;
    menu_group_id: number;
    access_level?: any;
    Menu_Group: string;
    Menu_Comments: string;
    MenuItems?: [];
}

export interface Navitem {
    Menu_Group_Id: number;
    Menu_Group: string;
    Menu_Comments: string;
    MenuItems: MenuItem[];
}


@Component({
  selector: 'app-adminsidebar',
  templateUrl: './sidebaradmin.component.html',
  styleUrls: []
})


export class AppSidebarAdminComponent implements OnDestroy {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;
  public menuDetails: any = []

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    private mcct: MenuService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    //this.menuDetails = this.menuItems.getAdminMenuitem()
    console.log('menu load AAM')
    this.loadmenu();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  loadmenu(){
    console.log('menu load function AAM')
    this.mcct.getMenus('AAM')
        .subscribe(
            (result) => {
             
                if(result.isValid){
                    this.menuDetails = JSON.parse(result.data);
                    let tempVariable = [];


                    this.menuDetails && this.menuDetails.map((itm, i) => {
                        let tempAssign = itm;
                        let tempChild = JSON.parse(JSON.stringify(itm.MenuItems));
                        tempAssign['Menu_Group'] = itm.Menu_Group.toLocaleLowerCase();
                        tempAssign['MenuItems'] = [];
                        tempChild && tempChild.map((chilItm, j) => {
                            let tempChiAssign = chilItm;
                            tempChiAssign['Menu_Group'] = itm.Menu_Group.toLocaleLowerCase() + '/' + chilItm.menu_function.toLocaleLowerCase();
                            tempChiAssign['Menu_Comments'] = chilItm.menu_description;
                            tempAssign['MenuItems'].push(tempChiAssign);
                        })
                        tempVariable.push(tempAssign)
                        
                    })
                    this.menuDetails = tempVariable
                    //console.log(tempVariable);
                }
                },
            (error) => {
               // console.log('subs error', error)
            }
        );
  }


}
