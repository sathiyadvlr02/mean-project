import {Component, HostBinding, Input, OnInit, AfterContentInit, ViewChild} from '@angular/core';
import { NavService } from './../../../nav.service';
import {Router} from '@angular/router';

import {animate, state, style, transition, trigger} from '@angular/animations';


// export interface NavItem {
//     displayName: string;
//     disabled?: boolean;
//     iconName: string;
//     route?: string;
//     children?: NavItem[];
//   }

@Component({
  selector: 'app-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class MenuListItemComponent implements OnInit {
//   expanded: boolean;
//   @HostBinding('attr.aria-expanded') ariaExpanded = false;
//   //@Input() item: Menu;
//   @Input() depth: number;
//   @Input() item: NavItem;

   public expanded:boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item;
  @Input() depth: number;


  constructor(public navService: NavService,
              public router: Router) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {
    this.navService.currentUrl.subscribe((url: string) => {
        if (this.item.Menu_Group && url) {
          //console.log(`Checking '/${this.item.Menu_Group}' against '${url}'`);
          this.expanded = url.indexOf(`/${this.item.Menu_Group}`) === 0;
          this.ariaExpanded = this.expanded;
          //console.log(`${this.item.Menu_Group} is expanded: ${this.expanded}`);
        }
      });
  }

  onItemSelected(item) {
    if (!item.MenuItems || !item.MenuItems.length) {
        this.router.navigate([item.Menu_Group]);
        //this.navService.closeNav();
      }
      if (item.MenuItems && item.MenuItems.length) {
        this.expanded = !this.expanded;
        //this.router.navigate([item.Menu_Group]);
      }
    }
    }


